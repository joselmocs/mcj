<?php


class SacOSSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Populando: dependencias do sac');

        // Tipos de OS
        $types = array(
            array("Não Definido", '/images/icons/issuetypes/genericissue.png'),
            array("Correção de Erro", '/images/icons/issuetypes/bug.png'),
            array("Ajuste de Relatório", '/images/icons/issuetypes/subtask_alternate.png'),
            array("Melhoria de Funcionalidade", '/images/icons/issuetypes/improvement.png'),
            array("Nova Funcionalidade", '/images/icons/issuetypes/newfeature.png')
        );

        foreach ($types as $obj) {
            $mod = new SacOSType();
            $mod->name = $obj[0];
            $mod->icon = $obj[1];
            $mod->save();
        }

        // Status de OS
        $status = array(
            array("Não Definido", '/images/icons/statuses/reopened.png'),
            array("Aberto", '/images/icons/statuses/open.png'),
            array("Em Progresso", '/images/icons/statuses/inprogress.png'),
            array("Em Teste", '/images/icons/statuses/resolved.png'),
            array("Fechado", '/images/icons/statuses/closed.png')
        );

        foreach ($status as $obj) {
            $mod = new SacOSStatus();
            $mod->name = $obj[0];
            $mod->icon = $obj[1];
            $mod->save();
        }
    }
}

