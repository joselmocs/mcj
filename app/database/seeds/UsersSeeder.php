<?php

use JCS\Auth\Models\Role;


class UsersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Populando: administradores');

        $user = new User;
        $user->username = "joselmo";
        $user->password = "zsedcx";
        $user->email = strtolower('joselmocs@gmail.com');
        $user->verified = 1;
        $user->save();

        $user->profile->fullname = "JOSELMO CARDOZO";
        $user->profile->avatar = "avatar";
        $user->profile->save();
        $user->roles()->sync(array(Role::where('tag', '=', 'sys_admin')->firstOrFail()->id));

        $user = new User;
        $user->username = "flavio";
        $user->password = "zsedcx";
        $user->email = strtolower('flavio@mcj.com.br');
        $user->verified = 1;
        $user->save();

        $user->profile->fullname = "FLAVIO COIMBRA";
        $user->profile->avatar = "avatar";
        $user->profile->save();
        $user->roles()->sync(array(Role::where('tag', '=', 'admin')->firstOrFail()->id));
    }
}