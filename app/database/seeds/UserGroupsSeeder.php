<?php

class UserGroupsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Populando: grupos de usuario');

        $grupos = array(
            array('Administrador do Sistema', 'sys_admin', 10),
            array('Administrador', 'admin', 9),
            array('Diretoria', 'director', 1),
            array('Coordenador Suporte', 'support_admin', 1),
            array('Suporte', 'support', 1),
            array('Comercial', 'commercial', 1),
            array('Homologação', 'homologation', 1),
            array('Desenvolvimento', 'development', 1)
        );

        foreach ($grupos as $grupo) {
            DB::table('user_roles')->insert(array(
                'name' => $grupo[0],
                'tag' => $grupo[1],
                'level' => $grupo[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }

}

