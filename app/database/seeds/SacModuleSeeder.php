<?php

class SacModuleSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Populando: modulos');

        $modules = array(
            "Administrador da Entidade",
            "Administrador do Sistema",
            "Agenda / Marcação de Consulta",
            "CHECAIH",
            "Controle de Estoque",
            "Corpo Clinico",
            "Custo Hospitalar",
            "Faturamento AIH / BPA / Convênios / Particular",
            "Faturamento APAC",
            "Financeiro",
            "Laboratório",
            "Recepção"
        );

        foreach ($modules as $module) {
            $mod = new SacModule();
            $mod->name = $module;
            $mod->save();
        }
    }

}

