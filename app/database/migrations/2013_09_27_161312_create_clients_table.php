<?php

use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_clients', function($table)
        {
            $table->increments('id');
            $table->integer('cod')->nullable()->index();
            $table->string('nome', 255)->nullable()->index();
            $table->string('cgc', 14)->nullable()->index();
            $table->string('situacao', 1)->default('I');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_clients');
    }

}