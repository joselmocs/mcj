<?php

use Illuminate\Database\Migrations\Migration;

class CreateClientAddress extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_client_address', function($table)
        {
            $table->increments('id');
            $table->string('address')->nullable();
            $table->string('number')->nullable();
            $table->string('district')->nullable();
            $table->string('city')->nullable()->index();
            $table->string('uf')->nullable();
            $table->string('zipcode')->nullable();

            $table->integer('client_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('sac_clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_client_address');
    }

}