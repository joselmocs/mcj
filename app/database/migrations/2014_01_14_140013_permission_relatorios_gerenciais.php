<?php

use Illuminate\Database\Migrations\Migration;


class PermissionRelatoriosGerenciais extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $scope = DB::table('user_permission_scope')->insertGetId(array(
            'name' => 'Relatórios Gerenciais',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        $permissions = array(
            array('Relatórios de OS\'s', 'report.os', 'Capacidade de visualizar os relatórios gerenciais de OS\'s.'),
            array('Relatórios de Chamados', 'report.support', 'Capacidade de visualizar os relatórios gerenciais de Chamados.')
        );

        foreach($permissions as $permission) {
            DB::table('user_permissions')->insert(array(
                'scope_id' => $scope,
                'name' => $permission[0],
                'tag' => $permission[1],
                'description' => $permission[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}