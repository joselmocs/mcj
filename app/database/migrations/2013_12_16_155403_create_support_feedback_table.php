<?php

use Illuminate\Database\Migrations\Migration;

class CreateSupportFeedbackTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_support_feedback', function($table)
        {
            $table->increments('id');
            $table->integer('support_id')->unsigned()->nullable();
            $table->integer('rating')->default(0);
            $table->text('observation')->nullable();
            $table->timestamps();

            $table->foreign('support_id')->references('id')->on('sac_support');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_support_feedback');
    }

}