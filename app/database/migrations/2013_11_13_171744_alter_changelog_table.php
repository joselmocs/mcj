<?php

use Illuminate\Database\Migrations\Migration;


class AlterChangelogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('changelog', function($table)
        {
            $table->string('version')->default('1.0')->nullable()->after('observation');
            $table->string('launch', 6)->default('000000')->after('version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('changelog', function($table)
        {
            $table->dropColumn('version');
            $table->dropColumn('launch');
        });
    }
}