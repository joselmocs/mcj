<?php

use Illuminate\Database\Migrations\Migration;

class ChangeChangelogReleased extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('changelog', function($table)
        {
            $table->timestamp('released')->nullable()->after('launch');
        });

        foreach(ChangeLog::all() as $changelog) {
            $changelog->released = Carbon::now();
            $changelog->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('changelog', function($table)
        {
            $table->dropColumn('released');
        });
    }

}