<?php

use Illuminate\Database\Migrations\Migration;

class CreateChangelogModuleAccess extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('changelog_module_access', function($table)
        {
            $table->increments('id');
            $table->integer('changelog_id')->unsigned();
            $table->integer('module_id')->unsigned();

            $table->foreign('changelog_id')->references('id')->on('changelog');
            $table->foreign('module_id')->references('id')->on('sac_modules');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('changelog_module_access');
    }

}