<?php

use JCS\Auth\Models\Permission;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsIncoming extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $scope = DB::table('user_permission_scope')->insertGetId(array(
            'name' => 'Clientes em Negociação',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        $permissions = array(
            array('Gerenciar Negociações', 'incoming.manage', 'Capacidade de criar uma nova negociação e alterar a situação da negociação com o cliente.'),
            array('Visualizar Negociações', 'incoming.view', 'Capacidade de visualizar a lista de negociações já feitas com o cliente.'),
            array('Visualizar Observações', 'incoming.text', 'Capacidade de visualizar o texto descritivo com detalhes da negociação.')
        );

        foreach($permissions as $permission) {
            DB::table('user_permissions')->insert(array(
                'scope_id' => $scope,
                'name' => $permission[0],
                'tag' => $permission[1],
                'description' => $permission[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }

        // Atualizamos a permissão antiga
        $permission = Permission::where('tag', '=', 'client.incoming')->first();
        $permission->name = "Lista de Clientes";
        $permission->tag = "incoming.client";
        $permission->scope_id = $scope;
        $permission->description = "Capacidade de visualizar a lista dos clientes que estão em negociação.";
        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}