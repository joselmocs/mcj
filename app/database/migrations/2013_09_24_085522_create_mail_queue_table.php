<?php

use Illuminate\Database\Migrations\Migration;


class CreateMailQueueTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_servers', function($table)
        {
            $table->increments('id');
            $table->string('name', 32);
            $table->string('description', 128)->nullable();
            $table->string('prefix', 32);
            $table->string('from', 128);
            $table->string('host_name', 128);
            $table->integer('host_port');
            $table->boolean('host_tls')->default(false);
            $table->string('host_username', 128);
            $table->string('host_password', 64);
            $table->boolean('enabled')->default(false);
            $table->boolean('working')->default(false);

            $table->timestamps();
        });

        Schema::create('mail_queues', function($table)
        {
            $table->increments('id');
            $table->text('message');
            $table->string('subject', 128);
            $table->string('content_type', 8);
            $table->integer('mode')->default(\JCS\Mail\Models\Queue::MODE_SINGLE);
            $table->boolean('working')->default(false);
            $table->timestamps();
        });

        Schema::create('mail_items', function($table)
        {
            $table->increments('id');
            $table->string('email', 128);
            $table->integer('queue_id')->unsigned()->nullable();
            $table->integer('status')->default(\JCS\Mail\Models\Mail::STATUS_WAITING);
            $table->timestamps();

            $table->foreign('queue_id')->references('id')->on('mail_queues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_items');
        Schema::dropIfExists('mail_queues');
        Schema::dropIfExists('mail_servers');
    }

}