<?php

use Illuminate\Database\Migrations\Migration;


class CreateChangelogTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('changelog', function($table)
        {
            $table->increments('id');
            $table->integer('os_id')->unsigned()->nullable();
            $table->string('summary', 256)->nullable();
            $table->text('description')->nullable();
            $table->text('result')->nullable();
            $table->text('action')->nullable();
            $table->text('observation')->nullable();
            $table->timestamps();

            $table->foreign('os_id')->references('id')->on('sac_os');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('changelog');
    }

}