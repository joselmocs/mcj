<?php

use Illuminate\Database\Migrations\Migration;


class CreateSupportPermissions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $scope = DB::table('user_permission_scope')->insertGetId(array(
            'name' => 'Chamados',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        $permissions = array(
            array('Visualizar o Monitor', 'support.monitor_view', 'Capacidade de visualizar o monitor de chamados.'),
            array('Atender Chamados', 'support.answer_call', 'Capacidade de atender um chamado.'),
            array('Auditar Chamados', 'support.audit_call', 'Capacidade de auditar um chamado.'),
            array('Visualizar Histórico', 'support.history_view', 'Capacidade de visualizar o histórico de chamados de outros operadores.')
        );

        foreach($permissions as $permission) {
            DB::table('user_permissions')->insert(array(
                'scope_id' => $scope,
                'name' => $permission[0],
                'tag' => $permission[1],
                'description' => $permission[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}