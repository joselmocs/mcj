<?php

use Illuminate\Database\Migrations\Migration;


class CreateClientModule extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_client_module', function($table)
        {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('module_id')->unsigned();

            $table->foreign('client_id')->references('id')->on('sac_clients');
            $table->foreign('module_id')->references('id')->on('sac_modules');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_client_module');
    }
}