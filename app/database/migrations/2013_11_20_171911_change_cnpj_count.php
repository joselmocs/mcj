<?php

use Illuminate\Database\Migrations\Migration;

class ChangeCnpjCount extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach(SacClient::all() as $client) {
            $client->cgc = str_repeat("0", 14 - strlen($client->cgc)) . $client->cgc;
            $client->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}