<?php

use JCS\Auth\Models\PermissionScope;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionIncomingMail extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = array(
            array('Enviar Negociação por Email', 'incoming.mail', 'Capacidade de enviar uma negociação realizada por email.')
        );

        $scope = PermissionScope::where('name', '=', 'Clientes em Negociação')->firstOrFail();
        foreach($permissions as $permission) {
            DB::table('user_permissions')->insert(array(
                'scope_id' => $scope->id,
                'name' => $permission[0],
                'tag' => $permission[1],
                'description' => $permission[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}