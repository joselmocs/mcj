<?php

use Illuminate\Database\Migrations\Migration;

class ChangeChangelogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('changelog', function($table)
        {
            $table->integer('user_id')->unsigned()->nullable()->after('id');
            $table->integer('glossary_id')->unsigned()->nullable()->after('os_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('glossary_id')->references('id')->on('glossary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('changelog', function($table)
        {
            $table->dropColumn('user_id');
            $table->dropColumn('glossary_id');
        });
    }

}