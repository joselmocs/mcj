<?php

use Illuminate\Database\Migrations\Migration;


class CreateChangelogClientAccess extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('changelog', function($table) {
            $table->boolean('free_access')->default(false)->after('observation');
        });

        Schema::create('changelog_client_access', function($table)
        {
            $table->increments('id');
            $table->integer('changelog_id')->unsigned();
            $table->integer('client_id')->unsigned();

            $table->foreign('changelog_id')->references('id')->on('changelog');
            $table->foreign('client_id')->references('id')->on('sac_clients');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('changelog', function($table) {
            $table->dropColumn('free_access');
        });

        Schema::dropIfExists('changelog_client_access');
    }

}