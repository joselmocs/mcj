<?php

use JCS\Auth\Models\Permission;
use JCS\Auth\Models\PermissionScope;
use Illuminate\Database\Migrations\Migration;

class AlterClientsAddDataContrato extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sac_clients', function($table)
        {
            $table->timestamp('client_since')->nullable()->after('is_incoming');
            $table->timestamp('last_revision')->nullable()->after('is_incoming');
        });

        $permissions = array(
            array('Editar Clientes', 'client.edit', 'Capacidade de editar os dados cadastrais de um cliente.')
        );

        $scope = PermissionScope::where('name', '=', 'Clientes')->firstOrFail();
        foreach($permissions as $permission) {
            DB::table('user_permissions')->insert(array(
                'scope_id' => $scope->id,
                'name' => $permission[0],
                'tag' => $permission[1],
                'description' => $permission[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sac_clients', function($table)
        {
            $table->dropColumn('client_since');
            $table->dropColumn('last_revision');
        });

        Permission::where('tag', '=', 'client.edit')->delete();
    }

}