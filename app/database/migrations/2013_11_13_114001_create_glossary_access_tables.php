<?php

use Illuminate\Database\Migrations\Migration;

class CreateGlossaryAccessTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('glossary', function($table) {
            $table->boolean('free_access')->default(false)->after('observation');
        });

        Schema::create('glossary_client_access', function($table)
        {
            $table->increments('id');
            $table->integer('glossary_id')->unsigned();
            $table->integer('client_id')->unsigned();

            $table->foreign('glossary_id')->references('id')->on('glossary');
            $table->foreign('client_id')->references('id')->on('sac_clients');

            $table->timestamps();
        });

        Schema::create('glossary_module_access', function($table)
        {
            $table->increments('id');
            $table->integer('glossary_id')->unsigned();
            $table->integer('module_id')->unsigned();

            $table->foreign('glossary_id')->references('id')->on('glossary');
            $table->foreign('module_id')->references('id')->on('sac_modules');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('changelog', function($table) {
            $table->dropColumn('free_access');
        });

        Schema::dropIfExists('changelog_client_access');
        Schema::dropIfExists('changelog_module_access');
    }

}