<?php

use MCJ\SACWin\Models\SCCAPEND;
use Illuminate\Database\Migrations\Migration;

class UpdateDataHomologacaoOs extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $osList = SacOS::where('ended_at', '=', null)->get();
        foreach($osList as $os)
        {
            $scApend = SCCAPEND::query()
                ->select(array('BAIXA'))
                ->where('PEDIDO', '=', $os->cod)
                ->firstOrFail();
            $os->ended_at = date('Y-m-d H:i:s', strtotime(sprintf("%s 00:00:00", $scApend->BAIXA)));
            $os->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}