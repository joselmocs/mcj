<?php

use Illuminate\Database\Migrations\Migration;


class ImportModulesClients extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $file = storage_path('migrations/import_modules_clients/') . 'cnpj_mcj.csv';

        $fb = fopen($file, 'r');
        while(!feof($fb)) {
            $lcli = explode(",", fgets($fb));

            if ($lcli[0] == "codigo") {
                continue;
            }

            try {
                $client = SacClient::where('cod', '=', $lcli[0])->firstOrFail();
            }
            catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                continue;
            }

            $client->modules()->delete();

            // Administrador de Entidade e Administrador do Sistema
            foreach(array(1, 2) as $nmod) {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find($nmod)->id;
                $module->save();
            }

            // Agenda
            if ($lcli[3] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(3)->id;
                $module->save();
            }

            // Corpo
            if ($lcli[4] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(6)->id;
                $module->save();
            }

            // Estoque
            if ($lcli[5] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(5)->id;
                $module->save();
            }

            // Fatura
            if ($lcli[6] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(8)->id;
                $module->save();
            }

            // Recepção
            if ($lcli[7] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(12)->id;
                $module->save();
            }

            // APAC
            if ($lcli[8] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(9)->id;
                $module->save();
            }

            // Laboratório
            if ($lcli[9] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(11)->id;
                $module->save();
            }

            // Custo
            if ($lcli[10] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(7)->id;
                $module->save();
            }

            // Financeiro
            if ($lcli[11] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(10)->id;
                $module->save();
            }

            // ChecaIH
            if ($lcli[12] == '"S"') {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = SacModule::find(4)->id;
                $module->save();
            }

        }

        fclose($fb);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}