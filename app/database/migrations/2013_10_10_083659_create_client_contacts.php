<?php

use Illuminate\Database\Migrations\Migration;


class CreateClientContacts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_mailgroups', function($table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('sac_departments', function($table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('sac_client_contacts', function($table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('fax')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('mailgroup_id')->nullable()->unsigned();
            $table->integer('department_id')->nullable()->unsigned();
            $table->integer('client_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('sac_clients');
            $table->foreign('mailgroup_id')->references('id')->on('sac_mailgroups');
            $table->foreign('department_id')->references('id')->on('sac_departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_client_contacts');
        Schema::dropIfExists('sac_departments');
        Schema::dropIfExists('sac_mailgroups');
    }
}