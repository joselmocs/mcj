<?php

use JCS\Auth\Models\Permission,
    JCS\Auth\Models\PermissionScope;
use Illuminate\Database\Migrations\Migration;


class CreateSacClientIncoming extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_client_incoming_status', function($table)
        {
            $table->increments('id');
            $table->string('name', 128)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('color', 6)->default('000000');
            $table->boolean('close_incoming')->default(false);
            $table->timestamps();
        });

        Schema::create('sac_client_incoming', function($table)
        {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('owner_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('sac_client_incoming_status');
            $table->foreign('client_id')->references('id')->on('sac_clients');
            $table->foreign('owner_id')->references('id')->on('users');
        });

        Schema::create('sac_client_incoming_contacts', function($table)
        {
            $table->increments('id');
            $table->integer('incoming_id')->unsigned()->nullable();
            $table->integer('operator_id')->unsigned()->nullable();
            $table->integer('contact_id')->unsigned()->nullable();
            $table->text('text')->nullable();
            $table->boolean('effected')->default(false);
            $table->integer('status_id')->unsigned()->nullable();
            $table->timestamp('date_contact');
            $table->timestamp('next_contact');
            $table->timestamps();

            $table->foreign('incoming_id')->references('id')->on('sac_client_incoming');
            $table->foreign('contact_id')->references('id')->on('sac_client_contacts');
            $table->foreign('status_id')->references('id')->on('sac_client_incoming_status');
            $table->foreign('operator_id')->references('id')->on('users');
        });

        Schema::table('sac_clients', function($table)
        {
            $table->boolean('is_incoming')->default(false)->after('situacao');
        });

        $scope = PermissionScope::where('name', '=', 'Clientes')->firstOrFail()->id;
        $permissions = array(
            array('Clientes em negociação', 'client.incoming', 'Capacidade de criar e gerenciar clientes em Negociação.')
        );

        foreach($permissions as $permission) {
            if (!Permission::where('tag', '=', $permission[1])->first()) {
                DB::table('user_permissions')->insert(array(
                    'scope_id' => $scope,
                    'name' => $permission[0],
                    'tag' => $permission[1],
                    'description' => $permission[2],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ));
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sac_clients', function($table)
        {
            $table->dropColumn('is_incoming');
        });

        Schema::dropIfExists('sac_client_incoming_contacts');
        Schema::dropIfExists('sac_client_incoming');
        Schema::dropIfExists('sac_client_incoming_status');
    }

}