<?php

use Illuminate\Database\Migrations\Migration;


class CreateSystemTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system', function($table)
        {
            $table->increments('id');
            $table->string('ip_sagwin_firebird', 64)->default('127.0.0.1');
            $table->string('path_sagwin_firebird', 255)->default('C:/mcj.gdb');
            $table->string('php_firebird_extension_path', 255)->default('php');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system');
    }

}