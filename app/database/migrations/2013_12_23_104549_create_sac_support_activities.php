<?php

use Illuminate\Database\Migrations\Migration;

class CreateSacSupportActivities extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_support_activities', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('support_id')->unsigned()->nullable();
            $table->timestamp('alive');
            $table->timestamp('last_message');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('support_id')->references('id')->on('sac_support');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_support_activities');
    }

}