<?php

use Illuminate\Database\Migrations\Migration;

class AlterOsTableHomologado extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sac_os', function($table)
        {
            $table->timestamp('ended_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sac_os', function($table)
        {
            $table->dropColumn('ended_at');
        });
    }

}