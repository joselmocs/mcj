<?php

use Illuminate\Database\Migrations\Migration;


class CreateGlossaryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('glossary', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('module_id')->unsigned()->nullable();
            $table->string('function', 256);
            $table->integer('classification')->default(0);
            $table->text('description')->nullable();
            $table->text('result')->nullable();
            $table->text('action')->nullable();
            $table->text('observation')->nullable();
            $table->integer('timespend')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('module_id')->references('id')->on('sac_modules');
        });

        Schema::create('glossary_activities', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('glossary_id')->unsigned()->nullable();
            $table->string('activity', 128);
            $table->string('description', 128)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('glossary_id')->references('id')->on('glossary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glossary_activities');
        Schema::dropIfExists('glossary');
    }

}