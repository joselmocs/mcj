<?php

use Illuminate\Database\Migrations\Migration;


class CreateOsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_os_type', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('icon');
            $table->timestamps();
        });

        Schema::create('sac_os_status', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('icon');
            $table->timestamps();
        });

        Schema::create('sac_os', function($table)
        {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('module_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->nullable();


            $table->integer('cod')->nullable();
            $table->string('summary')->nullable();
            $table->integer('resolution')->default(0);

            // Inicialmente este campo será uma string até que criemos
            // a relação de usuários sacweb -> sacwin
            $table->string('reporter')->nullable();
            $table->string('assigned')->nullable();

            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('sac_clients');
            $table->foreign('module_id')->references('id')->on('sac_modules');
            $table->foreign('type_id')->references('id')->on('sac_os_type');
            $table->foreign('status_id')->references('id')->on('sac_os_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_os');
        Schema::dropIfExists('sac_os_status');
        Schema::dropIfExists('sac_os_type');
    }

}