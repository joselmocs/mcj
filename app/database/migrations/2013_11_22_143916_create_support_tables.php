<?php

use Illuminate\Database\Migrations\Migration;


class CreateSupportTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_support', function($table)
        {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('operator_id')->unsigned()->nullable();
            $table->integer('type')->default(0)->nullable();
            $table->integer('status')->default(0)->nullable();
            $table->text('description')->nullable();
            $table->dateTime('answered_at')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('users');
            $table->foreign('operator_id')->references('id')->on('users');
        });

        Schema::create('sac_support_messages', function($table)
        {
            $table->increments('id');
            $table->integer('support_id')->unsigned()->nullable();
            $table->integer('sender_id')->unsigned()->nullable();
            $table->boolean('private')->default(false);
            $table->text('message')->nullable();
            $table->timestamps();

            $table->foreign('support_id')->references('id')->on('sac_support');
            $table->foreign('sender_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_support_messages');
        Schema::dropIfExists('sac_support');
    }

}