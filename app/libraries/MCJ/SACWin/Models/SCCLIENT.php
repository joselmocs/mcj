<?php

namespace MCJ\SACWin\Models;

use SacClient,
    SacClientAddress;


class SCCLIENT extends FBModel {

    public static $SITUATIONS = array(
        'I' => SacClient::SITUATION_INACTIVE,
        'A' => SacClient::SITUATION_ACTIVE,
        'D' => SacClient::SITUATION_DEBIT,
        'S' => SacClient::SITUATION_SUSPENDED,
        'R' => SacClient::SITUATION_RESCISSION,
        'T' => SacClient::SITUATION_OUTSOURCED
    );

    /**
     * Retorna o model eloquent referente a este FB model
     *
     * @return SacClient
     */
    public function eloquent()
    {
        if (!$this->eloquent) {
            $this->eloquent = SacClient::where('cod', '=', $this->COD)->first();
        }

        return $this->eloquent;
    }

    /**
     * Sincroniza o clientes do SACWIN com o cliente no sistema.
     *
     * @return void
     */
    public function sync()
    {
        $client = $this->eloquent();
        if ($client && $this->EMPRESA != "M") {
            $client->delete();
        }

        if ($this->EMPRESA != "M") {
            return;
        }

        if (!$client) {
            $client = new SacClient;
        }

        $client->cod = $this->COD;
        $client->nome = self::filter($this->NOME);
        $client->cgc = str_repeat("0", 14 - strlen($this->CGC)) . $this->CGC;

        // Há clientes em que a situação está nula no banco de dados, nestes casos (por falta de informação)
        // definiremos a situação 'inativo'.
        $client->situacao = (array_key_exists($this->SITUACAO, self::$SITUATIONS))
            ? self::$SITUATIONS[$this->SITUACAO]
            : SacClient::SITUATION_INACTIVE;

        $client->save();

        // Preenchemos o endereço
        $address = $client->address;
        if (!$address) {
            $address = new SacClientAddress;
            $address->client_id = $client->id;
        }

        $address->address = self::filter($this->ENDERECO);
        $address->number = $this->NUMERO;
        $address->district = self::filter($this->BAIRRO);
        $address->city = self::filter($this->CIDADE);
        $address->zipcode = $this->CEP;
        $address->uf = $this->UF;
        $address->save();
    }
}