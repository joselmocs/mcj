<?php

namespace MCJ\SACWin\Models;

use MCJ\SACWin\Connection;


class FBModel {

    /**
     * @var \Eloquent
     */
    public $eloquent;

    /**
     * Inicia a criação de um statment
     *
     * @return Connection
     */
    public static function query()
    {
        return new Connection(self::getModelName(), self::getNameSpace());
    }

    /**
     * Limpa os caracteres especiais da string.
     *
     * @param string $string
     * @return bool|string
     * @static
     */
    public static function filter($string)
    {
        return trim(iconv('ISO-8859-1', 'utf-8//IGNORE', $string));
    }

    /**
     * Retorna o nome do model que está extendendo esta base. Útil para descobrir qual
     * o nome da tabela que estamos utilizando.
     *
     * @return string
     * @static
     */
    public static function getModelName()
    {
        $nameSpaces = explode('\\', get_called_class());
        return $nameSpaces[count($nameSpaces) -1];
    }

    /**
     * Retorna o nome do model que está extendendo esta base. Útil para descobrir qual
     * o nome da tabela que estamos utilizando.
     *
     * @return string
     * @static
     */
    public static function getNameSpace()
    {
        return __NAMESPACE__ .'\\'. self::getModelName();
    }

    /**
     * @throws FBMethodNotImplementedException
     */
    public function eloquent() {
        throw new FBMethodNotImplementedException;
    }
}

class FBModelNotFoundException extends \RuntimeException {};
class FBMethodNotImplementedException extends \RuntimeException {};