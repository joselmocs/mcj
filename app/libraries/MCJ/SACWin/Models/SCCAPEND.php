<?php

namespace MCJ\SACWin\Models;

use SacOS,
    SacClient;


class SCCAPEND extends FBModel {

    public static $STATUS = array(
        '4' => SacOS::STATUS_CLOSED
    );

    /**
     * Retorna o model eloquent referente a este FB model
     *
     * @return SacOS
     */
    public function eloquent()
    {
        if (!$this->eloquent) {
            $this->eloquent = SacOS::where('cod', '=', $this->PEDIDO)->first();
        }

        return $this->eloquent;
    }

    /**
     * Retorna o técnico que reportou a OS
     */
    public function getTecnic($tecnic)
    {
        return SCTECNIC::query()
            ->where('COD', '=', $tecnic)
            ->firstOrFail();
    }

    /**
     * Sincroniza o clientes do SACWIN com o cliente no sistema.
     *
     * @return void
     */
    public function sync()
    {
        $os = $this->eloquent();
        if (!$os) {
            $os = new SacOS;
        }

        $os->cod = $this->PEDIDO;
        $os->client_id = SacClient::where('cod', '=', $this->COD)->firstOrFail()->id;
        $os->reporter = self::filter($this->getTecnic($this->TECNICO)->NOME);
        $os->assigned = self::filter($this->getTecnic($this->PROG)->NOME);
        $os->status_id = self::$STATUS[$this->FASE];
        $os->type_id = SacOS::TYPE_UNDEFINED;
        $os->resolution = SacOS::RES_FIXED;
        $os->description = null;
        $os->created_at = date('Y-m-d H:i:s',
            strtotime(sprintf("%s %s:%s:00", $this->DATA, substr($this->HORA, 0, 2), substr($this->HORA, -2)))
        );
        $os->ended_at = date('Y-m-d H:i:s', strtotime(sprintf("%s 00:00:00", $this->BAIXA)));
        $os->save();
    }
}