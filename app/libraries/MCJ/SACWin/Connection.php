<?php

namespace MCJ\SACWin;

use PDO;
use Config;
use MCJ\SACWin\Models\FBModelNotFoundException;


class Connection {

    /**
     * The active PDO connection.
     *
     * @var PDO
     */
    public static $pdo;

    /**
     * Self instance
     *
     * @var Connection
     */
    protected $instance;

    /**
     * Table
     *
     * @var string
     */
    protected $table;

    /**
     * Select do statment
     *
     * @var array
     */
    protected $selectStatments = array();

    /**
     * Condições do statment
     *
     * @var array
     */
    protected $whereStatments = array();

    /**
     * OrderBy do statment
     *
     * @var array
     */
    protected $orderByStatments = array();

    /**
     * @param string $table
     * @param string $namespace
     * @throws \InvalidArgumentException
     */
    public function __construct($table, $namespace)
    {
        if (empty($namespace) || empty($table)) {
            throw new \InvalidArgumentException('invalid connection arguments');
        }

        $this->table = $table;
        $this->namespace = $namespace;
    }

    /**
     * Define os campos que serão selecionados na query
     *
     * @param mixed $fields
     * @return $this
     */
    public function select(array $fields)
    {
        foreach ($fields as $field)
        {
            if (!in_array($field, $this->selectStatments))
            {
                $this->selectStatments[] = strtoupper($field);
            }
        }

        return $this;
    }

    /**
     * Define uma condição na query
     *
     * @param string $field
     * @param string $condition
     * @param mixed $value
     * @throws \InvalidArgumentException
     * @return $this
     */
    public function where($field, $condition, $value)
    {
        if (!is_string($field) || !is_string($condition)
                || !in_array(gettype($value), array('string', 'integer','array'))) {
            throw new \InvalidArgumentException('invalid where statment');
        }

        $this->whereStatments[] = array($field, $condition, $value);

        return $this;
    }

    /**
     * Define a ordenação da query
     *
     * @param string $field
     * @param string $order
     * @throws \InvalidArgumentException
     * @return $this
     */
    public function orderBy($field, $order = '')
    {
        if (!in_array($order, array('', 'asc', 'desc'))) {
            throw new \InvalidArgumentException('invalid orderBy statment');
        }

        $this->orderByStatments[] = array($field, $order);

        return $this;
    }

    /**
     * Executa o statment e retorna os resultados
     *
     * @return array
     */
    public function get()
    {
        $stmt = self::getPDO()->query(
              $this->buildSelectStatment()
            . $this->buildWhereStatment()
            . $this->buildOrderByStatment()
        );

        return ($stmt) ? $stmt->fetchALL(PDO::FETCH_CLASS, $this->namespace) : null;
    }

    /**
     * Retorna o primeiro resultado da query
     *
     * @return null
     */
    public function first()
    {
        $result = $this->get();

        return count($result) ? $result[0] : null;
    }

    /**
     * Retorna o primeiro resultado da query ou lança uma exceção
     *
     * @return object
     * @throws FBModelNotFoundException
     */
    public function firstOrFail()
    {
        $result = $this->first();
        if (null === $result) {
            throw new FBModelNotFoundException($this->queryString());
        }

        return $result;
    }

    /**
     * Retorna uma string com a query formada.
     *
     * @return string
     *
     */
    public function queryString()
    {
        $select = $this->buildSelectStatment();
        $where = $this->buildWhereStatment();
        $orderBy = $this->buildOrderByStatment();

        return $select . $where . $orderBy;
    }

    /**
     * Cria e/ou retorna a instancia PDO
     *
     * @return PDO
     */
    protected static function getPDO()
    {
        if (null === self::$pdo)
        {
            $system = \System::options();
            self::$pdo = new PDO(
                sprintf(
                    Config::get('database.connections.sacwin.dsn'),
                    $system->ip_sagwin_firebird,
                    $system->path_sagwin_firebird
                ),
                Config::get('database.connections.sacwin.username'),
                Config::get('database.connections.sacwin.password')
            );
        }

        return self::$pdo;
    }

    /**
     * Monta o statment select
     *
     * @return string
     */
    protected function buildSelectStatment()
    {
        $statment = 'select ';

        if (count($this->selectStatments))
        {
            for ($i = 0; $i < count($this->selectStatments); $i++)
            {
                $statment .= $this->selectStatments[$i];
                $statment .= ($i == count($this->selectStatments) -1) ? ' ' : ', ';
            }
        }
        else {
            $statment .= '* ';
        }
        $statment .= 'from '. $this->table .' ';

        return $statment;
    }

    /**
     * Monta o statment Where
     *
     * @return string
     */
    protected function buildWhereStatment()
    {
        $statment = '';
        if (count($this->whereStatments) > 0)
        {
            $statment .= 'where ';
            for ($i = 0; $i < count($this->whereStatments); $i++)
            {
                $statment .= $this->whereStatments[$i][0] .' '. $this->whereStatments[$i][1] .' ';
                if (gettype($this->whereStatments[$i][2]) == "array")
                {
                    $statment .= $this->whereStatments[$i][2][0] .' AND '. $this->whereStatments[$i][2][1];
                }
                else {
                    $statment .= $this->whereStatments[$i][2];
                }
                $statment .= ($i == count($this->whereStatments) -1) ? ' ' : ' AND ';
            }
        }

        return $statment;
    }

    /**
     * Monta o statment orderBy
     *
     * @return string
     */
    protected function buildOrderByStatment()
    {
        $statment = '';

        if (count($this->orderByStatments))
        {
            $statment .= 'order by ';
            for ($i = 0; $i < count($this->orderByStatments); $i++)
            {
                $statment .= $this->orderByStatments[$i][0] .' '. $this->orderByStatments[$i][1];
                $statment .= ($i == count($this->orderByStatments) -1) ? ' ' : ', ';
            }
        }

        return $statment;
    }
}