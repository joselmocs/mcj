<?php

namespace MCJ\SACWin\Console;

use SacOS;
use MCJ\SACWin\Models\SCCAPEND,
    MCJ\SACWin\Models\FBModelNotFoundException;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption,
    Symfony\Component\Console\Input\InputArgument,
    Symfony\Component\Process\Process,
    Symfony\Component\Filesystem\Filesystem;


class OSCommands extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sacwin:os';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa OS diretamente da base do Sacwin';

    /**
     * Create a new command instance.
     *
     * @return OSCommands
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        switch ($this->argument('mode'))
        {
            case 'sync':
                $this->syncCommand();
                break;
            case 'import':
                $this->importCommand();
                break;
            case 'clear':
                $this->clearCommand();
                break;
        }
    }

    /**
     * Sincroniza as OSs do SACWin criadas no intervalo de tempo definido pelas duas opções: 'start' e 'end'.
     * Exemplos de uso:
     *
     *     php artisan sacwin:os sync --start=last --end=today
     *         - Sincroniza as OS's a partir da data da última OS importada pelo sistema até o dia de hoje.
     *
     *     php artisan sacwin:os sync --start=2013-09-01 --end=2013-09-30
     *         - Sincroniza todas as OS's do dia 1 ao 31 de setembro de 2013.
     *
     * @return void
     */
    protected function syncCommand()
    {
        $this->info('synchronizing...');

        // Selecionamos os pedidos no sacwin que estão como status 'testado e ok' entre as datas
        // informadas nas opções enviadas no command.
        $scApends = SCCAPEND::query()
            ->select(array('PEDIDO'))
            ->where('FASE', '=', 4)
            ->where('DATA', 'BETWEEN', array(
                "DATE '". $this->getDateOption('start')->toDateString() ."'",
                "DATE '". $this->getDateOption('end')->toDateString() ."'"
            ))
            ->orderBy('PEDIDO');

        foreach ($scApends->get() as $scApend)
        {
            if (!$scApend->eloquent())
            {
                // Executamos a importação em outro processo para evitar problemas com
                // a seleção do campo TEXTO do sacwin.
                $process = new Process(sprintf(
                    '%s %s/artisan sacwin:os import --os=%s',
                    \System::options()->php_firebird_extension_path,
                    base_path(),
                    $scApend->PEDIDO
                ), null, null, null, 1200);

                $process->run(function($type, $buffer) {
                    if (Process::ERR === $type) {
                        $this->error(trim($buffer));
                    } else {
                        $this->info(trim($buffer));
                    }
                });
            }
        }

        $this->info('completed.');
    }

    /**
     * Faz a importação de uma OS.
     * Exemplo de uso:
     *
     *     pph artisan sacwin:os import --os=X (onde X é o número do pedido na tabela do sacwin)
     *
     */
    protected function importCommand()
    {
        $pedido = $this->option('os');

        try {
            $scApend = SCCAPEND::query()
                ->select(array('PEDIDO', 'COD', 'FASE', 'PROG', 'TECNICO', 'PRODUTO', 'DATA', 'HORA', 'BAIXA', 'TEXTO'))
                ->where('PEDIDO', '=', $pedido)
                ->firstOrFail();
        }
        catch(FBModelNotFoundException $e) {
            $this->error('OS not found, aborted.');
            return;
        }

        $this->info('importing new OS '. $scApend->PEDIDO);

        // Verificamos se os arquivos temporários da OS já existe, se positivo
        // removemos pois iremos criar um novo.
        $this->clearTemp($scApend);

        // Agora importamos o campo TEXTO (blob) pois ele precisa de um trabalho especial.
        // No sacwin, este campo armazena as informações da OS e as imagens na mesma
        // contida. O problema é que o formato é salvo em RTF, e como as imagens
        // estão anexas ao texto, o campo acaba ficando muito grande e causando erros
        // no php por uso da memória máxima permitida.
        $this->prepareTempFile($scApend);

        // Convertemos o RTF para html
        $this->convertRTF($scApend);

        // Importamos o html convertido
        $convertedText = $this->importHtml($scApend);

        if ($convertedText)
        {
            // Sincronizamos as informações da OS
            $scApend->sync();

            // Salvamos o html convertido e removemos os arquivos temporários
            $scApend->eloquent()->description = htmlentities($convertedText, ENT_QUOTES, 'utf-8', false);
            $scApend->eloquent()->save();
        }

        // Limpamos os arquivos temporários
        $this->clearTemp($scApend);
    }

    /**
     * Comando que removerá todas as OS ja inseridas no sistema
     *
     * @return void
     */
    protected function clearCommand()
    {
        if ($this->confirm('Confirms removing of all the entries? [y/N] ', false))
        {
            $fs = new Filesystem();
            SacOS::all()->each(function($item) use ($fs)
            {
                $item->delete();

                // Excluímos imagens das OS convertidas
                $absPath = public_path('upload/os/'. $item->pedido .'/');
                if ($fs->exists($absPath)) {
                    $fs->remove($absPath);
                }
            });

            $this->info('OS table emptied.');
        }
    }

    /**
     * Retorna a última OS importada pelo sistema.
     *
     * @throws \InvalidArgumentException
     * @return SacOS
     */
    protected function lastImportedOS()
    {
        $os = SacOS::select('created_at')->orderBy('created_at', 'desc')->limit(1)->first();
        if (!$os) {
            throw new \InvalidArgumentException('invalid \'last\' option: no OS was found.');
        }

        return $os;
    }

    /**
     * Verifica se opção de inicio e final de período existe, caso positivo já é retornado um objeto
     * carbom para facilitar a manupulação datetime.
     *
     * @param string $option
     * @throws \InvalidArgumentException
     * @return \Carbon
     */
    protected function getDateOption($option)
    {
        $date = null;
        switch ($this->option($option))
        {
            case 'today':
                $date = \Carbon::now(); break;

            case 'last':
                $date = \Carbon::createFromFormat('Y-m-d H:i:s', $this->lastImportedOS()->created_at);
                break;
            default:
                try {
                    $date = \Carbon::createFromFormat('Y-m-d', $this->option($option));
                }
                catch (\InvalidArgumentException $e) {
                    throw new \InvalidArgumentException(
                        'invalid '. $option .' date format. use: YYYY-MM-DD eg: 2001-12-31'
                    );
                }
                break;
        }

        return $date;
    }

    /**
     * Removemos os arquivos temporários usados na conversão do texto
     *
     * @param object $scApend
     * @return void
     */
    protected function clearTemp($scApend)
    {
        $fs = new Filesystem();
        if ($fs->exists($this->getRtfTempFileName($scApend))) {
            $fs->remove($this->getRtfTempFileName($scApend));
        }
        if ($fs->exists($this->getHtmlTempFileName($scApend))) {
            $fs->remove($this->getHtmlTempFileName($scApend));
        }
    }

    /**
     * Importamos o html convertido e armazenamos no banco
     *
     * @param object $scApend
     * @return mixed
     */
    protected function importHtml($scApend)
    {
        $htmlFile = $this->getHtmlTempFileName($scApend);

        $fs = new Filesystem();
        if ($fs->exists($htmlFile))
        {
            // Substituimos o path absoluto pelo relativo
            return str_replace(str_replace('\\', '/', public_path()), '', file_get_contents($htmlFile));
        }

        $this->error('converted file not found');
        return null;
    }

    /**
     * Faz a conversão do arquivo gerado RTF para arquivo HTML.
     */
    protected function convertRTF($scApend)
    {
        // Executamos a conversão do arquivo RTF para arquivo HTML
        $process = new Process(sprintf(
            '%s/external/rtf-converter/bin/Rtf2Html.exe %s /ID:%s /DS:content',
            base_path(),
            $this->getRtfTempFileName($scApend),
            $this->getImagePath($scApend)
        ), null, null, null, 240);
        $process->run(function($type, $buffer) {
            if (Process::ERR === $type) {
                $this->error(trim($buffer));
            }
        });
    }

    /**
     * Retorna o diretório em que as imagens precisam ser armazenadas
     *
     * @param object $scApend
     * @return string
     */
    protected function getImagePath($scApend)
    {
        $relativePath = 'upload/os/'. $scApend->PEDIDO .'/';
        $absPath = public_path($relativePath);

        $fs = new Filesystem();
        if ($fs->exists($absPath)) {
            $fs->remove($absPath);
        }

        return $absPath;
    }

    /**
     * Retorna o caminho para o arquivo temporário
     *
     * @param object $scApend
     * @return string
     */
    protected function getTempFileName($scApend)
    {
        return storage_path('temp/'. md5($scApend->PEDIDO));
    }

    /**
     * Retorna o arquivo temporário com a extenção .rtf
     *
     * @param object $scApend
     * @return string
     */
    protected function getRtfTempFileName($scApend)
    {
        return $this->getTempFileName($scApend) .'.rtf';
    }

    /**
     * Retorna o arquivo temporário com a extenção .html
     *
     * @param object $scApend
     * @return string
     */
    protected function getHtmlTempFileName($scApend)
    {
        return $this->getTempFileName($scApend) .'.html';
    }

    /**
     * Armazenamos o texto da OS em um arquivo para que possa ser convertido por outro
     * processo, depois lido e armazenado em formato HTML.
     *
     * @param object $scApend
     * @return string
     */
    protected function prepareTempFile($scApend)
    {
        $fs = new Filesystem();
        $fs->dumpFile($this->getRtfTempFileName($scApend), $scApend->TEXTO);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('mode', InputArgument::REQUIRED, '')
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('start', null, InputOption::VALUE_OPTIONAL, 'formato: YYYY-MM-DD', null),
            array('end', null, InputOption::VALUE_OPTIONAL, 'formato: YYYY-MM-DD', null),
            array('os', null, InputOption::VALUE_OPTIONAL, 'OS number in sacwin', null)
        );
    }
}