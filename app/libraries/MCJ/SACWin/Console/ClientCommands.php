<?php

namespace MCJ\SACWin\Console;

use SacClient;
use MCJ\SACWin\Models\SCCLIENT;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption,
    Symfony\Component\Console\Input\InputArgument;


class ClientCommands extends Command {

    /**
     * The console command name .
     *
     * @var string
     */
    protected $name = 'sacwin:client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return ClientCommands
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        switch ($this->argument('mode'))
        {
            case 'sync':
                $this->syncCommand();
                break;
            case 'clear':
                $this->clearCommand();
                break;
        }
    }

    /**
     * Sincroniza todos os usuarios do SACWin com os usuarios do sistema.
     *
     * @return void
     */
    private function syncCommand()
    {
        $this->info('synchronizing...');

        $clients = SCCLIENT::query()->get();
        foreach ($clients as $scClient) {
            $scClient->sync();
        }
        $this->info('completed.');
    }

    /**
     * Comando que removerá todas as OS ja inseridas no sistema
     *
     * @return void
     */
    private function clearCommand()
    {
        if ($this->confirm('Confirms removing of all the entries? [y/N] ', false))
        {
            SacClient::all()->each(function($item) {
                $item->delete();
            });
            $this->info('Client table emptied.');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('mode', InputArgument::REQUIRED, ''),
        );
    }
}