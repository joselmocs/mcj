<?php

namespace JCS\Mail\Models;

use Symfony\Component\Process\Process;


class Queue extends \Eloquent
{
    const MODE_SINGLE = 1;
    const MODE_CC     = 2;
    const MODE_BCC    = 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mail_queues';

    /**
     * Retorna os emails da lista.
     *
     * @return \JCS\Mail\Models\Mail
     */
    public function mails()
    {
        return $this->hasMany('JCS\Mail\Models\Mail', 'queue_id');
    }

    /**
     * Retorna o primeiro servidor smtp registrado no banco.
     *
     * @return \JCS\Mail\Models\SmtpServer
     */
    private function getSmtpServer()
    {
        return SmtpServer::first();
    }

    /**
     * Retorna a quantidade máxima de emails que cada lote poderá ter.
     *
     * @return int
     */
    private function getLotMaxSize()
    {
        return $this->mode == Queue::MODE_SINGLE ? 1 : 20;
    }

    /**
     * Cria lotes de emails.
     *
     * @return array of JCS\Mail\Models\Mail
     */
    private function generateLots()
    {
        $lots = array();
        foreach ($this->mails as $mail)
        {
            // Se não existir nenhum lot criaremos o primeiro.
            if (!count($lots)) {
                $lots[] = array();
            }

            // Criamos outro lot caso a quantidade de emails definida pelo Modo de Envio para
            // cada lote já tenha se esgotado.
            if (count($lots[count($lots) -1]) == $this->getLotMaxSize()) {
                array_push($lots, array());
            }
            array_push($lots[count($lots) -1], $mail);
        };

        return $lots;
    }

    /**
     * Faz o envio dos emails desta lista.
     *
     * @throws QueueAlreadyWorkingException
     * @throws QueueEmptyException
     * @return void
     */
    public function dispatch()
    {
        if (true === $this->working) {
            throw new QueueAlreadyWorkingException('list is already working');
        }

        $this->checkEmptyQueue();

        // Definimos true em 'working' para que a lista não seja chamada para execução
        // novamente, mesmo se for para envio de emails falhos.
        $this->setWorking(true);

        foreach ($this->generateLots() as $lot)
        {
            // Separamos os emails dos lotes e os adicionamos em um array
            $emails = array_map(function($mail) {
                return $mail->email;
            }, $lot);

            try {
                $this->getSmtpServer()->send(
                    $this->subject,
                    $this->filterMailMode(self::MODE_SINGLE, $emails),
                    $this->message,
                    $this->content_type,
                    $this->filterMailMode(self::MODE_CC, $emails),
                    $this->filterMailMode(self::MODE_BCC, $emails)
                );
                // Lot enviado com sucesso, agora podemos remover os emails que já foram enviados
                // do banco de dados.
                $this->deleteLot($lot);
            }
            catch (\Exception $e) {
                // Definimos o estado dos emails do lot como erro para serem enviados posteriormente.
                $this->setLotStatus($lot, Mail::STATUS_ERROR);
            }
        }

        $this->setWorking(false);

        // Verifica se a queue foi completamente executada e todos os seus emails removidos.
        $this->checkEmptyQueue();

        // Varre os emails restantes a procura de algum com status de erro. Nota: Esta checagem só
        // executará caso a checagem checkEmptyQueue for negativa.
        $this->checkForErrors();
    }

    /**
     * Verifica se algum email da queue possui o status de erro
     *
     * @throws QueueWithErrorException
     */
    public function checkForErrors()
    {
        $mails = Mail::where('queue_id', '=', $this->id)->where('status', '=', MAIL::STATUS_ERROR);
        if ($mails->count()) {
            throw new QueueWithErrorException('queue with mail errors');
        }
    }

    /**
     * Verifica se a queue ainda tem emails para serem enviados.
     *
     * @throws QueueEmptyException
     */
    private function checkEmptyQueue()
    {
        if (false == Mail::where('queue_id', '=', $this->id)->count()) {
            throw new QueueEmptyException('empty list of mails');
        }
    }

    /**
     * Define se a lista está sendo executada no momento ou não. Esta flag servirá para
     * que uma queue não seja executada duas vezes simultaneamente.
     *
     * @param boolean $working
     * @return void
     */
    private function setWorking($working)
    {
        $this->working = $working;
        $this->save();
    }

    /**
     * Deleta os emails do lot do banco de dados
     *
     * @param array $lot lista de emails do lote
     * @param int $status
     * @return void
     */
    private function setLotStatus($lot, $status)
    {
        foreach ($lot as $email) {
            $email->status = $status;
            $email->save();
        }
    }

    /**
     * Deleta os emails do lot do banco de dados
     *
     * @param array $lot lista de emails do lote
     * @return void
     */
    private function deleteLot($lot)
    {
        foreach ($lot as $email) {
            $email->delete();
        }
    }

    /**
     * Filtra os emails de acordo com o modo enviado
     *
     * @param int $mode modo de envio da lista
     * @param array $emails lista de emails do lote
     * @return array
     */
    private function filterMailMode($mode, $emails)
    {
        return $this->mode == $mode ? $emails : array();
    }

    /**
     * Retorna um array com os modos de envio disponíveis
     *
     * @return array
     */
    public static function getModes()
    {
        return array(
            self::MODE_SINGLE,
            self::MODE_CC,
            self::MODE_BCC
        );
    }

    /**
     * Iniciamos um novo processo enviando um comando para que o laravel execute
     * nosso comando de envio de emails.
     *
     * @param string $argument
     * @throws \InvalidArgumentException
     * @return void
     */
    public function execute($argument)
    {
        if (!in_array($argument, array('send', 'repair'))) {
            throw new \InvalidArgumentException('invalid command argument');
        }

        switch ($argument) {
            case 'send':
                $option = sprintf(' --queue=%d', $this->id);
                break;
            default:
                $option = '';
        }

        $process = new Process(sprintf('php %s/artisan jcs:mail'. $option .' %s', base_path(), $argument));
        $process->start();
    }
}

class QueueAlreadyWorkingException extends \RuntimeException {};
class QueueEmptyException extends \RuntimeException {};
class QueueWithErrorException extends \RuntimeException {};