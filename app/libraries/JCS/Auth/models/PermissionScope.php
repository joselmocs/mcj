<?php

namespace JCS\Auth\Models;


class PermissionScope extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_permission_scope';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('name');

    /**
     * Roles
     *
     * @return object
     */
    public function permissions()
    {
        return $this->hasMany('JCS\Auth\Models\Permission', 'scope_id');
    }
}