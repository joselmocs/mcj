<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'DashboardController@index');

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => '/user'), function() {
    Route::get('login', 'UserController@get_login');
    Route::get('logout', 'UserController@logout');

    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('login', 'UserController@post_login');
    });
});

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/

Route::group(array('before' => 'auth'), function() {
    Route::get('dashboard', 'DashboardController@index');
});


/*
|--------------------------------------------------------------------------
| Client Support Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => '/support'), function() {
    Route::any('/', 'SupportController@index');
    Route::any('request', 'SupportController@request');
    Route::get('live/{hid}', 'SupportController@live');
    Route::post('message/{hid}', 'SupportController@message');
    Route::get('history/{cid}', 'SupportController@history');
    Route::post('endcall', 'SupportController@endcall');
    Route::post('feedback', 'SupportController@feedback');
    Route::post('alive', 'SupportController@alive');
});


/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => '/admin', 'before' => 'auth|can:admin'), function() {
    Route::get('/', 'BackendController@overview');

    // Usuários
    Route::get('user', 'BackendUserController@index');
    Route::get('user/view/{username}', 'BackendUserController@view');

    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('user/create', 'BackendUserController@create');
        Route::post('user/edit', 'BackendUserController@edit');
        Route::post('user/password', 'BackendUserController@password');
        Route::post('user/group', 'BackendUserController@group');
    });

    // Grupos
    Route::get('group', 'BackendGroupController@index');
    Route::get('group/view/{group}', 'BackendGroupController@view');

    Route::get('permission', 'BackendPermissionController@index');

    // Permissões de Grupo
    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('permission/{group}/set', 'BackendPermissionController@permission_set');
        Route::get('permission/{group}/unset/{permission}', 'BackendPermissionController@permission_unset');
    });

    // Sistema
    Route::get('system/general', 'BackendSystemController@general');
    Route::group(array('before' => 'csrf'), function() {
        Route::post('system/general', 'BackendSystemController@general');
    });

    // Chamados
    Route::get('support/subject', 'BackendSacSupportController@subjects');
    Route::group(array('before' => 'csrf'), function() {
        Route::post('support/subject/create', 'BackendSacSupportController@subject_create');
        Route::post('support/subject/edit/{subject}', 'BackendSacSupportController@subject_edit');
        Route::get('support/subject/remove/{subject}', 'BackendSacSupportController@subject_remove');
        Route::post('support/subject/save', 'BackendSacSupportController@subject_save');
    });

    // E-Mail
    Route::get('mail/send', 'BackendMailController@send');
    Route::get('mail/queue', 'BackendMailController@queue');
    Route::get('mail/server', 'BackendMailController@server');
    Route::get('mail/server/create', 'BackendMailController@create');

    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('mail/send', 'BackendMailController@send');
        Route::get('mail/queue/repair', 'BackendMailController@repair');
        Route::get('mail/server/connection', 'BackendMailController@connection');
        Route::post('mail/server/create', 'BackendMailController@create');
        Route::any('mail/server/test/{server}', 'BackendMailController@test');
        Route::any('mail/server/edit/{server}', 'BackendMailController@edit');
        Route::any('mail/server/delete/{server}', 'BackendMailController@delete');
    });

    // Status de negociação
    Route::get('client/incoming_status', 'BackendClientController@incoming_status');
    Route::group(array('before' => 'csrf'), function() {
        Route::post('client/incoming_status/create', 'BackendClientController@incoming_status_create');
        Route::post('client/incoming_status/edit/{incoming_status}', 'BackendClientController@incoming_status_edit');
        Route::get('client/incoming_status/remove/{incoming_status}', 'BackendClientController@incoming_status_remove');
        Route::post('client/incoming_status/save', 'BackendClientController@incoming_status_save');
    });

    // Departamentos
    Route::get('client/department', 'BackendClientController@departments');
    Route::group(array('before' => 'csrf'), function() {
        Route::post('client/department/create', 'BackendClientController@department_create');
        Route::post('client/department/edit/{department}', 'BackendClientController@department_edit');
        Route::get('client/department/remove/{department}', 'BackendClientController@department_remove');
        Route::post('client/department/save', 'BackendClientController@department_save');
    });

    // Grupos de EMail
    Route::get('client/mailgroup', 'BackendClientController@mailgroups');
    Route::group(array('before' => 'csrf'), function() {
        Route::post('client/mailgroup/create', 'BackendClientController@mailgroup_create');
        Route::post('client/mailgroup/edit/{mailgroup}', 'BackendClientController@mailgroup_edit');
        Route::get('client/mailgroup/remove/{mailgroup}', 'BackendClientController@mailgroup_remove');
        Route::post('client/mailgroup/save', 'BackendClientController@mailgroup_save');
    });
});

/*
|--------------------------------------------------------------------------
| Changelog Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => '/changelog'), function() {
    Route::get('client/{cod}/{cnpj}', 'ChangelogController@client_index');
    Route::get('client/{cod}/{cnpj}/{changelog}', 'ChangelogController@client_view');
});

Route::group(array('prefix' => '/changelog', 'before' => 'auth|can:changelog.item_view'), function() {
    Route::get('/', 'ChangelogController@index');
    Route::get('view/{os}', 'ChangelogController@view');

    Route::group(array('before' => 'can:changelog.item_edit'), function() {
        Route::any('upload', 'ChangelogController@upload');
    });

    Route::group(array('before' => 'csrf'), function() {

        Route::group(array('prefix' => 'client_access', 'can:changelog.access_permission'), function() {
            Route::post('create/{changelog}', 'ChangelogController@client_access_create');
            Route::get('remove/{changelog}', 'ChangelogController@client_access_remove');
            Route::get('empty/{changelog}', 'ChangelogController@client_access_empty');
            Route::get('free/{changelog}', 'ChangelogController@client_access_free');
            Route::get('close/{changelog}', 'ChangelogController@client_access_close');
        });

        Route::group(array('prefix' => 'module_access', 'can:changelog.access_permission'), function() {
            Route::post('edit/{changelog}', 'ChangelogController@module_access_edit');
        });

        Route::group(array('before' => 'can:changelog.item_create'), function() {
            Route::get('create/{os}', 'ChangelogController@create');
            Route::get('glossary/{changelog}', 'ChangelogController@glossary');
        });

        Route::group(array('before' => 'can:changelog.item_edit'), function() {
            Route::post('edit/inline/{cv}', 'ChangelogController@inlineEdit');
        });
    });
});

/*
|--------------------------------------------------------------------------
| Glossario Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => '/glossary'), function() {
    Route::get('client/{cod}/{cnpj}', 'GlossaryController@client_index');
    Route::get('client/{cod}/{cnpj}/{glossary}', 'GlossaryController@client_view');
});

Route::group(array('prefix' => '/glossary', 'before' => 'auth|can:glossary.view'), function() {
    Route::get('/', 'GlossaryController@index');

    Route::group(array('before' => 'can:report.os'), function() {
        Route::get('new', 'GlossaryController@news');
    });

    Route::get('view/{id}', 'GlossaryController@view');
    Route::any('upload', 'GlossaryController@upload');

    // Permissão: glossary.item_create
    Route::group(array('before' => 'csrf|can:glossary.item_create'), function() {
        Route::post('create', 'GlossaryController@create');
    });

    // Permissão: glossary.item_remove
    Route::group(array('before' => 'csrf|can:glossary.item_remove'), function() {
        Route::get('remove/{id}', 'GlossaryController@remove');
    });

    Route::group(array('before' => 'csrf'), function() {

        Route::group(array('prefix' => 'client_access', 'can:glossary.access_permission'), function() {
            Route::post('create/{glossary}', 'GlossaryController@client_access_create');
            Route::get('remove/{glossary}', 'GlossaryController@client_access_remove');
            Route::get('empty/{glossary}', 'GlossaryController@client_access_empty');
            Route::get('free/{glossary}', 'GlossaryController@client_access_free');
            Route::get('close/{glossary}', 'GlossaryController@client_access_close');
        });

        Route::group(array('before' => 'can:glossary.item_edit'), function() {
            Route::post('edit/inline/{glossary}', 'GlossaryController@inlineEdit');
        });

        Route::group(array('prefix' => 'module_access', 'can:glossary.access_permission'), function() {
            Route::post('edit/{glossary}', 'GlossaryController@module_access_edit');
        });
    });
});


/*
|--------------------------------------------------------------------------
| SAC Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => '/sac', 'before' => 'auth'), function() {

    // Página principal do SAC
    Route::get('/', 'SacController@index');

    // Ordem de Serviço
    Route::group(array('prefix' => 'os', 'before' => 'can:os.item_view'), function() {
        Route::get('/', 'SacOSController@index');

        Route::group(array('before' => 'can:report.os'), function() {
            Route::get('documented', 'SacOSController@documented');
            Route::get('undocumented', 'SacOSController@undocumented');
        });

        Route::get('view/{os}', 'SacOSController@view');

        Route::group(array('before' => 'csrf|can:os.item_edit'), function() {
            Route::post('edit/inline/{os}', 'SacOSController@inlineEdit');
        });
    });

    // Clientes
    Route::group(array('prefix' => 'client', 'before' => 'can:client.view'), function() {
        Route::get('/', 'SacClientController@index');

        Route::get('view/{client}', 'SacClientController@view');

        Route::group(array('before' => 'csrf'), function() {
            Route::post('create', 'SacClientController@create');
        });

        Route::group(array('before' => 'csrf|can:client.edit'), function() {
            Route::post('edit/address', 'SacClientController@addressEdit');
            Route::post('edit/inline/{client}', 'SacClientController@inlineEdit');
        });

        Route::group(array('before' => 'csrf|can:client.sendmail'), function() {
            Route::any('mail/{client}', 'SacClientController@mail');
        });

        // Negociações
        Route::group(array('prefix' => 'incoming'), function() {

            Route::group(array('before' => 'can:incoming.client'), function() {
                Route::get('', 'SacClientController@incoming');
                Route::get('contacts_done', 'SacClientController@contacts_done');
                Route::get('contacts_pending', 'SacClientController@contacts_pending');
                Route::get('contacts_next', 'SacClientController@contacts_next');
            });

            Route::group(array('before' => 'csrf|can:incoming.view'), function() {
                Route::get('view/{client}', 'SacClientController@incoming_view');
            });

            Route::group(array('before' => 'csrf|can:incoming.mail'), function() {
                Route::post('contact_mail', 'SacClientController@contact_mail');
            });

            Route::group(array('before' => 'csrf|can:incoming.text'), function() {
                Route::get('contact', 'SacClientController@incoming_contact');
            });

            Route::group(array('before' => 'csrf|can:incoming.manage'), function() {
                Route::post('create', 'SacClientController@incoming_create');
                Route::post('start', 'SacClientController@incoming_start');
                Route::post('inline/{incoming}', 'SacClientController@incoming_inline');
            });
        });


        Route::group(array('prefix' => 'jsonp', 'before' => 'csrf'), function() {
            Route::get('find', 'SacClientController@find');
        });

        // Módulos
        Route::group(array('prefix' => 'module', 'before' => 'csrf|can:client.manage_modules'), function() {
            Route::post('edit/{client}', 'SacClientController@module_edit');
        });

        // Contatos
        Route::group(array('prefix' => 'contact', 'before' => 'csrf'), function() {
            Route::group(array('before' => 'can:contact.create'), function() {
                Route::post('create', 'SacClientController@contact_create');
            });

            Route::group(array('before' => 'can:contact.remove'), function() {
                Route::get('remove/{contact}', 'SacClientController@contact_remove');
            });

            Route::group(array('before' => 'can:contact.edit'), function() {
                Route::post('edit/{contact}', 'SacClientController@contact_edit');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Support Routes
    |--------------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'support'), function() {

        Route::post('talks', 'SacSupportController@talks');

        Route::group(array('before' => 'auth|can:support.monitor_view'), function() {
            Route::get('/', 'SacSupportController@index');
            Route::get('history', 'SacSupportController@history');
            Route::get('history/{cid}', 'SacSupportController@history_view');
            Route::post('monitor', 'SacSupportController@monitor');
            Route::any('chats', 'SacSupportController@chats');

            Route::post('reply', 'SacSupportController@reply');

            Route::group(array('before' => 'can:support.answer_call'), function() {
                Route::get('answer', 'SacSupportController@answer');
            });

            Route::post('commands', 'SacSupportController@commands');
        });

    });
});