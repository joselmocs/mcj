<?php

class DashboardController extends BaseController {

    /**
     * Exibe o dashboard
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        if (!Auth::check()) {
            return Redirect::action("UserController@get_login");
        }

        if (Auth::check() && Auth::user()->is('user')) {
            return Redirect::action("GlossaryController@index");
        }

        return $this->view_make('dashboard/dashboard');
    }

    /**
     * Retorna a quantidade de contatos que não foram retornados para o cliente
     *
     * @return int
     */
    public static function contatos_pendentes_ate_a_data()
    {
        $all_incomings = SacClient::where('is_incoming', '=', true)->get();

        $total = 0;
        foreach($all_incomings as $cliente)
        {
            if ($cliente->is_incoming) {
                try {
                    $ultimo_contato = $cliente->incoming->contacts()->orderBy('date_contact', 'desc')->limit(1)->firstOrFail();
                    if ($ultimo_contato->next_contact <= Carbon::now()) {
                        $total++;
                    }
                }
                catch(Exception $e) {
                    //
                }
            }
        }
        return $total;
    }

    /**
     * Retorna a quantidade de contatos previstos para os próximos 7 dias
     *
     * @return int
     */
    public static function contatos_previstos_semana()
    {
        $all_incomings = SacClient::where('is_incoming', '=', true)->get();

        $total = 0;
        foreach($all_incomings as $cliente)
        {
            if ($cliente->is_incoming) {
                try {
                    $ultimo_contato = $cliente->incoming->contacts()
                        ->orderBy('date_contact', 'desc')
                        ->limit(1)
                        ->firstOrFail();
                    if ($ultimo_contato->next_contact >= Carbon::now()->startOfDay() && $ultimo_contato->next_contact <= Carbon::now()->addDays(7)->endOfDay()) {
                        $total++;
                    }
                }
                catch(Exception $e) {
                    //
                }
            }
        }
        return $total;
    }

    /**
     * Retorna a quantidade de contatos realizados no mês corrente
     *
     * @return int
     */
    public static function contatos_do_mes()
    {
        return SacClientIncomingContact::whereBetween('date_contact', array(Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()))
            ->count();
    }

    /**
     * Retorna a quantidade de contatos realizados no dia
     *
     * @return int
     */
    public static function contatos_realizados_hoje()
    {
        return SacClientIncomingContact::whereBetween('date_contact', array(Carbon::now()->startOfDay(), Carbon::now()->endOfDay()))
            ->count();
    }

    /**
     * Retorna a quantidade de contatos realizados no mês anterior
     *
     * @return int
     */
    public static function contatos_do_mes_anterior()
    {
        return SacClientIncomingContact::whereBetween('date_contact', array(Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth()))
            ->count();
    }
}