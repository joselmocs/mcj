<?php


class ChangelogController extends BaseController {

    /**
     * Define o módulo ativo
     *
     * @return string
     */
    public $module = Module::CHANGELOG;

    /**
     * Exibe a página de listagem de changelogs
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'filters' => (object) array(
                'os' => Input::get('os', ''),
                'tags' => Input::get('tags', '')
            )
        ));

        $result = ChangeLog::select('changelog.*')
            ->join('sac_os', 'sac_os.id', '=', 'changelog.os_id');

        if (Input::get('os')) {
            $result->where('sac_os.cod', '=', Input::get('os'));
        }

        if (Input::get('search')) {
            $result->where('changelog.summary', 'like', '%'. Input::get('search') .'%');
        }

        $this->set_context(array(
            'changelogs' => $result->orderBy('created_at', 'desc')->paginate(50)
        ));

        return $this->view_make('changelog/index');
    }

    /**
     * Página de visualização do Changelog
     *
     * @param $os
     * @return Illuminate\Support\Facades\View
     */
    public function view($os)
    {
        $previous = null;
        // Armazenamos a url anterior apenas estiver vindo da página de listagem
        if (strpos(URL::previous(), URL::action('ChangelogController@index')) !== false) {
            $previous = URL::previous();
        }

        $changelog = ChangeLog::find($os);
        $this->set_context(array(
            'changelog' => $changelog,
            'previous' => $previous
        ));

        $this->set_json_context(array(
            'clientFind' => URL::action('SacClientController@find'),
            'completed' => array(
                'description' => !empty($changelog->description),
                'result' => !empty($changelog->result),
                'action' => !empty($changelog->action)
            ),
            'changelog' => array(
                'meses' => $changelog->monthToArray()
            )
        ));

        return $this->view_make('changelog/view');
    }

    /**
     * Cria um conteúdo de versão para uma OS
     *
     * @param integer $os
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create($os)
    {
        $os = SacOS::find($os);

        // Se o conteúdo de versão já existir redirecionamos para a página do mesmo
        if ($os->changelog) {
            return Redirect::action("ChangelogController@view", array($os->changelog->id));
        }

        $fields = array();
        if (empty($os->summary)) {
            array_push($fields, 'summary');
        }

        if (!$os->module_id) {
            array_push($fields, 'module');
        }

        if ($os->type_id == 1) {
            array_push($fields, 'type');
        }

        if ($fields) {
            return $this->headerMessage('error', 'É obrigatório o preenchimento dos campos destacados em vermelho '.
                'para que seja possível a criação do Conteúdo de Versão desta OS.', $fields);
        }

        $cl = new ChangeLog();
        $cl->summary = $os->summary;
        $cl->os_id = $os->id;
        $cl->released = Carbon::now();
        $cl->save();

        return Redirect::action("ChangelogController@view", array($cl->id));
    }

    /**
     * Retorna para a página anterior com a mensagem de erro informada
     *
     * @param string $type
     * @param string $message
     * @param array $fields
     * @return \Illuminate\Http\RedirectResponse
     */
    private function headerMessage($type, $message, array $fields)
    {
        return Redirect::back()->with("headerMessage", (object) array(
            'type' => $type,
            'message' => $message
        ))->with('errors', $fields);
    }

    /**
     * Edita o Conteúdo de Versão
     *
     * @param integer $cv
     * @return \Illuminate\Http\JsonResponse
     */
    public function inlineEdit($cv)
    {
        $cv = ChangeLog::where('id', '=', $cv)->firstOrFail();

        $field = Input::get('field');
        $value = Input::get('value');

        if ($field == "summary-val" && $this->isValidInput($value)) {
            $cv->summary = $value;
        }

        if ($field == "version-val") {
            $cv->version = $value;
        }

        if ($field == "released-val") {
            $dt = explode('/', $value);
            $cv->released = Carbon::create($dt[2], $dt[1], $dt[0]);
        }

        if ($field == "month-val" && (strlen($value) == 2)) {
            $cv->launch = $value . substr($cv->launch, 2, 4);
        }

        if ($field == "year-val" && (strlen($value) == 4)) {
            $cv->launch = substr($cv->launch, 0, 2) . $value;
        }

        if ($field == "description-val") {
            $cv->description = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        if ($field == "result-val") {
            $cv->result = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        if ($field == "action-val") {
            $cv->action = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        if ($field == "observation-val") {
            $cv->observation = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        $cv->save();

        return Response::json(array(
            'success' => true
        ));
    }

    /**
     * @param integer $changelog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function glossary($changelog)
    {
        $changelog = ChangeLog::find($changelog);

        if ($changelog) {
            // ID do usuário autenticado
            $user_id = Auth::user()->id;

            $glossary = new Glossary;
            $glossary->user_id = $user_id;
            $glossary->module_id = $changelog->os->module->id;
            $glossary->function = $changelog->summary;
            $glossary->description = $changelog->description;
            $glossary->result = $changelog->result;
            $glossary->action = $changelog->action;
            $glossary->observation = $changelog->observation;
            $glossary->save();

            $changelog->glossary_id = $glossary->id;
            $changelog->user_id = Auth::user()->id;
            $changelog->save();

            GlossaryController::activity($glossary, "criou");
        }

        return Redirect::action("ChangelogController@view", array($changelog->id));
    }

    /**
     * Dá permissão de acesso ao cliente para que ele possa visualizar este changelog
     *
     * @param integer $changelog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_create($changelog)
    {
        $changelog = ChangeLog::find($changelog);

        if (Input::get('client'))
        {
            $client = SacClient::find(Input::get('client'));

            $access = ChangeLogClientAccess::where('changelog_id', '=', $changelog->id)
                ->where('client_id', '=', $client->id)
                ->count();

            if (!$access) {
                $access = new ChangeLogClientAccess;
                $access->changelog_id = $changelog->id;
                $access->client_id = $client->id;
                $access->save();
            }
        }

        return Redirect::action("ChangelogController@view", array($changelog->id));
    }

    /**
     * Retira a permissão de acesso de todos os clientes deste changelog
     *
     * @param integer $changelog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_empty($changelog)
    {
        $changelog = ChangeLog::find($changelog);
        $changelog->clientAccess()->delete();

        return Redirect::back();
    }

    /**
     * Retira a permissão de acesso de um cliente deste changelog
     *
     * @param integer $changelog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_remove($changelog)
    {
        $access = ChangeLogClientAccess::where('changelog_id', '=', $changelog)
            ->where('client_id', '=', Input::get('client'));

        if ($access->count()) {
            $access->delete();
        }

        return Redirect::back();
    }

    /**
     * Libera o acesso do conteúdo de versão para todos os clientes
     *
     * @param integer $changelog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_free($changelog)
    {
        $changelog = ChangeLog::find($changelog);

        if ($changelog) {
            ChangeLogClientAccess::where('changelog_id', '=', $changelog->id)->delete();
            $changelog->free_access = true;
            $changelog->save();
        }

        return Redirect::back();
    }

    /**
     * Limita o acesso do conteúdo de versão para apenas clientes selecionados
     *
     * @param integer $changelog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_close($changelog)
    {
        $changelog = ChangeLog::find($changelog);

        if ($changelog) {
            $changelog->free_access = false;
            $changelog->save();
        }

        return Redirect::back();
    }

    /**
     * Exibe a página de listagem de changelogs liberadas para determinado cliente
     *
     * @param int $cod
     * @param string $cnpj
     * @return Illuminate\Support\Facades\View
     */
    public function client_index($cod, $cnpj)
    {
        $reverte = function($string) {
            return strtr($string, 'BRASILENHO', '0123456789');
        };

        $client = SacClient::where('cod', '=', $reverte($cod))
            ->where('cgc', '=', $reverte($cnpj))
            ->firstOrFail();

        $this->set_context(array(
            'client' => $client
        ));

        $this->set_context(array(
            'changelogs' => $client->changelogs()
        ));

        return $this->view_make('changelog/client/index');
    }

    /**
     * Exibe a página de visualização do conteúdo de versão para o cliente.
     *
     * @param int $cod
     * @param int $cnpj
     * @param int $changelog
     * @return \Illuminate\View\View
     */
    public function client_view($cod, $cnpj, $changelog)
    {
        $reverte = function($string) {
            return strtr($string, 'BRASILENHO', '0123456789');
        };

        $client = SacClient::where('cod', '=', $reverte($cod))
            ->where('cgc', '=', $reverte($cnpj))
            ->firstOrFail();
        $changelog = ChangeLog::where('id', '=', $changelog)->firstOrFail();

        if (!$client->canAccessChangelog($changelog)) {
            return Redirect::back();
        }

        $this->set_context(array(
            'client' => $client,
            'changelog' => $changelog
        ));

        return $this->view_make('changelog/client/view');
    }

    /**
     * Página de edição de acessos por módulo
     *
     * @param integer $changelog
     * @return Illuminate\Support\Facades\View
     */
    public function module_access_edit($changelog)
    {
        $changelog = ChangeLog::where('id', '=', $changelog)->firstOrFail();

        if (!Input::has('submit')) {
            $this->set_context(array(
                'changelog' => $changelog,
                'modules' => SacModule::all()
            ));

            return $this->view_make('changelog/access/module');
        }
        $changelog->moduleAccess()->delete();

        foreach(Input::get('modules', array()) as $module_id)
        {
            if (SacModule::where('id', '=', $module_id)->get())
            {
                $module = new ChangeLogModuleAccess;
                $module->changelog_id = $changelog->id;
                $module->module_id = $module_id;
                $module->save();
            }
        }

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                    'ChangelogController@view',
                    array($changelog->id)
                )
        ));
    }

    /**
     * Verifica se o valor informado é vazio: empty, false, array(), NULL ou 0.
     *
     * @param mixed $value
     * @return bool
     */
    private function isValidInput($value)
    {
        if (!empty($value)) {
            return true;
        }
        return false;
    }

    /**
     * Faz o upload de alguma imagem
     *
     * @return Illuminate\Support\Facades\View
     */
    public function upload()
    {
        $file = Input::file('file');
        $foldername = str_random(8);

        $destinationPath = public_path('upload/changelog/'. $foldername .'/');
        $filename = $file->getClientOriginalName();

        $file->move($destinationPath, $filename);

        return Response::json(array(
            "filelink" => '/upload/changelog/' . $foldername .'/'. $filename
        ));
    }
}