<?php

use JCS\Hashids\Hashids;


class SupportController extends BaseController {

    /**
     * Exibe a página inicial de suporte ou a página de login caso usuário não
     * esteja autenticado.
     */
    public function index()
    {
        if (Auth::check()) {
            return Redirect::action('SupportController@request');
        }

        $this->set_context(array(
            'forms' => (object) array(
                'username' => Input::get('username', '')
            )
        ));

        if (Request::isMethod('post'))
        {
            try {
                Auth::attempt(array(
                    'identifier' => Input::get('username'),
                    'password' => Input::get('password')
                ));

                return Redirect::back();
            }
            catch (Exception $e) {
                $this->set_context(array(
                    'form_errors' => array("Falha ao efetuar a autenticação.")
                ));
            }
        }

        return $this->view_make('support/login');
    }

    /**
     * Exibe a pagina inicial da tela de suporte
     *
     * @return Illuminate\Support\Facades\View
     */
    public function request()
    {
        if (!Auth::check()) {
            return Redirect::action('SupportController@index');
        }

        $in_progress = SacSupport::where('client_id', '=', Auth::user()->id)
            ->where('status', '!=', SacSupport::FINALIZADO)
            ->get();

        $this->set_context(array(
            'forms' => (object) array(
                'description' => Input::get('description', '')
            ),
            'supports' => SacSupport::where('client_id', '=', Auth::user()->id)
                    ->where('status', '!=', SacSupport::EM_ANDAMENTO)
                    ->orderBy('created_at', 'desc')
                    ->get(),
            'in_progress' => $in_progress,
            'subjects' => SacSupportSubject::all()
        ));

        if (!Input::has('submit') || count($in_progress)) {
            return $this->view_make('support/request');
        }

        $validate = array(
            "type" => "required",
            "description" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('support/request');
        }

        $user = Auth::user();

        $support = new SacSupport;
        $support->client_id = $user->id;
        $support->type = Input::get('type');
        $support->status = SacSupport::AGUARDANDO_ATENTENTE;
        $support->description = Input::get('description');
        $support->save();

        $supMessage = new SacSupportMessage;
        $supMessage->support_id = $support->id;
        $supMessage->sender_id = $user->id;
        $supMessage->message = Input::get('description', '');
        $supMessage->save();

        $supAct = new SacSupportActivitie;
        $supAct->support_id = $support->id;
        $supAct->user_id = $user->id;
        $supAct->alive = Carbon::now();
        $supAct->last_message = Carbon::now();
        $supAct->save();

        return Redirect::action('SupportController@live', array($support->hash_id()));
    }

    /**
     * Controller responsável pela exibição da página de conversação do cliente
     *
     * @param string $sid
     * @return \Illuminate\View\View
     */
    public function live($sid)
    {
        if (!Auth::check()) {
            return Redirect::action('SupportController@index');
        }

        $hid = new Hashids;
        $support = SacSupport::where('id', '=', $hid->decrypt($sid)[0])
            ->firstOrFail();

        if (!$support->operator_id) {
            return $this->view_make('support/waiting');
        }

        if ($support->status != SacSupport::EM_ANDAMENTO) {
            return Redirect::action('SupportController@request');
        }

        $this->set_context(array(
            'sid' => $sid,
            'support' => $support
        ));

        $this->set_json_context(array(
            'active_cid' => $support->hash_id(),
            'urls' => array(
                'endcall' => URL::action('SupportController@endcall'),
                'talks' => URL::action('SacSupportController@talks'),
                'answer' => URL::action('SupportController@message', array($support->hash_id())),
            )
        ));

        return $this->view_make('support/chat');
    }

    /**
     * Registra uma mensagem na conversa
     *
     * @param string $sid
     * @return \Illuminate\Http\RedirectResponse
     */
    public function message($sid)
    {
        $user = Auth::user();
        $support = SacSupportController::_support($sid);

        $supMessage = new SacSupportMessage;
        $supMessage->support_id = $support->id;
        $supMessage->sender_id = $user->id;
        $supMessage->message = Input::get('message', '');
        $supMessage->save();

        SacSupportActivitie::where('support_id', '=', $support->id)
            ->where('user_id', '=', $user->id)
            ->update(array('last_message' => Carbon::now()));

        return Redirect::back();
    }

    /**
     * Finaliza o chamado.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function endcall()
    {
        return SacSupportController::endcall(Input::get('active_cid'));
    }

    /**
     * Exibe o histórico do chamado para o cliente
     *
     * @param string $cid
     * @return \Illuminate\View\View
     */
    public function history($cid)
    {
        if (!Auth::check()) {
            return Redirect::action('SupportController@index');
        }

        $support = SacSupportController::_support($cid);

        $this->set_context(array(
            'support' => $support
        ));

        return $this->view_make('support/history');
    }

    /**
     * Formulário para avaliação do chamado.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function feedback()
    {
        $this->set_context(array(
            'rating' => Input::get('rating', 2),
            'observation' => Input::get('observation', ''),
            'cid' => Input::get('cid')
        ));

        if (!Input::get('submit')) {
            return $this->view_make('support/feedback');
        }

        $input = Input::all();
        $validate = array(
            'rating' => 'required|integer'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('support/feedback');
        }

        $cid = new Hashids;
        $feed = new SacSupportFeedback;
        $feed->support_id = $cid->decrypt(Input::get('cid'))[0];
        $feed->rating = Input::get('rating', 2);
        $feed->observation = Input::get('observation');
        $feed->save();

        return Response::json(array(
            'success' => true
        ));
    }
}