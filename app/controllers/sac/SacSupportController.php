<?php

use JCS\Hashids\Hashids;


class SacSupportController extends BaseController {

    /**
     * Define o módulo ativo
     *
     * @return string
     */
    public $module = Module::SAC;

    /**
     * Página principal do módulo de suporte onde contém o monitor e as conversas.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $this->set_json_context(array(
            'urls' => array(
                'monitor' => URL::action('SacSupportController@monitor'),
                'active_chats' => URL::action('SacSupportController@chats'),
                'reply_support' => URL::action('SacSupportController@reply'),
                'talks' => URL::action('SacSupportController@talks'),
                'answer' => URL::action('SacSupportController@answer'),
                'commands' => URL::action('SacSupportController@commands')
            ),
            'user_id' => Auth::user()->id
        ));

        return $this->view_make('sac/support/index');
    }

    public function reply()
    {
        $user = Auth::user();
        $support = self::_support(Input::get('hid'));

        $supMessage = new SacSupportMessage;
        $supMessage->support_id = $support->id;
        $supMessage->sender_id = $user->id;
        $supMessage->message = Input::get('message', '');
        $supMessage->save();

        SacSupportActivitie::where('support_id', '=', $support->id)
            ->where('user_id', '=', $user->id)
            ->update(array('last_message' => Carbon::now()));
    }

    public function chats()
    {
        $get_chats = Input::get('active', array());
        if (!is_array($get_chats)) {
            $get_chats = json_decode($get_chats);
        }

        $chats = array();
        $active_chats = array();

        $supports = SacSupport::where('status', '=', SacSupport::EM_ANDAMENTO);
        if (!Auth::user()->can('support.audit_call')) {
            $supports->where('operator_id', '=', Auth::user()->id);
        }

        foreach($supports->get() as $support)
        {
            if ($support->status == SacSupport::FINALIZADO) {
                continue;
            }

            array_push($active_chats, $support->hash_id());

            if (!in_array($support->hash_id(), $get_chats)) {
                array_push($chats, array(
                    'id' => $support->hash_id(),
                    'operator_id' => $support->operator_id,
                    'operator_name' => $support->operator->profile->fullname,
                    'client' => $support->client->profile->fullname
                ));
            }
        }

        return Response::json(array(
            'chats' => $chats,
            'active' => json_encode($active_chats),
            'deactive' => array_values(array_diff($get_chats, $active_chats))
        ));
    }

    /**
     * Retorna a conversa dos chamados que ainda não foram exibidos no chat.
     *
     */
    public function talks()
    {
        $messages = array();

        $talks = Input::get('talks', array());
        if (!is_array($talks)) {
            $talks = json_decode($talks);
        }

        $user = Auth::user();

        foreach($talks as $talk) {
            $sup = self::_support($talk[0]);
            $msgs = SacSupportMessage::where('support_id', '=', $sup->id);

            if ($talk[1]) {
                $msgs->where('id', '>', $talk[1]);
            }

            $messages[$talk[0]] = array();
            foreach($msgs->get() as $msg) {

                if ($msg->private && $user->id != $sup->operator_id) {
                    continue;
                }

                if ($msg->sender) {
                    $sender = $msg->sender->profile->fullname;
                    $sender_id = $msg->sender->id;
                }
                else {
                    $sender = "Sistema";
                    $sender_id = 0;
                }

                array_push($messages[$talk[0]], array(
                    'id' => $msg->id,
                    'name' => $sender,
                    'message' => $msg->message,
                    'author' => $sender_id,
                    'time' => Carbon::parse($msg->created_at)->format('d/m/Y H:i')
                ));
            }

            if ($sup->status == SacSupport::FINALIZADO) {
                array_push($messages[$talk[0]], false);
            }
        }

        return Response::json($messages);
    }

    /**
     * Retorna um json com o monitor de atendimendo de acordo com a permissão
     * do usuário.
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function monitor()
    {
        $supports = array(
            'monitor' => array(),
            'calls' => array()
        );

        foreach(SacSupport::where('status', '=', SacSupport::AGUARDANDO_ATENTENTE)->get() as $support) {
            array_push($supports['calls'], array(
                'hash_id' => $support->hash_id(),
                'client' => $support->client->profile->fullname,
                'description' => $support->description,
                'type' => $support->type,
                'started_at' => Carbon::parse($support->created_at)->diffForHumans()
            ));
        }

        foreach(SacSupport::where('status', '=', SacSupport::EM_ANDAMENTO)->get() as $support) {
            $created_at = new Carbon($support->answered_at);
            $duration = $created_at->diffInMinutes(Carbon::now());

            array_push($supports['monitor'], array(
                'hash_id' => $support->hash_id(),
                'client' => $support->client->profile->fullname,
                'operator' => $support->operator->profile->fullname,
                'status' => SacSupport::$status[$support->status],
                'duration' => $duration .' minuto(s)'
            ));
        }

        return Response::json($supports);
    }

    public function answer()
    {
        $sup = self::_support(Input::get('id'));

        if (!$sup) {
            // chamada nao encontrada
        }

        if ($sup->operator_id) {
            // chamada ja atendida
        }

        $user = Auth::user();

        $sup->status = SacSupport::EM_ANDAMENTO;
        $sup->operator_id = $user->id;
        $sup->answered_at = Carbon::now();
        $sup->save();

        $supMessage = new SacSupportMessage;
        $supMessage->support_id = $sup->id;
        $supMessage->sender_id = $user->id;
        $supMessage->message = "Chamado atendido pelo operador ". Auth::user()->profile->fullname .".";
        $supMessage->save();

        $supAct = new SacSupportActivitie;
        $supAct->support_id = $sup->id;
        $supAct->user_id = $user->id;
        $supAct->alive = Carbon::now();
        $supAct->last_message = Carbon::now();
        $supAct->save();

        return Response::json(array(
            'success' => true,
            'cid' => Input::get('id')
        ));
    }

    /**
     * Exibe a página de historico do Suporte
     *
     * @return Illuminate\Support\Facades\View
     */
    public function history()
    {
        $this->set_context(array(
            'filters' => (object) array(
                'cod' => Input::get('cod', ''),
                'tags' => Input::get('tags', '')
            )
        ));

        $result = SacSupport::select();
        if (Input::get('cod'))
        {
            $cod = Input::get('cod');
            try {
                $hid = new Hashids;
                $cod = $hid->decrypt(Input::get('cod'))[0];
            }
            catch (ErrorException $e) {
                //
            }

            $result->where('id', '=', $cod);
        }

        if (!Auth::user()->can('support.history_view')) {
            $result->where('operator_id', '=', Auth::user()->id);
        }

        $this->set_context(array(
            'supports' => $result->where('status', '=', SacSupport::FINALIZADO)
                                 ->orderBy('created_at', 'desc')
                                 ->paginate(20)
        ));

        return $this->view_make('sac/support/history/index');
    }

    /**
     * Exibe a página com o histórico
     *
     * @param string $cid
     * @return \Illuminate\View\View
     */
    public function history_view($cid)
    {
        $support = self::_support($cid);

        $this->set_context(array(
            'support' => $support
        ));

        return $this->view_make('sac/support/history/view');
    }

    public function commands()
    {
        switch (Input::get('command')) {
            case 'endcall':
                return self::endcall(Input::get('active_cid'));
            case 'alive':
                return self::alive();
        }

        return null;
    }

    /**
     * Executa várias checagens do chamado, como tempo de inatividade, chamado fechado, etc.
     */
    public function alive()
    {
        $inactive_sec_client = 5;

        $user = Auth::user();
        $cids = json_decode(Input::get('active_cids', "[]"));

        $check = array();

        foreach($cids as $cid) {
            $check[$cid] = array();
            $support = self::_support($cid);

            SacSupportActivitie::where('support_id', '=', $support->id)
                ->where('user_id', '=', $user->id)
                ->update(array('alive' => Carbon::now()));

            $client_activitie = SacSupportActivitie::where('support_id', '=', $support->id)
                ->where('user_id', '=', $support->client_id)
                ->firstOrFail();

            $last_message = new Carbon($client_activitie->last_message);
            $diff_in_secs = $last_message->diffInSeconds();

            if ($diff_in_secs > $inactive_sec_client) {
                $supMessage = new SacSupportMessage;
                $supMessage->support_id = $support->id;
                $supMessage->private = true;
                $supMessage->message = "O cliente está inativo a ". $diff_in_secs ." segundos.";
                $supMessage->save();
            }
        }

        return Response::json(array('teste' => $check));
    }

    /**
     * Retorna o chamado correspondente ao código hash enviado
     *
     * @param string $cid
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function _support($cid)
    {
        $hid = new Hashids;
        return SacSupport::where('id', '=', $hid->decrypt($cid)[0])->firstOrFail();
    }

    /**
     * Finaliza um chamado
     *
     * @param string $cid
     * @return \Illuminate\Http\JsonResponse
     */
    public static function endcall($cid)
    {
        $support = self::_support($cid);
        $user = Auth::user();

        if ($support->operator_id == $user->id) {
            $message = "Chamado finalizado pelo operador ". $user->username ." (". $user->id .").";
        }
        else if ($support->client_id == $user->id) {
            $message = "Chamado finalizado pelo usuário ". $user->username ." (". $user->id .").";
        }

        $support->status = SacSupport::FINALIZADO;
        $support->ended_at = Carbon::now();
        $support->save();

        $supMessage = new SacSupportMessage;
        $supMessage->support_id = $support->id;
        $supMessage->message = $message;
        $supMessage->save();

        SacSupportActivitie::where('support_id', '=', $support->id)->delete();

        return Response::json(array('success' => true));
    }
}