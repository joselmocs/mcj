<?php

class SacController extends BaseController {

    /**
     * Define o módulo ativo
     *
     * @return string
     */
    public $module = Module::SAC;

    /**
     * Exibe a página principal do SAC
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        return $this->view_make('sac/index');
    }
}