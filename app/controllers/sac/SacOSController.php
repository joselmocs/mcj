<?php

class SacOSController extends BaseController {

    /**
     * Define o módulo ativo
     *
     * @return string
     */
    public $module = Module::SAC;

    /**
     * Exibe a página de listagem de OS
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'filters' => (object) array(
                'cod' => Input::get('cod', ''),
                'tags' => Input::get('tags', '')
            )
        ));

        $result = SacOS::select();
        if (Input::get('cod')) {
            $result->where('sac_os.cod', '=', Input::get('cod'));
        }

        if (Input::get('tags')) {
            $result->where('sac_os.summary', 'like', '%'. Input::get('tags') .'%');
        }

        $this->set_context(array(
            'os' => $result->orderBy('created_at', 'desc')->paginate(50)
        ));

        return $this->view_make('sac/os/index');
    }

    /**
     * Retorna a quantidade de OS documentadas
     *
     * @param boolean $current_month
     * @return integer
     */
    public static function documented_count($current_month = true)
    {
        $count = 0;

        if ($current_month) {
            $start = Carbon::now();
            $end = Carbon::now();
        }
        else {
            $start = Carbon::now()->subMonth();
            $end = Carbon::now()->subMonth();
        }

        $result = SacOS::select();
        $result->whereBetween('ended_at', array($start->startOfMonth(), $end->endOfMonth()));
        foreach($result->get() as $os) {
            if ($os->changelog) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Retorna a quantidade de OS não documentadas no mes corrente
     *
     * @param boolean $current_month
     * @return integer
     */
    public static function undocumented_count($current_month = true)
    {
        $count = 0;

        if ($current_month) {
            $start = Carbon::now();
            $end = Carbon::now();
        }
        else {
            $start = Carbon::now()->subMonth();
            $end = Carbon::now()->subMonth();
        }

        $result = SacOS::select();
        $result->whereBetween('ended_at', array($start->startOfMonth(), $end->endOfMonth()));
        foreach($result->get() as $os) {
            if (!$os->changelog) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Exibe a página de listagem de OS não documentadas no mes corrente
     *
     * @return Illuminate\Support\Facades\View
     */
    public function undocumented()
    {
        if (Input::get('de')) {
            $fde = explode('/', Input::get('de'));
            $de = Carbon::create($fde[2], $fde[1], $fde[0]);
        }
        else {
            if (Input::get('previous')) {
                $de = Carbon::now()->subMonth()->startOfMonth();
            }
            else {
                $de = Carbon::now()->startOfMonth();
            }
        }

        if (Input::get('ate')) {
            $fate = explode('/', Input::get('ate'));
            $ate = Carbon::create($fate[2], $fate[1], $fate[0]);
        }
        else {
            if (Input::get('previous')) {
                $ate = Carbon::now()->subMonth()->endOfMonth();
            }
            else {
                $ate = Carbon::now()->endOfMonth();
            }
        }

        $result = SacOS::select();
        $result->whereBetween('ended_at', array($de, $ate));

        $listOs = array();
        foreach($result->orderBy('ended_at', 'desc')->get() as $os) {
            if (!$os->changelog) {
                array_push($listOs, $os);
            }
        }

        $this->set_context(array(
            'os' => $listOs,
            'filters' => (object) array(
                'de' => $de->format('d/m/Y'),
                'ate' => $ate->format('d/m/Y')
            )
        ));

        return $this->view_make('sac/os/undocumented');
    }

    /**
     * Exibe a página de listagem de OS documentadas
     *
     * @return Illuminate\Support\Facades\View
     */
    public function documented()
    {
        if (Input::get('de')) {
            $fde = explode('/', Input::get('de'));
            $de = Carbon::create($fde[2], $fde[1], $fde[0]);
        }
        else {
            if (Input::get('previous')) {
                $de = Carbon::now()->subMonth()->startOfMonth();
            }
            else {
                $de = Carbon::now()->startOfMonth();
            }
        }

        if (Input::get('ate')) {
            $fate = explode('/', Input::get('ate'));
            $ate = Carbon::create($fate[2], $fate[1], $fate[0]);
        }
        else {
            if (Input::get('previous')) {
                $ate = Carbon::now()->subMonth()->endOfMonth();
            }
            else {
                $ate = Carbon::now()->endOfMonth();
            }
        }

        $result = SacOS::select();
        $result->whereBetween('ended_at', array($de, $ate));

        $listOs = array();
        foreach($result->orderBy('ended_at', 'desc')->get() as $os) {
            if ($os->changelog) {
                array_push($listOs, $os);
            }
        }

        $this->set_context(array(
            'os' => $listOs,
            'filters' => (object) array(
                'de' => $de->format('d/m/Y'),
                'ate' => $ate->format('d/m/Y')
            )
        ));

        return $this->view_make('sac/os/documented');
    }

    /**
     * Página de visualização do OS
     *
     * @param integer $os
     * @return Illuminate\Support\Facades\View
     */
    public function view($os)
    {
        $os = SacOS::where('id', '=', $os)->firstOrFail();
        $previous = null;
        // Armazenamos a url anterior apenas estiver vindo da página de listagem
        if (strpos(URL::previous(), URL::action('SacOSController@index')) !== false) {
            $previous = URL::previous();
        }

        $this->set_context(array(
            'os' => $os,
            'previous' => $previous
        ));

        $this->set_json_context(array(
            'types' => SacOSType::all()->toJson(),
            'modules' => SacModule::all()->toJson()
        ));

        return $this->view_make('sac/os/view');
    }

    /**
     * Edita a OS
     *
     * @param integer $os
     * @return \Illuminate\Http\JsonResponse
     */
    public function inlineEdit($os)
    {
        $os = SacOS::where('id', '=', $os)->firstOrFail();

        $field = Input::get('field');
        $value = Input::get('value');

        if ($field == "summary-val" && $this->isValidInput($value)) {
            $os->summary = $value;
        }

        if ($field == "type-val" && $this->isValidInput($value)) {
            $os->type_id = $value;
        }

        if ($field == "module-val" && $this->isValidInput($value)) {
            $os->module_id = $value;
        }

        $os->save();

        return Response::json(array(
            'success' => true
        ));
    }

    /**
     * Verifica se o valor informado é vazio: empty, false, array(), NULL ou 0.
     *
     * @param mixed $value
     * @return bool
     */
    private function isValidInput($value)
    {
        if (!empty($value)) {
            return true;
        }
        return false;
    }
}