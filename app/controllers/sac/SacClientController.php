<?php

use JCS\Mail\Models\Queue,
    JCS\Mail\Campaign,
    JCS\Mail\ArraySource,
    JCS\Mail\Models\SmtpServer;


class SacClientController extends BaseController {

    /**
     * Define o módulo ativo
     *
     * @return string
     */
    public $module = Module::SAC;

    /**
     * Exibe a página de listagem de clientes efetivados
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'filters' => (object) array(
                'cod' => Input::get('cod', ''),
                'name' => Input::get('name', ''),
                'status' => Input::get('status', ''),
                'all' => Input::get('all', ''),
            )
        ));

        $result = SacClient::select('sac_clients.*');
        if (Input::get('cod')) {
            $result->where('sac_clients.cod', '=', Input::get('cod'));
        }
        else {
            $result->whereNotNull('sac_clients.cod');
        }

        if (Input::get('status')) {
            $result->where('sac_clients.situacao', '=', Input::get('status'));
        }

        if (Input::get('name')) {
            $result->join('sac_client_address', 'sac_client_address.client_id', '=', 'sac_clients.id')
                ->where('sac_clients.nome', 'like', '%'. Input::get('name') .'%')
                ->orWhere('sac_client_address.city', 'like', '%'. Input::get('name') .'%');
        }

        $result->orderBy('sac_clients.cod', 'asc');

        $this->set_context(array(
            'clients' => $result->paginate((Input::get('all') == "1") ? $result->count() : 50)
        ));

        return $this->view_make('sac/client/index');
    }

    /**
     * Lista os contatos que já foram feitos no intervalo de data informados
     *
     * @return Illuminate\Support\Facades\View
     */
    public function contacts_done()
    {
        if (Input::get('de')) {
            $fde = explode('/', Input::get('de'));
            $de = Carbon::create($fde[2], $fde[1], $fde[0]);
        }
        else {
            $de = Carbon::now()->startOfMonth();
        }

        if (Input::get('ate')) {
            $fate = explode('/', Input::get('ate'));
            $ate = Carbon::create($fate[2], $fate[1], $fate[0]);
        }
        else {
            $ate = Carbon::now()->endOfMonth();
        }

        $contatos = SacClientIncomingContact::whereBetween('date_contact', array($de->startOfDay(), $ate->endOfDay()));

        $this->set_context(array(
            'contacts' => $contatos->paginate(20),
            'filters' => (object) array(
                'de' => $de->format('d/m/Y'),
                'ate' => $ate->format('d/m/Y')
            )
        ));

        return $this->view_make('sac/client/incoming/contacts_done');
    }

    /**
     * Exibe página com todas as negociações do cliente
     *
     * @param integer $client
     * @return \Illuminate\View\View
     */
    public function incoming_view($client)
    {
        $this->set_context(array(
            'client' => SacClient::find($client)
        ));

        return $this->view_make('sac/client/incoming/view');
    }

    /**
     * Lista os contatos que estão pendentes
     *
     * @return Illuminate\Support\Facades\View
     */
    public function contacts_pending()
    {
        $contatos = array();

        $all_incomings = SacClient::where('is_incoming', '=', true)->get();
        foreach($all_incomings as $cliente)
        {
            if ($cliente->is_incoming) {
                try {
                    $ultimo_contato = $cliente->incoming->contacts()->orderBy('date_contact', 'desc')->limit(1)->firstOrFail();
                    if ($ultimo_contato->next_contact <= Carbon::now()->endOfDay()) {
                        array_push($contatos, $ultimo_contato);
                    }
                }
                catch(Exception $e) {
                    //
                }
            }
        }

        $this->set_context(array(
            'contacts' => $contatos
        ));

        return $this->view_make('sac/client/incoming/contacts_pending');
    }

    /**
     * Lista os contatos previstos para os próximos 7 dias
     *
     * @return Illuminate\Support\Facades\View
     */
    public function contacts_next()
    {
        if (Input::get('ate')) {
            $fate = explode('/', Input::get('ate'));
            $ate = Carbon::create($fate[2], $fate[1], $fate[0]);
        }
        else {
            $ate = Carbon::now()->addDays(7);
        }

        $contatos = array();

        $all_incomings = SacClient::where('is_incoming', '=', true)->get();
        foreach($all_incomings as $cliente)
        {
            if ($cliente->is_incoming) {
                try {
                    $ultimo_contato = $cliente->incoming->contacts()->orderBy('date_contact', 'desc')->limit(1)->firstOrFail();
                    if ($ultimo_contato->next_contact >= Carbon::now()->startOfDay() && $ultimo_contato->next_contact <= $ate->endOfDay()) {
                        array_push($contatos, $ultimo_contato);
                    }
                }
                catch(Exception $e) {
                    //
                }
            }
        }

        $this->set_context(array(
            'contacts' => $contatos,
            'qtd_dias' => $ate->diffInDays(Carbon::now()),
            'filters' => (object) array(
                'ate' => $ate->format('d/m/Y')
            )
        ));

        return $this->view_make('sac/client/incoming/contacts_next');
    }

    /**
     * Exibe a página de listagem de clientes em negociação
     *
     * @return Illuminate\Support\Facades\View
     */
    public function incoming()
    {
        $this->set_context(array(
            'filters' => (object) array(
                'name' => Input::get('name', ''),
                'status' => Input::get('status', ''),
                'all' => Input::get('all', '')
            )
        ));

        $result = SacClient::select('sac_clients.*');
        $result->where('is_incoming', '=', true);

        if (Input::get('status')) {
            $result->join('sac_client_incoming', 'sac_client_incoming.client_id', '=', 'sac_clients.id')
                ->join('sac_client_incoming_status', 'sac_client_incoming.status_id', '=', 'sac_client_incoming_status.id')
                ->where('sac_client_incoming_status.id', '=', Input::get('status'));
        }

        if (Input::get('name')) {
            $result->join('sac_client_address', 'sac_client_address.client_id', '=', 'sac_clients.id')
                ->where('sac_clients.nome', 'like', '%'. Input::get('name') .'%')
                ->orWhere('sac_client_address.city', 'like', '%'. Input::get('name') .'%');
        }

        $this->set_context(array(
            'clients' => $result
                    ->orderBy('sac_clients.cod', 'asc')
                    ->paginate((Input::get('all') == "1") ? $result->count() : 50)
        ));

        return $this->view_make('sac/client/incoming/index');
    }

    /**
     * Página de visualização dos dados do Cliente
     *
     * @param integer $client
     * @return Illuminate\Support\Facades\View
     */
    public function view($client)
    {
        $client = SacClient::where('id', '=', $client)->firstOrFail();
        $this->set_context(array(
            'client' => $client,
        ));

        if ($client->is_incoming) {
            $this->set_json_context(array(
                'status' => SacClientIncomingStatus::all()->toJson()
            ));
        }

        return $this->view_make('sac/client/view');
    }

    /**
     * Página de envio de email para Cliente
     *
     * @param integer $client
     * @return Illuminate\Support\Facades\View
     */
    public function mail($client)
    {
        $client = SacClient::where('id', '=', $client)->firstOrFail();
        $this->set_context(array(
            'client' => $client,
            'sent' => false,
            'mailserver' => SmtpServer::first(),
            'form' => (object) array(
                'subject' => Input::get('subject', ''),
                'copy_me' => Input::get('copy_me') == 'on',
                'type' => Input::get('type', 'plain'),
                'message' => Input::get('message', ''),
                'mode' => Input::get('mode', Queue::MODE_SINGLE),
                'contacts' => Input::get('contacts', array()),
            )
        ));

        if (!Input::has('submit')) {
            return $this->view_make('sac/client/mail');
        }

        $validate = array(
            'contacts' => 'required',
            'subject' => 'required|min:3|max:128',
            'message' => 'required'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/mail');
        }

        $campaign = new Campaign();

        if (Input::get('copy_me') == 'on') {
            $campaign->addAddress(new ArraySource(array(Auth::user()->email)));
        }

        $campaign->addAddress(new ArraySource(Input::get('contacts', array())))
            ->setMessage(Input::get('subject'), Input::get('message'), Input::get('type'))
            ->setMode(Input::get('mode'))
            ->save();

        $campaign->execute();

        $this->set_context(array(
            'sent' => true
        ));

        return $this->view_make('sac/client/mail');
    }

    /**
     * Página inline de edição de endereço de cliente
     *
     * @return Illuminate\Support\Facades\View
     */
    public function addressEdit()
    {
        $client = SacClient::where('id', '=', Input::get('client'))->firstOrFail();
        $this->set_context(array(
            'client' => $client,
            'forms' => (object) array(
                'address' => Input::get('address', $client->address->address),
                'number' => Input::get('number', $client->address->number),
                'district' => Input::get('district', $client->address->district),
                'city' => Input::get('city', $client->address->city),
                'uf' => Input::get('uf', $client->address->uf),
                'zipcode' => Input::get('zipcode', $client->address->zipcode),
            )
        ));

        if (!Input::has('submit')) {
            return $this->view_make('sac/client/inline_address');
        }

        $validate = array(
            'city' => 'required',
            'uf' => 'required'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/inline_address');
        }

        $client->address->address = strtoupper(Input::get('address', ''));
        $client->address->number = Input::get('number', '');
        $client->address->district = strtoupper(Input::get('district', ''));
        $client->address->city = strtoupper(Input::get('city', ''));
        $client->address->uf = strtoupper(Input::get('uf', ''));
        $client->address->zipcode = Input::get('zipcode', '');
        $client->address->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SacClientController@view',
                array($client->id)
            )
        ));
    }

    /**
     * Pesquisa os clientes e retorna em formato JSONP
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function find()
    {
        $db_clients = SacClient::select(array('sac_clients.*', 'sac_client_address.city', 'sac_client_address.uf'))
            ->where('nome', 'like', '%'. Input::get('q') .'%')
            ->orWhere('cod', '=', Input::get('q'))
            ->orWhere('cgc', 'like', Input::get('q') .'%')
            ->join('sac_client_address', 'sac_client_address.client_id', '=', 'sac_clients.id')
            ->orWhere('sac_client_address.city', 'like', Input::get('q') .'%')
            ->limit(Input::get('page_limit', 10))
            ->get();

        $clients = array();
        foreach ($db_clients as $client) {
            array_push($clients, array(
                'id' => $client->id,
                'text' => ucwords(strtolower($client->nome)),
                'cod' => $client->cod,
                'cnpj' => $client->cgc,
                'city' => ucfirst(strtolower($client->address->city)) .' - '. $client->address->uf
            ));
        }

        return Response::json(array(
            'total' => count($clients),
            'clients' => $clients
        ))->setCallback(Input::get('callback'));
    }

    /**
     * Exibe a página inline de criação de negociação
     *
     * @return Illuminate\Support\Facades\View
     */
    public function incoming_start()
    {
        $client = SacClient::find(Input::get('client'));

        $this->set_context(array(
            'incoming_status' => SacClientIncomingStatus::where('close_incoming', '=', false)->orderBy("name")->get(),
            'client' => $client,
            'forms' => (object) array(
                'status' => Input::get('status', '')
            )
        ));

        if (!Input::has("submit")) {
            return $this->view_make('sac/client/incoming/start');
        }

        $validate = array(
            'status' => 'required|integer'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/incoming/start');
        }

        $client->is_incoming = true;
        $client->save();

        if ($client->incoming) {
            $incoming = $client->incoming;
        }
        else {
            $incoming = new SacClientIncoming;
            $incoming->owner_id = Auth::user()->id;
            $incoming->client_id = $client->id;
            $incoming->save();
        }

        $incoming->status_id = SacClientIncomingStatus::find(Input::get('status'))->id;
        $incoming->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SacClientController@view',
                array($client->id)
            )
        ));
    }

    /**
     * Edita a Negociação
     *
     * @param integer $incoming
     * @return \Illuminate\Http\JsonResponse
     */
    public function incoming_inline($incoming)
    {
        $incoming = SacClientIncoming::where('id', '=', $incoming)->firstOrFail();

        $field = Input::get('field');
        $value = Input::get('value');

        if ($field == "incoming-status") {
            $incoming->status_id = $value;
        }

        $incoming->save();

        $update = false;
        if ($incoming->status->close_incoming) {
            $incoming->client->is_incoming = false;
            $incoming->client->save();
            $update = true;
        }

        return Response::json(array(
            'success' => true,
            'color' => $incoming->status->color,
            'update' => $update
        ));
    }

    /**
     * Exibe a página inline de criação de cliente
     *
     * @return Illuminate\Support\Facades\View
     */
    public function create()
    {
        $this->set_context(array(
            'incoming_status' => SacClientIncomingStatus::orderBy('name')->get(),
            'forms' => (object) array(
                'name' => Input::get('name', ''),
                'cgc' => Input::get('cgc', ''),
                'number' => Input::get('number', ''),
                'address' => Input::get('address', ''),
                'district' => Input::get('district', ''),
                'city' => Input::get('city', ''),
                'uf' => Input::get('uf', ''),
                'zipcode' => Input::get('zipcode', ''),
                'status' => Input::get('status', '')
            )
        ));

        if (!Input::has("submit")) {
            return $this->view_make('sac/client/form');
        }

        $validate = array(
            'name' => 'required|min:2|max:255',
            'city' => 'required|min:2|max:255',
            'uf' => 'required|min:2|max:2',
            'status' => 'required|integer'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/form');
        }

        $client = new SacClient;
        $client->nome = strtoupper(Input::get('name'));
        $client->cgc = preg_replace("/[^0-9]/", "", Input::get('cgc'));
        $client->situacao = SacClient::SITUATION_INCOMING;
        $client->is_incoming = true;
        $client->save();

        $incoming = new SacClientIncoming;
        $incoming->owner_id = Auth::user()->id;
        $incoming->client_id = $client->id;
        $incoming->status_id = SacClientIncomingStatus::find(Input::get('status'))->id;
        $incoming->save();

        $address = new SacClientAddress;
        $address->address = strtoupper(Input::get('address'));
        $address->number = Input::get('number');
        $address->district = strtoupper(Input::get('district'));
        $address->city = strtoupper(Input::get('city'));
        $address->uf = strtoupper(Input::get('uf'));
        $address->zipcode = preg_replace("/[^0-9]/", "", Input::get('zipcode'));
        $address->client_id = $client->id;
        $address->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SacClientController@view',
                array($client->id)
            )
        ));
    }

    /**
     * Envia uma cópia da negociação para os emails informados
     *
     * @return Illuminate\Support\Facades\View
     */
    public function contact_mail()
    {
        $contact = SacClientIncomingContact::find(Input::get('contact'));

        $this->set_context(array(
            'contact' => $contact,
            'forms' => (object) array(
                'mails' => Input::get('mails', '')
            )
        ));

        if (!Input::has("submit")) {
            return $this->view_make('sac/client/incoming/mail');
        }

        $validate = array(
            'mails' => 'required'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/incoming/mail');
        }

        $campaign = new Campaign();

        $mails = array();
        foreach(explode(",", Input::get('mails')) as $email) {
            $email = trim($email);
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                array_push($mails, $email);
            }
        }

        if (count($mails))
        {
            $message = sprintf("Contato efetivado: %s\nRealizado por: %s\nCliente: %s\nContato com: %s\nSituação: %s\nData do contato: %s\nData do próximo contato: %s\n\nObservações:\n%s",
                ($contact->effected == "on") ? "Sim" : "Não",
                $contact->operator->profile->fullname,
                $contact->client->nome,
                $contact->contact->name,
                $contact->status->name,
                Carbon::parse($contact->date_contact)->format('d/m/Y'),
                Carbon::parse($contact->next_contact)->format('d/m/Y'),
                html_entity_decode($contact->text, ENT_QUOTES, 'utf-8')
            );

            $campaign
                ->addAddress(new ArraySource($mails))
                ->setMessage("Contato com cliente ". $contact->client->nome, $message, 'plain')
                ->setMode(Queue::MODE_SINGLE)
                ->save();

            $campaign->execute();
        }

        return Response::json(array(
            'success' => true
        ));
    }

    /**
     * Exibe a página inline de criação de negociação
     *
     * @return Illuminate\Support\Facades\View
     */
    public function incoming_create()
    {
        $incoming = SacClientIncoming::find(Input::get('incoming'));

        $this->set_context(array(
            'incoming' => $incoming,
            'incoming_status' => SacClientIncomingStatus::orderBy("name")->get(),
            'forms' => (object) array(
                'contact' => Input::get('contact'),
                'text' => Input::get('text', ''),
                'date_contact' => Input::get('date_contact', Carbon::now()->format('d/m/Y')),
                'next_contact' => Input::get('next_contact', ''),
                'effected' => Input::get('effected', ""),
                'status' => Input::get('status', ''),
                'copy_email' => Input::get('copy_email', '')
            )
        ));

        if (!Input::has("submit")) {
            return $this->view_make('sac/client/incoming/form');
        }

        $validate = array(
            'contact' => 'required|integer',
            'text' => 'required',
            'date_contact' => 'required',
            'next_contact' => 'required',
            'status' => 'required|integer'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/incoming/form');
        }

        $status = SacClientIncomingStatus::find(Input::get('status'));

        $contact = new SacClientIncomingContact;
        $contact->operator_id = Auth::user()->id;
        $contact->contact_id = Input::get('contact');
        $contact->incoming_id = Input::get('incoming');
        $contact->text = htmlentities(Input::get('text'), ENT_QUOTES, 'utf-8', false);
        $contact->effected = Input::get('effected') == "on";
        $contact->status_id = $status->id;

        $dt = explode('/', Input::get('next_contact'));
        if (count($dt) == 3) {
            $contact->next_contact = Carbon::create($dt[2], $dt[1], $dt[0]);
        }

        $dt = explode('/', Input::get('date_contact'));
        $contact->date_contact = Carbon::create($dt[2], $dt[1], $dt[0]);

        $contact->save();

        $form_mail = Input::get('copy_email', '');
        if ($form_mail)
        {
            $campaign = new Campaign();

            $mails = array();
            foreach(explode(",", $form_mail) as $email) {
                $email = trim($email);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    array_push($mails, $email);
                }
            }

            $message = sprintf("Contato efetivado: %s\nRealizado por: %s\nCliente: %s\nContato com: %s\nSituação: %s\nData do contato: %s\nData do próximo contato: %s\n\nObservações:\n%s",
                (Input::get('effected') == "on") ? "Sim" : "Não",
                $contact->operator->profile->fullname,
                $contact->client->nome,
                SacClientContact::find(Input::get('contact'))->name,
                $status->name,
                Input::get('date_contact'),
                Input::get('next_contact'),
                Input::get('text')
            );

            $campaign
                ->addAddress(new ArraySource($mails))
                ->setMessage("Contato com cliente ". $contact->client->nome, $message, 'plain')
                ->setMode(Queue::MODE_SINGLE)
                ->save();

            $campaign->execute();
        }

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SacClientController@view',
                array($incoming->client->id)
            )
        ));
    }
    public function incoming_contact()
    {
        $contact = SacClientIncomingContact::find(Input::get('contact'));

        $this->set_context(array(
            'contact' => $contact,
            'incoming' => $contact->incoming,
            'client' => $contact->incoming->client
        ));

        return $this->view_make('sac/client/incoming/contact');
    }

    /**
     * Exibe a página inline de criação de contatos
     *
     * @return Illuminate\Support\Facades\View
     */
    public function contact_create()
    {
        $client = SacClient::find(Input::get('client'));

        $this->set_context(array(
            'contact' => null,
            'name' => Input::get('name', ''),
            'email' => Input::get('email', ''),
            'fax' => Input::get('fax', ''),
            'phone' => Input::get('phone', ''),
            'mobile' => Input::get('mobile', ''),
            'departments' => SacDepartment::all(),
            'department_selected' => Input::get('department'),
            'client' => $client
        ));

        if (!Input::has("submit")) {
            return $this->view_make('sac/client/contact/form');
        }

        $validate = array(
            'name' => 'required|min:2|max:64',
            'department' => 'required|integer',
            'email' => 'email'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/contact/form');
        }

        $contact = new SacClientContact;
        $contact->name = strtoupper(Input::get('name'));
        $contact->email = strtolower(Input::get('email'));
        $contact->fax = Input::get('fax');
        $contact->phone = Input::get('phone');
        $contact->mobile = Input::get('mobile');
        $contact->client_id = $client->id;
        $contact->department_id = Input::get('department');
        $contact->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SacClientController@view',
                array($client->id)
            )
        ));
    }

    /**
     * Exibe a página inline de edição de contatos
     *
     * @param integer $contact
     * @return Illuminate\Support\Facades\View
     */
    public function contact_edit($contact)
    {
        $contact = SacClientContact::find($contact);

        $this->set_context(array(
            'contact' => $contact,
            'name' => Input::get('name', $contact->name),
            'email' => Input::get('email', $contact->email),
            'fax' => Input::get('fax', $contact->fax),
            'phone' => Input::get('phone', $contact->phone),
            'mobile' => Input::get('mobile', $contact->mobile),
            'departments' => SacDepartment::all(),
            'department_selected' => Input::get('department', $contact->department_id),
            'client' => $contact->client
        ));

        if (!Input::has("submit")) {
            return $this->view_make('sac/client/contact/form');
        }

        $validate = array(
            'name' => 'required|min:2|max:64',
            'department' => 'required|integer',
            'email' => 'email'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('sac/client/contact/form');
        }

        $contact->name = strtoupper(Input::get('name'));
        $contact->email = strtolower(Input::get('email'));
        $contact->fax = Input::get('fax');
        $contact->phone = Input::get('phone');
        $contact->mobile = Input::get('mobile');
        $contact->department_id = Input::get('department');
        $contact->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SacClientController@view',
                array($contact->client->id)
            )
        ));
    }

    /**
     * Remove um contato
     *
     * @param integer $contact
     * @return Illuminate\Support\Facades\View
     */
    public function contact_remove($contact)
    {
        $contact = SacClientContact::find($contact);
        $contact->delete();

        return Redirect::back();
    }

    /**
     * Página de edição de módulos Cliente
     *
     * @param integer $client
     * @return Illuminate\Support\Facades\View
     */
    public function module_edit($client)
    {
        $client = SacClient::where('id', '=', $client)->firstOrFail();

        if (!Input::has('submit')) {
            $this->set_context(array(
                'client' => $client,
                'modules' => SacModule::all()
            ));

            return $this->view_make('sac/client/module/form');
        }
        $client->modules()->delete();

        foreach(Input::get('modules', array()) as $module_id)
        {
            if (SacModule::where('id', '=', $module_id)->get())
            {
                $module = new SacClientModule;
                $module->client_id = $client->id;
                $module->module_id = $module_id;
                $module->save();
            }
        }

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SacClientController@view',
                array($client->id)
            )
        ));
    }

    /**
     * Verifica se o valor informado é vazio: empty, false, array(), NULL ou 0.
     *
     * @param mixed $value
     * @return bool
     */
    private function isValidInput($value)
    {
        if (!empty($value)) {
            return true;
        }
        return false;
    }

    /**
     * Edita um cliente
     *
     * @param integer $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function inlineEdit($client)
    {
        $client = SacClient::find($client);

        $field = Input::get('field');
        $value = Input::get('value');

        if ($field == "client_since-val" && $this->isValidInput($value)) {
            $dt = explode('/', $value);
            $client->client_since = Carbon::create($dt[2], $dt[1], $dt[0])->startOfDay();
        }

        if ($field == "last_revision-val" && $this->isValidInput($value)) {
            $dt = explode('/', $value);
            $client->last_revision = Carbon::create($dt[2], $dt[1], $dt[0])->startOfDay();
        }

        if ($field == "cgc-val") {
            $client->cgc = preg_replace("/[^0-9]/", "", $value);
        }

        if ($field == "nome-val" && $this->isValidInput($value)) {
            $client->nome = strtoupper($value);
        }

        $client->save();

        return Response::json(array(
            'success' => true
        ));
    }
}