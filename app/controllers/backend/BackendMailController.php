<?php

use JCS\Auth\Models\Role,
    JCS\Mail\Campaign,
    JCS\Mail\ArraySource,
    JCS\Mail\GroupSource,
    JCS\Mail\ContactSource,
    JCS\Mail\Models\Mail,
    JCS\Mail\Models\Queue,
    JCS\Mail\Models\SmtpServer;


class BackendMailController extends BaseController {

    /**
     * Exibe página com os servidores smtp
     *
     * @return Illuminate\Support\Facades\View
     */
    public function server()
    {
        $this->set_context(array(
            'active_menu' => 'mail.server',
            'servers' => SmtpServer::all()
        ));
        return $this->view_make('backend/mail/server');
    }

    /**
     * Página para adição de servidor SMTP
     *
     * @return Illuminate\Support\Facades\View
     */
    public function create()
    {
        $this->set_context(array(
            'active_menu' => 'mail.server',
            'mserver' => null,
            'form' => (object) array(
                'action' => URL::action('BackendMailController@create'),
                'name' => Input::get('name', ''),
                'description' => Input::get('description', ''),
                'from' => Input::get('from', ''),
                'prefix' => Input::get('prefix', ''),
                'host' => Input::get('host', ''),
                'port' => Input::get('port', ''),
                'tls' => Input::get('tls', ''),
                'username' => Input::get('username', ''),
                'cpass' => 'on',
                'password' => Input::get('password', ''),
            )
        ));

        if (!Input::has("submit")) {
            return $this->view_make('backend/mail/form');
        }

        $validate = array(
            'name' => 'required|min:3|max:32',
            'description' => 'max:128',
            'from' => 'required|email|max:128',
            'prefix' => 'required|min:3|max:32',
            'host' => 'required|min:3|max:128',
            'port' => 'required|min:2|max:5',
            'username' => 'required|min:3|max:128',
            'password' => 'required|min:3|max:64'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages(),
                'form_errors' => array('Erro na validação dos dados. Por favor, verifique as informações abaixo.')
            ));
            return $this->view_make('backend/mail/form');
        }

        $mserver = new SmtpServer;
        $mserver->name = Input::get('name', '');
        $mserver->description = Input::get('description', '');
        $mserver->from = Input::get('from', '');
        $mserver->prefix = Input::get('prefix', '');
        $mserver->host_name = Input::get('host', '');
        $mserver->host_port = Input::get('port', '');
        $mserver->host_tls = Input::get('tls', '') == 'on';
        $mserver->host_username = Input::get('username', '');
        $mserver->host_password = Input::get('password', '');
        $mserver->save();

        return Redirect::action("BackendMailController@server");
    }

    /**
     * Página para edição de servidor SMTP
     *
     * @param $server
     * @return Illuminate\Support\Facades\View
     */
    public function edit($server)
    {
        $mserver = SmtpServer::find($server);

        $this->set_context(array(
            'active_menu' => 'mail.server',
            'mserver' => $mserver,
            'form' => (object) array(
                'action' => URL::action('BackendMailController@edit', array($mserver->id)),
                'name' => Input::get('name', $mserver->name),
                'description' => Input::get('description', $mserver->description),
                'from' => Input::get('from', $mserver->from),
                'prefix' => Input::get('prefix', $mserver->prefix),
                'host' => Input::get('host', $mserver->host_name),
                'port' => Input::get('port', $mserver->host_port),
                'tls' => Input::get('tls', ($mserver->host_tls) ? 'on' : ''),
                'username' => Input::get('username', $mserver->host_username),
                'cpass' => Input::get('cpass', ''),
                'password' => Input::get('password', ''),
            )
        ));

        if (!Input::has("submit")) {
            return $this->view_make('backend/mail/form');
        }

        $validate = array(
            'name' => 'required|min:3|max:32',
            'description' => 'max:128',
            'from' => 'required|email|max:128',
            'prefix' => 'required|min:3|max:32',
            'host' => 'required|min:3|max:128',
            'port' => 'required|min:2|max:5',
            'username' => 'required|min:3|max:128',
            'password' => 'required_with:cpass|min:3|max:64'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages(),
                'form_errors' => array('Erro na validação dos dados. Por favor, verifique as informações abaixo.')
            ));
            return $this->view_make('backend/mail/form');
        }

        $mserver->name = Input::get('name', '');
        $mserver->description = Input::get('description', '');
        $mserver->from = Input::get('from', '');
        $mserver->prefix = Input::get('prefix', '');
        $mserver->host_name = Input::get('host', '');
        $mserver->host_port = Input::get('port', '');
        $mserver->host_tls = Input::get('tls', '') == 'on';
        $mserver->host_username = Input::get('username', '');

        if (Input::get('cpass', '')) {
            $mserver->host_password = Input::get('password', '');
        }

        $mserver->save();

        return Redirect::action("BackendMailController@server");
    }


    /**
     * Exibe a página de confirmaçao de exlusão de servidor smtp/exclui um servidor smtp.
     *
     * @param $server
     * @return Illuminate\Support\Facades\Redirect
     */
    public function delete($server)
    {
        $this->set_context(array(
            'active_menu' => 'mail.server',
            'server' => SmtpServer::find($server)
        ));

        if (!Input::has("submit")) {
            return $this->view_make('backend/mail/delete');
        }

        SmtpServer::find($server)->delete();

        return Redirect::action("BackendMailController@server");
    }

    /**
     * Executa os testes que verificam se o servidor smtp está ok
     *
     * @return Illuminate\Support\Facades\View
     */
    public function connection()
    {
        $mserver = null;
        $validate = array(
            'from' => 'required|email|max:128',
            'prefix' => 'required|min:3|max:32',
            'host' => 'required|min:3|max:128',
            'port' => 'required|min:2|max:5',
            'username' => 'required|min:3|max:128',
            'password' => 'required|min:3|max:64'
        );

        // Se o utilizador estiver na página de edição alteramos as regras de validação da senha.
        if (Input::has('mserver')) {
            $mserver = SmtpServer::find(Input::get('mserver'));
            $validate['password'] = 'required_with:cpass|min:3|max:64';
        }

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'result' => 'error',
                'message' => 'Falha na validação dos dados. Por favor verifique os dados referentes ao servidor SMTP.'
            ));
            return $this->view_make('backend/mail/connection');
        }

        $tmail = new SmtpServer;
        $tmail->from = Input::get('from');
        $tmail->host_name = Input::get('host');
        $tmail->host_port = intval(Input::get('port'));
        $tmail->host_tls = Input::get('tls') == 'on';
        $tmail->host_username = Input::get('username');
        $tmail->host_password = Input::get('password');

        // Se não for informado a senha, buscamos a mesma no registro da configuração do servidor
        if ($mserver && !Input::get('password')) {
            $tmail->host_password = $mserver->host_password;
        }

        try {
            // Faz o envio do email de teste
            $tmail->send('Test email', $tmail->from, 'This is a test mail.');

            $this->set_context(array(
                'result' => 'success',
                'message' => 'A conexão foi bem sucedida.'
            ));
        }
        catch(Exception $e) {
            $this->set_context(array(
                'result' => 'error',
                'message' => $e->getMessage()
            ));
        }

        return $this->view_make('backend/mail/connection');
    }

    public function test($server)
    {
        $server = SmtpServer::find($server);

        $this->set_context(array(
            'active_menu' => 'mail.server',
            'server' => $server,
            'log' => null
        ));

        if (Request::isMethod("get")) {
            $this->set_context(array(
                'form' => (object) array(
                    'to' => $server->from,
                    'subject' => "Mensagem de Teste",
                    'type' => 'plain',
                    'message' => "Esta é uma mensagem de teste"
                        ."\nServidor: ". $server->name
                        ."\nSMTP Port: ". $server->host_port
                        ."\nDescrição: ". $server->description
                        ."\nDe: ". $server->from
                        ."\nUsuário: ". $server->host_username
                )
            ));

            return $this->view_make('backend/mail/test');
        }

        $this->set_context(array(
            'form' => (object) array(
                'to' => Input::get('to', ''),
                'subject' => Input::get('subject', ''),
                'type' => Input::get('type', 'plain'),
                'message' => Input::get('message', '')
            )
        ));

        $validate = array(
            'to' => 'required|email|max:128',
            'subject' => 'required|min:3|max:128',
            'type' => 'required'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));

            return $this->view_make('backend/mail/test');
        }

        try {
            $server->send(Input::get('subject'), Input::get('to'), Input::get('message'), Input::get('type', 'plain'));
            $this->set_context(array(
                'log' => "Sua mensagem de teste foi enviada com sucesso para ".
                         Input::get('to') .".\n\n". $server->getLogger()->dump()
            ));
        }
        catch(Exception $e) {
            $this->set_context(array(
                'log' => "Ocorreu um erro no envio do e-mail de teste:\n\n". $server->getLogger()->dump()
            ));
        }

        return $this->view_make('backend/mail/test');

    }

    /**
     * Exibe página de envio de email em massa
     *
     * @return Illuminate\Support\Facades\View
     */
    public function send()
    {
        $this->set_context(array(
            'active_menu' => 'mail.send',
            'sending' => null,
            'servers' => SmtpServer::all(),
            'groups' => Role::all(),
            'modules' => SacModule::all(),
            'departments' => SacDepartment::all(),
            'form' => (object) array(
                'subject' => Input::get('subject', ''),
                'copy_me' => Input::get('copy_me') == 'on',
                'type' => Input::get('type', 'plain'),
                'to' => Input::get('to', 'users'),
                'manual' => Input::get('manual', ''),
                'message' => Input::get('message', ''),
                'mode' => Input::get('mode', Queue::MODE_SINGLE),
                'groups' => Input::get('groups', array()),
                'modules' => Input::get('modules', array()),
                'departments' => Input::get('departments', array())
            )
        ));

        if (!Input::has('submit')) {
            return $this->view_make('backend/mail/send');
        }

        $validate = array(
            'to' => 'required',
            'subject' => 'required|min:3|max:128',
            'message' => 'required'
        );

        switch (Input::get('to')) {
            case 'users':
                $validate['groups'] = 'required';
                break;
            case 'clients':
                $validate['modules'] = 'required';
                $validate['departments'] = 'required';
                break;
            case 'manual':
                $validate['manual'] = 'required';
                break;
        }

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/mail/send');
        }

        $campaign = new Campaign();

        if (Input::get('copy_me') == 'on') {
            $campaign->addAddress(new ArraySource(array(Auth::user()->email)));
        }

        if (Input::get('to') == 'users') {
            $groups = Role::whereIn('id', Input::get('groups'))->get();
            $campaign->addAddress(new GroupSource($groups));
        }

        if (Input::get('to') == 'clients')
        {
            $contacts = SacClientContact::select(array('sac_client_contacts.*'));
            $contacts->whereIn('sac_client_contacts.department_id', Input::get('departments', array()));
            $contacts->join('sac_client_module', 'sac_client_module.client_id', '=', 'sac_client_contacts.client_id')
                ->whereIn('sac_client_module.module_id', Input::get('modules', array()));

            $campaign->addAddress(new ContactSource($contacts->get()));
        }

        if (Input::get('to') == 'manual')
        {
            $mails = array();
            foreach(explode(',', Input::get('manual')) as $email) {
                $email = trim($email);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    array_push($mails, $email);
                }
            }
            $campaign->addAddress(new ArraySource($mails));
        }

        if (!count($campaign->getMails())) {
            $this->set_context(array(
                'form_errors' => array('Não foi encontrado nenhum contato utilizando os filtros selecionados.')
            ));
            return $this->view_make('backend/mail/send');
        }

        $campaign
            ->setMessage(Input::get('subject'), Input::get('message'), Input::get('type'))
            ->setMode(Input::get('mode'))
            ->save();

        // Executa a queue
        $campaign->execute();

        $this->set_context(array(
            'sending' => $campaign->getMails()
        ));

        return $this->view_make('backend/mail/send');
    }

    /**
     * Exibe página com a fila de emails
     *
     * @return Illuminate\Support\Facades\View
     */
    public function queue()
    {
        $this->set_context(array(
            'active_menu' => 'mail.queue',
            'waiting' => Mail::where('status', '=', Mail::STATUS_WAITING)->get(),
            'error' => Mail::where('status', '=', Mail::STATUS_ERROR)->get()
        ));
        return $this->view_make('backend/mail/queue');
    }

    /**
     * Exibe página com a fila de emails
     *
     * @return Illuminate\Support\Facades\View
     */
    public function repair()
    {
        \JCS\Mail\Campaign::repair();
        return \Redirect::action('BackendMailController@queue');
    }
}