<?php


class BackendController extends BaseController {

    /**
     * Exibe a página inicial da administração com atalhos para
     * os outros itens da administração.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function overview()
    {
        $this->set_context(array(
            'active_menu' => 'overview'
        ));
        return $this->view_make('backend/overview');
    }
}
