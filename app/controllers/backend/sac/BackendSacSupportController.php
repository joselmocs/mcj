<?php


class BackendSacSupportController extends BaseController {

    /**
     * Exibe a lista assuntos disponíveis para chamados
     *
     * @return Illuminate\Support\Facades\View
     */
    public function subjects()
    {
        $this->set_context(array(
            'active_menu' => 'support.subjects',
            'subjects' => SacSupportSubject::all()
        ));

        return $this->view_make('backend/sac/support/subject');
    }

    /**
     * Exibe a página de criação de assunto
     *
     * @return Illuminate\Support\Facades\View
     */
    public function subject_create()
    {
        $this->set_context(array(
            'subject' => '',
            'id' => ''
        ));

        return $this->view_make('backend/sac/support/form');
    }

    /**
     * Exibe a página de edição de assunto
     *
     * @param integer $subject
     * @return Illuminate\Support\Facades\View
     */
    public function subject_edit($subject)
    {
        $subject = SacSupportSubject::find($subject);
        $this->set_context(array(
            'subject' => $subject->subject,
            'id' => $subject->id
        ));

        return $this->view_make('backend/sac/support/form');
    }

    /**
     * Remove um assunto
     *
     * @param integer $subject
     * @return Illuminate\Support\Facades\View
     */
    public function subject_remove($subject)
    {
        SacSupportSubject::find($subject)->delete();
        return Redirect::back();
    }

    /**
     * Salva um assunto
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function subject_save()
    {
        $this->set_context(array(
            'subject' => Input::get('subject', ''),
            'id' => Input::get('id', '')
        ));

        $input = Input::all();

        $validate = array(
            'subject' => 'required|min:3|max:128'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/sac/support/form');
        }

        if (Input::get('id')) {
            $supp = SacSupportSubject::find(Input::get('id'));
        }
        else {
            $supp = new SacSupportSubject;
        }

        $supp->subject = strtoupper($input['subject']);
        $supp->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::previous()
        ));
    }

}