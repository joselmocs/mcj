<?php

use JCS\Auth\Models\Role;


class BackendUserController extends BaseController {

    /**
     * Exibe a lista de usuários
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'active_menu' => 'user.users',
            'roles' => Role::all(),
            'filters' => (object) array(
                'userNameFilter' => Input::get('userNameFilter', ''),
                'fullNameFilter' => Input::get('fullNameFilter', ''),
                'emailFilter' => Input::get('emailFilter', ''),
                'group' => Input::get('group', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            )
        ));

        $users = User::select('users.id', 'users.username', 'users.email', 'users.disabled', 'user_profile.user_id')
            ->join('user_profile', 'user_profile.user_id', '=', 'users.id');

        if (Input::get('userNameFilter')) {
            $users->where('users.username', 'like', '%'. Input::get('userNameFilter') .'%');
        }
        if (Input::get('emailFilter')) {
            $users->where('users.email', 'like', '%'. Input::get('emailFilter') .'%');
        }
        if (Input::get('fullNameFilter')) {
            $users->where('user_profile.fullname', 'like', '%'. Input::get('fullNameFilter') .'%');
        }
        if (Input::get('group')) {
            $users->join('user_role_user', 'user_role_user.user_id', '=', 'users.id')
                ->where('user_role_user.role_id', '=', Input::get('group'));
        }

        $users = $users->orderBy('user_profile.created_at', 'desc')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'users' => $users
        ));

        return $this->view_make('backend/user/index');
    }

    /**
     * Exibe a página inline de criação de usuários
     *
     * @return Illuminate\Support\Facades\View
     */
    public function create()
    {
        $this->set_context(array(
            'username' => Input::get('username', ''),
            'password' => Input::get('password', ''),
            'fullname' => Input::get('fullname', ''),
            'email' => Input::get('email', ''),
            'sendEmail' => Input::get('sendEmail', 'off')
        ));

        if (!Input::has("submit")) {
            return $this->view_make('backend/user/create');
        }

        $input = Input::all();

        $validate = array(
            'username' => 'required|min:3|max:30|alpha_dash|unique:users',
            'password' => 'required|min:3|max:60',
            'confirm' => 'required|same:password',
            'fullname' => 'required|min:3|max:128',
            'email' => 'required|email|unique:users'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/user/create');
        }

        $user = new User;
        $user->username = strtolower($input['username']);
        $user->email = strtolower($input['email']);
        $user->password = Input::get('password');
        $user->verified = 1;
        $user->save();

        $user->profile->fullname = strtoupper($input['fullname']);
        $user->profile->avatar = 'avatar';
        $user->profile->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'BackendUserController@view',
                array($user->username)
            )
        ));
    }

    /**
     * Exibe as informacoes do usuario
     *
     * @param $username
     * @return Illuminate\Support\Facades\View
     */
    public function view($username)
    {
        $this->set_context(array(
            'active_menu' => 'user.users',
            'user' => User::where('username', '=', $username)->firstOrFail()
        ));

        return $this->view_make('backend/user/view');
    }

    /**
     * Verifica se o usuário antenticado possui permissão para manipular o
     * usuário enviado como parâmetro.
     *
     * @param $user
     * @return Illuminate\Support\Facades\View
     */
    public function check_can_manipulate($user) {
        if (Auth::user()->can('sys_admin') || !$user->can('sys_admin')) {
            return true;
        }
        return false;
    }

    /**
     * Edita informacoes do usuario
     *
     * @return Illuminate\Support\Facades\View
     */
    public function edit()
    {
        $user = User::find(Input::get('user_id'));

        // Verificamos se o usuário authenticado possui permissão
        // sobre o usuário.
        if (!$this->check_can_manipulate($user)) {
            return $this->view_make("errors/wo-permission");
        }

        $this->set_context(array(
            'user' => $user,
            'active_menu' => 'user.users',
            'username' => strtolower($user->username),
            'fullname' => $user->profile->fullname,
            'email' => strtolower($user->email),
            'disabled' => $user->disabled,
            'redirect' => Input::get('redirect', 0)
        ));

        if (!Input::has("submit")) {
            return $this->view_make('backend/user/edit');
        }

        $this->set_context(array(
            'username' => strtolower(Input::get('username', '')),
            'fullname' => strtoupper(Input::get('fullname', '')),
            'email' => strtolower(Input::get('email', '')),
            'disabled' => !Input::get('active', false)
        ));

        $input = Input::all();
        $validate = array(
            'username' => 'required|min:3|max:30|alpha_dash|unique:users,username,'. Input::get('user_id'),
            'fullname' => 'required|min:3|max:128',
            'email' => 'required|email|unique:users,email,'. Input::get('user_id')
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/user/edit');
        }

        $user->username = strtolower(Input::get('username'));
        $user->email = strtolower(Input::get('email'));
        $user->disabled = !Input::get('active', false);
        $user->save();
        $user->profile->fullname = strtoupper(Input::get('fullname'));
        $user->profile->save();

        $redirectTo = URL::action('BackendUserController@view', array($user->username));

        if (!Input::get('redirect')) {
            $redirectTo = Request::header('referer');
        }

        return Response::json(array(
            'success' => true,
            'redirectTo' => $redirectTo
        ));
    }

    /**
     * Altera a senha do usuário
     *
     * @return Illuminate\Support\Facades\View
     */
    public function password()
    {
        $user = User::where('username', '=', Input::get('username'))->firstOrFail();

        // Verificamos se o usuário authenticado possui permissão
        // sobre o usuário.
        if (!$this->check_can_manipulate($user)) {
            return $this->view_make("errors/wo-permission");
        }

        $this->set_context(array(
            'user' => $user
        ));

        if (!Input::has("submit")) {
            return $this->view_make('backend/user/password');
        }

        $input = Input::all();
        $validate = array(
            'password' => 'required|min:3|max:60',
            'confirm' => 'required|same:password'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/user/password');
        }

        $user->password = Input::get('password');
        $user->save();

        return Response::json(array('success' => true));
    }

    /**
     * Altera o grupo do usuário
     *
     * @return Illuminate\Support\Facades\View
     */
    public function group()
    {
        $user = User::where('username', '=', Input::get('username'))->firstOrFail();

        // Verificamos se o usuário authenticado possui permissão
        // sobre o usuário.
        if (!$this->check_can_manipulate($user)) {
            return $this->view_make("errors/wo-permission");
        }

        $this->set_context(array(
            'user' => $user,
            'all_groups' => Role::all(),
            'redirect' => Input::get('redirect', 0)
        ));

        if (!Input::has("submit")) {
            return $this->view_make('backend/user/group');
        }

        if (Input::has("join")) {
            $groupsResult = Input::get('groupsToJoin');
            foreach ($user->roles as $ugroup) {
                array_push($groupsResult, $ugroup->id);
            }
            $user->roles()->sync($groupsResult);
        }

        $alertToHeader = null;
        if (Input::has("leave")) {
            $groupsResult = array();
            $groupsToLeave = Input::get('groupsToLeave');

            foreach ($user->roles as $ugroup) {
                // Precisamos verificar se o usuário está tentando remover
                // a permissão de administrador do sistema de si mesmo. Se
                // positivo precisamos 'barra-lo'.
                if (Auth::user()->id == $user->id && $ugroup->can('sys_admin')) {
                    array_push($groupsResult, $ugroup->id);
                    $alertToHeader = array(
                        'type' => 'error',
                        'message' => 'Não é possível remover si mesmo de um grupo com permissão de '.
                            'Administrador do Sistema'
                    );
                }
                else if (!in_array($ugroup->id, $groupsToLeave)) {
                    array_push($groupsResult, $ugroup->id);
                }
            }
            $user->roles()->sync($groupsResult);
        }

        if (!Input::get('redirect')) {
            return Redirect::back()->with('headerMessage', $alertToHeader);
        }

        return Redirect::action("BackendUserController@view", array(
            strtolower($user->username)
        ))->with('headerMessage', $alertToHeader);
    }
}