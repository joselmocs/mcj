<?php


class BackendClientController extends BaseController {

    /**
     * Exibe a página que lista os status de negociação
     *
     * @return Illuminate\Support\Facades\View
     */
    public function incoming_status()
    {
        $this->set_context(array(
            'active_menu' => 'client.incoming.status',
            'status' => SacClientIncomingStatus::orderBy('name')->get()
        ));

        return $this->view_make('backend/client/incoming/index');
    }

    /**
     * Exibe a página de criação de status de negociação
     *
     * @return Illuminate\Support\Facades\View
     */
    public function incoming_status_create()
    {
        $this->set_context(array(
            'name' => '',
            'description' => '',
            'id' => '',
            'color' => '000000',
            'close_incoming' => ""
        ));

        return $this->view_make('backend/client/incoming/form');
    }

    /**
     * Exibe a página de edição de status de negociação
     *
     * @param integer $incoming_status
     * @return Illuminate\Support\Facades\View
     */
    public function incoming_status_edit($incoming_status)
    {
        $incoming = SacClientIncomingStatus::find($incoming_status);
        $this->set_context(array(
            'name' => $incoming->name,
            'description' => $incoming->description,
            'id' => $incoming->id,
            'color' => $incoming->color,
            'close_incoming' => ($incoming->close_incoming) ? "on" : ""
        ));

        return $this->view_make('backend/client/incoming/form');
    }

    /**
     * Remove um status de negociação
     *
     * @param integer $incoming_status
     * @return Illuminate\Support\Facades\View
     */
    public function incoming_status_remove($incoming_status)
    {
        $incoming = SacClientIncomingStatus::find($incoming_status);
        $incoming->delete();

        return Redirect::back();
    }

    public function incoming_status_save()
    {
        $this->set_context(array(
            'name' => Input::get('name', ''),
            'description' => Input::get('description', ''),
            'id' => Input::get('id', ''),
            'color' => Input::get('color', '000000'),
            'close_incoming' => Input::get('close_incoming')
        ));

        $validate = array(
            'name' => 'required|min:3|max:64',
            'description' => 'max:256',
            'color' => 'required|min:6|max:6'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/client/incoming/form');
        }

        if (Input::get('id')) {
            $incoming = SacClientIncomingStatus::find(Input::get('id'));
        }
        else {
            $incoming = new SacClientIncomingStatus;
        }

        $incoming->name = strtoupper(Input::get('name'));
        $incoming->description = Input::get('description');
        $incoming->color = Input::get('color');
        $incoming->close_incoming = Input::get('close_incoming') == "on";
        $incoming->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::previous()
        ));
    }


    /**
     * Exibe a página que lista os departamentos
     *
     * @return Illuminate\Support\Facades\View
     */
    public function departments()
    {
        $this->set_context(array(
            'active_menu' => 'client.department',
            'departments' => SacDepartment::orderBy('name', 'asc')->get()
        ));

        return $this->view_make('backend/client/department/index');
    }

    /**
     * Exibe a página de criação de departamento
     *
     * @return Illuminate\Support\Facades\View
     */
    public function department_create()
    {
        $this->set_context(array(
            'name' => '',
            'description' => '',
            'id' => ''
        ));

        return $this->view_make('backend/client/department/form');
    }

    /**
     * Exibe a página de edição de departamento
     *
     * @param integer $department
     * @return Illuminate\Support\Facades\View
     */
    public function department_edit($department)
    {
        $department = SacDepartment::find($department);
        $this->set_context(array(
            'name' => $department->name,
            'description' => $department->description,
            'id' => $department->id
        ));

        return $this->view_make('backend/client/department/form');
    }

    /**
     * Remove um departamento
     *
     * @param integer $department
     * @return Illuminate\Support\Facades\View
     */
    public function department_remove($department)
    {
        $department = SacDepartment::find($department);
        $department->delete();

        return Redirect::back();
    }

    public function department_save()
    {
        $this->set_context(array(
            'name' => Input::get('name', ''),
            'description' => Input::get('description', ''),
            'id' => Input::get('id', '')
        ));

        $input = Input::all();

        $validate = array(
            'name' => 'required|min:3|max:64',
            'description' => 'max:256'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/client/department/form');
        }

        if (Input::get('id')) {
            $dept = SacDepartment::find(Input::get('id'));
        }
        else {
            $dept = new SacDepartment;
        }

        $dept->name = strtoupper($input['name']);
        $dept->description = $input['description'];
        $dept->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::previous()
        ));
    }

    /**
     * Exibe a página inicial da administração com atalhos para
     * os outros itens da administração.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function mailgroups()
    {
        $this->set_context(array(
            'active_menu' => 'client.mailgroup',
            'mailgroups' => SacMailGroup::orderBy('name', 'asc')->get()
        ));
        return $this->view_make('backend/client/mailgroup/index');
    }

    /**
     * Exibe a página de criação de grupo de email
     *
     * @return Illuminate\Support\Facades\View
     */
    public function mailgroup_create()
    {
        $this->set_context(array(
            'name' => '',
            'description' => '',
            'id' => ''
        ));

        return $this->view_make('backend/client/mailgroup/form');
    }

    /**
     * Exibe a página de edição de grupo de email
     *
     * @param integer $mailgroup
     * @return Illuminate\Support\Facades\View
     */
    public function mailgroup_edit($mailgroup)
    {
        $mailgroup = SacMailGroup::find($mailgroup);
        $this->set_context(array(
            'name' => $mailgroup->name,
            'description' => $mailgroup->description,
            'id' => $mailgroup->id
        ));

        return $this->view_make('backend/client/mailgroup/form');
    }

    /**
     * Remove um grupo de email
     *
     * @param integer $mailgroup
     * @return Illuminate\Support\Facades\View
     */
    public function mailgroup_remove($mailgroup)
    {
        $mailgroup = SacMailGroup::find($mailgroup);
        $mailgroup->delete();

        return Redirect::back();
    }

    public function mailgroup_save()
    {
        $this->set_context(array(
            'name' => Input::get('name', 'description'),
            'description' => Input::get('name', 'description'),
            'id' => Input::get('name', 'id')
        ));

        $input = Input::all();

        $validate = array(
            'name' => 'required|min:3|max:64',
            'description' => 'max:256'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/client/mailgroup/form');
        }

        if (Input::get('id')) {
            $dept = SacMailGroup::find(Input::get('id'));
        }
        else {
            $dept = new SacMailGroup;
        }

        $dept->name = strtoupper($input['name']);
        $dept->description = $input['description'];
        $dept->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::previous()
        ));
    }

}