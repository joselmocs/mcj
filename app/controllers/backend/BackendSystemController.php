<?php


class BackendSystemController extends BaseController {

    /**
     * Exibe página com as configurações gerais do servidor
     *
     * @return Illuminate\Support\Facades\View
     */
    public function general()
    {
        $this->set_context(array(
            'active_menu' => 'system.general',
            'form' => (object) array(
                'ip_sagwin_firebird' => Input::get('ip_sagwin_firebird', System::options()->ip_sagwin_firebird),
                'path_sagwin_firebird' => Input::get('path_sagwin_firebird', System::options()->path_sagwin_firebird),
                'php_firebird_extension_path' => Input::get('php_firebird_extension_path', System::options()->php_firebird_extension_path)
            )
        ));

        if (!Input::has('submit')) {
            return $this->view_make('backend/system/general');
        }

        $validate = array(
            'ip_sagwin_firebird' => 'required|ip',
            'path_sagwin_firebird' => 'required',
            'php_firebird_extension_path' => 'required'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/system/general');
        }

        $system = System::options();
        $system->ip_sagwin_firebird = Input::get('ip_sagwin_firebird', '');
        $system->path_sagwin_firebird = Input::get('path_sagwin_firebird', '');
        $system->php_firebird_extension_path = Input::get('php_firebird_extension_path', '');
        $system->save();

        $alertToHeader = array(
            'type' => 'success',
            'message' => 'Configurações salvas com sucesso.'
        );

        return Redirect::action('BackendSystemController@general')
            ->with('headerMessage', $alertToHeader);
    }

}
