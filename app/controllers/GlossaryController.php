<?php


class GlossaryController extends BaseController {

    /**
     * Define o módulo ativo
     *
     * @return string
     */
    public $module = Module::GLOSSARY;

    /**
     * Lista de classificações do item de glossário
     *
     * @return string
     */
    public $classification = array(
        array('id' => 0, 'name' => 'Nenhuma'),
        array('id' => 1, 'name' => 'Crítica'),
        array('id' => 2, 'name' => 'Advertência')
    );

    /**
     * Exibe a lista de Glossários
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'filters' => (object) array(
                'search' => Input::get('search', '')
            )
        ));

        $result = Glossary::select(array('glossary.*', 'sac_modules.name'))
            ->join('sac_modules', 'sac_modules.id', '=', 'glossary.module_id');

        if (Input::get('search')) {
            $result->where('glossary.function', 'like', '%'. Input::get('search') .'%');
        }

        $this->set_context(array(
            'glossaries' => $result->orderBy('created_at', 'desc')->paginate(50),
            'last_activities' => GlossaryActivity::orderBy('created_at', 'desc')->take(5)->get()
        ));

        return $this->view_make('glossary/index');
    }

    /**
     * Exibe a lista de Glossários criadas no mês corrente
     *
     * @return Illuminate\Support\Facades\View
     */
    public function news()
    {
        $this->set_context(array(
            'filters' => (object) array(
                'search' => Input::get('search', '')
            )
        ));

        $result = Glossary::select(array('glossary.*', 'sac_modules.name'))
            ->join('sac_modules', 'sac_modules.id', '=', 'glossary.module_id');

        if (Input::get('search')) {
            $result->where('glossary.function', 'like', '%'. Input::get('search') .'%');
        }

        if (Input::get('previous')) {
            $de = Carbon::now()->subMonth()->startOfMonth();
            $ate = Carbon::now()->subMonth()->endOfMonth();
        }
        else {
            $de = Carbon::now()->startOfMonth();
            $ate = Carbon::now()->endOfMonth();
        }

        $result->whereBetween('glossary.created_at', array($de, $ate));

        $this->set_context(array(
            'glossaries' => $result->orderBy('created_at', 'desc')->paginate(50)
        ));

        return $this->view_make('glossary/index');
    }

    /**
     * Retorna a quantidade de novos glossários no mes corrente
     *
     * @param boolean $current_month
     * @return integer
     */
    public static function new_glossary_count($current_month = true)
    {
        if ($current_month) {
            $start = Carbon::now();
            $end = Carbon::now();
        }
        else {
            $start = Carbon::now()->subMonth();
            $end = Carbon::now()->subMonth();
        }

        $result = Glossary::select();
        $result->whereBetween('created_at', array($start->startOfMonth(), $end->endOfMonth()));

        return $result->count();
    }

    /**
     * Exibe a página de visualizacao de item do glossário
     *
     * @param integer $glossary
     * @return Illuminate\Support\Facades\View
     */
    public function view($glossary)
    {
        $this->set_context(array(
            'glossary' => Glossary::where("id", "=", $glossary)->firstOrFail(),
            'classifications' => $this->classification
        ));

        $this->set_json_context(array(
            'clientFind' => URL::action('SacClientController@find'),
            'upload_path' => URL::action('GlossaryController@upload'),
            'modules' => SacModule::all()->toJson(),
            'classifications' => json_encode($this->classification)
        ));

        return $this->view_make('glossary/view');
    }

    /**
     * Exibe a página de criação de item do glossário
     *
     * @return Illuminate\Support\Facades\View
     */
    public function create()
    {
        $this->set_context(array(
            'modules' => SacModule::orderBy('name')->get(),
            'module_selected' => Input::get('module', 0),
            'function' => Input::get('function', '')
        ));

        if (!Input::has("submit")) {
            return $this->view_make('glossary/create');
        }

        $input = Input::all();
        $validate = array(
            'function' => 'required|min:3|max:256'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('glossary/create');
        }

        $item = new Glossary;
        $item->user_id = Auth::user()->id;
        $item->module_id = Input::get('module');
        $item->function = Input::get('function');
        $item->save();

        // Registra a atividade feita no item
        GlossaryController::activity($item, "criou");

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action('GlossaryController@view', array($item->id))
        ));
    }

    /**
     * Faz o upload de alguma imagem
     *
     * @return Illuminate\Support\Facades\View
     */
    public function upload()
    {
        $file = Input::file('file');
        $foldername = str_random(8);

        $destinationPath = public_path('upload/glossary/'. $foldername .'/');
        $filename = $file->getClientOriginalName();

        $file->move($destinationPath, $filename);

        return Response::json(array(
            "filelink" => '/upload/glossary/' . $foldername .'/'. $filename
        ));
    }

    /**
     * Remove um item do glossário
     *
     * @param integer $glossary
     * @return Illuminate\Support\Facades\View
     */
    public function remove($glossary)
    {
        $item = Glossary::find($glossary);

        if ($item->changelog) {
            $item->changelog->glossary_id = null;
            $item->changelog->save();
        }

        $item->activities()->delete();
        $item->delete();

        return Redirect::action("GlossaryController@index");
    }

    /**
     * Armazena a atividade feita pelo usuario autenticado
     *
     * @param Glossary $glossary
     * @param string $activity
     * @param string $description
     * @return Illuminate\Support\Facades\View
     */
    public static function activity($glossary, $activity, $description = null)
    {
        $activ = new GlossaryActivity;
        $activ->user_id = Auth::user()->id;
        $activ->glossary_id = $glossary->id;
        $activ->activity = $activity;
        $activ->description = $description;
        $activ->save();
    }

    /**
     * Exibe a página de listagem de glossario liberadas para determinado cliente
     *
     * @param int $cod
     * @param string $cnpj
     * @return Illuminate\Support\Facades\View
     */
    public function client_index($cod, $cnpj)
    {
        $reverte = function($string) {
            return strtr($string, 'BRASILENHO', '0123456789');
        };

        $client = SacClient::where('cod', '=', $reverte($cod))
            ->where('cgc', '=', $reverte($cnpj))
            ->firstOrFail();

        $this->set_context(array(
            'client' => $client,
            'glossaries' => $client->glossaries()
        ));

        return $this->view_make('glossary/client/index');
    }

    /**
     * Exibe a página de visualização do glossario para o cliente.
     *
     * @param int $cod
     * @param int $cnpj
     * @param int $glossary
     * @return \Illuminate\View\View
     */
    public function client_view($cod, $cnpj, $glossary)
    {
        $reverte = function($string) {
            return strtr($string, 'BRASILENHO', '0123456789');
        };

        $client = SacClient::where('cod', '=', $reverte($cod))
            ->where('cgc', '=', $reverte($cnpj))
            ->firstOrFail();
        $glossary = Glossary::where('id', '=', $glossary)->firstOrFail();

        if (!$client->canAccessGlossary($glossary)) {
            return Redirect::back();
        }

        $this->set_context(array(
            'client' => $client,
            'glossary' => $glossary
        ));

        return $this->view_make('glossary/client/view');
    }

    /**
     * Página de edição de acessos por módulo
     *
     * @param integer $glossary
     * @return Illuminate\Support\Facades\View
     */
    public function module_access_edit($glossary)
    {
        $glossary = Glossary::where('id', '=', $glossary)->firstOrFail();

        if (!Input::has('submit')) {
            $this->set_context(array(
                'glossary' => $glossary,
                'modules' => SacModule::all()
            ));

            return $this->view_make('glossary/access/module');
        }
        $glossary->moduleAccess()->delete();

        foreach(Input::get('modules', array()) as $module_id)
        {
            if (SacModule::where('id', '=', $module_id)->get())
            {
                $module = new GlossaryModuleAccess;
                $module->glossary_id = $glossary->id;
                $module->module_id = $module_id;
                $module->save();
            }
        }

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                    'GlossaryController@view',
                    array($glossary->id)
                )
        ));
    }

    /**
     * Dá permissão de acesso ao cliente para que ele possa visualizar este glossário
     *
     * @param integer $glossary
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_create($glossary)
    {
        $glossary = Glossary::find($glossary);

        if (Input::get('client'))
        {
            $client = SacClient::find(Input::get('client'));

            $access = GlossaryClientAccess::where('glossary_id', '=', $glossary->id)
                ->where('client_id', '=', $client->id)
                ->count();

            if (!$access) {
                $access = new GlossaryClientAccess;
                $access->glossary_id = $glossary->id;
                $access->client_id = $client->id;
                $access->save();
            }
        }

        return Redirect::action("GlossaryController@view", array($glossary->id));
    }

    /**
     * Retira a permissão de acesso de todos os clientes deste changelog
     *
     * @param integer $glossary
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_empty($glossary)
    {
        $glossary = Glossary::find($glossary);
        $glossary->clientAccess()->delete();

        return Redirect::back();
    }

    /**
     * Retira a permissão de acesso de um cliente deste glossário
     *
     * @param integer $glossary
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_remove($glossary)
    {
        $access = GlossaryClientAccess::where('glossary_id', '=', $glossary)
            ->where('client_id', '=', Input::get('client'));

        if ($access->count()) {
            $access->delete();
        }

        return Redirect::back();
    }

    /**
     * Libera o acesso do glossário para todos os clientes
     *
     * @param integer $glossary
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_free($glossary)
    {
        $glossary = Glossary::find($glossary);

        if ($glossary) {
            GlossaryClientAccess::where('glossary_id', '=', $glossary->id)->delete();
            $glossary->free_access = true;
            $glossary->save();
        }

        return Redirect::back();
    }

    /**
     * Limita o acesso do glossário para apenas clientes selecionados
     *
     * @param integer $glossary
     * @return \Illuminate\Http\RedirectResponse
     */
    public function client_access_close($glossary)
    {
        $glossary = Glossary::find($glossary);

        if ($glossary) {
            $glossary->free_access = false;
            $glossary->save();
        }

        return Redirect::back();
    }

    /**
     * Edita o glossário
     *
     * @param integer $glossary
     * @return \Illuminate\Http\JsonResponse
     */
    public function inlineEdit($glossary)
    {
        $glossary = Glossary::find($glossary);

        $field = Input::get('field');
        $value = Input::get('value');

        if ($field == "function-val" && $this->isValidInput($value)) {
            $glossary->function = $value;
        }

        if ($field == "module-val" && $this->isValidInput($value)) {
            $glossary->module_id = $value;
        }

        if ($field == "classification-val") {
            $glossary->classification = $value;
        }

        if ($field == "description-val") {
            $glossary->description = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        if ($field == "result-val") {
            $glossary->result = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        if ($field == "action-val") {
            $glossary->action = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        if ($field == "observation-val") {
            $glossary->observation = htmlentities(Input::get('value'), ENT_QUOTES, 'utf-8', false);
        }

        $glossary->save();

        return Response::json(array(
            'success' => true
        ));
    }

    /**
     * Verifica se o valor informado é vazio: empty, false, array(), NULL ou 0.
     *
     * @param mixed $value
     * @return bool
     */
    private function isValidInput($value)
    {
        if (!empty($value)) {
            return true;
        }
        return false;
    }

}