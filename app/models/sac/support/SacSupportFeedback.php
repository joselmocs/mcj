<?php


class SacSupportFeedback extends Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_support_feedback';

    const RUIM = 1;
    const REGULAR = 2;
    const BOM = 3;
    const OTIMO = 4;

    /**
     * Avaliações do chamado
     *
     * @var array
     */
    public static $rating = array(
        self::RUIM => 'Ruim',
        self::REGULAR => 'Regular',
        self::BOM => 'Bom',
        self::OTIMO => 'Ótimo'
    );

    /**
     * Retorna o chamado da mensagem
     *
     * @return SacSupport
     */
    public function support()
    {
        return $this->belongsTo('SacSupport', 'support_id');
    }
}
