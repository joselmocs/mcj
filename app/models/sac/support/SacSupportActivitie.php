<?php


class SacSupportActivitie extends Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_support_activities';

    /**
     * Retorna o chamado da mensagem
     *
     * @return SacSupport
     */
    public function support()
    {
        return $this->belongsTo('SacSupport', 'support_id');
    }

    /**
     * Retorna o usuário
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}
