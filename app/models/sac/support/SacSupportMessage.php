<?php


class SacSupportMessage extends Eloquent
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_support_messages';

    /**
     * Retorna o usuário que enviou a mensagem
     *
     * @return User
     */
    public function sender()
    {
        return $this->belongsTo('User', 'sender_id');
    }

    /**
     * Retorna o chamado da mensagem
     *
     * @return SacSupport
     */
    public function support()
    {
        return $this->belongsTo('SacSupport', 'support_id');
    }
}
