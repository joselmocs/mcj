<?php

use JCS\Hashids\Hashids;


class SacSupport extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_support';

    /**
     * Lista com os tipos de chamada
     *
     * @var array
     */
    public static $type = array(
        '0' => '',
        '1' => 'Financeiro',
        '2' => 'Outros'
    );

    const AGUARDANDO_ATENTENTE = 1;
    const EM_ANDAMENTO = 2;
    const FINALIZADO = 3;

    /**
     * Lista com os estados da chamada
     *
     * @var array
     */
    public static $status = array(
        self::AGUARDANDO_ATENTENTE => 'Aguardando atendente',
        self::EM_ANDAMENTO => 'Em andamento',
        self::FINALIZADO => 'Finalizado'
    );

    /**
     * Retorna o operador que está atendendo o chamado
     *
     * @return User
     */
    public function operator()
    {
        return $this->belongsTo('User', 'operator_id');
    }

    /**
     * Retorna o cliente que fez o chamado
     *
     * @return User
     */
    public function client()
    {
        return $this->belongsTo('User', 'client_id');
    }

    /**
     * Retorna as mensagens da conversa
     *
     * @return SacSupportMessage
     */
    public function messages()
    {
        return $this->hasMany('SacSupportMessage', 'support_id');
    }

    /**
     * Retorna a avaliaçao do chamado se existir
     *
     * @return SacSupportFeedback
     */
    public function feedback()
    {
        return $this->hasOne('SacSupportFeedback', 'support_id');
    }

    /**
     * Retorna a ID usando hash
     *
     * @return string
     */
    public function hash_id()
    {
        $hid = new Hashids;
        return $hid->encrypt($this->id);
    }
}