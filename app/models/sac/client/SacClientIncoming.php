<?php


class SacClientIncoming extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_client_incoming';

    /**
     * Retorna o Usuario responsável pela negociação
     *
     * @return User
     */
    public function owner()
    {
        return $this->belongsTo('User', 'owner_id');
    }

    /**
     * Retorna o Cliente da negociação
     *
     * @return User
     */
    public function client()
    {
        return $this->belongsTo('SacClient', 'client_id');
    }

    /**
     * Retorna o status da negociação
     *
     * @return User
     */
    public function status()
    {
        return $this->belongsTo('SacClientIncomingStatus', 'status_id');
    }

    /**
     * Retorna os contatos da negociação
     *
     * @return SacClientIncomingContact
     */
    public function contacts()
    {
        return $this->hasMany('SacClientIncomingContact', 'incoming_id');
    }
}