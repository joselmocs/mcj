<?php

class SacModule extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_modules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name');

    /**
     * Retorna os clientes que tem o módulo
     *
     * @return SacClientModule
     */
    public function clients()
    {
        return $this->hasMany('SacClientModule', 'module_id');
    }
}