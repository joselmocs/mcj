<?php


class SacClientAddress extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_client_address';

    /**
     * Retorna o cliente
     *
     * @return SacClient
     */
    public function client()
    {
        return $this->belongsTo('SacClient', 'client_id');
    }
}