<?php


class SacClient extends Eloquent {

    const SITUATION_INACTIVE = 'I';
    const SITUATION_ACTIVE = 'A';
    const SITUATION_DEBIT = 'D';
    const SITUATION_SUSPENDED = 'S';
    const SITUATION_RESCISSION = 'R';
    const SITUATION_OUTSOURCED = 'T';
    const SITUATION_INCOMING = 'C';

    public static $STATUS = array(
        self::SITUATION_ACTIVE => "Ativo",
        self::SITUATION_INACTIVE => "Inativo",
        self::SITUATION_SUSPENDED => "Suspenso",
        self::SITUATION_RESCISSION => "Cancelado",
        self::SITUATION_INCOMING => "Em Negociação",
        self::SITUATION_OUTSOURCED => "Terceirizado"
    );

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_clients';

    /**
     * Funciona como uma forma de cache de changelogs do cliente.
     *
     * @var null|array
     */
    protected $list_changelogs = null;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model)
        {
            $model->user()->delete();
            $model->contacts()->delete();
            $model->os()->delete();
            $model->address()->delete();
            $model->modules()->delete();
        });
    }

    /**
     * Retorna o Usuario pertencente a este cliente
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Retorna as informações de negociação do cliente
     *
     * @return SacClientIncoming
     */
    public function incoming()
    {
        return $this->hasOne('SacClientIncoming', 'client_id');
    }

    /**
     * Retorna as OS do cliente
     *
     * @return SacOS
     */
    public function os()
    {
        return $this->hasMany('SacOS', 'client_id');
    }

    /**
     * Retorna os contatos do cliente
     *
     * @return SacClientContact
     */
    public function contacts()
    {
        return $this->hasMany('SacClientContact', 'client_id');
    }

    /**
     * Retorna os contatos do cliente
     *
     * @return SacClientAddress
     */
    public function address()
    {
        return $this->hasOne('SacClientAddress', 'client_id');
    }

    /**
     * Retorna os módulos do cliente
     *
     * @return SacClientModule
     */
    public function modules()
    {
        return $this->hasMany('SacClientModule', 'client_id');
    }

    /**
     * Retorna uma lista de conteúdo de versão que o cliente tem acesso.
     *
     * @return array of ChangeLog
     */
    public function changelogs()
    {
        $changelogs = array();

        // Retorna os changelogs que são liberadas para todos os clientes
        foreach(ChangeLog::where('free_access', '=', 1)->where('released', '<=', Carbon::now())->get() as $changelog)
        {
            if (!array_key_exists($changelog->id, $changelogs)) {
                $changelogs[$changelog->id] = $changelog;
            }
        }

        // Retorna os changelogs que o cliente tem permissão dada individualmente
        foreach(ChangeLogClientAccess::where('client_id', '=', $this->id)->get() as $changelog)
        {
            if (!array_key_exists($changelog->changelog_id, $changelogs) && $changelog->changelog->released <= Carbon::now()) {
                $changelogs[$changelog->changelog_id] = $changelog->changelog;
            }
        }

        // Retorna os changelogs que o cliente tem permissão por módulo
        foreach($this->modules as $module)
        {
            foreach(ChangeLogModuleAccess::where('module_id', '=', $module->module->id)->get() as $changelog)
            {
                if (!array_key_exists($changelog->changelog_id, $changelogs) && $changelog->changelog->released <= Carbon::now()) {
                    $changelogs[$changelog->changelog_id] = $changelog->changelog;
                }
            }
        }

        return $changelogs;
    }

    /**
     * Retorna uma lista de glossario que o cliente tem acesso.
     *
     * @return array of Glossary
     */
    public function glossaries()
    {
        $glossaries = array();

        // Retorna os itens de glossário que todos os clientes tem acesso
        foreach(Glossary::where('free_access', '=', 1)->get() as $glossary)
        {
            if (!array_key_exists($glossary->id, $glossaries)) {
                $glossaries[$glossary->id] = $glossary;
            }
        }

        // Retorna os itens de glossário que o cliente tem permissão dada individualmente
        foreach(GlossaryClientAccess::where('client_id', '=', $this->id)->get() as $glossary)
        {
            if (!array_key_exists($glossary->glossary_id, $glossaries)) {
                $glossaries[$glossary->glossary_id] = $glossary->glossary;
            }
        }

        // Retorna os itens de glossário que o cliente tem permissão por módulo
        foreach($this->modules as $module)
        {
            foreach(GlossaryModuleAccess::where('module_id', '=', $module->module->id)->get() as $glossary)
            {
                if (!array_key_exists($glossary->glossary_id, $glossaries)) {
                    $glossaries[$glossary->glossary_id] = $glossary->glossary;
                }
            }
        }

        return $glossaries;
    }

    /**
     * Retorna o código da empresa usando a 'criptografia' do sacwin.
     *
     * @return string
     */
    public function cod_cript()
    {
        return strtr($this->cod,'0123456789','BRASILENHO');
    }

    /**
     * Retorna o cnpj da empresa usando a 'criptografia' do sacwin.
     *
     * @return string
     */
    public function cgc_cript()
    {
        return strtr($this->cgc,'0123456789','BRASILENHO');
    }

    /**
     * Retorna o cnpj da empresa usando a 'criptografia' do sacwin.
     *
     * @return string
     */
    public function masked_cgc()
    {
        $cgc = $this->cgc;
        if (strlen($cgc) == 14) {
            $cgc = substr($this->cgc, 0, 2) .".".
                   substr($this->cgc, 2, 3) .".".
                   substr($this->cgc, 5, 3) ."/".
                   substr($this->cgc, 8, 4) ."-".
                   substr($this->cgc, 12, 2);
        }

        return $cgc;
    }

    /**
     * Verifica se o usuário tem permissão de acesso ao conteúdo de versão enviado
     *
     * @param ChangeLog $changelog
     * @return bool
     */
    public function canAccessChangelog($changelog)
    {
        foreach($this->changelogs() as $cl) {
            if ($cl->id == $changelog->id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica se o usuário tem permissão de acesso ao glossario enviado
     *
     * @param ChangeLog $glossary
     * @return bool
     */
    public function canAccessGlossary($glossary)
    {
        foreach($this->glossaries() as $cl) {
            if ($cl->id == $glossary->id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica se o cliente tem o módulo específico habilitado
     *
     * @param integer $module
     * @return bool
     */
    public function hasModule($module)
    {
        foreach($this->modules as $dbmodule) {
            if ($module == $dbmodule->module_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna boolean informando se o cliente ainda está em negociação
     *
     * @return boolean
     */
    public function is_new()
    {
        return $this->cod == null;
    }
}