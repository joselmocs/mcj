<?php


class SacMailGroup extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_mailgroups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'description');
}