<?php


class SacClientContact extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_client_contacts';

    /**
     * Retorna o cliente
     *
     * @return SacClient
     */
    public function client()
    {
        return $this->belongsTo('SacClient', 'client_id');
    }

    /**
     * Retorna o departamento do contato
     *
     * @return SacDepartment
     */
    public function department()
    {
        return $this->belongsTo('SacDepartment', 'department_id');
    }
}