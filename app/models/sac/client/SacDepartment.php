<?php


class SacDepartment extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_departments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'description');

    /**
     * Removemos a relação que este departamento tem com os clientes para
     * que seja possível a remoção.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {
            $model->contacts()->update(array('department_id' => null));
        });
    }

    /**
     * Retorna os contatos de todas as empresas que pertencem ao 'mesmo' departamento
     *
     * @return SacClientContact
     */
    public function contacts()
    {
        return $this->hasMany('SacClientContact', 'department_id');
    }
}