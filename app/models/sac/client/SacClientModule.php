<?php


class SacClientModule extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_client_module';

    /**
     * Retorna o changelog
     *
     * @return ChangeLog
     */
    public function client()
    {
        return $this->belongsTo('SacClient', 'client_id');
    }

    /**
     * Retorna o o cliente
     *
     * @return SacClient
     */
    public function module()
    {
        return $this->belongsTo('SacModule', 'module_id');
    }
}