<?php


class SacClientIncomingContact extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_client_incoming_contacts';

    /**
     * Retorna o Usuario pertencente a este cliente
     *
     * @return User
     */
    public function contact()
    {
        return $this->belongsTo('SacClientContact', 'contact_id');
    }

    /**
     * Retorna o Cliente da negociação
     *
     * @return User
     */
    public function client()
    {
        return $this->incoming->client();
    }

    /**
     * Retorna o Usuario pertencente a este cliente
     *
     * @return User
     */
    public function operator()
    {
        return $this->belongsTo('User', 'operator_id');
    }

    /**
     * Retorna a negociação deste contato
     *
     * @return SacClientIncoming
     */
    public function incoming()
    {
        return $this->belongsTo('SacClientIncoming', 'incoming_id');
    }

    /**
     * Retorna o status do contato da negociação
     *
     * @return User
     */
    public function status()
    {
        return $this->belongsTo('SacClientIncomingStatus', 'status_id');
    }
}