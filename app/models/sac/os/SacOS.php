<?php


class SacOS extends Eloquent {

    const STATUS_UNDEFINED = 1;
    const STATUS_OPEN = 2;
    const STATUS_IN_PROGRESS = 3;
    const STATUS_IN_TEST = 4;
    const STATUS_CLOSED = 5;

    const RES_NOT_FIXED = 1;
    const RES_FIXED = 2;
    const RES_DUPLICATED = 3;

    const TYPE_UNDEFINED = 1;
    const TYPE_BUG = 2;
    const TYPE_REPORT = 3;
    const TYPE_IMPROVEMENT = 4;
    const TYPE_NEW_FEATURE = 5;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sac_os';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model)
        {
            $model->changelog()->delete();
        });
    }

    /**
     * Retorna o cliente que solicitou a OS
     *
     * @return SacClient
     */
    public function client()
    {
        return $this->belongsTo('SacClient', 'client_id');
    }

    /**
     * Retorna o módulo da OS
     *
     * @return SacModule
     */
    public function module()
    {
        return $this->belongsTo('SacModule', 'module_id');
    }

    /**
     * Retorna o changelog da OS
     *
     * @return SacOS
     */
    public function changelog()
    {
        return $this->hasOne('ChangeLog', 'os_id');
    }

    /**
     * Retorna o tipo da OS
     *
     * @return SacOSType
     */
    public function type()
    {
        return $this->belongsTo('SacOSType', 'type_id');
    }

    /**
     * Retorna o status da OS
     *
     * @return SacOSStatus
     */
    public function status()
    {
        return $this->belongsTo('SacOSStatus', 'status_id');
    }
}