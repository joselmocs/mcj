<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;


class System extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system';

    /**
     * Retorna o primeiro registro com as configurações do sistema
     *
     * @return System
     */
    public static function options()
    {
        try {
            $option = self::firstOrFail();
        }
        catch (ModelNotFoundException $e) {
            $option = new System;
            $option->save();
        }

        return $option;
    }
}