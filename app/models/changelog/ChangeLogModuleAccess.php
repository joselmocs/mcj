<?php


class ChangeLogModuleAccess extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'changelog_module_access';

    /**
     * Retorna o changelog
     *
     * @return ChangeLog
     */
    public function changelog()
    {
        return $this->belongsTo('ChangeLog', 'changelog_id');
    }

    /**
     * Retorna o módulo
     *
     * @return SacModule
     */
    public function module()
    {
        return $this->belongsTo('SacModule', 'module_id');
    }
}