<?php


class ChangeLogClientAccess extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'changelog_client_access';

    /**
     * Retorna o changelog
     *
     * @return ChangeLog
     */
    public function changelog()
    {
        return $this->belongsTo('ChangeLog', 'changelog_id');
    }

    /**
     * Retorna o cliente
     *
     * @return SacClient
     */
    public function client()
    {
        return $this->belongsTo('SacClient', 'client_id');
    }
}