<?php


class ChangeLog extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'changelog';

    /**
     * Lista com os meses do ano
     *
     * @var array
     */
    public static $_meses = array(
        '00' => '-',
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
    );

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model)
        {
            if ($model->glossary) {
                $model->glossary()->delete();
            }
            $model->clientAccess()->delete();
            $model->moduleAccess()->delete();

        });
    }

    /**
     * Retorna o usuário criador do Conteúdo de Versão
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Retorna as OS do cliente
     *
     * @return SacOS
     */
    public function os()
    {
        return $this->belongsTo('SacOS', 'os_id');
    }

    /**
     * Retorna o glossario do conteúdo de versão se existir
     *
     * @return Glossary
     */
    public function glossary()
    {
        return $this->belongsTo('Glossary', 'glossary_id');
    }

    /**
     * Retorna os clientes que podem acessar o conteúdo de versão
     *
     * @return ChangeLogClientAccess
     */
    public function clientAccess()
    {
        return $this->hasMany('ChangeLogClientAccess', 'changelog_id');
    }

    /**
     * Retorna os módulos que podem acessar o conteúdo de versão
     *
     * @return ChangeLogModuleAccess
     */
    public function moduleAccess()
    {
        return $this->hasMany('ChangeLogModuleAccess', 'changelog_id');
    }

    /**
     * Verifica se o cliente tem o módulo específico habilitado
     *
     * @param integer $module
     * @return bool
     */
    public function hasModuleAccess($module)
    {
        foreach($this->moduleAccess as $dbmodule) {
            if ($module == $dbmodule->module_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna um array com o mes e ano do lançamento do changelog
     *
     * @return array
     */
    public function launch()
    {
        $launchDate = array();
        $launchDate['mes'] = substr($this->launch, 0, 2);
        $launchDate['ano'] = substr($this->launch, 2, 4);

        return (object) $launchDate;
    }

    /**
     * Retorna o nome do mes do lançamento do conteúdo de versão
     *
     * @return string
     */
    public function mes()
    {
        return self::$_meses[$this->launch()->mes];
    }

    /**
     * Retorna o nome do mes do lançamento do conteúdo de versão
     *
     * @return string
     */
    public function ano()
    {
        return ($this->launch()->ano == "0000") ? '-' : $this->launch()->ano;
    }

    /**
     * Retorna a lista de meses do ano com o formato para que o script editableField entenda.
     *
     * @return array
     */
    public function monthToArray()
    {
        $meses = array();
        foreach(self::$_meses as $k => $v) {
            array_push($meses, array('id' => $k, 'name' => $v));
        }

        return $meses;
    }
}