<?php


class GlossaryClientAccess extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'glossary_client_access';

    /**
     * Retorna o changelog
     *
     * @return ChangeLog
     */
    public function glossary()
    {
        return $this->belongsTo('Glossary', 'glossary_id');
    }

    /**
     * Retorna o cliente
     *
     * @return SacClient
     */
    public function client()
    {
        return $this->belongsTo('SacClient', 'client_id');
    }
}