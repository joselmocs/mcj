<?php


class GlossaryModuleAccess extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'glossary_module_access';

    /**
     * Retorna o changelog
     *
     * @return ChangeLog
     */
    public function glossary()
    {
        return $this->belongsTo('Glossary', 'glossary_id');
    }

    /**
     * Retorna o módulo
     *
     * @return SacModule
     */
    public function module()
    {
        return $this->belongsTo('SacModule', 'module_id');
    }
}