<?php


class GlossaryActivity extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'glossary_activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'activity');

    /**
     * Retorna o item do glossário correpondente
     *
     * @return Glossary
     */
    public function glossary()
    {
        return $this->belongsTo('Glossary', 'glossary_id');
    }

    /**
     * Retorna o usuário da atividade
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}