<?php


class Glossary extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'glossary';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'module_id', 'user_id', 'function', 'timespend');

    /**
     * Retorna o Usuario criador deste Item
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Retorna o Módulo pertencente a este Item
     *
     * @return SacModule
     */
    public function module()
    {
        return $this->belongsTo('SacModule', 'module_id');
    }

    /**
     * Retorna as atividades feitas neste Item
     *
     * @return User
     */
    public function activities()
    {
        return $this->hasMany('GlossaryActivity', 'glossary_id')->orderBy('created_at', 'desc');
    }

    /**
     * Se existir, retorna o conteúdo de versão referente ao glossário
     *
     * @return ChangeLog
     */
    public function changelog()
    {
        return $this->hasOne('ChangeLog', 'glossary_id');
    }

    /**
     * Retorna a última atividade do item
     *
     * @return User
     */
    public function last_edited()
    {
        return $this->activities()->take(1)->first();
    }

    /**
     * Retorna os clientes que podem acessar o glossário
     *
     * @return ChangeLogClientAccess
     */
    public function clientAccess()
    {
        return $this->hasMany('GlossaryClientAccess', 'glossary_id');
    }

    /**
     * Retorna os módulos que podem acessar o glossário
     *
     * @return ChangeLogModuleAccess
     */
    public function moduleAccess()
    {
        return $this->hasMany('GlossaryModuleAccess', 'glossary_id');
    }

    /**
     * Verifica se o cliente tem o módulo específico habilitado
     *
     * @param integer $module
     * @return bool
     */
    public function hasModuleAccess($module)
    {
        foreach($this->moduleAccess as $dbmodule) {
            if ($module == $dbmodule->module_id) {
                return true;
            }
        }
        return false;
    }
}