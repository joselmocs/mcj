<?php


class Module {

    /**
     * Módulo atual
     *
     * @var integer
     */
    public static $_active = self::DASHBOARD;

    /**
     * Lista de módulos disponíveis
     *
     * @var integer
     */
    const
        DASHBOARD = 0,
        SAC       = 1,
        GLOSSARY  = 2,
        CHANGELOG = 3
    ;

    /**
     * Define o módulo ativo
     *
     */
    public static function set($module) {
        self::$_active = $module;
    }

    /**
     * Verifica se o módulo enviado está ativo ou não
     *
     * @param $module
     * @return bool
     */
    public static function is($module) {
        if ($module == self::$_active) {
            return true;
        }
        return false;
    }
}