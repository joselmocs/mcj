@extends('template')

@section('styles')
{{ HTML::style('/packages/select2/select2.css') }}
@endsection

@include('sac/app-menu')
@include('templates.ef-form-edit')

@section('scripts')
    @if (Auth::user()->can('os.item_edit'))
        {{ HTML::script('/scripts/libs/jquery.maskedinput.js') }}
        {{ HTML::script('/scripts/apps/os/edit.js') }}
        {{ HTML::script('/packages/select2/select2.js') }}
        {{ HTML::script('/packages/select2/select2_locale_pt-BR.js') }}
    @endif
@endsection

@section('content')
<section id="content" class="body-client">
    <header class="issue-header aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('SacOSController@index') }}">Ordem de Serviço</a></li>
                    <li class="aui-nav-selected">{{ $context->os->cod }}</li>
                </ol>
                <h1 id="summary-val" data-action="{{ URL::action('SacOSController@inlineEdit', array($context->os->id)) }}" @if (isset($errors) && in_array('summary', $errors)) style="color:red;" @endif>
                    @if ($context->os->summary)
                    {{ $context->os->summary }}
                    @else
                    Clique para adicionar um resumo
                    @endif
                </h1>
            </div>
            @if ($context->previous)
            <div class="aui-page-header-actions">
                <a id="return-to-search" href="{{ $context->previous }}" title="Retornar à Pesquisa">Retornar à Pesquisa</a>
            </div>
            @endif
        </div>
    </header>

    <nav class="aui-navgroup aui-navgroup-horizontal">
        <div class="aui-navgroup-inner">
            <div class="aui-navgroup-primary">
                <ul class="aui-nav">
                    <li class="aui-nav-selected"><a href="{{ URL::action('SacOSController@view', array($context->os->id)) }}">OS</a></li>
                    @if ($context->os->changelog)
                        @if (Auth::user()->can('changelog.item_view'))
                        <li><a href="{{ URL::action('ChangelogController@view', array($context->os->changelog->id)) }}">Conteúdo de Versão</a></li>
                        @endif
                    @else
                        @if (Auth::user()->can('changelog.item_create'))
                        <li><a href="{{ URL::action('ChangelogController@create', array($context->os->id)) }}?_token={{ csrf_token() }}">Conteúdo de Versão</a></li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content issue-body-content">

                <div class="aui-group issue-body">

                    <div class="aui-item issue-main-column">
                        <div id="details-module" class="module toggle-wrap">

                            <div class="mod-content">
                                <ul class="property-list two-cols">

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">OS:</strong>
                                            <span class="value"><strong>{{ $context->os->cod }}</strong></span>
                                        </div>
                                    </li>

                                    <li class="item item-right">
                                        <div class="wrap">
                                            <strong class="name">Andamento:</strong>
                                            <img alt="Fechada" height="16" src="{{ $context->os->status->icon }}" width="16"> {{ $context->os->status->name }}
                                        </div>
                                    </li>

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Tipo:</strong>
                                            <span class="value" id="type-val" data-type="select" data-pk="{{ $context->os->type->id }}" data-action="{{ URL::action('SacOSController@inlineEdit', array($context->os->id)) }}" @if (isset($errors) && in_array('type', $errors)) style="color:red;" @endif>
                                                <img height="16" src="{{ $context->os->type->icon }}" width="16"> {{ $context->os->type->name }}
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item item-right">
                                        <div class="wrap">
                                            <strong class="name">Resolução:</strong>
                                            @if ($context->os->resolution == SacOS::RES_NOT_FIXED)
                                            <span class="aui-icon aui-icon-error"></span> Não Resolvido
                                            @endif
                                            @if ($context->os->resolution == SacOS::RES_FIXED)
                                            <span class="aui-icon aui-icon-success"></span> Resolvido
                                            @endif
                                            @if ($context->os->resolution == SacOS::RES_DUPLICATED)
                                            <span class="aui-icon aui-icon-warning"></span> Duplicado
                                            @endif
                                        </div>
                                    </li>

                                    <li class="item full-width">
                                        <div class="wrap">
                                            <strong class="name">Módulo:</strong>
                                            <span class="value" id="module-val" data-type="select" data-action="{{ URL::action('SacOSController@inlineEdit', array($context->os->id)) }}" @if (isset($errors) && in_array('module', $errors)) style="color:red;" @endif
                                            @if ($context->os->module)
                                             data-pk="{{ $context->os->module->id }}">
                                                {{ $context->os->module->name }}
                                            @else
                                             data-pk="">
                                                Módulo não definido
                                            @endif
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item full-width">
                                        <div class="wrap">
                                            <strong class="name">Cliente:</strong>
                                            <span class="value">
                                                @if (Auth::user()->can('client.view'))
                                                <a href="{{ URL::action('SacClientController@view', array($context->os->client->id)) }}">{{ $context->os->client->nome }}</a>
                                                @else
                                                {{ $context->os->client->nome }}
                                                @endif
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item full-width">
                                        <div class="wrap" id="wrap-labels">
                                            <strong class="name">Palavras Chaves:</strong>
                                            <div class="labels-wrap value">
                                                @if ($context->os->tags)
                                                <ul class="labels" id="labels-10002-value">
                                                    <li><a class="lozenge" href=""><span>teste</span></a></li>
                                                </ul>
                                                @else
                                                Nenhuma
                                                @endif
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>

                        <div id="descriptionmodule" class="module toggle-wrap">
                            <div id="descriptionmodule_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Descrição</h2>
                            </div>
                            <div class="mod-content">
                                <div class="user-content-block">{{ html_entity_decode($context->os->description, ENT_QUOTES, 'utf-8') }}</div>
                            </div>
                        </div>

                    </div>
                    <div class="aui-item issue-side-column" id="viewissuesidebar">


                        <div id="peoplemodule" class="module toggle-wrap">
                            <div id="peoplemodule_heading" class="mod-header">
                                <ul class="ops">
                                </ul>
                                <h2 class="toggle-title">Pessoas</h2>
                            </div>
                            <div class="mod-content">
                                <ul class="item-details" id="peopledetails">
                                    <li class="people-details">
                                        <dl>
                                            <dt>Atribuído a:</dt>
                                            <dd>
                                                <span class="aui-avatar aui-avatar-small">
                                                <div class="aui-avatar-inner">
                                                    <img src="/images/useravatar/avatar.png">
                                                </div>
                                                </span>
                                                {{ $context->os->assigned }}
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>Relatado por:</dt>
                                            <dd>
                                            <span class="aui-avatar aui-avatar-small">
                                                <div class="aui-avatar-inner">
                                                    <img src="/images/useravatar/avatar.png">
                                                </div>
                                            </span>
                                            {{ $context->os->reporter }}
                                            </dd>
                                        </dl>
                                    </li>
                                </ul>
                                <ul class="item-details">
                                    <li>
                                        <dl>
                                            <dt>Acompanhado por:</dt>
                                            <dd>
                                                <a id="view-watcher-list" href="javascript: void(0);" title="View Watchers" class="">
                                                    <span id="watcher-data" class="aui-badge watch-state-off">0</span>
                                                </a>
                                                <a id="watching-toggle" class="watch-state-off" href="javascript: void(0);" rel="10003">Acompanhar esta OS</a><span class="icon"></span>
                                            </dd>
                                        </dl>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Datas</h2>
                            </div>
                            <div class="mod-content">
                                <ul class="property-list">

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Iniciado em:</strong>
                                            <span id="type-val" class="value">
                                                {{ Carbon::parse($context->os->created_at)->toDayDateTimeString() }}
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Homologado em:</strong>
                                            <span id="type-val" class="value">
                                                {{ Carbon::parse($context->os->ended_at)->toDayDateTimeString() }}
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>

</section>


<div id="inline-dialog-share-entity-popup-viewissue" class="aui-inline-dialog" style="left: 1340px; top: 178px; display: none;"><div class="contents aui-box-shadow" style="width: 300px;"><form action="#" method="post" class="aui recipients-form share-content-popup  viewissue "><div class="issuenav-permalink padding"><label>Link to Issue</label><input class="permalink text long-field" value="http://localhost:8081/browse/DEMO-5" readonly=""></div><fieldset class="padding"><label for="sharenames-multi-select">User name or email</label><div class="autocomplete-user-target"><div class="jira-multi-select long-field" id="sharenames-multi-select"><textarea autocomplete="off" id="sharenames-textarea" class="text long-field" wrap="off"></textarea><div class="aui-list" id="sharenames-suggestions" tabindex="-1"></div><span class="icon noloading"><span>More</span></span><div class="recipients"><ol></ol></div></div><select id="sharenames" name="sharenames" class="share-user-picker hidden multi-select-select" multiple="multiple" style="display: none;"></select></div><ol class="recipients"></ol><div><label for="note">Note</label></div><textarea class="textarea long-field" id="note" placeholder="Add an optional note"></textarea><div class="button-panel"><div class="status"><div class="icon"></div><div class="progress-messages"></div></div><div class="buttons"><input accesskey="s" title="Press Alt+s to submit this form" class="button submit" type="submit" value="Share" disabled=""> <a accesskey="`" title="Press Alt+` to cancel" class="close-dialog" href="javascript:">Cancel</a></div></div></fieldset></form></div><div id="arrow-share-entity-popup-viewissue" class="arrow" style="position: absolute; left: 189px; right: auto; top: -7px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="#cccccc" d="M0,8L8,0L16,8" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="#cccccc" d="M0,8L8,0L16,8" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></div></div>

@endsection