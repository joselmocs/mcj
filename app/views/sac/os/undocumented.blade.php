@extends('template')

@include('sac/app-menu')

@section('scripts')
{{ HTML::script('/scripts/apps/changelog/index.js') }}
{{ HTML::script('/scripts/apps/os/undocumented.js') }}
@endsection

@section('styles')
{{ HTML::style('/packages/jqueryui/jquery-ui-datepicker-1.10.3.css') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Ordem de Serviço sem Conteúdo de Versão</h1>
            </div>
        </div>
    </header>

    <form class="navigator-search" method="get" action="{{ URL::action('SacOSController@undocumented') }}">
        <div class="search-container" style="float: left;">
            <ul class="criteria-list">
                <li class="text-query">
                    <div class="text-query-container">
                        <input class="search-entry text" type="text" name="de" placeholder="de" value="{{ $context->filters->de }}">
                        <input class="search-entry text" type="text" name="ate" placeholder="ate" value="{{ $context->filters->ate }}">
                    </div>
                </li>
                <li>
                    <button class="aui-button aui-button-subtle search-button tooltip" type="submit" title="Pesquisar">
                        <span class="aui-icon aui-icon-small aui-iconfont-search">Pesquisar</span>
                    </button>
                </li>
            </ul>

        </div>
        <div style="float: right;">
            <a href="{{ URL::action('SacOSController@undocumented') }}" class="aui-button save-as-new-filter" title="Limpar Filtros">Limpar Filtros</a>
        </div>
        <div class="clear"></div>
    </form>

    <div class="results-panel navigator-item box-content">
        <div class="navigator-content">

            @if (!count($context->os))
            <div class="navigator-group">
                <div class="navigator-content empty-results">
                    <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                        <h3>Nenhum resultado correspondente à sua pesquisa</h3>
                        <p class="no-results-hint">Tente modificar seus critérios de pesquisa</p>
                    </div>
                </div>
            </div>
            @else

            <div class="issue-table-info-bar navigator-results">
                <div class="results-count aui-item">
                    <span class="results-count-text">
                        <span class="results-count-total results-count-link">{{ count($context->os) }}</span> resultado(s)
                    </span>
                    <a href="" class="refresh-table tooltip" title="Atualizar resultados">Atualizar</a>
                </div>
            </div>

            <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                <thead>
                <tr>
                    <th style="width: 22px;" class="aui-table-column-unsortable">T</th>
                    <th style="width: 60px;">#</th>
                    <th>Cliente</th>
                    <th>Iniciado em</th>
                    <th>Homologado em</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($context->os as $os)
                <tr>
                    <td class="issuetype tooltip-sw" title="{{ $os->type->name }}">
                        <img src="{{ $os->type->icon }}" />
                    </td>
                    <td>
                        <a href="{{ URL::action('SacOSController@view', array($os->id)) }}">{{ $os->cod }}</a>
                    </td>
                    <td>
                        <a href="{{ URL::action('SacClientController@view', array($os->client->id)) }}">{{ $os->client->nome }}</a>
                    </td>
                    <td>
                        <span title="{{ Carbon::parse($os->created_at)->toDayDateTimeString() }}">
                            <time datetime="{{ Carbon::parse($os->created_at)->format('Y-m-d\TH:i:sO') }}">{{ Carbon::parse($os->created_at)->toFormattedDateString() }}</time>
                        </span>
                    </td>
                    <td>
                        <span title="{{ Carbon::parse($os->ended_at)->toDayDateTimeString() }}">
                            <time datetime="{{ Carbon::parse($os->ended_at)->format('Y-m-d\TH:i:sO') }}">{{ Carbon::parse($os->ended_at)->toFormattedDateString() }}</time>
                        </span>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            @endif
        </div>
    </div>

</section>
@endsection