@extends('template')

@include('sac/app-menu')

@section('scripts')
{{ HTML::script('/scripts/apps/changelog/index.js') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Ordem de Serviço</h1>
            </div>
        </div>
    </header>

    <form class="navigator-search" method="get" action="{{ URL::action('SacOSController@index') }}">
        <div class="search-container" style="float: left;">
            <ul class="criteria-list">
                <li class="text-query">
                    <div class="text-query-container">
                        <input class="search-entry text" type="text" name="cod" placeholder="#" style="width: 80px;" value="{{ $context->filters->cod }}">
                        <input class="search-entry text" type="text" name="tags" placeholder="pesquisar por..." style="width: 300px;" value="{{ $context->filters->tags }}">
                    </div>
                </li>
                <li>
                    <button class="aui-button aui-button-subtle search-button tooltip" type="submit" title="Pesquisar">
                        <span class="aui-icon aui-icon-small aui-iconfont-search">Pesquisar</span>
                    </button>
                </li>
            </ul>

        </div>
        <div style="float: right;">
            <a href="{{ URL::action('SacOSController@index') }}" class="aui-button save-as-new-filter" title="Limpar Filtros">Limpar Filtros</a>
        </div>
        <div class="clear"></div>
    </form>

    <div class="results-panel navigator-item box-content">
        <div class="navigator-content">

            @if (!count($context->os))
            <div class="navigator-group">
                <div class="navigator-content empty-results">
                    <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                        <h3>Nenhum resultado correspondente à sua pesquisa</h3>
                        <p class="no-results-hint">Tente modificar seus critérios de pesquisa</p>
                    </div>
                </div>
            </div>
            @else

            <div class="issue-table-info-bar navigator-results">
                <div class="results-count aui-item">
                    <span class="results-count-text">
                        <span class="results-count-start">{{ $context->os->getFrom() }}</span>–<span class="results-count-end">{{ $context->os->getTo() }}</span> de <span class="results-count-total results-count-link">{{ $context->os->getTotal() }}</span>
                    </span>
                    <a href="" class="refresh-table tooltip" title="Atualizar resultados">Atualizar</a>
                </div>
            </div>

            <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                <thead>
                <tr>
                    <th style="width: 22px;" class="aui-table-column-unsortable">T</th>
                    <th style="width: 60px;">#</th>
                    <th>Resumo</th>
                    <th>Andamento</th>
                    <th style="width: 90px;">Resolução</th>
                    <th style="width: 100px;">Iniciado em</th>
                    <th style="width: 100px;">Homologado em</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($context->os as $os)
                <tr>
                    <td class="issuetype tooltip-sw" title="{{ $os->type->name }}">
                        <img src="{{ $os->type->icon }}" />
                    </td>
                    <td>
                        {{ $os->cod }}
                    </td>
                    <td>
                        <a href="{{ URL::action('SacOSController@view', array($os->id)) }}">
                            @if ($os->summary)
                            {{ $os->summary }}
                            @else
                            Não definido
                            @endif
                        </a>
                    </td>
                    <td>
                        <img src="{{ $os->status->icon }}" height="16" width="16" alt="{{ $os->status->name }}" title="{{ $os->status->name }}">
                        <span>{{ $os->status->name }}</span>
                    </td>
                    <td>
                        @if ($os->resolution == SacOS::RES_NOT_FIXED)
                        <span class="aui-icon aui-icon-error"></span> Não Resolvido
                        @endif
                        @if ($os->resolution == SacOS::RES_FIXED)
                        <span class="aui-icon aui-icon-success"></span> Resolvido
                        @endif
                        @if ($os->resolution == SacOS::RES_DUPLICATED)
                        <span class="aui-icon aui-icon-warning"></span> Duplicado
                        @endif
                    </td>
                    <td>
                        <span title="{{ Carbon::parse($os->created_at)->toDayDateTimeString() }}">
                            <time datetime="{{ Carbon::parse($os->created_at)->format('Y-m-d\TH:i:sO') }}">{{ Carbon::parse($os->created_at)->toFormattedDateString() }}</time>
                        </span>
                    </td>
                    <td>
                        <span title="{{ Carbon::parse($os->ended_at)->toDayDateTimeString() }}">
                            <time datetime="{{ Carbon::parse($os->ended_at)->format('Y-m-d\TH:i:sO') }}">{{ Carbon::parse($os->ended_at)->toFormattedDateString() }}</time>
                        </span>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            <div style="margin-top: 30px;text-align: right;">
                {{ $context->os->links() }}
            </div>

            @endif
        </div>
    </div>

</section>
@endsection