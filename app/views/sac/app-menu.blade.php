@section('app_menu')
<ul class="aui-nav">
    @if (Auth::user()->can('os.item_view'))
    <li>
        <a href="{{ URL::action('SacOSController@index') }}">Ordem de Serviço</a>
    </li>
    @endif
    @if (Auth::user()->can('client.view'))
    <li>
        <a href="" aria-owns="client-drop" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="dropdown2-header2">
            Clientes<span class="aui-icon-dropdown"></span>
        </a>
        <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" id="client-drop" aria-hidden="true">
            <div class="aui-dropdown2-section">
                <ul>
                    <li><a href="{{ URL::action('SacClientController@index') }}">Lista de clientes</a></li>
                    @if (Auth::user()->can('incoming.client'))
                    <li><a href="{{ URL::action('SacClientController@incoming') }}">Em negociação</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </li>
    @endif
    @if (Auth::user()->can('support.monitor_view'))
    <li>
        <a href="" aria-owns="suporte-drop" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="dropdown2-header2">
            Chamados<span class="aui-icon-dropdown"></span>
        </a>
        <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" id="suporte-drop" aria-hidden="true">
            <div class="aui-dropdown2-section">
                <ul>
                    <li><a href="{{ URL::action('SacSupportController@index') }}">Monitor</a></li>
                    <li><a href="{{ URL::action('SacSupportController@history') }}">Histórico</a></li>
                </ul>
            </div>
        </div>
    </li>
    @endif
</ul>
@endsection
