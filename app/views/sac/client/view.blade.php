@extends('template')
@include('sac/app-menu')

@section('styles')
{{ HTML::style('/packages/select2/select2.css') }}
{{ HTML::style('/packages/jqueryui/jquery-ui-datepicker-1.10.3.css') }}
@endsection

@include('templates.ef-form-edit')

@section('scripts')
{{ HTML::script('/scripts/apps/sac/client/view.js') }}
{{ HTML::script('/packages/select2/select2.js') }}
{{ HTML::script('/packages/select2/select2_locale_pt-BR.js') }}

@if (Auth::user()->can('client.edit'))
{{ HTML::script('/scripts/libs/jquery.maskedinput.js') }}
{{ HTML::script('/scripts/apps/sac/client/edit.js') }}
@endif

@endsection

@section('content')
<section id="content" class="body-client">

    <header class="issue-header aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li>
                        <a href="{{ URL::action('SacClientController@index') }}">Clientes</a>
                    </li>
                    @if ($context->client->is_incoming)
                    <li>
                        <a href="{{ URL::action('SacClientController@incoming') }}">Em Negociação</a>
                    </li>
                    @endif
                    <li class="aui-nav-selected">
                        @if ($context->client->is_new())
                        ???
                        @else
                        {{ $context->client->cod }}
                        @endif
                    </li>
                </ol>
                <h1 @if ($context->client->is_new() && Auth::user()->can('client.edit')) id="nome-val" data-action="{{ URL::action('SacClientController@inlineEdit', array($context->client->id)) }}" @endif>
                    {{ $context->client->nome }}
                </h1>
            </div>
        </div>
    </header>


    <nav class="aui-navgroup aui-navgroup-horizontal">
        <div class="aui-navgroup-inner">
            <div class="aui-navgroup-primary">
                <ul class="aui-nav">
                    <li class="aui-nav-selected"><a href="{{ URL::action('SacClientController@view', array($context->client->id)) }}">Cliente</a></li>
                    @if (Auth::user()->can('client.sendmail') && $context->client->contacts()->where('email', '!=', '')->count())
                    <li><a href="{{ URL::action('SacClientController@mail', array($context->client->id)) }}?_token={{ csrf_token() }}">Enviar Email</a></li>
                    @endif
                </ul>
            </div>

            @if (Auth::user()->can('incoming.manage') && !$context->client->is_incoming)
            <div class="aui-navgroup-secondary">
                <ul class="aui-nav">
                    <li>
                        <a href="#dropdown2-nav2" aria-owns="dropdown2-nav2" aria-haspopup="true" class="aui-dropdown2-trigger" data-container="#aui-hnav-example" aria-controls="dropdown2-nav2">
                            <span class="aui-icon aui-icon-small aui-iconfont-configure">Opções</span>
                        </a><!-- .aui-dropdown2 -->
                        <div id="dropdown2-nav2" class="aui-dropdown2 aui-style-default" data-dropdown2-alignment="right" aria-hidden="true">
                            <ul class="aui-list-truncate">
                                <li>
                                    <a href="javascript: void(0)" name="start_incoming" data-client="{{ $context->client->id }}">Iniciar Negociação</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            @endif
        </div>
    </nav>


    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">

            <section class="aui-page-panel-content issue-body-content">

                <div class="mod-content">
                    <ul class="property-list two-cols">

                        <li class="item full-width">
                            <div class="wrap">
                                <strong class="name">COD:</strong>
                                <span class="value">
                                    <strong>
                                        @if ($context->client->is_new())
                                        ???
                                        @else
                                        {{ $context->client->cod }}
                                        @endif
                                    </strong>
                                </span>
                            </div>
                        </li>

                        <li class="item">
                            <div class="wrap">
                                <strong class="name">Situação:</strong>
                                <span class="value">
                                    {{ SacClient::$STATUS[$context->client->situacao] }}
                                </span>
                            </div>
                        </li>

                        @if (!$context->client->is_new() && Auth::user()->can('glossary.view'))
                        <li class="item item-right">
                            <div class="wrap">
                                <strong class="name">Glossário:</strong>
                                <span class="value">
                                    <a href="{{ URL::action('GlossaryController@client_index', array($context->client->cod_cript(), $context->client->cgc_cript())) }}" target="_blank">Acessar</a>
                                </span>
                            </div>
                        </li>
                        @endif

                        <li class="item full-left">
                            <div class="wrap">
                                <strong class="name">CNPJ:</strong>
                                <span class="value" @if ($context->client->is_new() && Auth::user()->can('client.edit')) id="cgc-val" data-mask="99.999.999/9999-99" data-action="{{ URL::action('SacClientController@inlineEdit', array($context->client->id)) }}" @endif>
                                    @if ($context->client->cgc)
                                    {{ $context->client->masked_cgc() }}
                                    @else
                                    Não informado
                                    @endif
                                </span>
                            </div>
                        </li>

                        @if (!$context->client->is_new() && Auth::user()->can('changelog.item_view'))
                        <li class="item item-right">
                            <div class="wrap">
                                <strong class="name">Conteúdo de Versão:</strong>
                                <span class="value">
                                    <a href="{{ URL::action('ChangelogController@client_index', array($context->client->cod_cript(), $context->client->cgc_cript())) }}" target="_blank">Acessar</a>
                                </span>
                            </div>
                        </li>
                        @endif

                        <li class="item full-width">
                            <div class="wrap">
                                <strong class="name">Endereço:</strong>
                                <span class="value @if ($context->client->is_new() && Auth::user()->can('client.edit')) editable-field@endif">
                                    @if ($context->client->address->address)
                                    {{ $context->client->address->address }},
                                    @endif
                                    @if ($context->client->address->number)
                                    {{ $context->client->address->number }},
                                    @endif
                                    @if ($context->client->address->district)
                                    {{ $context->client->address->district }} -
                                    @endif
                                    {{ $context->client->address->city }}
                                    - {{ $context->client->address->uf }}
                                    @if ($context->client->address->zipcode)
                                    /{{ $context->client->address->zipcode }}
                                    @endif
                                    @if ($context->client->is_new() && Auth::user()->can('client.edit'))
                                    <span class="overlay-icon icon icon-edit-sml" id="edit-address" data-client="{{ $context->client->id }}">
                                    @endif
                                </span>
                            </div>
                        </li>

                    </ul>
                </div>

                <div class="module toggle-wrap">
                    <div class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Contatos</h2>
                    </div>
                    <div class="mod-content">
                        @if (!$context->client->contacts()->count())
                        Nenhum contato encontrado.
                        @else
                        <table class="aui aui-table-rowhover issue-table">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Departamento</th>
                                <th>&nbsp;</th>
                                <th class="align-right">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($context->client->contacts as $contact)
                            <tr>
                                <td>{{ $contact->name }}<br /><a href="mailto:{{ $contact->email }}"><span class="email">{{ $contact->email }}</span></a></td>
                                <td>
                                    @if ($contact->department)
                                    {{ $contact->department->name }}
                                    @else
                                    Não definido
                                    @endif
                                </td>
                                <td>
                                    @if ($contact->phone)
                                    <strong>Telefone:</strong> {{ $contact->phone }}<br />
                                    @endif
                                    @if ($contact->mobile)
                                    <strong>Celular:</strong> {{ $contact->mobile }}<br />
                                    @endif
                                    @if ($contact->fax)
                                    <strong>FAX:</strong> {{ $contact->fax }}
                                    @endif
                                </td>
                                <td class="align-right">
                                    <ul class="operations-list">
                                        @if (Auth::user()->can('contact.edit'))
                                        <li><a data-contact="{{ $contact->id }}" name="edit_contact" href="javascript: void(0);">Editar</a></li>
                                        @endif
                                        @if (Auth::user()->can('contact.remove'))
                                        <li><a data-location="{{ URL::action('SacClientController@contact_remove', array($contact->id)) }}" data-name="{{ $contact->name }}" name="remove_contact" href="javascript: void(0);">Remover</a></li>
                                        @endif
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                        <div class="aui-buttons" style="margin-top: 10px;float: right;">
                            @if (Auth::user()->can('contact.create'))
                            <button class="aui-button" name="create_contact" data-client="{{ $context->client->id }}">
                                <span class="aui-icon aui-icon-small aui-iconfont-user">User </span> Novo Contato
                            </button>
                            @endif
                        </div>
                    </div>
                </div>

                @if ($context->client->is_incoming)
                <div class="module toggle-wrap">
                    <div class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Negociações</h2>
                    </div>
                    <div class="mod-content">
                        @if (Auth::user()->can('incoming.view') && $context->client->incoming->contacts()->count())
                        <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                            <thead>
                            <tr>
                                <th style="width: 10px;" class="aui-table-column-unsortable">&nbsp;</th>
                                <th>Realizado por</th>
                                <th>Falou com</th>
                                <th>Situação</th>
                                @if (Auth::user()->can('incoming.text'))
                                <th class="aui-table-column-unsortable">Observações</th>
                                @endif
                                <th>Data do Contato</th>
                                <th class="align-right">Próximo Contato</th>
                                @if (Auth::user()->can('incoming.mail'))
                                <th style="width: 10px;" class="aui-table-column-unsortable">&nbsp;</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($context->client->incoming->contacts as $contact)
                            <tr>
                                <td class="issuetype">
                                    @if ($contact->effected)
                                    <span class="aui-icon aui-icon-success tooltip-sw" title="Contato efetivado">Success</span>
                                    @else
                                    <span class="aui-icon aui-icon-error tooltip-sw" title="Contato NÃO efetivado">Error</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $contact->operator->profile->fullname }}
                                </td>
                                <td>
                                    {{ $contact->contact->name }}
                                </td>
                                <td>
                                    <span style="color: #{{ $contact->status->color }}">{{ $contact->status->name }}</span>
                                </td>
                                @if (Auth::user()->can('incoming.text'))
                                <td>
                                    {{ Str::limit(html_entity_decode($contact->text, ENT_QUOTES, 'utf-8'), 20, '...') }}
                                    <span class="aui-icon aui-icon-small aui-iconfont-share tooltip-sw" style="cursor: pointer;" title="Abrir observações..." name="bt-open-contact" data-id="{{ $contact->id }}" data-contact="{{ $contact->contact->name }}" data-date="{{ Carbon::parse($contact->date_contact)->format('d/m/Y') }}">Open</span>
                                </td>
                                @endif
                                <td>
                                    {{ Carbon::parse($contact->date_contact)->format('d M Y') }}

                                </td>
                                <td class="align-right">
                                    {{ Carbon::parse($contact->next_contact)->format('d M Y') }}
                                </td>
                                @if (Auth::user()->can('incoming.mail'))
                                <td class="align-right aui-compact-button-column">
                                    <button class="aui-button aui-button-subtle aui-button-compact" name="bt_mail_contact" data-contact="{{ $contact->id }}"><span class="aui-icon aui-icon-small aui-iconfont-email">Mail</span></button>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        Nenhuma negociação encontrada.
                        @endif
                        @if ($context->client->is_incoming)
                        @if (Auth::user()->can('incoming.manage'))
                        <div class="aui-buttons" style="margin-top: 10px;float: right;">
                            <button class="aui-button" id="create_incoming" data-incoming="{{ $context->client->incoming->id }}">
                                <span class="aui-icon aui-icon-small aui-iconfont-doc">New </span> Nova Negociação
                            </button>
                        </div>
                        @endif
                        @if (Auth::user()->can('incoming.view'))
                        <div class="aui-buttons" style="margin: 10px 10px 0 0;float: right;">
                            <a class="aui-button" href="{{ URL::action('SacClientController@incoming_view', array($context->client->id)) }}?_token={{ csrf_token() }}" target="_blank">
                                <span class="aui-icon aui-icon-small aui-iconfont-devtools-file-commented">View </span> Visualizar Negociações
                            </a>
                        </div>
                        @endif
                        @endif
                    </div>
                </div>
                @endif

                @if (!$context->client->is_new())
                <div class="module toggle-wrap">
                    <div class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Ordens de Serviço</h2>
                    </div>
                    <div class="mod-content">
                        @if (!$context->client->os()->count())
                        Nenhuma OS encontrada.
                        @else
                        <table class="aui aui-table-rowhover issue-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Resumo</th>
                                <th>Andamento</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($context->client->os as $os)
                            <tr>
                                <td>{{ $os->cod }}</td>
                                <td>
                                    <a href="{{ URL::action('SacOSController@view', array($os->id)) }}">
                                        @if ($os->summary)
                                        {{ $os->summary }}
                                        @else
                                        Não definido
                                        @endif
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ $os->status->icon }}" height="16" width="16" alt="{{ $os->status->name }}" title="{{ $os->status->name }}">
                                    <span>{{ $os->status->name }}</span>
                                </td>
                            @endforeach
                            </tbody>
                        </table>
                        @endif

                    </div>
                </div>
                @endif

            </section>

            <aside class="aui-page-panel-sidebar issue-body-content">

                @if ($context->client->is_incoming)
                <div id="details-module" class="module toggle-wrap">
                    <div id="details-module_heading" class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Cliente em Negociação</h2>
                    </div>
                    <div class="mod-content">
                        <ul class="property-list">

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Responsável:</strong>
                                    <span class="value">
                                        {{ $context->client->incoming->owner->profile->fullname }}
                                    </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Situação:</strong>
                                    <span class="value"
                                    @if (Auth::user()->can('incoming.manage'))
                                      id="incoming-status" data-type="select" data-action="{{ URL::action('SacClientController@incoming_inline', array($context->client->incoming->id)) }}" data-pk="{{ $context->client->incoming->status->id }}"
                                    @endif
                                    style="color: #{{ $context->client->incoming->status->color }}">
                                    {{ $context->client->incoming->status->name }}
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                @endif

                <div id="details-module" class="module toggle-wrap">
                    <div id="details-module_heading" class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Datas</h2>
                    </div>
                    <div class="mod-content">
                        <ul class="property-list">

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Cliente desde:</strong>
                                    <span class="value" id="client_since-val" data-type="datepicker" data-action="{{ URL::action('SacClientController@inlineEdit', array($context->client->id)) }}">
                                        @if ($context->client->client_since)
                                        {{ Carbon::parse($context->client->client_since)->format('d/m/Y') }}
                                        @else
                                        Não Informado
                                        @endif
                                    </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Revisado em:</strong>
                                    <span class="value" id="last_revision-val" data-type="datepicker" data-action="{{ URL::action('SacClientController@inlineEdit', array($context->client->id)) }}">
                                        @if ($context->client->last_revision)
                                        {{ Carbon::parse($context->client->last_revision)->format('d/m/Y') }}
                                        @else
                                        Não Revisado
                                        @endif
                                    </span>
                                </div>
                            </li>

                            <!--
                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Criado:</strong>
                                    <span class="value">
                                        {{ Carbon::parse($context->client->created_at)->toDayDateTimeString() }}
                                    </span>
                                </div>
                            </li>
                            -->

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Atualizado em:</strong>
                                    <span id="type-val" class="value">
                                        {{ Carbon::parse($context->client->updated_at)->toDayDateTimeString() }}
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="details-module" class="module toggle-wrap">
                    <div id="details-module_heading" class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Módulos</h2>
                    </div>
                    <div class="mod-content">
                        @if (!$context->client->modules()->count())
                        NENHUM MÓDULO SELECIONADO
                        @else
                        <table class="aui aui-table-rowhover issue-table">
                            <tbody>
                            @foreach ($context->client->modules as $module)
                            <tr>
                                <td>{{ $module->module->name }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif

                        @if (Auth::user()->can('client.manage_modules'))
                        <div class="aui-toolbar2">
                            <div class="aui-toolbar2-inner">
                                <div class="aui-toolbar2-secondary">
                                    <div class="aui-buttons">
                                        <button class="aui-button" name="edit_modules" data-client="{{ $context->client->id }}">Editar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>

            </aside>
        </div>
    </div>

</section>
@endsection