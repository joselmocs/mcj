@extends('template')
@include('sac/app-menu')

@section('scripts')
{{ HTML::script('/scripts/apps/sac/client/mail.js') }}
@endsection

@section('content')
<section id="content">

    <header class="issue-header aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('SacClientController@index') }}">Clientes</a></li>
                    <li class="aui-nav-selected">{{ $context->client->cod }}</li>
                </ol>
                <h1>{{ $context->client->nome }}</h1>
            </div>
        </div>
    </header>

    <nav class="aui-navgroup aui-navgroup-horizontal">
        <div class="aui-navgroup-inner">
            <div class="aui-navgroup-primary">
                <ul class="aui-nav">
                    <li><a href="{{ URL::action('SacClientController@view', array($context->client->id)) }}">Cliente</a></li>
                    <li class="aui-nav-selected"><a href="{{ URL::action('SacClientController@mail', array($context->client->id)) }}?_token={{ csrf_token() }}">Enviar Email</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">

            <section class="aui-page-panel-content">

                <header class="aui-page-header">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h2>Enviar Email</h2>
                        </div>
                    </div>
                </header>

                @if (!$context->mailserver)
                <div class="aui-message info">
                    <span class="aui-icon icon-info"></span>
                    <p>Não há nenhum servidor de email configurado. Por favor contate um administrador.</p>
                </div>
                @else

                    @if ($context->sent)
                    <p class="item-description">
                        Os emails foram adicionados com sucesso à fila de envio.
                    </p>

                    <form class="aui">
                        <div class="buttons-container buttons-separator">
                            <div class="buttons">
                                <a href="{{ URL::action('SacClientController@view', array($context->client->id)) }}" class="aui-button">OK</a>
                            </div>
                        </div>
                    </form>

                    @else

                    <p class="item-description">
                        Você pode enviar um e-mail para os contatos do cliente a partir do formulário abaixo.
                    </p>

                    <form action="{{ URL::action('SacClientController@mail', array($context->client->id)) }}" method="post" class="aui">

                        <div class="aui-group">
                            <div class="aui-item">

                                <fieldset>
                                    <div class="field-group">
                                        <label for="subject">Assunto<span class="aui-icon icon-required"> required</span></label>
                                        <input class="text long-field" type="text" name="subject" value="{{ $context->form->subject }}" id="subject" />
                                        @if ($context->field_errors && $context->field_errors->has('subject'))
                                        <div class="error">{{ $context->field_errors->first('subject') }}</div>
                                        @endif
                                    </div>

                                    <div class="field-group">
                                        <label for="type">Tipo da Mensagem</label>
                                        <select class="select long-field" name="type" id="type">
                                            <option value="plain" @if ($context->form->type == "plain") selected="selected" @endif>Texto</option>
                                            <option value="html" @if ($context->form->type == "html") selected="selected" @endif>HTML</option>
                                        </select>
                                        @if ($context->field_errors && $context->field_errors->has('type'))
                                        <div class="error">{{ $context->field_errors->first('type') }}</div>
                                        @endif
                                    </div>

                                    <div class="field-group">
                                        <label for="message">Mensagem<span class="aui-icon icon-required"> required</span></label>
                                        <textarea class="textarea long-field" rows="12" name="message" id="message" >{{ $context->form->message }}</textarea>
                                        @if ($context->field_errors && $context->field_errors->has('message'))
                                        <div class="error">{{ $context->field_errors->first('message') }}</div>
                                        @endif
                                    </div>

                                    <div class="field-group">
                                        <label for="mode">Modo de Envio</label>
                                        <select class="select long-field" name="mode" id="mode">
                                            <option data-desc="Os emails serão encaminhados individualmente." value="1" @if ($context->form->mode == "1") selected="selected" @endif>Individual</option>
                                            <option data-desc="Os emails serão enviados em lotes de 20 destinatários." value="2" @if ($context->form->mode == "2") selected="selected" @endif>Com Cópia</option>
                                            <option data-desc="Os emails serão enviados em lotes de 20 destinatários." value="3" @if ($context->form->mode == "3") selected="selected" @endif>Com Cópia Oculta</option>
                                        </select>
                                        @if ($context->field_errors && $context->field_errors->has('mode'))
                                        <div class="error">{{ $context->field_errors->first('mode') }}</div>
                                        @endif
                                        <div class="description"></div>
                                    </div>

                                    <fieldset class="group">

                                        <div class="checkbox">
                                            <input class="checkbox" type="checkbox" name="copy_me" id="copy_me" @if ($context->form->copy_me) checked="checked" @endif />
                                            <label for="copy_me">Quero receber uma cópia ({{ Auth::user()->email }})</label>
                                        </div>
                                    </fieldset>

                                </fieldset>

                                <div class="hidden">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                </div>

                                <div class="buttons-container buttons-separator">
                                    <div class="buttons">
                                        <input type="submit" name="submit" value="Enviar" class="aui-button">
                                        <a href="{{ URL::action('SacClientController@view', array($context->client->id)) }}" class="aui-button aui-button-link">Cancelar</a>
                                    </div>
                                </div>
                            </div>

                            <div class="aui-item">

                                @if ($context->field_errors && $context->field_errors->has('contacts'))
                                <div class="aui-message error">
                                    <p class="title">
                                        <span class="aui-icon icon-error"></span>
                                        <strong>É necessário selecionar pelo menos um destinatário.</strong>
                                    </p>
                                </div>
                                <div class="clear">&nbsp;</div>
                                @endif

                                <div>
                                    [ <a href="javascript: void(0);" name="check_all">Marcar Todos</a> ]
                                    [ <a href="javascript: void(0);" name="uncheck_all">Desmarcar Todos</a> ]
                                    <div class="clear">&nbsp;</div>
                                </div>

                                <table class="aui aui-table-rowhover issue-table">
                                    <tbody>
                                    @foreach ($context->client->contacts as $contact)
                                    @if (!empty($contact->email))
                                    <tr class="tr-module @if (in_array($contact->id, $context->form->contacts)) active @endif">
                                        <td><input class="checkbox" type="checkbox" name="contacts[]" value="{{ $contact->email }}" @if (in_array($contact->id, $context->form->contacts)) checked="checked" @endif></td>
                                        <td>{{ $contact->name }}</td>
                                        <td>{{ $contact->department->name }}</td>
                                        <td>{{ $contact->email }}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>

                    @endif
                @endif

            </section>

        </div>
    </div>

</section>
@endsection