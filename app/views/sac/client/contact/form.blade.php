{{ HTML::script('/scripts/libs/jquery.maskedinput.js') }}

<form @if ($context->contact) action="{{ URL::action('SacClientController@contact_edit', array($context->contact->id)) }}" @else action="{{ URL::action('SacClientController@contact_create') }}" @endif
    class="aui" id="contact-edit" method="post">
    <div class="form-body">
        <div class="field-group">
            <label for="name">Nome<span class="aui-icon icon-required"> required</span></label>
            <input class="text" id="name" maxlength="64" name="name" type="text" value="{{ $context->name }}"/>
            @if ($context->field_errors && $context->field_errors->has('name'))
            <div class="error">{{ $context->field_errors->first('name') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="department">Departamento<span class="aui-icon icon-required"> required</span></label>
            <select class="select" id="department" name="department">
                @foreach($context->departments as $department)
                <option value="{{ $department->id }}" @if ($department->id == $context->department_selected)selected="selected" @endif>{{ $department->name }}</option>
                @endforeach
            </select>
            @if ($context->field_errors && $context->field_errors->has('department'))
            <div class="error">{{ $context->field_errors->first('department') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="email">Email</label>
            <input class="text" id="email" maxlength="256" name="email" type="text" value="{{ $context->email }}"/>
            @if ($context->field_errors && $context->field_errors->has('email'))
            <div class="error">{{ $context->field_errors->first('email') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="phone">Telefone</label>
            <input class="text" id="phone" maxlength="32" name="phone" type="text" value="{{ $context->phone }}"/>
            @if ($context->field_errors && $context->field_errors->has('phone'))
            <div class="error">{{ $context->field_errors->first('phone') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="mobile">Celular</label>
            <input class="text" id="mobile" maxlength="32" name="mobile" type="text" value="{{ $context->mobile }}"/>
            @if ($context->field_errors && $context->field_errors->has('mobile'))
            <div class="error">{{ $context->field_errors->first('mobile') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="fax">FAX</label>
            <input class="text" id="fax" maxlength="32" name="fax" type="text" value="{{ $context->fax }}"/>
            @if ($context->field_errors && $context->field_errors->has('fax'))
            <div class="error">{{ $context->field_errors->first('fax') }}</div>
            @endif
        </div>

        <div class="hidden">
            <input name="client" type="hidden" value="{{ $context->client->id }}">
            <input name="submit" type="hidden" value="true">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>

<script type="text/javascript">
require(['helper/core'], function(core) {
    core.phonemask("#phone");
    core.phonemask("#mobile");
    core.phonemask("#fax");
});
</script>