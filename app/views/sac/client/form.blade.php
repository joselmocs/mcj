<form action="{{ URL::action('SacClientController@create') }}" class="aui" id="client-edit" method="post">
    <div class="form-body">
        <div class="field-group">
            <label for="name">Cliente<span class="aui-icon icon-required"> required</span></label>
            <input class="text" id="name" maxlength="255" name="name" type="text" value="{{ $context->forms->name }}"/>
            @if ($context->field_errors && $context->field_errors->has('name'))
            <div class="error">{{ $context->field_errors->first('name') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="status">Situação<span class="aui-icon icon-required"> required</span></label>
            <select class="select" id="status" name="status">
                <option value=""></option>
                @foreach($context->incoming_status as $status)
                <option value="{{ $status->id }}" @if ($status->id == $context->forms->status)selected="selected" @endif>{{ $status->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-group">
            <label for="cgc">CNPJ</label>
            <input class="text" id="cgc" maxlength="255" name="cgc" type="text" value="{{ $context->forms->cgc }}"/>
            @if ($context->field_errors && $context->field_errors->has('cgc'))
            <div class="error">{{ $context->field_errors->first('cgc') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="address">Endereço</label>
            <input class="text" id="address" maxlength="255" name="address" type="text" value="{{ $context->forms->address }}"/>
            @if ($context->field_errors && $context->field_errors->has('address'))
            <div class="error">{{ $context->field_errors->first('address') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="number">Número</label>
            <input class="text" id="number" maxlength="255" name="number" type="text" value="{{ $context->forms->number }}"/>
            @if ($context->field_errors && $context->field_errors->has('number'))
            <div class="error">{{ $context->field_errors->first('number') }}</div>
            @endif
        </div>


        <div class="field-group">
            <label for="district">Bairro</label>
            <input class="text" id="district" maxlength="255" name="district" type="text" value="{{ $context->forms->district }}"/>
            @if ($context->field_errors && $context->field_errors->has('district'))
            <div class="error">{{ $context->field_errors->first('district') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="city">Cidade<span class="aui-icon icon-required"> required</span></label>
            <input class="text" id="city" maxlength="255" name="city" type="text" value="{{ $context->forms->city }}"/>
            @if ($context->field_errors && $context->field_errors->has('city'))
            <div class="error">{{ $context->field_errors->first('city') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="city">UF<span class="aui-icon icon-required"> required</span></label>
            <input class="text" id="uf" maxlength="255" name="uf" type="text" value="{{ $context->forms->uf }}"/>
            @if ($context->field_errors && $context->field_errors->has('uf'))
            <div class="error">{{ $context->field_errors->first('uf') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="zipcode">CEP</label>
            <input class="text" id="zipcode" maxlength="255" name="zipcode" type="text" value="{{ $context->forms->zipcode }}"/>
            @if ($context->field_errors && $context->field_errors->has('zipcode'))
            <div class="error">{{ $context->field_errors->first('zipcode') }}</div>
            @endif
        </div>

        <!-- // .group -->
        <div class="hidden">
            <input name="submit" type="hidden" value="true">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>

{{ HTML::script('/scripts/libs/jquery.maskedinput.js') }}
<script type="text/javascript">
    require([], function() {
        AJS.$('#cgc').mask('99.999.999/9999-99');
        AJS.$('#zipcode').mask('99999-999');
    });
</script>