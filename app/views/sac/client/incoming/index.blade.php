@extends('template')
@include('sac/app-menu')

@section('styles')
{{ HTML::style('/packages/select2/select2.css') }}
@endsection

@section('scripts')
{{ HTML::script('/packages/select2/select2.js') }}
{{ HTML::script('/packages/select2/select2_locale_pt-BR.js') }}
{{ HTML::script('/scripts/apps/sac/client/index.js') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('SacClientController@index') }}">Clientes</a></li>
                </ol>
                <h1>Lista de Clientes em negociação</h1>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <button class="aui-button" name="new-item">Novo</button>
                </div>
            </div>
        </div>
    </header>

    <form class="navigator-search" name="client-search" method="get" action="{{ URL::action('SacClientController@incoming') }}">
        <input type="hidden" name="all" value="{{ $context->filters->all }}" />
        <div class="search-container" style="float: left;">
            <ul class="criteria-list">
                <li class="text-query">
                    <div class="text-query-container">
                        <input class="search-entry text" type="text" name="name" placeholder="pesquisar por..." style="width: 300px;" value="{{ $context->filters->name }}">
                        <select class="select long-field" id="filter-status" name="status" style="min-width: 340px;">
                            <option value="">Todas as situações</option>
                            @foreach(SacClientIncomingStatus::where('close_incoming', '=', false)->get() as $status)
                            <option value="{{ $status->id }}" @if ($context->filters->status == $status->id)selected="selected" @endif>{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </li>
                <li>
                    <button class="aui-button aui-button-subtle search-button tooltip" type="submit" title="Pesquisar">
                        <span class="aui-icon aui-icon-small aui-iconfont-search">Pesquisar</span>
                    </button>
                </li>
            </ul>

        </div>
        <div style="float: right;">
            <a href="{{ URL::action('SacClientController@incoming') }}" class="aui-button save-as-new-filter" title="Limpar Filtros">Limpar Filtros</a>
        </div>
        <div class="clear"></div>
    </form>

    <div class="results-panel navigator-item box-content">
        <div class="navigator-content">

            @if (!count($context->clients))
            <div class="navigator-group">
                <div class="navigator-content empty-results">
                    <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                        <h3>Nenhum resultado correspondente à sua pesquisa</h3>
                        <p class="no-results-hint">Tente modificar seus critérios de pesquisa</p>
                    </div>
                </div>
            </div>
            @else

            <div class="issue-table-info-bar navigator-results">
                <div class="results-count aui-item">
                    <span class="results-count-text">
                        <span class="results-count-start">{{ $context->clients->getFrom() }}</span>–<span class="results-count-end">{{ $context->clients->getTo() }}</span> de <span class="results-count-total results-count-link">{{ $context->clients->getTotal() }}</span>
                    </span>
                    <a href="" class="refresh-table tooltip" title="Atualizar resultados">Atualizar</a>
                    @if (!$context->filters->all)
                    <a href="javascript: void(0);" name="view_all">Exibir todos</a>
                    @endif
                </div>
            </div>

            <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Cidade</th>
                    <th>CGC</th>
                    <th style="text-align: right;">Estado da Negociação</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($context->clients as $client)
                <tr>
                    <td>
                        <a href="{{ URL::action('SacClientController@view', array($client->id)) }}">{{ $client->nome }}</a>
                    </td>
                    <td>
                        {{ $client->address->city }} - {{ $client->address->uf }}
                    </td>
                    <td>
                        {{ $client->masked_cgc() }}
                    </td>
                    <td style="text-align: right;">
                        <span style="color: #{{ $client->incoming->status->color }}">{{ $client->incoming->status->name }}</span>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            <div style="margin-top: 30px;text-align: right;">
                {{ $context->clients->links() }}
            </div>

            @endif
        </div>
    </div>

</section>
@endsection