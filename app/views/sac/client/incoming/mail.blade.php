<form action="{{ URL::action('SacClientController@contact_mail') }}" class="aui" method="post">
    <div class="form-body">

        <div class="field-group">
            <label for="mails">Emails <span class="aui-icon icon-required"> required</span></label>
            <input class="text long-field" id="mails" name="mails" type="text" value="{{ $context->forms->mails }}"/>
            @if ($context->field_errors && $context->field_errors->has('mails'))
            <div class="error">{{ $context->field_errors->first('mails') }}</div>
            @endif
            <div class="description">Informe emails separados por vírgula</div>
        </div>

        <div class="hidden">
            <input name="contact" type="hidden" value="{{ $context->contact->id }}">
            <input name="submit" type="hidden" value="true">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>
