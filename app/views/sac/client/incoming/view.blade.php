<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=EDGE" http-equiv="X-UA-Compatible">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ Config::get('global.site.title') }}</title>
    <link rel="stylesheet" href="/packages/aui/aui/css/aui-all.css" media="all">
    <!--[if lt IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie.css" media="all"><![endif]-->
    <!--[if IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie9.css" media="all"><![endif]-->
    {{ HTML::style('/styles/aui.custom.css') }}
    <link rel="stylesheet" href="/styles/incoming.view.css" media="all">
</head>

<body class="aui-layout aui-theme-default">

<section id="content" class="body-client">

    <header class="issue-header aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li class="aui-nav-selected">
                        Negociações
                    </li>
                </ol>
                <h1>
                    {{ $context->client->nome }}
                </h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">

            <section class="aui-page-panel-content issue-body-content">

                @foreach ($context->client->incoming->contacts as $contact)
                <table class="aui issue-table compact-table">
                    <thead>
                    <tr>
                        <th style="width: 10px;">&nbsp;</th>
                        <th class="col-date">Data do Contato</th>
                        <th class="col-name">Realizado por</th>
                        <th class="col-name">Falou com</th>
                        <th>Situação</th>
                        <th class="align-right col-date">Próximo Contato</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="issuetype">
                            @if ($contact->effected)
                            <span class="aui-icon aui-icon-success tooltip-sw" title="Contato efetivado">Success</span>
                            @else
                            <span class="aui-icon aui-icon-error tooltip-sw" title="Contato NÃO efetivado">Error</span>
                            @endif
                        </td>
                        <td>
                            {{ Carbon::parse($contact->date_contact)->format('d M Y') }}
                        </td>
                        <td>
                            {{ $contact->operator->profile->fullname }}
                        </td>
                        <td>
                            {{ $contact->contact->name }}
                        </td>
                        <td>
                            <span style="color: #{{ $contact->status->color }}">{{ $contact->status->name }}</span>
                        </td>
                        <td class="align-right">
                            {{ Carbon::parse($contact->next_contact)->format('d M Y') }}
                        </td>
                    </tr>
                    @if (Auth::user()->can('incoming.text'))
                    <tr>
                        <td colspan="6" class="row-desc">
                            {{ nl2br(html_entity_decode($contact->text, ENT_QUOTES, 'utf-8')) }}
                        </td>
                    </tr>
                    @endif
                    </tbody>
                </table>
                <div>&nbsp;<br /></div>
                @endforeach
            </section>
        </div>
    </div>
</section>

<footer>

</footer>

</body>