<form action="{{ URL::action('SacClientController@incoming_start') }}" class="aui" id="incoming-new" method="post">
    <div class="form-body">

        <div class="field-group">
            <label for="status">Situação<span class="aui-icon icon-required"> required</span></label>
            <select class="select long-field" id="status" name="status">
                <option value=""></option>
                @foreach($context->incoming_status as $status)
                <option value="{{ $status->id }}" @if ($status->id == $context->forms->status)selected="selected" @endif>{{ $status->name }}</option>
                @endforeach
            </select>
            @if ($context->field_errors && $context->field_errors->has('status'))
            <div class="error">{{ $context->field_errors->first('status') }}</div>
            @endif
            <div class="description">Estato atual da negociação com o cliente.</div>
        </div>

        <div class="hidden">
            <input name="client" type="hidden" value="{{ $context->client->id }}">
            <input name="submit" type="hidden" value="true">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>