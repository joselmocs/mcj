@extends('template')
@include('sac/app-menu')

@section('styles')
{{ HTML::style('/packages/jqueryui/jquery-ui-datepicker-1.10.3.css') }}
@endsection

@section('scripts')
<script type="text/javascript">
    AJS.$(document).ready(function() {
        AJS.$("input[name=ate]").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: "+7d"
        });
    });
</script>
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Lista de Contatos previstos para os próximos {{ $context->qtd_dias }} dias</h1>
            </div>
        </div>
    </header>

    <form class="navigator-search" method="get" action="{{ URL::action('SacClientController@contacts_next') }}">
        <div class="search-container" style="float: left;">
            <ul class="criteria-list">
                <li class="text-query">
                    <div class="text-query-container">
                        <input class="search-entry text" type="text" name="ate" placeholder="ate" value="{{ $context->filters->ate }}">
                    </div>
                </li>
                <li>
                    <button class="aui-button aui-button-subtle search-button tooltip" type="submit" title="Pesquisar">
                        <span class="aui-icon aui-icon-small aui-iconfont-search">Pesquisar</span>
                    </button>
                </li>
            </ul>

        </div>
        <div style="float: right;">
            <a href="{{ URL::action('SacClientController@contacts_next') }}" class="aui-button save-as-new-filter" title="Limpar Filtros">Limpar Filtros</a>
        </div>
        <div class="clear"></div>
    </form>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <div class="results-panel navigator-item box-content">
                <div class="navigator-content">

                    @if (!count($context->contacts))
                    <div class="navigator-group">
                        <div class="navigator-content empty-results">
                            <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                                <h3>Nenhum resultado correspondente à sua pesquisa</h3>
                                <p class="no-results-hint">Tente modificar seus critérios de pesquisa</p>
                            </div>
                        </div>
                    </div>
                    @else

                    <div class="issue-table-info-bar navigator-results">
                        <div class="results-count aui-item">
                    <span class="results-count-text">
                        <span class="results-count-total results-count-link">{{ count($context->contacts) }}</span> resultado(s)
                    </span>
                            <a href="" class="refresh-table tooltip" title="Atualizar resultados">Atualizar</a>
                        </div>
                    </div>

                    <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                        <thead>
                        <tr>
                            <th>Realizado por</th>
                            <th>Falou com</th>
                            <th>Cliente</th>
                            <th>Data do Contato</th>
                            <th>Próximo Contato</th>
                            <th style="text-align: right;">Situação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($context->contacts as $contact)
                        <tr>
                            <td>
                                {{ $contact->operator->profile->fullname }}
                            </td>
                            <td>
                                {{ $contact->contact->name }}
                            </td>
                            <td>
                                <a href="{{ URL::action('SacClientController@view', array($contact->client->id)) }}">{{ $contact->client->nome }}</a>
                            </td>
                            <td>
                                {{ Carbon::parse($contact->date_contact)->format('d M Y') }}
                            </td>
                            <td>
                                {{ Carbon::parse($contact->next_contact)->format('d M Y') }}
                            </td>
                            <td style="text-align: right;">
                                <span style="color: #{{ $contact->status->color }}">{{ $contact->status->name }}</span>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @endif
                </div>
            </div>

        </div>
    </div>

</section>
@endsection