
{{ HTML::style('/packages/jqueryui/jquery-ui-datepicker-1.10.3.css') }}

<form action="{{ URL::action('SacClientController@incoming_create') }}" class="aui" id="indoming-new" method="post">
    <div class="form-body">

        <div class="field-group">
            <label for="contact">Contato com <span class="aui-icon icon-required"> required</span></label>
            <select class="select long-field" id="contact" name="contact">
                @foreach($context->incoming->client->contacts as $contact)
                <option value="{{ $contact->id }}" @if ($contact->id == $context->forms->contact)selected="selected" @endif>{{ $contact->name }}</option>
                @endforeach
            </select>
            @if ($context->field_errors && $context->field_errors->has('contact'))
            <div class="error">{{ $context->field_errors->first('contact') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="status">Situação<span class="aui-icon icon-required"> required</span></label>
            <select class="select long-field" id="status" name="status">
                <option value=""></option>
                @foreach($context->incoming_status as $status)
                <option value="{{ $status->id }}" @if ($status->id == $context->forms->status)selected="selected" @endif>{{ $status->name }}</option>
                @endforeach
            </select>
            @if ($context->field_errors && $context->field_errors->has('status'))
            <div class="error">{{ $context->field_errors->first('status') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="text">Observações <span class="aui-icon icon-required"> required</span></label>
            <textarea class="textarea long-field" rows="6" id="text" name="text">{{ $context->forms->text }}</textarea>
            @if ($context->field_errors && $context->field_errors->has('text'))
            <div class="error">{{ $context->field_errors->first('text') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="date_contact">Data deste Contato <span class="aui-icon icon-required"> required</span></label>
            <input class="text long-field" id="date_contact" maxlength="32" name="date_contact" type="text" value="{{ $context->forms->date_contact }}"/>
            @if ($context->field_errors && $context->field_errors->has('date_contact'))
            <div class="error">{{ $context->field_errors->first('date_contact') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="next_contact">Próximo Contato <span class="aui-icon icon-required"> required</span></label>
            <input class="text long-field" id="next_contact" maxlength="32" name="next_contact" type="text" value="{{ $context->forms->next_contact }}"/>
            @if ($context->field_errors && $context->field_errors->has('next_contact'))
            <div class="error">{{ $context->field_errors->first('next_contact') }}</div>
            @endif
        </div>

        <div class="field-group">
            <label for="copy_email">Enviar Cópia</label>
            <input class="text long-field" id="copy_email" name="copy_email" type="text" value="{{ $context->forms->copy_email }}"/>
            <div class="description">Informe emails separados por vírgula</div>
            @if ($context->field_errors && $context->field_errors->has('copy_email'))
            <div class="error">{{ $context->field_errors->first('copy_email') }}</div>
            @endif
        </div>

        <fieldset class="group">
            <div class="checkbox">
                <input @if ($context->forms->effected == "on") checked="checked"@endif class="checkbox" id="effected" name="effected" type="checkbox" />
                <label for="effected">Contato efetivado</label>
            </div>
        </fieldset>

        <div class="hidden">
            <input name="incoming" type="hidden" value="{{ $context->incoming->id }}">
            <input name="submit" type="hidden" value="true">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>

<script type="text/javascript">

(function() {
    require([], function() {
        AJS.$("#next_contact").datepicker({ dateFormat: "dd/mm/yy" });
        AJS.$("#date_contact").datepicker({ dateFormat: "dd/mm/yy" });
    });
})();
</script>