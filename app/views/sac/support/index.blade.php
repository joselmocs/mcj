@extends('template')

@include('sac/app-menu')

@section('scripts')
{{ HTML::script('/scripts/apps/sac/support/monitor.js') }}
{{ HTML::script('/scripts/apps/sac/support/chat.js') }}
{{ HTML::script('/scripts/libs/jquery.slimscroll.js') }}
@endsection

@section('page-class')body-chat @endsection

@section('content')
<section id="content">
    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">

            <div class="aui-page-panel-nav">
                <nav class="aui-navgroup aui-navgroup-vertical">
                    <div class="aui-navgroup-inner">
                        <div class="aui-navgroup-primary">
                            <div class="admin-menu-links">

                                <ul class="aui-nav" id="ul-monitor">
                                    <li class="aui-nav-selected">
                                        <a href="javascript: void(0);">Monitor</a>
                                    </li>
                                </ul>
                                <div id="tabs-container">
                                    <div id="ul-audit-{{ Auth::user()->id }}" class="title-container-chat" style="display: none;">
                                        <div class="aui-nav-heading">Meus Chamados</div>
                                        <ul class="aui-nav" name="ul-active-chats"></ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </nav>
            </div>

            <section class="aui-page-panel-content">
                <div style="min-height: 460px;" id="section-content">

                    <div id="section-monitor">
                        <section style="margin-bottom: 40px;">
                            <h2>Monitor de Chamadas</h2>

                            <table class="aui aui-table-rowhover issue-table compact-table">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Descrição</th>
                                    <th style="width: 88px;">Ações</th>
                                    <th style="width: 140px;text-align: right;">Iniciado</th>
                                </tr>
                                </thead>
                                <tbody id="sup-calls-trs"></tbody>
                            </table>
                        </section>

                        <section>
                            <h2>Chamados em Andamento</h2>

                            <table class="aui aui-table-rowhover issue-table compact-table">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Operador</th>
                                    <th>Estado</th>
                                    <th style="width: 100px;text-align: right;">Duração</th>
                                </tr>
                                </thead>
                                <tbody id="sup-monitor-trs"></tbody>
                            </table>
                        </section>
                    </div>


                </div>
            </section>

            <aside class="aui-page-panel-sidebar" style="display: none;">

                <div class="aui-toolbar2">
                    <div class="aui-toolbar-2-inner">
                        <div class="aui-toolbar2-primary">

                        </div>
                        <div class="aui-toolbar2-secondary">
                            <button class="aui-button aui-button-primary" id="end_call">Finalizar Chamado</button>
                        </div>
                    </div>
                </div>
            </aside>

        </div>
    </div>
</section>
@endsection

@section('templates')

<script type="text/x-template" title="ul-audit">
    <div id="ul-audit-{operator_id}" class="title-container-chat">
        <div class="aui-nav-heading">{operator_name}</div>
        <ul class="aui-nav" name="ul-active-chats"></ul>
    </div>
</script>

<script type="text/x-template" title="chat-line">
    <li data-author="{author}">
        <div class="author">{name}</div>
        <div class="time">{time}</div>
        <div class="clear"></div>
        <div class="chat-messages"></div>
    </li>
</script>
<script type="text/x-template" title="chat-container">
    <div name="chat-container" data-hid="{hash_id}" class="chat" data-last="" style="display: none;">
        <h2>{client}</h2>
        <div class="border-new-msg inactive" name="border_start"></div>
        <ul class="messages"></ul>
        <div class="border-new-msg inactive" name="border_end"></div>
        <form class="aui form-reply" name="reply-support">
            <div class="bottom-container">
                <textarea class="textarea long-field" name="message" rows="3" style="max-width: 100% !important;" placeholder="O que você quer dizer?"></textarea>
                <div class="buttons">
                    <input class="aui-button aui-button-primary" type="submit" name="submit" value="Enviar">
                    <a href="#" class="cancel">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</script>
<script type="text/x-template" title="sup-monitor-wreg">
    <tr>
        <td>{client}</td>
        <td>{operator}</td>
        <td>{status}</td>
        <td style="text-align: right;">{duration}</td>
    </tr>
</script>
<script type="text/x-template" title="sup-calls-wreg">
    <tr>
        <td>{client}</td>
        <td>{description}</td>
        <td class="aui-compact-button-column">
            @if (Auth::user()->can('support.answer_call'))
            <button class="aui-button aui-button-compact aui-button-primary" name="answer" data-id="{hash_id}">
                <span class="aui-icon aui-icon-small aui-iconfont-share"></span> Atender
            </button>
            @endif
        </td>
        <td style="text-align: right;">{started_at}</td>
    </tr>
</script>
<script type="text/x-template" title="sup-monitor-woreg">
    <tr>
        <td colspan="4">Nenhum chamado ativo no momento.</td>
    </tr>
</script>
<script type="text/x-template" title="sup-calls-woreg">
    <tr>
        <td colspan="4">Nenhum pedido de chamado no momento.</td>
    </tr>
</script>
<script type="text/x-template" title="list-chat-item">
    <li data-chat-id="{id}" data-chat-client="{client}" data-operator="{operator}">
        <a href="javascript: void(0);">{client} <span class="aui-badge" style="display: none;" data-count="0">0</span></a>
    </li>
</script>
@endsection