@extends('template')

@include('sac/app-menu')

@section('scripts')

@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Histórico de Suporte</h1>
            </div>
        </div>
    </header>

    <form class="navigator-search" method="get" action="{{ URL::action('SacSupportController@history') }}">
        <div class="search-container" style="float: left;">
            <ul class="criteria-list">
                <li class="text-query">
                    <div class="text-query-container">
                        <input class="search-entry text" type="text" name="cod" placeholder="#" style="width: 80px;" value="{{ $context->filters->cod }}">
                        <input class="search-entry text" type="text" name="tags" placeholder="pesquisar por..." style="width: 300px;" value="{{ $context->filters->tags }}">
                    </div>
                </li>
                <li>
                    <button class="aui-button aui-button-subtle search-button tooltip" type="submit" title="Pesquisar">
                        <span class="aui-icon aui-icon-small aui-iconfont-search">Pesquisar</span>
                    </button>
                </li>
            </ul>

        </div>
        <div style="float: right;">
            <a href="{{ URL::action('SacOSController@index') }}" class="aui-button save-as-new-filter" title="Limpar Filtros">Limpar Filtros</a>
        </div>
        <div class="clear"></div>
    </form>

    <div class="results-panel navigator-item box-content">
        <div class="navigator-content">

            @if (!count($context->supports))
            <div class="navigator-group">
                <div class="navigator-content empty-results">
                    <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                        <h3>Nenhum histórico de pesquisa encontrado</h3>
                        <p class="no-results-hint">Tente modificar seus critérios de busca</p>
                    </div>
                </div>
            </div>
            @else

            <div class="issue-table-info-bar navigator-results">
                <div class="results-count aui-item">
                    <span class="results-count-text">
                        <span class="results-count-start">{{ $context->supports->getFrom() }}</span>–<span class="results-count-end">{{ $context->supports->getTo() }}</span> de <span class="results-count-total results-count-link">{{ $context->supports->getTotal() }}</span>
                    </span>
                    <a href="{{ URL::action('SacSupportController@index') }}" class="refresh-table tooltip" title="Atualizar resultados">Atualizar</a>
                </div>
            </div>

            <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Operador</th>
                    <th>Assunto</th>
                    <th style="width: 100px;">Iniciado em</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($context->supports as $support)
                <tr>
                    <td>
                        {{ $support->client->profile->fullname }}
                    </td>
                    <td>
                        {{ $support->operator->profile->fullname }}
                    </td>
                    <td>
                        <a href="{{ URL::action('SacSupportController@history_view', array($support->hash_id())) }}">{{ $support->description }}</a>
                    </td>
                    <td>
                        <span title="{{ Carbon::parse($support->created_at)->toDayDateTimeString() }}">
                            <time datetime="{{ Carbon::parse($support->created_at)->format('Y-m-d\TH:i:sO') }}">{{ Carbon::parse($support->created_at)->toFormattedDateString() }}</time>
                        </span>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            <div style="margin-top: 30px;text-align: right;">
                {{ $context->supports->links() }}
            </div>

            @endif
        </div>
    </div>

</section>
@endsection