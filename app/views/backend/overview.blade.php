@extends('backend/template')

@section('backendContent')
<section class="aui-page-panel-content">
    <div class="aui-group">
        <div class="aui-item">
            <section class="admin-section">
                <h3>Contas de Usuários</h3>
                <ul class="overview-links">
                    <li>
                        <a href="{{ URL::action('BackendUserController@index') }}" class="icon-admin-users">Usuários</a>
                        <span class="description">Crie e edite os detalhes dos utilizadores; altere permissões, senha e grupo.</span>
                    </li>
                    <li>
                        <a href="{{ URL::action('BackendGroupController@index') }}" class="icon-admin-groups">Grupos</a>
                        <span class="description">Visualize os usuários de cada grupo.</span>
                    </li>
                    <li>
                        <a href="{{ URL::action('BackendPermissionController@index') }}" class="icon-admin-permissions">Permissões</a>
                        <span class="description">Visualize os detalhes das permissões concedidas a cada grupos.</span>
                    </li>
                </ul>
            </section>
        </div>
        <div class="aui-item">
            <section class="admin-section">
                <h3>Definições</h3>
                <ul class="overview-links">
                    <li>
                        <a href="{{ URL::action('BackendSystemController@general') }}" class="icon-admin-serversettings">Configurações Gerais</a>
                        <span class="description">Configurações gerais do servidor.</span>
                    </li>
                    <li>
                        <a href="{{ URL::action('BackendMailController@server') }}" class="icon-admin-mailserver">Servidor de e-Mail</a>
                        <span class="description">Configuração SMTP para envio de notificações.</span>
                    </li>
                </ul>
            </section>
        </div>
    </div>
</section>
@endsection