@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/mail.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Enviar Email</h2>
            </div>
        </div>
    </header>
    <p class="item-description">
    @if ($context->sending)
        Sua mensagem será enviada para os seguintes emails:
    @else
        Você pode enviar um e-mail para os usuários a partir do formulário abaixo.
    @endif
    </p>

    @if (count($context->form_errors) > 0)
    <div class="aui-message error">
        <span class="aui-icon icon-error"></span>

        <p>@foreach ($context->form_errors as $e)
            {{ $e }}<br />
            @endforeach
        </p>
    </div>
    @endif

    @if (!count($context->servers))
    <div class="aui-message info">
        <span class="aui-icon icon-info"></span>
        <p>Para enviar um e-mail você precisa <a href="{{ URL::action('BackendMailController@create') }}">configurar</a> um servidor de e-mail.</p>
    </div>
    @else

    @if ($context->sending)

    <form class="aui">
        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="button" name="ok-sending-mail" value="OK" class="aui-button" data-location="{{ URL::action('BackendMailController@send') }}">
            </div>
        </div>
    </form>

    <table class="aui aui-table-sortable aui-table-rowhover">
        <thead>
        <tr>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->sending as $sending)
        <tr>
            <td>
                <span>{{ $sending }}</span>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @else
    <form action="{{ URL::action('BackendMailController@send') }}" method="post" class="aui">
        <fieldset>

            <div class="field-group">
                <label for="type">Enviar para</label>
                <select class="select long-field" name="to" id="to">
                    <option value="users" @if ($context->form->to == "users") selected="selected" @endif>Usuários do Sistema</option>
                    <option value="clients" @if ($context->form->to == "clients") selected="selected" @endif>Clientes</option>
                    <option value="manual" @if ($context->form->to == "manual") selected="selected" @endif>Informar Manualmente</option>
                </select>
                @if ($context->field_errors && $context->field_errors->has('to'))
                <div class="error">{{ $context->field_errors->first('to') }}</div>
                @endif
            </div>

            <div class="field-group group-item users-item" style="display: none;">
                <div class="aui-group">
                    <div class="aui-item">
                        <h2>Grupos de Usuários</h2>
                        <select class="multi-select long-field" size="8" multiple="multiple" id="groups" name="groups[]">
                            @foreach($context->groups as $group)
                            <option value="{{ $group->id }}" @if (in_array($group->id, $context->form->groups)) selected="selected" @endif>{{ $group->name }}</option>
                            @endforeach
                        </select>
                        @if ($context->field_errors && $context->field_errors->has('groups'))
                        <div class="error">{{ $context->field_errors->first('groups') }}</div>
                        @endif
                        <div class="description">Envia uma cópia do email para os usuários dos grupos selecionados.</div>
                        <div class="clear">&nbsp;</div>
                    </div>
                </div>
            </div>

            <div class="field-group group-item clients-item" style="display: none;">
                <div class="aui-group">
                    <div class="aui-item">
                        <h2>Módulos</h2>
                        <select class="multi-select long-field" size="8" multiple="multiple" id="modules" name="modules[]">
                            @foreach($context->modules as $module)
                            <option value="{{ $module->id }}" @if (in_array($module->id, $context->form->modules)) selected="selected" @endif>{{ $module->name }}</option>
                            @endforeach
                        </select>
                        @if ($context->field_errors && $context->field_errors->has('modules'))
                        <div class="error">{{ $context->field_errors->first('modules') }}</div>
                        @endif
                        <div class="description">Envia uma cópia do email para os clientes que tenham os módulos selecionados.</div>
                    </div>

                    <div class="aui-item">
                        <h2>Departamentos</h2>
                        <select class="multi-select long-field" size="8" multiple="multiple" id="departments" name="departments[]">
                            @foreach($context->departments as $department)
                            <option value="{{ $department->id }}" @if (in_array($department->id, $context->form->departments)) selected="selected" @endif>{{ $department->name }}</option>
                            @endforeach
                        </select>
                        @if ($context->field_errors && $context->field_errors->has('departments'))
                        <div class="error">{{ $context->field_errors->first('departments') }}</div>
                        @endif
                        <div class="description">Envia o email apenas para os contatos que pertencem aos departamentos selecionados.</div>
                    </div>
                </div>

                <div class="description"><strong>Nota:</strong> Um usuário receberá o e-mail apenas uma vez, mesmo se ele for membro de mais de uma opção acima.</div>
                <div class="clear">&nbsp;</div>
            </div>

            <div class="field-group group-item manual-item" style="display: none;">
                <div class="aui-group">
                    <div class="aui-item">
                        <h2>Informar Manualmente</h2>
                        <input type="text" class="text long-field" id="manual" name="manual" value="{{ $context->form->manual }}">
                        @if ($context->field_errors && $context->field_errors->has('manual'))
                        <div class="error">{{ $context->field_errors->first('manual') }}</div>
                        @endif
                        <div class="description">Você pode informar mais de um email separando-os por vírgula.</div>
                        <div class="clear">&nbsp;</div>
                    </div>
                </div>
            </div>

            <div class="field-group">
                <label for="subject">Assunto<span class="aui-icon icon-required"> required</span></label>
                <input class="text long-field" type="text" name="subject" value="{{ $context->form->subject }}" id="subject" />
                @if ($context->field_errors && $context->field_errors->has('subject'))
                <div class="error">{{ $context->field_errors->first('subject') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="type">Tipo da Mensagem</label>
                <select class="select long-field" name="type" id="type">
                    <option value="plain" @if ($context->form->type == "plain") selected="selected" @endif>Texto</option>
                    <option value="html" @if ($context->form->type == "html") selected="selected" @endif>HTML</option>
                </select>
                @if ($context->field_errors && $context->field_errors->has('type'))
                <div class="error">{{ $context->field_errors->first('type') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="message">Mensagem<span class="aui-icon icon-required"> required</span></label>
                <textarea class="textarea" rows="12" name="message" id="message" style="max-width: 80%;">{{ $context->form->message }}</textarea>
                @if ($context->field_errors && $context->field_errors->has('message'))
                <div class="error">{{ $context->field_errors->first('message') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="mode">Modo de Envio</label>
                <select class="select long-field" name="mode" id="mode">
                    <option data-desc="Os emails serão encaminhados individualmente." value="1" @if ($context->form->mode == "1") selected="selected" @endif>Individual</option>
                    <option data-desc="Os emails serão enviados em lotes de 20 destinatários." value="2" @if ($context->form->mode == "2") selected="selected" @endif>Com Cópia</option>
                    <option data-desc="Os emails serão enviados em lotes de 20 destinatários." value="3" @if ($context->form->mode == "3") selected="selected" @endif>Com Cópia Oculta</option>
                </select>
                @if ($context->field_errors && $context->field_errors->has('mode'))
                <div class="error">{{ $context->field_errors->first('mode') }}</div>
                @endif
                <div class="description"></div>
            </div>

            <fieldset class="group">

                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="copy_me" id="copy_me" @if ($context->form->copy_me) checked="checked" @endif />
                    <label for="copy_me">Quero receber uma cópia ({{ Auth::user()->email }})</label>
                </div>
            </fieldset>

        </fieldset>

        <div class="hidden">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        </div>

        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="submit" name="submit" value="Enviar" class="aui-button">
                <a href="{{ URL::action('BackendMailController@send') }}" class="aui-button aui-button-link">Cancelar</a>
            </div>
        </div>
    </form>
    @endif
    @endif

</section>
@endsection