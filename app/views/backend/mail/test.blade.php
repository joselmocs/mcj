@extends('backend/template')

@section('backendContent')
<section class="aui-page-panel-content">

    <h3 class="formtitle">Enviar Email</h3>
    <p>Você pode enviar um e-mail de teste aqui.</p>

    <form action="{{ URL::action('BackendMailController@test', array($context->server->id)) }}?_token={{ csrf_token() }}" method="post" class="aui">

        <fieldset>
            <div class="field-group">
                <label for="to">Para<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="to" value="{{ $context->form->to }}" id="to" />
                @if ($context->field_errors && $context->field_errors->has('to'))
                <div class="error">{{ $context->field_errors->first('to') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="subject">Assunto<span class="aui-icon icon-required"> required</span></label>
                <input class="text long-field" type="text" name="subject" value="{{ $context->form->subject }}" id="subject" />
                @if ($context->field_errors && $context->field_errors->has('subject'))
                <div class="error">{{ $context->field_errors->first('subject') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="type">Tipo da Mensagem</label>
                <select class="select" name="type" id="type">
                    <option value="plain" @if ($context->form->type == "plain") selected="selected" @endif>Texto</option>
                    <option value="html" @if ($context->form->type == "html") selected="selected" @endif>HTML</option>
                </select>
                @if ($context->field_errors && $context->field_errors->has('type'))
                <div class="error">{{ $context->field_errors->first('type') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="message">Mensagem<span class="aui-icon icon-required"> required</span></label>
                <textarea class="textarea long-field" rows="8" name="message" id="message">{{ $context->form->message }}</textarea>
                @if ($context->field_errors && $context->field_errors->has('message'))
                <div class="error">{{ $context->field_errors->first('message') }}</div>
                @endif
            </div>

        </fieldset>

        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="submit" name="submit" value="Enviar" class="aui-button">
                <a href="{{ URL::action('BackendMailController@server') }}" class="aui-button aui-button-link">Cancelar</a>
            </div>
        </div>
    </form>
</section>

<aside class="aui-page-panel-sidebar">
    <h3>Log</h3>
    <form class="aui">
    <textarea class="textarea long" style="max-width:100%;font-size:12px;min-height:352px;">{{ $context->log }}</textarea>
    <div class="description">Log dos eventos do envio de e-mail.</div>
    </form>

</aside>

@endsection