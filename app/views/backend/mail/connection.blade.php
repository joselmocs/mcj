<div class="aui-message {{ $context->result }}">
    <span class="aui-icon icon-{{ $context->result }}"></span>
    <p>{{ $context->message }}</p>
</div>