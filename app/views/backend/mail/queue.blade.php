@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/mail.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Fila de e-Mail</h2>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a id="create_user" class="aui-button" href="javascript: window.location = '';">
                        <span class="aui-icon aui-icon-small aui-iconfont-build"></span> Atualizar
                    </a>
                </div>
            </div>
        </div>
    </header>
    <p class="item-description">Esta página mostra como está a fila de espera/erros de envio de emails.</p>

    <div class="aui-tabs horizontal-tabs" style="margin-top: 30px;">
        <ul class="tabs-menu">
            <li class="menu-item active-tab">
                <a href="#tab-waiting">
                    <strong>Fila de Emails
                        @if (count($context->waiting))
                        <span class="aui-lozenge aui-lozenge-subtle aui-lozenge-info">{{ count($context->waiting) }}</span>
                        @endif
                    </strong>
                </a>
            </li>
            <li class="menu-item">
                <a href="#tab-error">
                    <strong>Falhas no Envio
                        @if (count($context->error))
                        <span class="aui-lozenge aui-lozenge-inline aui-lozenge-error">{{ count($context->error) }}</span>
                        @endif
                    </strong>
                </a>
            </li>
        </ul>

        <div class="tabs-pane active-pane" id="tab-waiting">
            <table class="aui aui-table-rowhover">
                <thead>
                <tr>
                    <th>Assunto</th>
                    <th width="26%">Email</th>
                    <th width="24%">Em fila desde</th>
                </tr>
                </thead>
                @if (count($context->waiting))
                <tbody>
                @foreach($context->waiting as $mail)
                <tr>
                    <td>{{ $mail->queue->subject }}</td>
                    <td>{{ $mail->email }}</td>
                    <td>{{ Carbon::parse($mail->queue->created_at)->toDayDateTimeString() }}</td>
                </tr>
                @endforeach
                </tbody>
                @else
                <tbody>
                <tr>
                    <td colspan="3">Atualmente não há itens para ser enviados.</td>
                </tr>
                </tbody>
                @endif
            </table>
        </div>

        <div class="tabs-pane" id="tab-error">

            <button class="aui-button aui-button-subtle to-right" name="repair" data-location="{{ URL::action('BackendMailController@repair') }}?_token={{ csrf_token() }}">
                <span class="aui-icon aui-icon-small aui-iconfont-share">Configure</span> Reenviar
            </button>

            <table class="aui aui-table-rowhover">
                <thead>
                <tr>
                    <th>Assunto</th>
                    <th width="26%">Email</th>
                    <th width="24%">Em fila desde</th>
                </tr>
                </thead>
                @if (count($context->error))
                    <tbody>
                    @foreach($context->error as $mail)
                    <tr>
                        <td>{{ $mail->queue->subject }}</td>
                        <td>{{ $mail->email }}</td>
                        <td>{{ Carbon::parse($mail->queue->created_at)->toDayDateTimeString() }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                @else
                <tbody>
                <tr>
                    <td colspan="3">Atualmente não houve nenhuma falha nos envios.</td>
                </tr>
                </tbody>
                @endif
            </table>
        </div>
    </div>

</section>
@endsection