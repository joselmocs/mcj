@extends('backend/template')

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Servidor de e-Mail</h2>
            </div>
        </div>
    </header>
    <p class="item-description">Quando habilitado e configurado, o sistema será capaz de enviar e-mails para os usuários.</p>

    <div id="smtp-mail-servers-panel">
        <h3 class="formtitle">Servidor SMTP</h3>
        @if (count($context->servers))
            <p>A tabela abaixo mostra o servidor de email SMTP configurado para o sistema.</p>

        <table class="aui aui-table-rowhover">
            <thead>
            <tr>
                <th width="28%">Nome</th>
                <th>Detalhes</th>
                <th width="30%">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($context->servers as $server)
            <tr>
                <td>
                    <strong>{{ $server->name }}</strong>
                    <div class="description">{{ $server->description }}</div>
                </td>
                <td>
                    <ul class="item-details">
                        <li>
                            <dl>
                                <dt>From:</dt>
                                <dd>{{ $server->from }}</dd>
                            </dl>
                            <dl>
                                <dt>Prefix:</dt>
                                <dd>{{ $server->prefix }}</dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>Host:</dt>
                                <dd>{{ $server->host_name }}</dd>
                            </dl>
                            <dl>
                                <dt>SMTP Port:</dt>
                                <dd>{{ $server->host_port }}</dd>
                            </dl>
                            <dl>
                                <dt>Username:</dt>
                                <dd>{{ $server->host_username }}</dd>
                            </dl>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="operations-list">
                        <li>
                            <a href="{{ URL::action('BackendMailController@edit', array($server->id)) }}?_token={{ csrf_token() }}">Editar</a>
                        </li>
                        <li>
                            <a href="{{ URL::action('BackendMailController@delete', array($server->id)) }}?_token={{ csrf_token() }}">Excluir</a>
                        </li>
                        <li>
                            <a href="{{ URL::action('BackendMailController@test', array($server->id)) }}?_token={{ csrf_token() }}">Enviar Email de Teste</a>
                        </li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

        @else
        <div class="aui-message info">
            <span class="aui-icon icon-info"></span>Você ainda não tem um servidor SMTP configurado.
        </div>
        @endif

        <div class="buttons-container aui-toolbar2 noprint">
            <a class="aui-button" id="add-new-smtp-server" href="{{ URL::action('BackendMailController@create') }}">Configurar novo servidor SMTP</a>
        </div>

    </div>
</section>
@endsection