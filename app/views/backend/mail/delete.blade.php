@extends('backend/template')

@section('backendContent')
<section class="aui-page-panel-content">

    <form action="{{ URL::action('BackendMailController@delete', array($context->server->id)) }}" method="post" class="aui">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <h3 class="formtitle">Excluir Servidor de e-Mail</h3>
        <p>Tem certeza de que deseja excluir <b>{{ $context->server->name }}</b>?</p>

        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="submit" name="submit" value="Excluir" class="aui-button">
                <a href="{{ URL::action('BackendMailController@server') }}" class="aui-button aui-button-link">Cancelar</a>
            </div>
        </div>
    </form>

</section>
@endsection