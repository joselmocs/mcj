@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/users.js') }}
{{ HTML::script('/scripts/apps/backend/groups.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ul class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('BackendGroupController@index') }}">Grupos</a></li>
                </ul>
                <h2>{{ $context->group->name }}</h2>
            </div>
        </div>
    </header>

    @if (!$context->group->can("sys_admin"))
    <div class="module" style="margin-top: 50px;">
        <form action="{{ URL::action('BackendPermissionController@permission_set', array($context->group->tag)) }}" class="aui" method="post" name="set_permission">
            <h3>Adicionar Permissão</h3>
            <fieldset>
                <div class="field-group">
                    <label for="permission">Permissão</label>
                    <select class="select" name="selected-permission" style="max-width: 100% !important;">
                        <option value="-1">Por favor, selecione uma permissão</option>
                        @foreach ($context->another_scopes as $id => $scope)
                        @if (count($scope['permissions']))
                        <optgroup label="{{ $scope['name'] }}">
                            @foreach ($scope['permissions'] as $permission)
                            @if (Auth::user()->can('sys_admin') || $permission->tag != "sys_admin")
                            <option value="{{ $permission->id }}" data-description="{{ $permission->description }}" data-name="{{ $permission->name }}">{{ $scope['name'] }}: {{ $permission->name }}</option>
                            @endif
                            @endforeach
                        </optgroup>
                        @endif
                        @endforeach
                    </select>
                </div>
            </fieldset>
            <div class="buttons-container">
                <div class="buttons">
                    <input class="button submit" type="button" value="Adicionar" name="bt_set_permission">
                </div>
            </div>
            <div class="hidden">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </div>
        </form>
    </div>
    @endif

    @foreach ($context->group_scopes as $scope)
    <table class="aui aui-table-rowhover" style="margin-top: 30px;">
        <thead>
        <tr>
            <th><h2>{{ $scope['name'] }}</h2></th>
            <th width="10%" class="align-right">Ações</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($scope['permissions'] as $id => $permission)
            <tr>
                <td>
                    <b>{{ $permission->name }}</b>
                    <div class="description">{{ $permission->description }}</div>
                </td>
                <td class="align-right">
                    @if (Auth::user()->can('sys_admin') || $permission->tag != "sys_admin")
                    <a href="javascript: void(0)" data-group="{{ $context->group->name }}" data-permission="{{ $scope['name'] }}: {{ $permission->name }}" data-redirect="{{ URL::action('BackendPermissionController@permission_unset', array($context->group->tag, $permission->tag)) }}?_token={{ csrf_token() }}" name="unset-group-permission">Remover</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endforeach

</section>

@endsection