<form action="{{ URL::action('BackendUserController@group') }}" class="aui top-label" id="user-group" method="post">
    <div class="form-body">
        <div class="form-body" style="max-height: 271px;">
            <div id="userGroupPicker" class="aui-group">
                <div class="aui-item">
                    <div class="field-group">
                        <label for="groupsToJoin">Grupos Disponíveis</label>
                        @if (count($context->all_groups) == count($context->user->roles))
                        <div class="aui-message info">
                            <span class="aui-icon icon-info"></span>Este usuário é um membro de todos os grupos.
                        </div>
                        @else
                        <select id="groupsToJoin" name="groupsToJoin[]" class="select full-width-field" multiple="multiple" size="10">
                            @foreach ($context->all_groups as $group)

                            @if (Auth::user()->can('sys_admin') || !$group->can("sys_admin"))
                            @if (!$context->user->is($group->tag))
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                            @endif
                            @endif

                            @endforeach
                        </select>
                        <p>
                            <input class="aui-button" name="join" type="submit" value="Participar dos grupos selecionados">
                        </p>
                        @endif
                    </div>
                </div>
                <div class="aui-item">
                    <div class="field-group">
                        <label for="groupsToLeave">Grupos Atuais</label>
                        @if (!count($context->user->roles))
                        <div class="aui-message info">
                            <span class="aui-icon icon-info"></span>Este usuário não é membro de nenhum grupo.
                        </div>
                        @else
                        <select id="groupsToLeave" name="groupsToLeave[]" class="select full-width-field" multiple="multiple" size="10">
                            @foreach ($context->user->roles as $group)
                            @if (Auth::user()->can('sys_admin') || !$group->can("sys_admin"))
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                            @endif
                            @endforeach
                        </select>
                        <p>
                            <input class="aui-button" name="leave" type="submit" value="Sair dos grupos selecionados">
                        </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // .group -->
    <div class="hidden">
        <input name="submit" type="hidden" value="true">
        <input name="username" type="hidden" value="{{ $context->user->username }}"/>
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    </div>
</form>