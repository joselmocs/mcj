<form action="{{ URL::action('BackendUserController@edit') }}" class="aui" id="user-edit" method="post">
    <div class="form-body">
        @if (count($context->form_errors))
        <div class="aui-message error">
            <span class="aui-icon icon-error"></span>
            <p>
                @foreach($context->form_errors as $error)
                {{ $error }}<br />
                @endforeach
            </p>
        </div>
        @endif
        <div class="field-group">
            <label for="user-edit-username">Usuário<span class="aui-icon icon-required"> required</span></label>
            <input class="text " id="user-edit-username" maxlength="255" name="username" type="text" value="{{ $context->username }}"/>
            @if ($context->field_errors && $context->field_errors->has('username'))
            <div class="error">{{ $context->field_errors->first('username') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="user-edit-fullName">Nome Completo<span class="aui-icon icon-required"> required</span></label>
            <input class="text " id="user-edit-fullName" maxlength="255" name="fullname" type="text" value="{{ $context->fullname }}"/>
            @if ($context->field_errors && $context->field_errors->has('fullname'))
            <div class="error">{{ $context->field_errors->first('fullname') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label for="user-edit-email">Email<span class="aui-icon icon-required"> required</span></label>
            <input class="text " id="user-edit-email" maxlength="255" name="email" type="text" value="{{ $context->email }}"/>
            @if ($context->field_errors && $context->field_errors->has('email'))
            <div class="error">{{ $context->field_errors->first('email') }}</div>
            @endif
        </div>
        <fieldset class="group ">
            <div class="checkbox">
                <input @if (!$context->disabled) checked="checked"@endif class="checkbox" id="user-edit-active" name="active" type="checkbox" />
                <label for="user-edit-active">Ativo</label>
            </div>
        </fieldset>
        <!-- // .group -->
        <div class="hidden">
            <input type="hidden" name="redirect" value="{{ $context->redirect }}" />
            <input name="submit" type="hidden" value="true">
            <input name="user_id" type="hidden" value="{{ $context->user->id }}"/>
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>