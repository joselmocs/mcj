@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/users.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ul class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('BackendUserController@index') }}">Usuários</a></li>
                </ul>
                <h2>{{ $context->user->profile->fullname }} @if ($context->user->disabled) (Inativo) @endif</h2>
            </div>
            @if (Auth::user()->can('sys_admin') || !$context->user->can('sys_admin'))
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a href="#" class="aui-dropdown2-trigger aui-button" aria-owns="user-edit-options" aria-haspopup="true" aria-controls="user-edit-options">
                        <span class="aui-icon aui-icon-small aui-iconfont-user"></span>
                        Ações
                    </a>
                </div>
                <span></span>
                <div class="aui-dropdown2 aui-style-default" id="user-edit-options">
                    <div class="aui-dropdown2-section">
                        <ul data-redirect="1" data-user="{{ $context->user->username }}" data-id="{{ $context->user->id }}" data-fullname="{{ $context->user->profile->fullname }}">
                            <li><a name="edituser_link" href="#">Editar Perfil</a></li>
                            <li><a name="editpass_link" href="#">Definir Senha</a></li>
                        </ul>
                    </div>
                    <div class="aui-dropdown2-section">
                        <ul data-user="{{ $context->user->username }}" data-id="{{ $context->user->id }}" data-fullname="{{ $context->user->profile->fullname }}">
                            <li><a name="editgroups_link" href="#">Editar Grupos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </header>

    <div class="module vcard" id="viewUserDetails">
        <div class="mod-header">
            <h3>Informações da Conta</h3>
        </div>
        <div class="mod-content">
            <div class="aui-group">
                <div class="aui-item">
                    <ul class="item-details">
                        <li data-userdata-group="user-details">
                            <dl data-userdata-row="username">
                                <dt>Nome de Usuário:</dt>
                                <dd id="username">{{ strtolower($context->user->username) }}</dd>
                            </dl>
                            <dl data-userdata-row="fullname">
                                <dt>Nome Completo:</dt>
                                <dd id="displayName" class="fn">{{ $context->user->profile->fullname }}</dd>
                            </dl>
                            <dl data-userdata-row="email">
                                <dt>Email:</dt>
                                <dd><a class="email" href="mailto:{{ $context->user->email }}">{{ $context->user->email }}</a></dd>
                            </dl>
                        </li>
                        <li data-userdata-row="user-directory">
                            <dl data-userdata-row="groups">
                                <dt>Grupos:</dt>
                                <dd id="groups" class="user-group-info">
                                    @if (count($context->user->roles))
                                    <ul class="aui-nav">
                                        @foreach ($context->user->roles as $role)
                                        <li>{{ $role->name }}</li>
                                        @endforeach
                                    </ul>
                                    @else
                                    <div class="aui-message warning"><span class="aui-icon icon-warning"></span>
                                        <p>
                                            Este usuário não pertence a nenhum grupo. Isto significa que o utilizador não será capaz de entrar no sistema até que seja adicionado a um grupo.
                                        </p>
                                    </div>
                                    @endif
                                </dd>
                            </dl>
                        </li>
                    </ul>
                </div>
                <div class="aui-item">
                    <ul class="item-details">
                        <li data-userdata-group="login-details">
                            <dl data-userdata-row="login-count">
                                <dt>Total de Acessos:</dt>
                                <dd id="loginCount">
                                    @if ($context->user->profile->count_login)
                                        {{ $context->user->profile->count_login }}
                                    @else
                                        Não registrado
                                    @endif
                                </dd>
                            </dl>
                            <dl data-userdata-row="last-login">
                                <dt>Último Acesso:</dt>
                                <dd id="lastLogin">
                                    @if ($context->user->profile->count_login)
                                        {{ Carbon::parse($context->user->profile->last_login)->toDayDateTimeString() }}
                                    @else
                                        Não registrado
                                    @endif
                                </dd>
                            </dl>
                            <dl data-userdata-row="previous-login">
                                <dt>Acesso Anterior:</dt>
                                <dd id="previousLogin">
                                    @if ($context->user->profile->count_login > 1)
                                        {{ Carbon::parse($context->user->profile->previous_login)->toDayDateTimeString() }}
                                    @else
                                        Não registrado
                                    @endif
                                </dd>
                            </dl>
                            <dl data-userdata-row="last-failed-login">
                                <dt>Última Falha de Acesso:</dt>
                                <dd id="lastFailedLogin">
                                    @if ($context->user->profile->count_login_failed)
                                    {{ Carbon::parse($context->user->profile->last_login_failed)->toDayDateTimeString() }}
                                    @else
                                        Não registrado
                                    @endif
                                </dd>
                            </dl>
                            <dl data-userdata-row="total-failed-login-count">
                                <dt>Total de Falhas de Acesso:</dt>
                                <dd id="totalFailedLoginCount">
                                    @if ($context->user->profile->count_login_failed)
                                        {{ $context->user->profile->count_login_failed }}
                                    @else
                                        Não registrado
                                    @endif
                                </dd>
                            </dl>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection