<div class="jira-dialog-content">
    <form action="{{ URL::action('BackendUserController@create') }}" class="aui" method="post">
        <div class="form-body">
            <div class="field-group">
                <label for="user-create-username">Nome de Usuário<span class="aui-icon icon-required"> required</span></label>
                <input class="text " id="user-create-username" maxlength="255" name="username" type="text" value="{{ $context->username }}"/>
                @if ($context->field_errors && $context->field_errors->has('username'))
                <div class="error">{{ $context->field_errors->first('username') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="user-create-password">Senha<span class="aui-icon icon-required"> required</span></label>
                <input autocomplete="off" class="text " id="user-create-password" name="password" type="password" value="{{ $context->password }}"/>
                @if ($context->field_errors && $context->field_errors->has('password'))
                <div class="error">{{ $context->field_errors->first('password') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="user-create-confirm">Confirme<span class="aui-icon icon-required"> required</span></label>
                <input autocomplete="off" class="text " id="user-create-confirm" name="confirm" type="password"/>
                @if ($context->field_errors && $context->field_errors->has('confirm'))
                <div class="error">{{ $context->field_errors->first('confirm') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="user-create-fullname">Nome Completo<span class="aui-icon icon-required"> required</span></label>
                <input class="text " id="user-create-fullname" maxlength="255" name="fullname" type="text" value="{{ $context->fullname }}" />
                @if ($context->field_errors && $context->field_errors->has('fullname'))
                <div class="error">{{ $context->field_errors->first('fullname') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="user-create-email">Email<span class="aui-icon icon-required"> required</span></label>
                <input class="text " id="user-create-email" maxlength="255" name="email" type="text" value="{{ $context->email }}" />
                @if ($context->field_errors && $context->field_errors->has('email'))
                <div class="error">{{ $context->field_errors->first('email') }}</div>
                @endif
            </div>
            <fieldset class="group" style="display: none;">
                <div class="checkbox">
                    <input class="checkbox" id="user-create-sendEmail" name="sendEmail" type="checkbox" value="true" @if ($context->sendEmail == "on")checked="checked"@endif/>
                    <label for="user-create-sendEmail">Enviar notificação por email</label>
                    <div class="description">
                        Enviar um e-mail para o usuário que você acabou de criar, o que lhes permitirá criar a sua própria senha.
                    </div>
                </div>
            </fieldset>
            <div class="hidden">
                <input name="submit" type="hidden" value="true">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
            </div>
        </div>
    </form>
    <!-- // .aui #user-create -->
</div>