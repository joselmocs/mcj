@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/users.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Usuários</h2>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a id="create_user" class="aui-button" href="#" title="Cria um novo usuário">
                        <span class="aui-icon aui-icon-small aui-iconfont-add"></span> Usuário
                    </a>
                </div>
            </div>
        </div>
    </header>

    <form action="{{ URL::action('BackendUserController@index') }}" class="aui top-label" name="users_filter" method="get">
        <div class="form-body">
            <h3>Filtrar Usuário</h3>
            <div class="aui-group">
                <div class="aui-item">
                    <div class="field-group">
                        <label for="user-filter-userNameFilter">Nome de Usuário</label>
                        <input class="text full-width-field" maxlength="255" name="userNameFilter" type="text" value="{{ $context->filters->userNameFilter }}">
                    </div>
                </div>
                <div class="aui-item">
                    <div class="field-group">
                        <label for="user-filter-fullNameFilter">Nome</label>
                        <input class="text full-width-field" maxlength="255" name="fullNameFilter" type="text" value="{{ $context->filters->fullNameFilter }}">
                    </div>
                </div>
                <div class="aui-item">
                    <div class="field-group">
                        <label for="user-filter-emailFilter">Email</label>
                        <input class="text full-width-field" maxlength="255" name="emailFilter" type="text" value="{{ $context->filters->emailFilter }}">
                    </div>
                </div>
                <div class="aui-item">
                    <div class="field-group">
                        <label for="user-filter-group">Grupo</label>
                        <select class="select full-width-field" name="group">
                            <option value="">Qualquer</option>
                            @foreach ($context->roles as $role)
                            <option value="{{ $role->id }}" @if ($context->filters->group == $role->id)selected="selected"@endif>{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="aui-item">
                    <div class="field-group">
                        <label for="user-filter-usersPerPage">Por Página</label>
                        <select class="select full-width-field" name="max">
                            @foreach ($context->paginate as $key => $value)
                            <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttons-container form-footer">
            <div class="buttons">
                <input class="aui-button" name="" title="" type="submit" value="Filtrar">
                <a class="aui-button aui-button-link cancel" href="{{ URL::action('BackendUserController@index') }}">limpar filtros</a>
            </div>
        </div>
    </form>

    <div class="aui-group count-pagination">
        <div class="results-count aui-item">
            Exibindo <strong>{{ $context->users->getFrom() }}</strong>
            a <strong>{{ $context->users->getTo() }}</strong>
            de <strong>{{ $context->users->getTotal() }}</strong> usuário(s).
        </div>
    </div>

    <table class="aui aui-table-rowhover">
        <thead>
            <tr>
                <th>Usuário</th>
                <th>Nome Completo</th>
                <th class="minNoWrap">Info. de Acesso</th>
                <th>Grupos</th>
                <th class="minNoWrap align-right">Operações</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($context->users as $user)
        <tr data-user="{{ $user->username }}">
            <td data-cell-type="username">
                <div>
                    <a class="user-hover user-avatar" style="background-image:url('/images/useravatar/avatar_small.png');" href="{{ URL::action('BackendUserController@view', array(strtolower($user->username))) }}">
                        <span class="username">
                            @if ($user->disabled)
                            <del>{{ strtolower($user->username) }}</del><br />
                            (Inativo)
                            @else
                            {{ strtolower($user->username) }}
                            @endif
                        </span>
                    </a>
                </div>
            </td>
            <td data-cell-type="fullname">
                <span class="fn">
                    @if ($user->disabled)
                    <del>{{ $user->profile->fullname }}</del>
                    @else
                    {{ $user->profile->fullname }}
                    @endif
                </span><br>
                <a href="mailto:{{ $user->email }}"><span class="email">{{ $user->email }}</span></a>
            </td>
            <td data-cell-type="login-details" class="minNoWrap">
            @if ($user->profile->count_login > 0)
                <strong>Acessos:</strong> {{ $user->profile->count_login }}<br />
                <strong>Último:</strong> {{ Carbon::parse($user->profile->last_login)->toDayDateTimeString() }}
            @else
                Não registrado
            @endif
            </td>
            <td data-cell-type="user-groups">
                <ul>
                    @foreach ($user->roles()->orderBy('level', 'desc')->get() as $group)
                    <li><a href="{{ URL::action('BackendGroupController@view', array($group->tag)) }}">{{ $group->name }}</a></li>
                    @endforeach
                </ul>
            </td>
            <td data-cell-type="operations" class="align-right">
                @if (Auth::user()->can('sys_admin') || !$user->can('sys_admin'))
                <ul class="operations-list" data-user="{{ $user->username }}" data-id="{{ $user->id }}" data-fullname="{{ $user->profile->fullname }}">
                    <li><a name="editgroups_link" href="javascript: void(0);">Grupos</a></li>
                    <li><a name="edituser_link" href="javascript: void(0);">Editar</a></li>
                </ul>
                @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <div class="aui-group count-pagination">
        <div class="aui-item align-right">
            <div>{{ $context->users->links() }}</div>
        </div>
    </div>

</section>
@endsection