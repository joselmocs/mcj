<form action="{{ URL::action('BackendUserController@delete') }}" class="aui" id="user-delete" method="post">
    <div class="aui-message warning">
        <span class="aui-icon icon-warning"></span>
        <p>
            Você está prestes a excluir o usuário '{{ $context->user->username }}'. Essa ação não pode ser desfeita.
        </p>
    </div>
    <!-- // .group -->
    <div class="hidden">
        <input name="submit" type="hidden" value="true">
        <input name="username" type="hidden" value="{{ $context->user->username }}"/>
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    </div>
</form>