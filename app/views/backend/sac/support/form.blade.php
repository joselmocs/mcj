<div class="jira-dialog-content">
    <form action="{{ URL::action('BackendSacSupportController@subject_save') }}" class="aui" method="post">
        <div class="form-body">
            <div class="field-group">
                <label for="create-name">Assunto<span class="aui-icon icon-required"> required</span></label>
                <input class="text " id="create-subject" maxlength="255" name="subject" type="text" value="{{ $context->subject }}"/>
                @if ($context->field_errors && $context->field_errors->has('subject'))
                <div class="error">{{ $context->field_errors->first('subject') }}</div>
                @endif
            </div>
            <div class="hidden">
                <input name="id" type="hidden" value="{{ $context->id }}">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
            </div>
        </div>
    </form>
</div>