@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/subject.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Assuntos de Interesse</h2>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a id="create_subject" class="aui-button" href="javascript: void(0)" title="Cria um novo assunto" data-form="{{ URL::action('BackendSacSupportController@subject_create') }}">
                        <span class="aui-icon aui-icon-small aui-iconfont-add"></span> Assunto
                    </a>
                </div>
            </div>
        </div>
    </header>

    <table class="aui aui-table-rowhover compact-table">
        <thead>
        <tr>
            <th>Assunto</th>
            <th class="align-right aui-table-column-unsortable"></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->subjects as $subject)
        <tr>
            <td>{{ $subject->subject }}</td>
            <td class="align-right aui-compact-button-column">
                <button aria-owns="dropdown-actions-{{ $subject->id }}" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" aria-controls="dropdown-actions-{{ $subject->id }}">
                    <span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span>
                </button>
                <div id="dropdown-actions-{{ $subject->id }}" class="aui-dropdown2 aui-style-default" aria-hidden="true" data-dropdown2-alignment="right">
                    <ul class="aui-list-truncate">
                        <li>
                            <a href="javascript:void(0);" data-form="{{ URL::action('BackendSacSupportController@subject_edit', array($subject->id), false) }}" name="edit">Editar</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-location="{{ URL::action('BackendSacSupportController@subject_remove', array($subject->id), false) }}" data-subject="{{ $subject->subject }}" name="remove">Remover</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</section>
@endsection