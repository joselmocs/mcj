@extends('backend/template')

@section('backendContent')
<section class="aui-page-panel-content">

    @if (count($context->form_errors) > 0)
    <div class="aui-message error">
        <span class="aui-icon icon-error"></span>
        <p>@foreach ($context->form_errors as $e)
            {{ $e }}<br />
        @endforeach
        </p>
    </div>
    @endif

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Configurações Gerais</h2>
            </div>
        </div>
    </header>

    <p class="item-description">Use esta página para editar configurações avançadas do sistema. Qualquer alteração feita aqui pode afetar a integração com o Sagwin.</p>

    <form action="{{ URL::action('BackendSystemController@general') }}" method="post" class="aui">
        <div class="hidden">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        </div>
        <fieldset>
            <div class="field-group">
                <label for="subject">IP Sagwin Firebird<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="ip_sagwin_firebird" value="{{ $context->form->ip_sagwin_firebird }}" />
                @if ($context->field_errors && $context->field_errors->has('ip_sagwin_firebird'))
                <div class="error">{{ $context->field_errors->first('ip_sagwin_firebird') }}</div>
                @endif
                <div class="description">IP externo do banco de dados do Sagwin.</div>
            </div>
        </fieldset>
        <fieldset>
            <div class="field-group">
                <label for="subject">Caminho GDB <span class="aui-icon icon-required"> required</span></label>
                <input class="text long-field" type="text" name="path_sagwin_firebird" value="{{ $context->form->path_sagwin_firebird }}" />
                @if ($context->field_errors && $context->field_errors->has('path_sagwin_firebird'))
                <div class="error">{{ $context->field_errors->first('path_sagwin_firebird') }}</div>
                @endif
                <div class="description">Caminho até o banco de dados do Sagwin.</div>
            </div>
        </fieldset>
        <fieldset>
            <div class="field-group">
                <label for="subject">PHP Firebird <span class="aui-icon icon-required"> required</span></label>
                <input class="text long-field" type="text" name="php_firebird_extension_path" value="{{ $context->form->php_firebird_extension_path }}" />
                @if ($context->field_errors && $context->field_errors->has('php_firebird_extension_path'))
                <div class="error">{{ $context->field_errors->first('php_firebird_extension_path') }}</div>
                @endif
                <div class="description">Caminho até o diretório do PHP que contém a extenção Firebird</div>
            </div>
        </fieldset>
        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="submit" name="submit" value="Salvar alterações" class="aui-button">
                <a href="{{ URL::action('BackendSystemController@general') }}" class="aui-button aui-button-link">Cancelar</a>
            </div>
        </div>
    </form>
</section>
@endsection