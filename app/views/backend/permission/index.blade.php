@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/users.js') }}
{{ HTML::script('/scripts/apps/backend/groups.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">
    @foreach ($context->permissions as $scope)
    <table class="aui aui-table-rowhover" style="margin-top: 30px;">
        <thead>
        <tr>
            <th><h2>{{ $scope->name }}</h2></th>
            <th style="width: 260px;">Grupos</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($scope->permissions as $permission)
        <tr>
            <td><b>{{ $permission->name }}</b>
                <div class="description">{{ $permission->description }}</div>
            </td>
            <td>
                <ul>
                    @foreach($permission->roles as $group)
                    <li>{{ $group->name }}
                        @if (Auth::user()->can('sys_admin') || !$group->can("sys_admin"))
                        (<a href="javascript: void(0);" data-group="{{ $group->name }}" data-permission="{{ $scope->name }}: {{ $permission->name }}" data-redirect="{{ URL::action('BackendPermissionController@permission_unset', array($group->tag, $permission->tag)) }}?_token={{ csrf_token() }}" name="unset-group-permission">Remover</a>)
                        @endif
                    </li>
                    @endforeach
                </ul>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @endforeach
</section>
@endsection