@extends('template')

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Administração</h1>
            </div><!-- .aui-page-header-main -->
        </div><!-- .aui-page-header-inner -->
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">

            <div class="aui-page-panel-nav">
                <nav class="aui-navgroup aui-navgroup-vertical">
                    <div class="aui-navgroup-inner">
                        <div class="aui-navgroup-primary">
                            <div class="admin-menu-links">
                                <ul class="aui-nav">
                                    <li @if ($context->active_menu == 'overview')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendController@overview') }}">Visão Geral</a>
                                    </li>
                                </ul>
                                <div class="aui-nav-heading">Contas de Usuários</div>
                                <ul class="aui-nav">
                                    <li @if ($context->active_menu == 'user.users')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendUserController@index') }}">Usuários</a>
                                    </li>
                                    <li @if ($context->active_menu == 'user.groups')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendGroupController@index') }}">Grupos</a>
                                    </li>
                                    <li @if ($context->active_menu == 'user.permissions')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendPermissionController@index') }}">Permissões</a>
                                    </li>
                                </ul>
                                <div class="aui-nav-heading">CLIENTES</div>
                                <ul class="aui-nav">
                                    <li @if ($context->active_menu == 'client.department')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendClientController@departments') }}">Departamentos</a>
                                    </li>
                                    <li @if ($context->active_menu == 'client.mailgroup')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendClientController@mailgroups') }}">Grupos de e-Mail</a>
                                    </li>
                                    <li @if ($context->active_menu == 'client.incoming.status')class="aui-nav-selected"@endif>
                                    <a href="{{ URL::action('BackendClientController@incoming_status') }}">Status de Negociação</a>
                                    </li>
                                </ul>
                                <div class="aui-nav-heading">Chamados</div>
                                <ul class="aui-nav">
                                    <li @if ($context->active_menu == 'support.subjects')class="aui-nav-selected"@endif>
                                    <a href="{{ URL::action('BackendSacSupportController@subjects') }}">Assuntos de Interesse</a>
                                    </li>
                                </ul>
                                <div class="aui-nav-heading">EMAIL</div>
                                <ul class="aui-nav">
                                    <li @if ($context->active_menu == 'mail.send')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendMailController@send') }}">Enviar e-Mail</a>
                                    </li>
                                    <li @if ($context->active_menu == 'mail.server')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendMailController@server') }}">Servidor de e-Mail</a>
                                    </li>
                                    <li @if ($context->active_menu == 'mail.queue')class="aui-nav-selected"@endif>
                                        <a href="{{ URL::action('BackendMailController@queue') }}">Fila de e-Mail</a>
                                    </li>
                                </ul>
                                <div class="aui-nav-heading">Definições</div>
                                <ul class="aui-nav">
                                    <li @if ($context->active_menu == 'system.general')class="aui-nav-selected"@endif>
                                    <a href="{{ URL::action('BackendSystemController@general') }}">Configurações Gerais</a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                    </div>
                </nav>
            </div><!-- .aui-page-panel-nav -->
            @yield('backendContent')
            <!-- .aui-page-panel-content -->
        </div>
    </div>
</section>
@endsection
