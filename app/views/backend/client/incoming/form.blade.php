{{ HTML::style('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/ui-lightness/jquery-ui.css') }}
{{ HTML::style('/packages/colorpick/jquery.colorpicker.css') }}

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js"></script>
<script src="/packages/colorpick/jquery.colorpicker.js"></script>
<script src="/packages/colorpick/jquery.ui.colorpicker-pt-br.js"></script>
<script src="/packages/colorpick/jquery.ui.colorpicker-pantone.js"></script>
<script src="/packages/colorpick/jquery.ui.colorpicker-rgbslider.js"></script>
<script src="/packages/colorpick/jquery.ui.colorpicker-memory.js"></script>
<script src="/packages/colorpick/jquery.ui.colorpicker-cmyk-parser.js"></script>
<script src="/packages/colorpick/jquery.ui.colorpicker-cmyk-percentage-parser.js"></script>

<div class="jira-dialog-content">
    <form action="{{ URL::action('BackendClientController@incoming_status_save') }}" class="aui" method="post">
        <div class="form-body">
            <div class="field-group">
                <label for="create-name">Nome<span class="aui-icon icon-required"> required</span></label>
                <input class="text " id="create-name" maxlength="255" name="name" type="text" value="{{ $context->name }}"/>
                @if ($context->field_errors && $context->field_errors->has('name'))
                <div class="error">{{ $context->field_errors->first('name') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="create-description">Descrição</label>
                <input class="text " id="create-description" name="description" type="text" value="{{ $context->description }}"/>
                @if ($context->field_errors && $context->field_errors->has('description'))
                <div class="error">{{ $context->field_errors->first('description') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="create-color">Cor</label>
                <input class="text " id="create-color" name="color" type="text" value="{{ $context->color }}"/>
                @if ($context->field_errors && $context->field_errors->has('color'))
                <div class="error">{{ $context->field_errors->first('color') }}</div>
                @endif
            </div>
            <fieldset class="group">
                <div class="checkbox">
                    <input @if ($context->close_incoming == "on") checked="checked"@endif class="checkbox" id="close_incoming" name="close_incoming" type="checkbox" />
                    <label for="close_incoming">Fechar a negociação com este status.</label>
                </div>
            </fieldset>
            <div class="hidden">
                <input name="id" type="hidden" value="{{ $context->id }}">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    (function() {
        require([], function() {
            AJS.$('#create-color').colorpicker();
        });
    })();
</script>