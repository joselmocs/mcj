@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/incoming.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Status de Negociação</h2>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a id="create_incoming_status" class="aui-button" href="javascript: void(0)" title="Cria um novo departamento" data-form="{{ URL::action('BackendClientController@incoming_status_create') }}">
                        <span class="aui-icon aui-icon-small aui-iconfont-add"></span> Status de Negociação
                    </a>
                </div>
            </div>
        </div>
    </header>

    @if (!count($context->status))
    <div class="navigator-group">
        <div class="navigator-content empty-results">
            <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                <h3>Nenhum status encontrado</h3>
                <p class="no-results-hint">&nbsp;</p>
            </div>
        </div>
    </div>
    @else
    <table class="aui aui-table-sortable aui-table-rowhover compact-table">
        <thead>
        <tr>
            <th>Nome</th>
            <th class="aui-table-column-unsortable">Descrição</th>
            <th style="width: 120px;">Fecha negociação</th>
            <th style="width: 60px;" class="align-right aui-table-column-unsortable"></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->status as $status)
        <tr>
            <td>
                <span style="color: #{{ $status->color }}">{{ $status->name }}</span>
            </td>
            <td>
                <span style="color: #{{ $status->color }}">{{ $status->description }}</span>
            </td>
            <td>
                @if ($status->close_incoming)
                Sim
                @else
                Não
                @endif
            </td>
            <td class="align-right aui-compact-button-column">
                <button aria-owns="dropdown-actions-{{ $status->id }}" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" aria-controls="dropdown-actions-{{ $status->id }}">
                    <span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span>
                </button>
                <div id="dropdown-actions-{{ $status->id }}" class="aui-dropdown2 aui-style-default" aria-hidden="true" data-dropdown2-alignment="right">
                    <ul class="aui-list-truncate">
                        <li>
                            <a href="javascript:void(0);" data-form="{{ URL::action('BackendClientController@incoming_status_edit', array($status->id), false) }}" name="edit">Editar</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-location="{{ URL::action('BackendClientController@incoming_status_remove', array($status->id), false) }}" data-name="{{ $status->name }}" name="remove">Remover</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @endif

</section>
@endsection