@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/departments.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Departamentos</h2>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a id="create_department" class="aui-button" href="javascript: void(0)" title="Cria um novo departamento" data-form="{{ URL::action('BackendClientController@department_create') }}">
                        <span class="aui-icon aui-icon-small aui-iconfont-add"></span> Departamento
                    </a>
                </div>
            </div>
        </div>
    </header>

    @if (!count($context->departments))
    <div class="navigator-group">
        <div class="navigator-content empty-results">
            <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                <h3>Nenhum departamento encontrado</h3>
                <p class="no-results-hint">&nbsp;</p>
            </div>
        </div>
    </div>
    @else
    <table class="aui aui-table-sortable aui-table-rowhover compact-table">
        <thead>
        <tr>
            <th style="width: 60px;">#</th>
            <th>Nome</th>
            <th class="aui-table-column-unsortable">Descrição</th>
            <th class="align-right aui-table-column-unsortable"></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->departments as $department)
        <tr>
            <td>
                {{ $department->id }}
            </td>
            <td>
                {{ $department->name }}
            </td>
            <td>
                {{ $department->description }}
            </td>
            <td class="align-right aui-compact-button-column">
                <button aria-owns="dropdown-actions-{{ $department->id }}" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" aria-controls="dropdown-actions-{{ $department->id }}">
                    <span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span>
                </button>
                <div id="dropdown-actions-{{ $department->id }}" class="aui-dropdown2 aui-style-default" aria-hidden="true" data-dropdown2-alignment="right">
                    <ul class="aui-list-truncate">
                        <li>
                            <a href="javascript:void(0);" data-form="{{ URL::action('BackendClientController@department_edit', array($department->id), false) }}" name="edit">Editar</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-location="{{ URL::action('BackendClientController@department_remove', array($department->id), false) }}" data-name="{{ $department->name }}" name="remove">Remover</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @endif

</section>
@endsection