<div class="jira-dialog-content">
    <form action="{{ URL::action('BackendClientController@department_save') }}" class="aui" method="post">
        <div class="form-body">
            <div class="field-group">
                <label for="create-name">Nome<span class="aui-icon icon-required"> required</span></label>
                <input class="text " id="create-name" maxlength="255" name="name" type="text" value="{{ $context->name }}"/>
                @if ($context->field_errors && $context->field_errors->has('name'))
                <div class="error">{{ $context->field_errors->first('name') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label for="create-description">Descrição</label>
                <input class="text " id="create-description" name="description" type="text" value="{{ $context->description }}"/>
                @if ($context->field_errors && $context->field_errors->has('description'))
                <div class="error">{{ $context->field_errors->first('description') }}</div>
                @endif
            </div>
            <div class="hidden">
                <input name="id" type="hidden" value="{{ $context->id }}">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
            </div>
        </div>
    </form>
</div>