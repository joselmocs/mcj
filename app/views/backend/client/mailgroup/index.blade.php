@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/mailgroups.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Grupos de Email</h2>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a id="create_mailgroup" class="aui-button" href="javascript: void(0)" title="Cria um novo departamento" data-form="{{ URL::action('BackendClientController@mailgroup_create', array(), false) }}">
                        <span class="aui-icon aui-icon-small aui-iconfont-add"></span> Grupo de Email
                    </a>
                </div>
            </div>
        </div>
    </header>

    @if (!count($context->mailgroups))
    <div class="navigator-group">
        <div class="navigator-content empty-results">
            <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                <h3>Nenhum grupo de email encontrado</h3>
                <p class="no-results-hint">&nbsp;</p>
            </div>
        </div>
    </div>
    @else
    <table class="aui aui-table-sortable aui-table-rowhover compact-table">
        <thead>
        <tr>
            <th style="width: 60px;">#</th>
            <th>Nome</th>
            <th class="aui-table-column-unsortable">Descrição</th>
            <th class="align-right aui-table-column-unsortable"></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->mailgroups as $mailgroup)
        <tr>
            <td>
                {{ $mailgroup->id }}
            </td>
            <td>
                {{ $mailgroup->name }}
            </td>
            <td>
                {{ $mailgroup->description }}
            </td>
            <td class="align-right aui-compact-button-column">
                <button aria-owns="dropdown-actions-{{ $mailgroup->id }}" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" aria-controls="dropdown-actions-{{ $mailgroup->id }}">
                    <span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span>
                </button>
                <div id="dropdown-actions-{{ $mailgroup->id }}" class="aui-dropdown2 aui-style-default" aria-hidden="true" data-dropdown2-alignment="right">
                    <ul class="aui-list-truncate">
                        <li>
                            <a href="javascript:void(0);" data-form="{{ URL::action('BackendClientController@mailgroup_edit', array($mailgroup->id), false) }}" name="edit">Editar</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-location="{{ URL::action('BackendClientController@mailgroup_remove', array($mailgroup->id), false) }}" data-name="{{ $mailgroup->name }}" name="remove">Remover</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @endif

</section>
@endsection