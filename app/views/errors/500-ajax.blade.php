<div class="aui-message error">
    <p class="title">
        <strong>Opa, parece que algo deu errado.</strong>
    </p>
    <p>Ocorreu uma falha na requisição, por favor contate um administrador.</p>
    <p>Você pode <a href="{{ URL::previous() }}">Voltar</a> ou ir para a <a href="/">Página Inicial</a></p>
    <span class="aui-icon icon-error"></span>
</div>