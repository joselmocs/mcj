<div class="aui-message error">
    <p class="title">
        <strong>Sem Permissão!</strong>
    </p>
    <p>Você não tem permissão suficiente para executar esta ação.</p>
    <p>Você pode <a href="{{ URL::previous() }}">Voltar</a> ou ir para a <a href="/">Página Inicial</a></p>
    <span class="aui-icon icon-error"></span>
</div>