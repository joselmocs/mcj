@extends('template')

@section('content')
<section id="content">
    <div class="aui-page-panel center">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <header class="aui-page-header">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h1>XSRF Security Token Missing</h1>
                        </div><!-- .aui-page-header-main -->
                    </div><!-- .aui-page-header-inner -->
                </header>
                <div class="aui-message warning"><span class="aui-icon icon-warning"></span>
                    <p>Your browser has not provided any parameters at all and your input has been lost.  Press back now to try and resend the data.  This is most likely caused by a bug in the browser.  FireFox 3.6.0 have been known to exhibit this behaviour.</p>
                    <p>We could not complete this action due to a missing form token.</p>
                    <p>You may have cleared your browser cookies, which could have resulted in the expiry of your current form token. A new form token has been reissued.</p>
                    <p>
                        Request URL: {{ URL::getRequest()->getPathInfo() }}
                    </p>
                </div>
                <div class="aui-message">
                    <p>Você pode <a href="{{ URL::previous() }}">Voltar</a> ou ir para a <a href="/">Página Inicial</a></p>
                </div>

            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection