<div class="aui-message error">
    <p class="title">
        <strong>Página não encontrada</strong>
    </p>
    <p>Não foi possível encontrar o que você estava procurando.</p>
    <p>Você pode <a href="{{ URL::previous() }}">Voltar</a> ou ir para a <a href="/">Página Inicial</a></p>
    <span class="aui-icon icon-error"></span>
</div>