@extends('template')

@section('content')
<section id="content">
    <div class="aui-page-panel center">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <header>
                    <h1>Você agora está desconectado</h1>
                </header>
                <div class="aui-message info">
                    <span class="aui-icon icon-info"></span>
                    <p class="title">
                        <strong>Qualquer login automático também foi interrompido.</strong>
                    </p>
                    <p>Não quis sair? <a href="{{ URL::action('UserController@get_login') }}">Entre novamente</a>.</p>
                </div>

            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection