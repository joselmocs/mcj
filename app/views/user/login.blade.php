@extends('template')

@section('scripts')
{{ HTML::script('/scripts/apps/user.login.js') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>{{ Config::get('global.site.slogan') }}</h1>
            </div><!-- .aui-page-header-main -->
        </div><!-- .aui-page-header-inner -->
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <div class="aui-group">
                    <div class="aui-item">
                        <div class="gadget">
                            <div class="item-header"><h3 class="title">Introdução</h3></div>
                            <div class="item-content">Bem vindo à MCJ</div>
                        </div>
                    </div>
                    <div class="aui-item">
                        <div class="gadget" id="gadget-form-login">
                            <div class="item-header"><h3 class="title">Entrar</h3></div>
                            <div class="item-content"></div>
                        </div>
                    </div>
                </div>
            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection