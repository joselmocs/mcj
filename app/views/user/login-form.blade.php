<div id="inline-login">
    @if (count($context->form_errors) > 0)
    <div class="aui-message error">
        <p class="title">
            <span class="aui-icon icon-error"></span>
            <strong>Falha na autenticação</strong>
        </p>
        <p>@foreach ($context->form_errors as $e)
            {{ $e }}<br />
            @endforeach
        </p>
    </div>
    @endif
    <form method="post" action="{{ URL::action('UserController@post_login') }}" class="aui">
        <div class="hidden">
            <input type="hidden" name="returnUrl" value="{{ URL::action('DashboardController@index') }}" />
        </div>
        <div class="field-group">
            <label accesskey="u" for="login-form-username" id="usernamelabel">Usuário</label>
            <input class="text medium-field" name="username" type="text" value="{{ $context->username }}">
            @if ($context->field_errors && $context->field_errors->has('username'))
            <div class="error">{{ $context->field_errors->first('username') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label accesskey="p" id="passwordlabel">Senha</label>
            <input class="text medium-field" name="password" type="password">
            @if ($context->field_errors && $context->field_errors->has('password'))
            <div class="error">{{ $context->field_errors->first('password') }}</div>
            @endif
        </div>
        <fieldset class="group">
            <div class="checkbox" id="rememberme">
                <input id="login-form-remember-me" class="checkbox" name="remember" type="checkbox" @if ($context->remember == "on")checked="checked"@endif>
                <label for="login-form-remember-me">Lembrar o meu login neste computador</label>
            </div>
        </fieldset>
        <div class="field-group">
            <div class="buttons">
                <input class="button" id="login" type="submit" value="Entrar">
                <a class="cancel" href="#">Não consegue acessar sua conta?</a>
            </div>
        </div>
        <div class="hidden">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"
        </div>
    </form>
    <script type="text/javascript">
    require(["apps/user.login-inline"], function(a) {
        a.load();
    });
    </script>
</div>