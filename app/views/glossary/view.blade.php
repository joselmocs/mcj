@extends('template')

@section('styles')
{{ HTML::style('/packages/redactor/css/redactor.css') }}
{{ HTML::style('/packages/select2/select2.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/changelog/index.js') }}
{{ HTML::script('/scripts/apps/glossary/index.js') }}
@if (Auth::user()->can('glossary.item_edit'))
    {{ HTML::script('/packages/redactor/js/fontcolor.js') }}
    {{ HTML::script('/packages/redactor/js/redactor.js') }}
    {{ HTML::script('/packages/redactor/js/langs/pt_br.js') }}
    {{ HTML::script('/packages/select2/select2.js') }}
    {{ HTML::script('/packages/select2/select2_locale_pt-BR.js') }}
    {{ HTML::script('/scripts/libs/jquery.maskedinput.js') }}
    {{ HTML::script('/scripts/apps/glossary/edit.js') }}
@endif
@endsection

@include('glossary.menu')
@include('templates.ef-form-edit')

@section('content')
<section id="content" class="">
    <header class="issue-header aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('GlossaryController@index') }}">Glossário</a></li>
                </ol>
                <h1 id="function-val" data-action="{{ URL::action('GlossaryController@inlineEdit', array($context->glossary->id)) }}">
                    @if ($context->glossary->function)
                    {{ $context->glossary->function }}
                    @else
                    Clique para adicionar uma função
                    @endif
                </h1>
            </div>

            @if (Auth::user()->can('glossary.item_remove'))
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <button class="aui-button" name="remove-item" data-item="{{ $context->glossary->id }}">Remover</button>
                </div>
            </div>
            @endif
        </div>
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content issue-body-content">
                <div id="container-message"></div>
                <div class="aui-group issue-body">

                    <div class="aui-item issue-main-column">

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Descrição</h2>
                            </div>
                            <div class="mod-content">
                                <div id="description-val" data-type="textarea" data-action="{{ URL::action('GlossaryController@inlineEdit', array($context->glossary->id)) }}">
                                @if (!empty($context->glossary->description))
                                    {{ html_entity_decode($context->glossary->description, ENT_QUOTES, 'utf-8') }}
                                @else
                                    Clique para editar
                                @endif
                                </div>
                            </div>
                        </div>

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Resultado Esperado</h2>
                            </div>
                            <div class="mod-content">
                                <div id="result-val" data-type="textarea" data-action="{{ URL::action('GlossaryController@inlineEdit', array($context->glossary->id)) }}">
                                    @if (!empty($context->glossary->result))
                                        {{ html_entity_decode($context->glossary->result, ENT_QUOTES, 'utf-8') }}
                                    @else
                                        Clique para editar
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Ação</h2>
                            </div>
                            <div class="mod-content">
                                <div id="action-val" data-type="textarea" data-action="{{ URL::action('GlossaryController@inlineEdit', array($context->glossary->id)) }}">
                                    @if (!empty($context->glossary->action))
                                        {{ html_entity_decode($context->glossary->action, ENT_QUOTES, 'utf-8') }}
                                    @else
                                        Clique para editar
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Observação</h2>
                            </div>
                            <div class="mod-content">
                                <div id="observation-val" data-type="textarea" data-action="{{ URL::action('GlossaryController@inlineEdit', array($context->glossary->id)) }}">
                                    @if (!empty($context->glossary->observation))
                                        {{ html_entity_decode($context->glossary->observation, ENT_QUOTES, 'utf-8') }}
                                    @else
                                        Clique para editar
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="aui-item issue-side-column" id="viewissuesidebar">


                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Detalhes</h2>
                            </div>
                            <div class="mod-content">
                                <ul class="property-list">

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Adicionado por:</strong>
                                            <span id="type-val" class="value">
                                                {{ $context->glossary->user->profile->fullname }}
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Módulo:</strong>
                                            <span class="value" id="module-val" data-type="select" data-action="{{ URL::action('GlossaryController@inlineEdit', array($context->glossary->id)) }}"
                                                @if ($context->glossary->module)
                                                    data-pk="{{ $context->glossary->module->id }}">
                                                    {{ $context->glossary->module->name }}
                                                @else
                                                    data-pk="">
                                                    Módulo não definido
                                                @endif
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Classificação:</strong>

                                            <span class="value" id="classification-val" data-type="select" data-action="{{ URL::action('GlossaryController@inlineEdit', array($context->glossary->id)) }}"
                                                @if ($context->glossary->classification)
                                                    data-pk="{{ $context->glossary->classification }}">
                                                    {{ $context->classifications[$context->glossary->classification]['name'] }}
                                                @else
                                                    data-pk="">
                                                    Nenhuma
                                                @endif
                                            </span>
                                        </div>
                                    </li>

                                    @if (Auth::user()->can('admin'))
                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Tempo Gasto:</strong>
                                            <span id="type-val" class="value">
                                                {{ $context->glossary->timespend }} minuto(s)
                                            </span>
                                        </div>
                                    </li>
                                    @endif

                                </ul>
                            </div>
                        </div>

                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Datas</h2>
                            </div>
                            <div class="mod-content">
                                <ul class="property-list">

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Criado:</strong>
                                            <span id="type-val" class="value">
                                                {{ Carbon::parse($context->glossary->created_at)->toDayDateTimeString() }}
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Atualizado:</strong>
                                            <span id="type-val" class="value">
                                                {{ Carbon::parse($context->glossary->updated_at)->toDayDateTimeString() }}
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Permissão de acesso por Cliente</h2>
                            </div>
                            <div class="mod-content">

                                @if (!$context->glossary->free_access && Auth::user()->can('glossary.access_permission'))
                                <form action="{{ URL::action('GlossaryController@client_access_create', array($context->glossary->id)) }}?_token={{ csrf_token() }}" method="post" class="aui" style="margin-bottom: 10px;">
                                    <div style="width: 80%;float: left;">
                                        <input type="text" id="client" name="client" style="width: 100%;">
                                    </div>
                                    <div style="width: 59px;float: right;">
                                        <button class="aui-button aui-button-subtle search-button" type="submit">Incluir</button>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                                @endif

                                <table class="aui aui-table-rowhover">
                                    <tbody>
                                    @if ($context->glossary->free_access)
                                    <tr>
                                        <td colspan="2">ACESSO LIBERADO PARA TODOS OS CLIENTES</td>
                                    </tr>
                                    @else

                                        @if (!$context->glossary->clientAccess()->count())
                                        <tr>
                                            <td colspan="2">NENHUM CLIENTE SELECIONADO</td>
                                        </tr>
                                        @else
                                        @foreach ($context->glossary->clientAccess as $access)
                                        <tr>
                                            <td>
                                                <a href="{{ URL::action('SacClientController@view', array($access->client->id)) }}">{{ $access->client->nome }}</a>
                                            </td>
                                            <td class="align-right aui-compact-button-column">
                                                @if (Auth::user()->can('glossary.access_permission'))
                                                <button aria-owns="dropdown-actions-{{ $access->id }}" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" aria-controls="dropdown-actions-{{ $access->id }}">
                                                    <span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span>
                                                </button>
                                                <div id="dropdown-actions-{{ $access->id }}" class="aui-dropdown2 aui-style-default" aria-hidden="true" data-dropdown2-alignment="right">
                                                    <ul class="aui-list-truncate">
                                                        <li><a href="javascript:void(0);" name="remove_access" data-location="{{ URL::action('GlossaryController@client_access_remove', array($context->glossary->id)) }}?_token={{ csrf_token() }}&client={{ $access->client->id }}" data-client="{{ $access->client->nome }}">Remover Acesso</a></li>
                                                    </ul>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>

                                @if (Auth::user()->can('glossary.access_permission'))
                                <div class="aui-toolbar2">
                                    <div class="aui-toolbar2-inner">
                                        <div class="aui-toolbar2-primary">
                                            <div class="aui-buttons">
                                                @if ($context->glossary->free_access)
                                                <button class="aui-button tooltip" name="change_access" original-title="Restringe o acesso deste glossário apenas para clientes selecionados" data-location="{{ URL::action('GlossaryController@client_access_close', array($context->glossary->id)) }}">Limitar acesso</button>
                                                @else
                                                <button class="aui-button tooltip" name="change_access" original-title="Libera o acesso deste glossário para todos os clientes" data-location="{{ URL::action('GlossaryController@client_access_free', array($context->glossary->id)) }}">Liberar acesso</button>
                                                @endif
                                            </div>
                                        </div>
                                        @if ($context->glossary->clientAccess()->count())
                                        <div class="aui-toolbar2-secondary">
                                            <div class="aui-buttons">
                                                <button class="aui-button" id="empty_access" data-location="{{ URL::action('GlossaryController@client_access_empty', array($context->glossary->id)) }}">Remover acessos</button>
                                            </div>
                                        </div>
                                        @endif
                                    </div><!-- .aui-toolbar-inner -->
                                </div>
                                @endif

                            </div>
                        </div>

                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Permissão de acesso por Módulo</h2>

                            </div>
                            <div class="mod-content">
                                @if (!$context->glossary->moduleAccess()->count())
                                NENHUM MÓDULO SELECIONADO
                                @else
                                <table class="aui aui-table-rowhover issue-table">
                                    <tbody>
                                    @foreach ($context->glossary->moduleAccess as $module)
                                    <tr>
                                        <td>{{ $module->module->name }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <p class="item-description">
                                    Todos os clientes com os módulos listados acima terão acesso à este glossário.
                                </p>

                                @endif

                                @if (Auth::user()->can('glossary.access_permission'))
                                <div class="aui-toolbar2">
                                    <div class="aui-toolbar2-inner">
                                        <div class="aui-toolbar2-secondary">
                                            <div class="aui-buttons">
                                                <button class="aui-button" name="edit_access_modules" data-glossary="{{ $context->glossary->id }}">Editar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>


                    </div>
                </div>
            </section>
        </div>
    </div>

</section>
@endsection