@section('app_menu')
<ul class="aui-nav">
    <li>
        <a href="{{ URL::action('GlossaryController@index') }}">Glossário</a>
    </li>
    @if (Auth::user()->can('glossary.item_create'))
    <li>
        <a class="aui-button aui-style aui-button-primary aui-nav-imagelink" href="javascript: void(0);" name="new-item">Novo Item</a>
    </li>
    @endif
</ul>
@endsection