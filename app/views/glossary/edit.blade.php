@extends('template')
@include('glossary.menu')

@section('styles')
{{ HTML::style('/packages/redactor/css/redactor.css') }}
@endsection

@section('scripts')
{{ HTML::script('/packages/redactor/js/fontcolor.js') }}
{{ HTML::script('/packages/redactor/js/redactor.js') }}
{{ HTML::script('/packages/redactor/js/langs/pt_br.js') }}
{{ HTML::script('/scripts/apps/glossary/edit.js') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('GlossaryController@index') }}">Glossário</a></li>
                    <li>{{ $context->glossary->module->name }}</li>
                </ol>
                <h1>{{ $context->glossary->function }}</h1>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <button class="aui-button aui-button-primary" id="save-item">
                        <span class="aui-icon aui-icon-small aui-iconfont-success">Salvar</span>
                        <span>Salvar</span>
                    </button>
                    <button class="aui-button" id="view-item" data-view="{{ URL::action('GlossaryController@view', array($context->glossary->id)) }}">
                        <span class="aui-icon aui-icon-small aui-iconfont-view">Visualizar</span>
                        Visualizar
                    </button>
                </div>
            </div>
        </div>
    </header>

    <div class="aui-page-panel min-height-440">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <div id="aui-message-bar"></div>
                <form action="{{ URL::action('GlossaryController@save') }}" method="post" id="form-save-glossary" class="aui">
                    <input type="hidden" name="timespend" value="0" />
                    <div class="aui-group">
                        <div class="aui-item">
                            <h2>Módulo</h2>
                            <select class="select long-field" name="module">
                                @foreach($context->modules as $module)
                                <option value="{{ $module->id }}" @if ($module->id == $context->glossary->module->id)selected="selected"@endif>{{ $module->name }}</option>
                                @endforeach
                            </select>
                            <div class="description"></div>
                        </div>
                        <div class="aui-item">
                            <h2>Função</h2>
                            <input class="text long-field" type="text" name="function" value="{{ $context->glossary->function }}">
                            <div class="description"></div>
                        </div>
                        <div class="aui-item">
                            <h2>Classificação</h2>
                            <select class="select long-field" name="classification">
                                @foreach($context->classifications as $index => $class)
                                <option value="{{ $index }}" @if ($index == $context->glossary->classification)selected="selected"@endif>{{ $class }}</option>
                                @endforeach
                            </select>
                            <div class="description"></div>
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="aui-tabs horizontal-tabs" id="tabs-example1">
                        <ul class="tabs-menu">
                            <li class="menu-item active-tab">
                                <a href="#tabs-description"><strong>Descrição</strong></a>
                            </li>
                            <li class="menu-item">
                                <a href="#tabs-result"><strong>Resultado esperado</strong></a>
                            </li>
                            <li class="menu-item">
                                <a href="#tabs-action"><strong>Ações</strong></a>
                            </li>
                            <li class="menu-item">
                                <a href="#tabs-observation"><strong>Observações</strong></a>
                            </li>
                        </ul>
                        <div class="tabs-pane active-pane" id="tabs-description">
                            <textarea name="description" class="redactor-text">
                                {{ html_entity_decode($context->glossary->description, ENT_QUOTES, 'utf-8') }}
                            </textarea>
                        </div>
                        <div class="tabs-pane" id="tabs-result">
                            <textarea name="result" class="redactor-text">
                                {{ html_entity_decode($context->glossary->result, ENT_QUOTES, 'utf-8') }}
                            </textarea>
                        </div>
                        <div class="tabs-pane" id="tabs-action">
                            <textarea name="action" class="redactor-text">
                                {{ html_entity_decode($context->glossary->action, ENT_QUOTES, 'utf-8') }}
                            </textarea>
                        </div>
                        <div class="tabs-pane" id="tabs-observation">
                            <textarea name="observation" class="redactor-text">
                                {{ html_entity_decode($context->glossary->observation, ENT_QUOTES, 'utf-8') }}
                            </textarea>
                        </div>
                    </div>

                    <div class="hidden">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="glossary" value="{{ $context->glossary->id }}" />
                    </div>
                </form>
            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection