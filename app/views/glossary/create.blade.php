<form action="{{ URL::action('GlossaryController@create') }}" class="aui" id="glossary-edit" method="post">
    <div class="form-body">
        <div class="field-group">
            <label for="glossary-create-module">Módulo<span class="aui-icon icon-required"> required</span></label>
            <select class="select" id="glossary-create-module" name="module">
                @foreach($context->modules as $module)
                <option value="{{ $module->id }}" @if ($module->id == $context->module_selected)selected="selected" @endif>{{ $module->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-group">
            <label for="glossary-create-function">Função<span class="aui-icon icon-required"> required</span></label>
            <input class="text " id="glossary-create-function" maxlength="255" name="function" type="text" value="{{ $context->function }}"/>
            @if ($context->field_errors && $context->field_errors->has('function'))
            <div class="error">{{ $context->field_errors->first('function') }}</div>
            @endif
        </div>
        <!-- // .group -->
        <div class="hidden">
            <input name="submit" type="hidden" value="true">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>