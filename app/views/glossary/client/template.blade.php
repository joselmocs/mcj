<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=EDGE" http-equiv="X-UA-Compatible">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ Config::get('global.site.title') }}</title>
    <link rel="stylesheet" href="/packages/aui/aui/css/aui-all.css" media="all">
    <!--[if lt IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie.css" media="all"><![endif]-->
    <!--[if IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie9.css" media="all"><![endif]-->
    {{ HTML::style('/styles/aui.custom.css') }}
    {{ HTML::style('/styles/changelog.css') }}
    @yield('styles')
</head>
<body class="aui-layout aui-theme-default">

<div id="page">
    <header id="header">
        <div id="slide">
            <div id="topomenu">
                <div id="slider" class="nivoSlider">
                    <img src="http://186.202.186.235/images/slide1.jpg" alt="image1" />
                    <img src="http://186.202.186.235/images/slide2.jpg" alt="image2" />
                    <img src="http://186.202.186.235/images/slide3.jpg" alt="image3" />
                </div>
            </div>
        </div>
        <div id="sep"></div>
    </header>

    <section id="content">
        @yield('page-header')
        @yield('content')
    </section>
</div>

<footer id="footer">
    <section class="footer-body">
        <ul id="aui-footer-list">
            <li>Rua da Bahia, 570 | 9° Andar | Centro | Belo Horizonte | Minas Gerais | (31) 3214-0600 - 2013 © Copyright All rights reserved to MCJ</li>
        </ul>
    </section>
</footer>

<script type="text/javascript">
//<![CDATA[
@if (isset($json))
    var _context = {{ $json }};
@else
    var _context = [];
@endif
//]]>
</script>
<script src="/packages/aui/aui/js/aui-all.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="/packages/aui/aui/js/aui-ie.js" type="text/javascript"></script><![endif]-->
<script data-main="/scripts/bootstrap" src="/scripts/libs/require-2.1.8.js" type="text/javascript"></script>
@yield('scripts')

</body>
</html>