@extends('glossary/client/template')

@section('scripts')
<script type="text/javascript" src="http://186.202.186.235/js/nivo.slider.js"></script>
<script type="text/javascript">
    AJS.$("#slider").nivoSlider({
        effect: 'sliceUpDown',//sliceUpDown
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 1200,
        pauseTime: 6000,
        startSlide: 0,
        directionNav: false,
        directionNavHide: true,
        controlNav: false,
        controlNavThumbs: false,
        pauseOnHover: false,
        manualAdvance: false
    });
</script>
@endsection

@section('page-header')
<header class="aui-page-header">
    <div class="aui-page-header-inner">
        <div class="aui-page-header-main">
            <ol class="aui-nav aui-nav-breadcrumbs">
                <li><a href="{{ URL::action('GlossaryController@client_index', array($context->client->cod_cript(), $context->client->cgc_cript())) }}">Glossário</a></li>
                <li>{{ $context->client->nome }}</li>
            </ol>
            <h1>{{ $context->glossary->function }}</h1>
        </div>
    </div>
</header>
@endsection

@section('content')
<section id="content">
    <div class="aui-page-panel content-changelog">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content issue-body-content">
                <div class="issue-body">

                    @if (!empty($context->glossary->description))
                    <div class="module toggle-wrap">
                        <div class="mod-header">
                            <ul class="ops"></ul>
                            <h2 class="toggle-title">Descrição</h2>
                        </div>
                        <div class="mod-content">
                            {{ html_entity_decode($context->glossary->description, ENT_QUOTES, 'utf-8') }}
                        </div>
                    </div>
                    @endif

                    @if (!empty($context->glossary->result))
                    <div class="module toggle-wrap">
                        <div class="mod-header">
                            <ul class="ops"></ul>
                            <h2 class="toggle-title">Resultado Esperado</h2>
                        </div>
                        <div class="mod-content">
                            {{ html_entity_decode($context->glossary->result, ENT_QUOTES, 'utf-8') }}
                        </div>
                    </div>
                    @endif

                    @if (!empty($context->glossary->action))
                    <div class="module toggle-wrap">
                        <div class="mod-header">
                            <ul class="ops"></ul>
                            <h2 class="toggle-title">Ação</h2>
                        </div>
                        <div class="mod-content">
                            {{ html_entity_decode($context->glossary->action, ENT_QUOTES, 'utf-8') }}
                        </div>
                    </div>
                    @endif

                    @if (!empty($context->glossary->observation))
                    <div class="module toggle-wrap">
                        <div class="mod-header">
                            <ul class="ops"></ul>
                            <h2 class="toggle-title">Observação</h2>
                        </div>
                        <div class="mod-content">
                            {{ html_entity_decode($context->glossary->observation, ENT_QUOTES, 'utf-8') }}
                        </div>
                    </div>
                    @endif

                </div>
            </section>
        </div>
    </div>
</section>
@endsection