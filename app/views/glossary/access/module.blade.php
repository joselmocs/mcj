<form action="{{ URL::action('GlossaryController@module_access_edit', array($context->glossary->id)) }}" method="post">
    <table class="aui aui-table-rowhover issue-table">
        <tbody>
        @foreach ($context->modules as $module)
        <tr class="tr-module @if ($context->glossary->hasModuleAccess($module->id)) active @endif">
            <td><input class="checkbox" type="checkbox" name="modules[]" value="{{ $module->id }}" @if ($context->glossary->hasModuleAccess($module->id)) checked="checked" @endif></td>
            <td>{{ $module->name }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <input type="hidden" name="submit" value="true">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
</form>

<script type="text/javascript">
    require([], function() {
        AJS.$('td').click(function() {
            var $el = AJS.$(this).parent();
            if ($el.hasClass('active')) {
                $el.removeClass('active').find('.checkbox').removeAttr('checked');
            }
            else {
                $el.addClass('active').find('.checkbox').attr('checked', 'checked');
            }
        });
    });
</script>