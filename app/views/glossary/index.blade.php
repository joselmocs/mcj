@extends('template')
@include('glossary/menu')

@section('scripts')
{{ HTML::script('/scripts/apps/glossary/index.js') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Glossário MCJ</h1>
            </div>
        </div>
    </header>

    <form class="navigator-search" method="get" action="{{ URL::action('GlossaryController@index') }}">
        <div class="search-container" style="float: left;">
            <ul class="criteria-list">
                <li class="text-query">
                    <div class="text-query-container">
                        <input class="search-entry text" type="text" name="search" placeholder="pesquisar por..." style="width: 300px;" value="{{ $context->filters->search }}">
                    </div>
                </li>
                <li>
                    <button class="aui-button aui-button-subtle search-button tooltip" type="submit" title="Pesquisar">
                        <span class="aui-icon aui-icon-small aui-iconfont-search">Pesquisar</span>
                    </button>
                </li>
            </ul>

        </div>
        <div style="float: right;">
            <a href="{{ URL::action('GlossaryController@index') }}" class="aui-button save-as-new-filter" title="Limpar Filtros">Limpar Filtros</a>
        </div>
        <div class="clear"></div>
    </form>

    <div class="results-panel navigator-item box-content">
        <div class="navigator-content">

            @if (!count($context->glossaries))
            <div class="navigator-group">
                <div class="navigator-content empty-results">
                    <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                        <h3>Nenhum resultado correspondente à sua pesquisa</h3>
                        <p class="no-results-hint">Tente modificar seus critérios de pesquisa</p>
                    </div>
                </div>
            </div>
            @else

            <div class="issue-table-info-bar navigator-results">
                <div class="results-count aui-item">
                    <span class="results-count-text">
                        <span class="results-count-start">{{ $context->glossaries->getFrom() }}</span>–<span class="results-count-end">{{ $context->glossaries->getTo() }}</span> de <span class="results-count-total results-count-link">{{ $context->glossaries->getTotal() }}</span>
                    </span>
                    <a href="" class="refresh-table tooltip" title="Atualizar resultados">Atualizar</a>
                </div>
            </div>

            <table class="aui aui-table-sortable aui-table-rowhover issue-table">
                <thead>
                <tr>
                    <th>Função</th>
                    <th>Módulo</th>
                    <th style="width: 100px;">Criado em</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($context->glossaries as $glossary)
                <tr>
                    <td>
                        <a href="{{ URL::action('GlossaryController@view', array($glossary->id)) }}">{{ $glossary->function }}</a>
                    </td>
                    <td>
                        {{ $glossary->module->name }}
                    </td>
                    <td>
                        <span title="{{ Carbon::parse($glossary->created_at)->toDayDateTimeString() }}">
                            <time datetime="{{ Carbon::parse($glossary->created_at)->format('Y-m-d\TH:i:sO') }}">{{ Carbon::parse($glossary->created_at)->toFormattedDateString() }}</time>
                        </span>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            <div style="margin-top: 30px;text-align: right;">
                {{ $context->glossaries->links() }}
            </div>

            @endif
        </div>
    </div>

</section>
@endsection