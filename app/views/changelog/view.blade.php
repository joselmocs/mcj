@extends('template')

@section('styles')
{{ HTML::style('/packages/redactor/css/redactor.css') }}
{{ HTML::style('/packages/select2/select2.css') }}
{{ HTML::style('/packages/jqueryui/jquery-ui-datepicker-1.10.3.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/changelog/index.js') }}
@if (Auth::user()->can('changelog.item_edit'))
    {{ HTML::script('/packages/redactor/js/fontcolor.js') }}
    {{ HTML::script('/packages/redactor/js/redactor.js') }}
    {{ HTML::script('/packages/redactor/js/langs/pt_br.js') }}
    {{ HTML::script('/packages/select2/select2.js') }}
    {{ HTML::script('/packages/select2/select2_locale_pt-BR.js') }}
{{ HTML::script('/scripts/libs/jquery.maskedinput.js') }}
    {{ HTML::script('/scripts/apps/changelog/edit.js') }}
@endif
@endsection

@include('templates.ef-form-edit')

@section('app_menu')
<ul class="aui-nav">
    <li>
        <a href="{{ URL::action('ChangelogController@index') }}">Conteúdo de Versão</a>
    </li>
</ul>
@endsection

@section('content')
<section id="content" class="">
    <header class="issue-header aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('ChangelogController@index') }}">Conteúdo de Versão</a></li>
                    <li>{{ $context->changelog->os->cod }}</li>
                </ol>
                <h1 id="summary-val" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                    @if ($context->changelog->summary)
                    {{ $context->changelog->summary }}
                    @else
                    Clique para adicionar um resumo
                    @endif
                </h1>
            </div>
            @if ($context->previous)
            <div class="aui-page-header-actions">
                <a id="return-to-search" href="{{ $context->previous }}" title="Retornar à Pesquisa">Retornar à Pesquisa</a>
            </div>
            @endif
        </div>
    </header>

    <nav class="aui-navgroup aui-navgroup-horizontal">
        <div class="aui-navgroup-inner">
            <div class="aui-navgroup-primary">
                <ul class="aui-nav">
                    <li><a href="{{ URL::action('SacOSController@view', array($context->changelog->os->id)) }}">OS</a></li>
                    <li class="aui-nav-selected"><a href="{{ URL::action('ChangelogController@view', array($context->changelog->id)) }}">Conteúdo de Versão</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content issue-body-content">
                <div id="container-message"></div>
                <div class="aui-group issue-body">

                    <div class="aui-item issue-main-column">

                        @if (Auth::user()->can('changelog.item_create'))
                        <div class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Detalhes</h2>
                            </div>
                            <div class="mod-content">
                                <ul class="property-list two-cols">
                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Versão:</strong>
                                            <span class="value" id="version-val" data-type="text" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                                                @if (!empty($context->changelog->version))
                                                {{ $context->changelog->version }}
                                                @else
                                                Clique para adicionar uma versão
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="item item-right">
                                        <div class="wrap">
                                            <strong class="name">Distribuído em:</strong>
                                            <div class="left" style="width: 110px;">
                                                <span class="value" id="month-val" data-type="select" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}" data-pk="{{ $context->changelog->launch()->mes }}">
                                                    {{ $context->changelog->mes() }}
                                                </span>
                                            </div>
                                            <div class="left" style="margin-right: 30px;">
                                                <span class="value">
                                                    de
                                                </span>
                                            </div>
                                            <div class="left" style="width: 70px;">
                                                <span class="value" id="year-val" data-type="text" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                                                    @if ($context->changelog->launch()->ano == "0000")
                                                        -
                                                    @else
                                                        {{ $context->changelog->launch()->ano }}
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item item-left">
                                        <div class="wrap">
                                            <strong class="name">Glossário:</strong>
                                            <span class="value">
                                            @if ($context->changelog->glossary)
                                                <a href="{{ URL::action('GlossaryController@view', array($context->changelog->glossary->id)) }}">{{ $context->changelog->glossary->function }}</a>
                                            @else
                                                 <a name="create_glossary" href="javascript: void(0);" data-location="{{ URL::action('ChangelogController@glossary', array($context->changelog->id)) }}?_token={{ csrf_token() }}">Enviar este item para o Glossário</a>
                                            @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="item item-right">
                                        <div class="wrap">
                                            <strong class="name">Publicado em:</strong>
                                            <span class="value" id="released-val" data-type="datepicker" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                                                {{ Carbon::parse($context->changelog->released)->format('d/m/Y') }}
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @endif

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Descrição</h2>
                            </div>
                            <div class="mod-content">
                                <div id="description-val" data-type="textarea" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                                @if (!empty($context->changelog->description))
                                    {{ html_entity_decode($context->changelog->description, ENT_QUOTES, 'utf-8') }}
                                @else
                                    Clique para editar
                                @endif
                                </div>
                            </div>
                        </div>

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Resultado Esperado</h2>
                            </div>
                            <div class="mod-content">
                                <div id="result-val" data-type="textarea" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                                    @if (!empty($context->changelog->result))
                                        {{ html_entity_decode($context->changelog->result, ENT_QUOTES, 'utf-8') }}
                                    @else
                                        Clique para editar
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Ação</h2>
                            </div>
                            <div class="mod-content">
                                <div id="action-val" data-type="textarea" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                                    @if (!empty($context->changelog->action))
                                        {{ html_entity_decode($context->changelog->action, ENT_QUOTES, 'utf-8') }}
                                    @else
                                        Clique para editar
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="module toggle-wrap">
                            <div class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Observação</h2>
                            </div>
                            <div class="mod-content">
                                <div id="observation-val" data-type="textarea" data-action="{{ URL::action('ChangelogController@inlineEdit', array($context->changelog->id)) }}">
                                    @if (!empty($context->changelog->observation))
                                        {{ html_entity_decode($context->changelog->observation, ENT_QUOTES, 'utf-8') }}
                                    @else
                                        Clique para editar
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="aui-item issue-side-column" id="viewissuesidebar">


                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Datas</h2>
                            </div>
                            <div class="mod-content">
                                <ul class="property-list">

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Criado:</strong>
                                            <span id="type-val" class="value">
                                                {{ Carbon::parse($context->changelog->created_at)->toDayDateTimeString() }}
                                            </span>
                                        </div>
                                    </li>

                                    <li class="item">
                                        <div class="wrap">
                                            <strong class="name">Atualizado:</strong>
                                            <span id="type-val" class="value">
                                                {{ Carbon::parse($context->changelog->updated_at)->toDayDateTimeString() }}
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Permissão de acesso por Cliente</h2>
                            </div>
                            <div class="mod-content">

                                @if (!$context->changelog->free_access && Auth::user()->can('changelog.access_permission'))
                                <form action="{{ URL::action('ChangelogController@client_access_create', array($context->changelog->id)) }}?_token={{ csrf_token() }}" method="post" class="aui" style="margin-bottom: 10px;">
                                    <div style="width: 80%;float: left;">
                                        <input type="text" id="client" name="client" style="width: 100%;">
                                    </div>
                                    <div style="width: 59px;float: right;">
                                        <button class="aui-button aui-button-subtle search-button" type="submit">Incluir</button>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                                @endif

                                <table class="aui aui-table-rowhover">
                                    <tbody>
                                    @if ($context->changelog->free_access)
                                    <tr>
                                        <td colspan="2">ACESSO LIBERADO PARA TODOS OS CLIENTES</td>
                                    </tr>
                                    @else

                                        @if (!$context->changelog->clientAccess()->count())
                                        <tr>
                                            <td colspan="2">NENHUM CLIENTE SELECIONADO</td>
                                        </tr>
                                        @else
                                        @foreach ($context->changelog->clientAccess as $access)
                                        <tr>
                                            <td>
                                                <a href="{{ URL::action('SacClientController@view', array($access->client->id)) }}">{{ $access->client->nome }}</a>
                                            </td>
                                            <td class="align-right aui-compact-button-column">
                                                @if (Auth::user()->can('changelog.access_permission'))
                                                <button aria-owns="dropdown-actions-{{ $access->id }}" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" aria-controls="dropdown-actions-{{ $access->id }}">
                                                    <span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span>
                                                </button>
                                                <div id="dropdown-actions-{{ $access->id }}" class="aui-dropdown2 aui-style-default" aria-hidden="true" data-dropdown2-alignment="right">
                                                    <ul class="aui-list-truncate">
                                                        <li><a href="javascript:void(0);" name="remove_access" data-location="{{ URL::action('ChangelogController@client_access_remove', array($context->changelog->id)) }}?_token={{ csrf_token() }}&client={{ $access->client->id }}" data-client="{{ $access->client->nome }}">Remover Acesso</a></li>
                                                    </ul>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>

                                @if (Auth::user()->can('changelog.access_permission'))
                                <div class="aui-toolbar2">
                                    <div class="aui-toolbar2-inner">
                                        <div class="aui-toolbar2-primary">
                                            <div class="aui-buttons">
                                                @if ($context->changelog->free_access)
                                                <button class="aui-button tooltip" name="change_access" original-title="Restringe o acesso deste Conteúdo de Versão apenas para clientes selecionados" data-location="{{ URL::action('ChangelogController@client_access_close', array($context->changelog->id)) }}">Limitar acesso</button>
                                                @else
                                                <button class="aui-button tooltip" name="change_access" original-title="Libera o acesso deste Conteúdo de Versão para todos os clientes" data-location="{{ URL::action('ChangelogController@client_access_free', array($context->changelog->id)) }}">Liberar acesso</button>
                                                @endif
                                            </div>
                                        </div>
                                        @if ($context->changelog->clientAccess()->count())
                                        <div class="aui-toolbar2-secondary">
                                            <div class="aui-buttons">
                                                <button class="aui-button" id="empty_access" data-location="{{ URL::action('ChangelogController@client_access_empty', array($context->changelog->id)) }}">Remover acessos</button>
                                            </div>
                                        </div>
                                        @endif
                                    </div><!-- .aui-toolbar-inner -->
                                </div>
                                @endif

                            </div>
                        </div>


                        <div id="details-module" class="module toggle-wrap">
                            <div id="details-module_heading" class="mod-header">
                                <ul class="ops"></ul>
                                <h2 class="toggle-title">Permissão de acesso por Módulo</h2>

                            </div>
                            <div class="mod-content">
                                @if (!$context->changelog->moduleAccess()->count())
                                NENHUM MÓDULO SELECIONADO
                                @else
                                <table class="aui aui-table-rowhover issue-table">
                                    <tbody>
                                    @foreach ($context->changelog->moduleAccess as $module)
                                    <tr>
                                        <td>{{ $module->module->name }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <p class="item-description">
                                    Todos os clientes com os módulos listados acima terão acesso à este conteúdo de versão.
                                </p>

                                @endif

                                @if (Auth::user()->can('changelog.access_permission'))
                                <div class="aui-toolbar2">
                                    <div class="aui-toolbar2-inner">
                                        <div class="aui-toolbar2-secondary">
                                            <div class="aui-buttons">
                                                <button class="aui-button" name="edit_access_modules" data-changelog="{{ $context->changelog->id }}">Editar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>


                    </div>
                </div>
            </section>
        </div>
    </div>

</section>
@endsection