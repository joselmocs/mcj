@extends('template')

@section('app_menu')
<ul class="aui-nav">
    <li>
        <a href="{{ URL::action('ChangelogController@index') }}">Conteúdo de Versão</a>
    </li>
</ul>
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/changelog/index.js') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Conteúdo de Versão</h1>
            </div>
        </div>
    </header>

    <form class="navigator-search" method="get" action="{{ URL::action('ChangelogController@index') }}">
        <div class="search-container" style="float: left;">
            <ul class="criteria-list">
                <li class="text-query">
                    <div class="text-query-container">
                        <input class="search-entry text" type="text" name="os" placeholder="#" style="width: 80px;" value="{{ $context->filters->os }}">
                        <input class="search-entry text" type="text" name="search" placeholder="pesquisar por..." style="width: 200px;" value="{{ $context->filters->tags }}">
                    </div>
                </li>
                <li>
                    <button class="aui-button aui-button-subtle search-button tooltip" type="submit" title="Pesquisar">
                        <span class="aui-icon aui-icon-small aui-iconfont-search">Pesquisar</span>
                    </button>
                </li>
            </ul>

        </div>
        <div style="float: right;">
            <a href="{{ URL::action('ChangelogController@index') }}" class="aui-button save-as-new-filter" title="Limpar Filtros">Limpar Filtros</a>
        </div>
        <div class="clear"></div>
    </form>

    <div class="results-panel navigator-item box-content">
        <div class="navigator-content">

            @if (!count($context->changelogs))
            <div class="navigator-group">
                <div class="navigator-content empty-results">
                    <div class="jira-adbox jira-adbox-medium no-results no-results-message">
                        <h3>Nenhum resultado correspondente à sua pesquisa</h3>
                        <p class="no-results-hint">Tente modificar seus critérios de pesquisa</p>
                    </div>
                </div>
            </div>
            @else

            <div class="issue-table-info-bar navigator-results">
                <div class="results-count aui-item">
                    <span class="results-count-text">
                        <span class="results-count-start">{{ $context->changelogs->getFrom() }}</span>–<span class="results-count-end">{{ $context->changelogs->getTo() }}</span> de <span class="results-count-total results-count-link">{{ $context->changelogs->getTotal() }}</span>
                    </span>
                    <a href="{{ URL::action('ChangelogController@index') }}" class="refresh-table tooltip" title="Atualizar resultados">Atualizar</a>
                </div>
            </div>

            <table class="aui aui-table-sortable aui-table-rowhover issue-table">
                <thead>
                <tr>
                    <th style="width: 22px;">T</th>
                    <th style="width: 60px;">OS</th>
                    <th>Resumo</th>
                    <th style="width: 100px;">Versão</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($context->changelogs as $changelog)
                <tr>
                    <td class="issuetype tooltip-sw" title="{{ $changelog->os->type->name }}">
                        <img src="{{ $changelog->os->type->icon }}" />
                    </td>
                    <td>
                        <a href="{{ URL::action('SacOSController@view', array($changelog->os->id)) }}" title="Abrir OS #{{ $changelog->os->cod }}" class="tooltip">{{ $changelog->os->cod }}</a>
                    </td>
                    <td>
                        <a href="{{ URL::action('ChangelogController@view', array($changelog->id)) }}">{{ $changelog->summary }}</a>
                    </td>
                    <td>
                        {{ $changelog->version }}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            <div style="margin-top: 30px;text-align: right;">
                {{ $context->changelogs->links() }}
            </div>

            @endif
        </div>
    </div>

</section>
@endsection