@extends('changelog/client/template')

@section('scripts')
<script type="text/javascript" src="http://186.202.186.235/js/nivo.slider.js"></script>
<script type="text/javascript">
AJS.$("#slider").nivoSlider({
    effect: 'sliceUpDown',//sliceUpDown
    slices: 15,
    boxCols: 8,
    boxRows: 4,
    animSpeed: 1200,
    pauseTime: 6000,
    startSlide: 0,
    directionNav: false,
    directionNavHide: true,
    controlNav: false,
    controlNavThumbs: false,
    pauseOnHover: false,
    manualAdvance: false
});
</script>
@endsection

@section('page-header')
<header class="aui-page-header">
    <div class="aui-page-header-inner">
        <div class="aui-page-header-main">
            <ol class="aui-nav aui-nav-breadcrumbs">
                <li><a href="{{ URL::action('ChangelogController@client_index', array($context->client->cod_cript(), $context->client->cgc_cript())) }}">Conteúdo de Versão</a></li>
            </ol>
            <h1>{{ $context->client->nome }}</h1>
        </div>
    </div>
</header>
@endsection

@section('content')
<section id="content">
    <div class="aui-page-panel content-changelog">
        <div class="aui-page-panel-inner">

            <div class="results-panel navigator-item box-content">
                <div class="navigator-content">

                    <table class="aui aui-table-sortable aui-table-rowhover issue-table">
                        <thead>
                        <tr>
                            <th>Resumo</th>
                            <th>Disponibilizado</th>
                            <th style="width: 100px;">Versão</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (!count($context->changelogs))
                        <tr>
                            <td colspan="3">Nenhum conteúdo de versão encontrado.</td>
                        </tr>
                        @else
                        @foreach ($context->changelogs as $changelog)
                        <tr>
                            <td>
                                <a href="{{ URL::action('ChangelogController@client_view', array($context->client->cod_cript(), $context->client->cgc_cript(), $changelog->id)) }}">{{ $changelog->summary }}</a>
                            </td>
                            <td>
                                {{ $changelog->mes() }}/{{ $changelog->ano() }}
                            </td>
                            <td>
                                {{ $changelog->version }}
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection