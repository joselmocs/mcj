<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=EDGE" http-equiv="X-UA-Compatible">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ Config::get('global.site.title') }}</title>
    <link rel="stylesheet" href="/packages/aui/aui/css/aui-all.css" media="all">
    <!--[if lt IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie.css" media="all"><![endif]-->
    <!--[if IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie9.css" media="all"><![endif]-->
    {{ HTML::style('/styles/aui.custom.css') }}
    @yield('styles')
</head>
<body class="aui-layout aui-theme-default @yield('page-class')">

<header id="header">
    @if (isset($headerMessage) && !is_null($headerMessage))
    <div class="aui-message {{ $headerMessage['type'] }}">
        <p class="title">{{ $headerMessage['message'] }}</p>
        <span class="aui-icon aui-icon-small aui-iconfont-{{ $headerMessage['type'] }}"></span>
    </div>
    @endif
    <nav class="aui-header aui-dropdown2-trigger-group">
        @if (Auth::check())
        <div class="aui-header-before">
            <a class="aui-dropdown2-trigger app-switcher-trigger" aria-owns="app-switcher" aria-haspopup="true" tabindex="0" aria-controls="app-switcher">
                <span class="aui-icon aui-icon-small aui-iconfont-appswitcher">Aplicações</span>
            </a>
            <div id="app-switcher" class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" data-dropdown2-alignment="left" aria-hidden="true">
                <div class="aui-dropdown2-section">
                    <ul class="nav-links" role="radiogroup">
                        @if (Auth::user()->can('admin'))
                        <li class="nav-link">
                            <a href="/dashboard" class="aui-dropdown2-radio interactive @if (Module::is(Module::DASHBOARD))checked@endif" role="radio" aria-checked="false">
                                <span class="nav-link-label">Painel do Sistema</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->can('os.item_view') || Auth::user()->can('client.view'))
                        <li class="nav-link">
                            <a href="{{ URL::action('SacController@index') }}" class="aui-dropdown2-radio interactive @if (Module::is(Module::SAC))checked@endif" role="radio" aria-checked="false">
                                <span class="nav-link-label">SAC</span>
                            </a>
                        </li>
                        @endif
                        <li class="nav-link">
                            <a href="{{ URL::action('GlossaryController@index') }}" class="aui-dropdown2-radio interactive @if (Module::is(Module::GLOSSARY))checked@endif" role="radio" aria-checked="false">
                                <span class="nav-link-label">Glossário</span>
                            </a>
                        </li>
                        @if (Auth::user()->can('changelog.item_view'))
                        <li class="nav-link">
                            <a href="{{ URL::action('ChangelogController@index') }}" class="aui-dropdown2-radio interactive @if (Module::is(Module::CHANGELOG))checked@endif" role="radio" aria-checked="false">
                                <span class="nav-link-label">Conteúdo de Versão</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        @endif
        <div class="aui-header-primary">
            <h1 class="aui-header-logo aui-header-logo-aui" id="logo">
                <a href="/">
                    <img src="/images/icon-mcj-logo.png" alt="MCJ" />
                </a>
            </h1>
            @yield('app_menu')
        </div><!-- .aui-header-inner-->

        <div class="aui-header-secondary">
            <ul class="aui-nav">
                @if (Auth::check())
                @if (Auth::user()->can('admin'))
                <li>
                    <a href="#dropdown2-aui" aria-owns="dropdown2-aui" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="dropdown2-header2">
                        Desenvolvimento<span class="aui-icon-dropdown"></span>
                    </a>
                    <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" id="dropdown2-aui" aria-hidden="true">
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="https://developer.atlassian.com/design/latest/" target="_blank">AUI Design</a></li>
                            </ul>
                        </div>
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="https://developer.atlassian.com/display/AUI/Atlassian+User+Interface+%28AUI%29+Developer+Documentation" target="_blank">API</a></li>
                                <li><a href="/packages/aui/docs/applicationHeader.html" target="_blank">Documentação</a></li>
                                <li><a href="/packages/aui/sandbox/index.html" target="_blank">Sandbox</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="/admin"><span class="aui-icon aui-icon-small aui-iconfont-configure">Administração</span></a>
                </li>
                @endif
                <li>
                    <a href="#dropdown2-header9" aria-owns="dropdown2-header9" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="dropdown2-header4">
                        <div class="aui-avatar aui-avatar-small">
                            <div class="aui-avatar-inner">
                                <img src="/images/useravatar/avatar.png" />
                            </div>
                        </div>
                    </a>
                    <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" id="dropdown2-header9" aria-hidden="true">
                        <div class="aui-dropdown2-section" style="display: none;">
                            <ul>
                                <li><a href="#">Perfil</a></li>
                                <li><a href="#">Minhas Mensagens (0)</a></li>
                            </ul>
                        </div>
                        <div class="aui-dropdown2-section">
                            <strong>{{ Auth::user()->profile->fullname }}</strong>
                        </div>
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="{{ URL::action('UserController@logout') }}">Sair</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                @else
                <li style="display: none;">
                    <a href="#dropdown2-header7" aria-owns="dropdown2-header7" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="dropdown2-header3"><span class="aui-icon aui-icon-small aui-iconfont-help">Help</span><span class="aui-icon-dropdown"></span></a>
                    <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" id="dropdown2-header7" aria-hidden="true">
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="#" class="active">Ajuda Online</a></li>
                                <li><a href="#">Atalhos de Teclado</a></li>
                                <li><a href="#">Sobre</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="{{ URL::action('UserController@get_login') }}">Entrar</a>
                </li>
                @endif
            </ul>
        </div><!-- .aui-header-inner-->
    </nav><!-- .aui-header -->
</header><!-- #header -->

@yield('content')

<footer id="footer">
    <section class="footer-body">
        <ul id="aui-footer-list">
            <li>Copyright &copy; 2013 MCJ</li>
            @if (Auth::check() && Auth::user()->is('admin'))
            <li><a href="#">Relatar um problema</a></li>
            @endif
        </ul>
    </section>
</footer>

@yield('templates')

<script type="text/javascript">
//<![CDATA[
@if (isset($json))
var _context = {{ $json }};
@else
var _context = [];
@endif
//]]>
</script>
<script src="/packages/aui/aui/js/aui-all.js" type="text/javascript"></script>
<script src="/scripts/libs/ion.sound.min.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="/packages/aui/aui/js/aui-ie.js" type="text/javascript"></script><![endif]-->
<script data-main="/scripts/bootstrap" src="/scripts/libs/require-2.1.8.js" type="text/javascript"></script>
@yield('scripts')

</body>
</html>