@section('templates')
<script type="text/x-template" title="ef-form-edit">
    <form class="aui" action="">
        <div class="inline-edit-fields">
            <div class="field-group">
                {field}
            </div>
        </div>
        <span class="overlay-icon throbber"></span>
        <div class="save-options">
            <button type="submit" class="aui-button submit">
                <span class="icon icon-save">Salvar</span>
            </button>
            <button type="cancel" class="aui-button cancel">
                <span class="icon icon-cancel">Cancel</span>
            </button>
        </div>
    </form>
</script>
@endsection