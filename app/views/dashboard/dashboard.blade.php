@extends('template')

@section('scripts')
{{ HTML::script('/scripts/apps/dashboard/index.js') }}
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Painel do Sistema</h1>
            </div><!-- .aui-page-header-main -->
        </div><!-- .aui-page-header-inner -->
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <div class="aui-group">
                    <div class="aui-item">
                        <div class="gadget">
                            <div class="item-header"><h3 class="title">Introdução</h3></div>
                            <div class="item-content">Bem vindo à MCJ</div>
                        </div>
                    </div>
                    @if (Auth::user()->can('report.os'))
                    <div class="aui-item">
                        <div class="gadget">
                            <div class="item-header">
                                <h3 class="title">
                                    <span>RESUMO GERENCIAL DO MÊS <span id="text-current-past">ATUAL</span></span>
                                    <span class="aui-lozenge dash-lozenge" style="display: none;" id="bt-lozenge-current">VISUALIZAR MÊS ATUAL</span>
                                    <span class="aui-lozenge dash-lozenge" id="bt-lozenge-previous">VISUALIZAR MÊS ANTERIOR</span>
                                </h3>

                            </div>
                            <div class="item-content" id="dash-gerencial-current">
                                <div>
                                    <span class="aui-badge">{{ SacOSController::documented_count() }}</span> <a href="{{ URL::action('SacOSController@documented') }}">OS's documentadas</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ SacOSController::undocumented_count() }}</span> <a href="{{ URL::action('SacOSController@undocumented') }}">OS's não documentadas</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ GlossaryController::new_glossary_count() }}</span> <a href="{{ URL::action('GlossaryController@news') }}">novos itens no Glossário</a>
                                </div>
                            </div>
                            <div class="item-content" id="dash-gerencial-previous" style="display: none;">
                                <div>
                                    <span class="aui-badge">{{ SacOSController::documented_count(false) }}</span> <a href="{{ URL::action('SacOSController@documented') }}?previous=1">OS's documentadas</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ SacOSController::undocumented_count(false) }}</span> <a href="{{ URL::action('SacOSController@undocumented') }}?previous=1">OS's não documentadas</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ GlossaryController::new_glossary_count(false) }}</span> <a href="{{ URL::action('GlossaryController@news') }}?previous=1">novos itens no Glossário</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                @if (Auth::user()->can('incoming.view'))
                <div class="aui-group">
                    <div class="aui-item">
                        <div class="gadget">
                            <div class="item-header"><h3 class="title">RESUMO DE NEGOCIAÇÕES</h3></div>
                            <div class="item-content">
                                <div>
                                    <span class="aui-badge">{{ DashboardController::contatos_realizados_hoje() }}</span> <a href="{{ URL::action('SacClientController@contacts_done') }}?de={{ Carbon::now()->startOfDay()->format('d/m/Y') }}&ate={{ Carbon::now()->endOfDay()->format('d/m/Y') }}" style="color: red; font-weight: bold;">Contatos realizados no dia</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ DashboardController::contatos_pendentes_ate_a_data() }}</span> <a href="{{ URL::action('SacClientController@contacts_pending') }}">Contatos pendentes até a data atual</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ DashboardController::contatos_previstos_semana() }}</span> <a href="{{ URL::action('SacClientController@contacts_next') }}">Contatos previstos para os próximos 7 dias</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ DashboardController::contatos_do_mes() }}</span> <a href="{{ URL::action('SacClientController@contacts_done') }}">Contatos realizados no mês corrente</a>
                                </div>
                                <div>
                                    <span class="aui-badge">{{ DashboardController::contatos_do_mes_anterior() }}</span> <a href="{{ URL::action('SacClientController@contacts_done') }}?de={{ Carbon::now()->subMonth()->startOfMonth()->format('d/m/Y') }}&ate={{ Carbon::now()->subMonth()->endOfMonth()->format('d/m/Y') }}">Contatos realizados no mês anterior</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="aui-item">

                    </div>
                </div>
                @endif
            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection
