@extends('support/template')

@section('content')
<section id="content" class="body-client">

    <header class="issue-header aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="{{ URL::action('SupportController@request') }}">Central de Atendimento</a></li>
                    <li class="aui-nav-selected">{{ $context->support->hash_id() }}</li>
                </ol>
                <h1>{{ $context->support->description }}</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">

            <section class="aui-page-panel-content issue-body-content">

                <div class="mod-content">
                    <ul class="property-list two-cols">

                        <li class="item item-left">
                            <div class="wrap">
                                <strong class="name">ID:</strong>
                                <span class="value"><strong>{{ $context->support->id }}</strong></span>
                            </div>
                        </li>

                        <li class="item item-right">
                            <div class="wrap">
                                <strong class="name">Cliente:</strong>
                                <span class="value">{{ $context->support->client->profile->fullname }}</span>
                            </div>
                        </li>

                        <li class="item item-left">
                            <div class="wrap">
                                <strong class="name">Código:</strong>
                                <span class="value"><strong>{{ $context->support->hash_id() }}</strong></span>
                            </div>
                        </li>

                        <li class="item item-right">
                            <div class="wrap">
                                <strong class="name">Operador:</strong>
                                <span class="value">
                                    <span class="value">{{ $context->support->operator->profile->fullname }}</span>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="module toggle-wrap">
                    <div class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Hisórico</h2>
                    </div>
                    <div class="mod-content">
                        <div class="user-content-block">
                            <ul class="messages">
                                @foreach($context->support->messages as $message)
                                <li>
                                    <div class="author">
                                        @if ($message->sender)
                                        {{ $message->sender->profile->fullname }}
                                        @else
                                        Sistema
                                        @endif
                                    </div>
                                    <div class="time">{{ Carbon::parse($message->created_at)->format('d/m/Y H:i') }}</div>
                                    <div class="clear"></div>
                                    <div class="chat-messages">{{ $message->message }}</div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

            </section>

            <aside class="aui-page-panel-sidebar issue-body-content">

                <div class="module toggle-wrap">
                    <div id="details-module_heading" class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Minha Avaliação</h2>
                    </div>
                    @if ($context->support->feedback)
                    <div class="mod-content">
                        <ul class="property-list">

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Nota:</strong>
                                    <span class="value rating rating-{{ $context->support->feedback->rating }}"">
                                    {{ SacSupportFeedback::$rating[$context->support->feedback->rating] }}
                                    </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Observação:</strong>
                                    <span class="value">
                                        {{ $context->support->feedback->observation }}
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    @else
                    <p>Este chamado não foi avaliado.</p>
                    @endif
                </div>

                <div class="module toggle-wrap">
                    <div class="mod-header">
                        <ul class="ops"></ul>
                        <h2 class="toggle-title">Datas</h2>
                    </div>
                    <div class="mod-content">
                        <ul class="property-list">

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Iniciado em:</strong>
                                    <span id="type-val" class="value">
                                        {{ Carbon::parse($context->support->created_at)->toDayDateTimeString() }}
                                    </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="wrap">
                                    <strong class="name">Finalizado em:</strong>
                                    <span id="type-val" class="value">
                                        {{ Carbon::parse($context->support->updated_at)->toDayDateTimeString() }}
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </aside>
        </div>
    </div>

</section>
@endsection