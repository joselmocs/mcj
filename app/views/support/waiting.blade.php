@extends('support.template')

@section('page-type')aui-page-focused aui-page-focused-medium @endsection

@section('scripts')
<script type="text/javascript">
setTimeout(function() {
    window.location = '';
}, 3000);
</script>
@endsection

@section('content')
<div class="aui-page-panel margin-fix">
    <div class="aui-page-panel-inner">
        <section class="aui-page-panel-content">
            <h2>Suporte MCJ</h2>
            <form class="aui">
                <fieldset>
                    Por favor, aguarde um momento para que seja atendido.
                </fieldset>
            </form>
        </section><!-- .aui-page-panel-content -->
    </div><!-- .aui-page-panel-inner -->
</div><!-- .aui-page-panel -->
@endsection