@extends('support/template')

@section('scripts')
<script type="text/javascript">
    AJS.$('button[name=continuar]').click(function() {
        window.location = AJS.$(this).attr('data-url');
    });
</script>
@endsection

@section('content')
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>Central de Atendimento</h1>
            </div>
        </div>
    </header>
    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">

            <section class="aui-page-panel-content">

                @if (count($context->in_progress))
                <h2>Chamado em Andamento</h2>
                <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                    <thead>
                    <tr>
                        <th>Assunto</th>
                        <th>Duração</th>
                        <th style="width: 100px;">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($context->in_progress as $support)
                    <tr>
                        <td>
                            <a href="{{ URL::action('SupportController@live', array($support->hash_id())) }}">{{ $support->description }}</a>
                        </td>
                        <td>
                            {{ Carbon::parse($support->created_at)->diffInMinutes(Carbon::now()) }} minuto(s)
                        </td>
                        <td class="aui-compact-button-column">
                            <button name="continuar" class="aui-button aui-button-compact aui-button-primary" data-url="{{ URL::action('SupportController@live', array($support->hash_id())) }}">
                                <span class="aui-icon aui-icon-small aui-iconfont-share"></span> Continuar
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                <h2>Histórico de Atendimento</h2>
                <table class="aui aui-table-sortable aui-table-rowhover issue-table compact-table">
                    <thead>
                    <tr>
                        <th>Assunto</th>
                        <th style="width: 100px;">Iniciado em</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($context->supports as $support)
                    <tr>
                        <td>
                            <a href="{{ URL::action('SupportController@history', array($support->hash_id())) }}">{{ $support->description }}</a>
                        </td>
                        <td>
                        <span title="{{ Carbon::parse($support->created_at)->toDayDateTimeString() }}">
                            <time datetime="{{ Carbon::parse($support->created_at)->format('Y-m-d\TH:i:sO') }}">{{ Carbon::parse($support->created_at)->toFormattedDateString() }}</time>
                        </span>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </section>

            <aside class="aui-page-panel-sidebar">
                <h2>Solicitar Suporte</h2>
                <form method="post" class="aui">
                    <fieldset>
                        <div class="field-group">
                            <label for="d-fname">Assunto de Interesse</label>
                            <select class="select long-field" name="type">
                                @foreach($context->subjects as $subject)
                                <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field-group">
                            <label for="comment">Mensagem<span class="aui-icon icon-required"> required</span></label>
                            <textarea class="textarea long-field" name="description" rows="3" placeholder="Breve descrição do que deseja…">{{ $context->forms->description }}</textarea>
                            @if ($context->field_errors && $context->field_errors->has('description'))
                            <div class="error">{{ $context->field_errors->first('description') }}</div>
                            @endif
                        </div>
                    </fieldset>

                    <div class="buttons-container">
                        <div class="buttons">
                            <input class="aui-button aui-button-primary" type="submit" name="submit" value="Solicitar Suporte" @if (count($context->in_progress)) disabled="disabled" @endif>
                        </div>
                    </div>
                </form>
            </aside>

        </div><!-- .aui-page-panel-inner -->
    </div><!-- .aui-page-panel -->
</section>
@endsection