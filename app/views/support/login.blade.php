@extends('support.template')

@section('page-type')page-type-indra page-type-login @endsection

@section('styles')
{{ HTML::style('/styles/support.login.css') }}
@endsection

@section('content')
<section id="content" role="main">

    <div id="login-panel" class="login-panel aui-page-panel">
        <header class="indra-header aui-page-header" style="padding-bottom:10px;">
            <div class="aui-page-header-inner">
                <div class="aui-pageheader-main">
                    <h1>Suporte MCJ</h1>
                    <p><a href="http://www.mcj.com.br">mcj.com.br</a></p>
                </div>
            </div>
        </header>

        @if (count($context->form_errors) > 0)
        <div class="aui-message error">
            <p class="title">
                <span class="aui-icon icon-error"></span>
                @foreach ($context->form_errors as $e)
                {{ $e }}<br />
                @endforeach
            </p>
        </div>
        @endif

        <div class="aui-group indra-login-method  indra-login-multiple ">

            <div class="aui-item indra-login-item indra-login-gapps">
                <p>
                    algum texto
                </p>
            </div>
            <div class="aui-item indra-login-separator">&nbsp;</div>

            <div class="aui-item indra-login-item indra-login-crowd">

                <form class="aui top-label " action="{{ URL::action('SupportController@index') }}" method="POST">
                    <div class="field-group" id="username-field">
                        <label for="username">Usuário </label>
                        <input type="text" id="username" name="username" value="{{ $context->forms->username }}" class="text full-width-field">
                    </div>

                    <div class="field-group" id="password-field">
                        <label for="password">Senha </label>
                        <input type="password" id="password" name="password" value="" class="password full-width-field">
                    </div>

                    <div class="buttons-container">
                        <div class="buttons">
                            <button type="submit" id="login" class="aui-button aui-button-primary">Acessar</button>
                        </div>
                    </div>

                    <p class="indra-signup">
                        <a href="" id="forgot" name="forgot">Não consegue acessar sua conta?</a><br><br>
                    </p>
                </form>
            </div>
        </div>
    </div>

</section>
@endsection