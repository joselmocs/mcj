<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=EDGE" http-equiv="X-UA-Compatible">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ Config::get('global.site.title') }}</title>
    <link rel="stylesheet" href="/packages/aui/aui/css/aui-all.css" media="all">
    <!--[if lt IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie.css" media="all"><![endif]-->
    <!--[if IE 9]><link rel="stylesheet" href="/packages/aui/aui/css/aui-ie9.css" media="all"><![endif]-->
    {{ HTML::style('/styles/aui.custom.css') }}
    @yield('styles')
</head>

<body class="aui-layout aui-theme-default @yield('page-type')">

<div id="page">
    <header id="header" role="banner">
        <nav class="aui-header aui-dropdown2-trigger-group" role="navigation" id="test1">
            <div class="aui-header-inner">
                <div class="aui-header-primary">
                    <h1 class="aui-header-logo aui-header-logo-aui" id="logo">
                        <a href="/">
                            <img src="/images/icon-mcj-logo.png" alt="MCJ" />
                        </a>
                    </h1>
                    @yield('app_menu')
                </div>

                <div class="aui-header-secondary">
                    <ul class="aui-nav">
                        <li>
                            <a href="#" id="stash-issue-collector-link">Enviar Feedback</a>
                        </li>
                        <li>
                            <a href="#helpDropdown" aria-owns="help-dropdown" aria-haspopup="true" class="aui-dropdown2-trigger">
                                <span class="aui-icon aui-icon-small aui-iconfont-help">Ajuda</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div>
                <div id="help-dropdown" class="aui-dropdown2 aui-style-default">
                    <ul>
                        <li>
                            <a href="" target="_blank">Glossário</a>
                        </li>
                        <li>
                            <a href="" target="_blank">Conteúdo de Versão</a>
                        </li>
                    </ul>
                </div>
            </div> <!-- /Dropdown Nav Builder -->
        </nav>
    </header><!-- #header -->

    <section id="content" role="main">
        @yield('content')
    </section><!-- #content -->

    <footer id="footer">
        <section class="footer-body">
            <ul id="aui-footer-list">
                <li>Copyright &copy; 2013 MCJ</li>
                @if (Auth::check() && Auth::user()->is('admin'))
                <li><a href="#">Relatar um problema</a></li>
                @endif
            </ul>
        </section>
    </footer>

</div><!-- #page -->

@yield('templates')

<script type="text/javascript">
//<![CDATA[
@if (isset($json))
    var _context = {{ $json }};
@else
    var _context = [];
@endif
//]]>
</script>
<script src="/packages/aui/aui/js/aui-all.js" type="text/javascript"></script>
<script src="/scripts/libs/ion.sound.min.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="/packages/aui/aui/js/aui-ie.js" type="text/javascript"></script><![endif]-->
<script data-main="/scripts/bootstrap" src="/scripts/libs/require-2.1.8.js" type="text/javascript"></script>
@yield('scripts')

</body>
</html>