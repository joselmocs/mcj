<form action="{{ URL::action('SupportController@feedback') }}" class="aui" id="form-feedback" method="post">
    <div class="form-body">
        <p style="margin-bottom: 20px">
            Gostaria de avaliar este chamado? Esta avaliação é muito importante bla bla...
        </p>

        @if (count($context->form_errors))
        <div class="aui-message error">
            <span class="aui-icon icon-error"></span>
            <p>
                @foreach($context->form_errors as $error)
                {{ $error }}<br />
                @endforeach
            </p>
        </div>
        @endif
        <div class="field-group">
            <label for="fb-rating">Avaliação</label>
            <select class="select long-field" id="fb-rating" name="rating">
                @foreach(SacSupportFeedback::$rating as $rating => $texto)
                <option value="{{ $rating }}" @if ($rating == $context->rating)selected="selected" @endif>{{ $texto }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-group">
            <label for="fb-observation">Observação</label>
            <textarea class="textarea long-field" id="fb-observation" name="observation" rows="4">{{ $context->observation }}</textarea>
            @if ($context->field_errors && $context->field_errors->has('observation'))
            <div class="error">{{ $context->field_errors->first('observation') }}</div>
            @endif
        </div>

        <!-- // .group -->
        <div class="hidden">
            <input name="submit" type="hidden" value="true">
            <input name="cid" type="hidden" value="{{ $context->cid }}"/>
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        </div>
    </div>
</form>