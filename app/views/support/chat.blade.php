@extends('support/template')

@section('page-type')body-chat @endsection

@section('scripts')
{{ HTML::script('/scripts/apps/support/chat.js') }}
{{ HTML::script('/scripts/libs/jquery.slimscroll.js') }}
@endsection

@section('content')
<section id="content">
<div class="aui-page-panel margin-fix">
    <div class="aui-page-panel-inner">
        <section class="aui-page-panel-content">

            <div style="min-height: 460px;" id="section-content">

                <div name="chat-container" data-hid="{{ $context->support->hash_id() }}" class="chat" data-last="">
                    <h2>{{ $context->support->operator->profile->fullname }}</h2>
                    <div class="border-new-msg inactive" name="border_start"></div>
                    <ul class="messages"></ul>
                    <div class="border-new-msg inactive" name="border_end"></div>
                    @if ($context->support->status == SacSupport::EM_ANDAMENTO)
                    <form class="aui form-reply" name="reply-support" style="left: 20px;">
                        <div class="bottom-container">
                            <textarea class="textarea long-field" name="message" rows="3" style="max-width: 100% !important;" placeholder="O que você quer dizer?"></textarea>
                            <div class="buttons">
                                <input class="aui-button aui-button-primary" type="submit" name="submit" value="Enviar">
                            </div>
                        </div>
                    </form>
                    @endif
                </div>

            </div>

        </section><!-- .aui-page-panel-content -->

        <aside class="aui-page-panel-sidebar">

            <div class="aui-toolbar2">
                <div class="aui-toolbar-2-inner">
                    <div class="aui-toolbar2-primary">

                    </div>
                    <div class="aui-toolbar2-secondary">
                        @if ($context->support->status == SacSupport::EM_ANDAMENTO)
                        <button class="aui-button aui-button-primary" id="end_call">Finalizar Chamado</button>
                        @endif
                    </div>
                </div>
            </div>
        </aside>

    </div><!-- .aui-page-panel-inner -->
</div><!-- .aui-page-panel -->
</section>

<script type="text/x-template" title="chat-line">
    <li data-author="{author}">
        <div class="author">{name}</div>
        <div class="time">{time}</div>
        <div class="clear"></div>
        <div class="chat-messages"></div>
    </li>
</script>

@endsection