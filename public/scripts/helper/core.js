/*!
 * core.js
 * Objeto container de objetos singleton
 *
 * Joselmo Cardozo
 */

(function() {
    define([
        "libs/jsuri-1.1.1",
        "libs/shortcut",
        "helper/loading",
        "helper/sound"
    ], function(jsuri, shortcut, loading, sound) {

        AJS.$(".tooltip").tooltip({gravity: 's'});
        AJS.$(".tooltip-sw").tooltip({gravity: 'sw'});

        AJS.$(".toggle-wrap h2.toggle-title").click(function() {
            var $parent = AJS.$(this).parent().parent();
            if ($parent.hasClass("collapsed")) {
                $parent.removeClass("collapsed");
                $parent.find('.mod-content').show();
            }
            else {
                $parent.addClass("collapsed");
                $parent.find('.mod-content').hide();
            }
        });

        var
            phonemask = function(input) {
                var $input = AJS.$(input);
                $input.focusout(function(){
                    var phone, element;
                    element = AJS.$(this);
                    element.unmask();
                    phone = element.val().replace(/\D/g, '');
                    if(phone.length > 10) {
                        element.mask("(99) 99999-999?9");
                    } else {
                        element.mask("(99) 9999-9999?9");
                    }
                }).trigger('focusout');
            },
            singleton = function () {
                return {
                    uri: jsuri.parse(document.URL),
                    shortcut: shortcut,
                    context: _context,
                    loading: loading,
                    phonemask: phonemask,
                    sound: sound
                }
            };
        return singleton();
    });
})();