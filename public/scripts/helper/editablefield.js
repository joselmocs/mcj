/*!
 * helper.editablefield.js
 * Cria inputs editáveis
 *
 * Joselmo Cardozo
 */

define([
    'helper/messages'
], function(messages) {
    function EditableField() {

        this.element = null;
        this.default_value = null;
        this.defaults = {
            source: {},
            editor: false,
            _token: AJS.$("meta[name=csrf-token]").attr("content"),
            error: null,
            success: null,
            upload_path: '/changelog/upload'
        };

        this.init = function(el, settings) {
            this.element = AJS.$(el);
            this.defaults = AJS.$.extend({}, this.defaults, settings);

            this._el().init();
            this._el().start();
        };

        this._define_default_value = function() {
            if (this._is_select()) {
                var _op = this._field().find('option:selected');

                var icon = '';
                if (_op.attr('data-icon')) {
                    icon = "<img src='"+ _op.attr('data-icon') +"' width='16'>";
                }

                this.default_value = AJS.$("<span>"+ icon +" "+ _op.html() +"</span>");

                this.element.attr("data-pk", _op.val());
            }
            else {
                this.default_value = AJS.$.trim(this._field().val());
            }
        };

        this._field = function() {
            return AJS.$(this.element.find('[name='+ this.element.attr('id') +']'));
        };

        this._is_select = function() {
            return (this.element.attr("data-type") == "select");
        };

        this._is_textarea = function() {
            return (this.element.attr("data-type") == "textarea");
        };

        this._form = function()
        {
            var self = this;
            return {
                bind: function() {
                    self.element.find('button[type=cancel]').click(function(e) {
                        e.preventDefault();
                        self._el().start();
                    });

                    self.element.find('button[type=submit]').click(function(e) {
                        e.preventDefault();
                        self._form().save();
                    });

                    self.element.find('form').submit(function(e) {
                        e.preventDefault();
                        self.element.find('button[type=submit]').click();
                    });

                    self.element.find('.datepicker').datepicker({ dateFormat: "dd/mm/yy" });

                    if (self.defaults.editor) {
                        self.element.find('.redactor-text').redactor({
                            lang: 'pt_br',
                            fixed: true,
                            toolbarFixedBox: true,
                            airButtons: ['formatting', '|', 'bold', 'italic', 'deleted', '|',
                                'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
                                'image', 'video', 'file', 'table', 'link', '|', '|', 'alignment', '|', 'horizontalrule'],
                            plugins: ['fontcolor'],
                            imageUpload: self.defaults.upload_path,
                            clipboardUploadUrl: self.defaults.upload_path
                        });
                    }

                    AJS.$("[class=select2]").select2();

                    var masked = self.element.find("input[masked=1]");
                    if (masked.length) {
                        masked.mask(masked.attr('data-mask'));
                    }
                },

                unbind: function()
                {
                    self.element.find('button').unbind('click');
                    self.element.find('.datepicker').datepicker('destroy');
                    self.element.find('input[masked=1]').unmask();
                },

                save: function()
                {
                    self._el().saving();

                    AJS.$.ajax({
                        type: 'post',
                        url: self.element.attr('data-action'),
                        data: {
                            'field': self._field().attr('name'),
                            'value': self._field().val(),
                            '_token': self.defaults._token
                        },
                        error: function() {
                            messages.error(
                                'Desculpe, houve uma falha na comunicação com o Servidor.',
                                'Feche esta caixa de diálogo e atualize a página. Se problema persistir ' +
                                'contate um administrador'
                            );

                            if (typeof self.defaults.error === "function") {
                                self.defaults.error();
                            }
                        },
                        success: function(data, textStatus, obj) {
                            self._define_default_value();

                            if (data.success) {
                                self._el().start();
                            }
                            else {
                                self._el().edit();
                            }

                            if (typeof self.defaults.success === "function") {
                                self.defaults.success(data);
                            }
                        }
                    });
                }
            };
        };

        this._el = function()
        {
            var self = this;

            return {
                init: function() {
                    self.default_value = AJS.$.trim(self.element.html());
                },

                reset: function() {
                    self._form().unbind();
                    self.element.empty();
                    self.element.removeClass('active');
                    self.element.removeClass('saving');
                    self.element.removeClass('inactive');
                    self.element.removeClass('editable-field');
                    self.element.removeAttr('title');
                },

                start: function()
                {
                    self._el().reset();

                    self.element.addClass('inactive');
                    self.element.addClass('editable-field');
                    self.element.attr('title', 'Clique para editar');

                    self.element.append(self.default_value);
                    self.element.append(self._tpl.edit_icon());

                    self.element.find('.icon-edit-sml').bind('click', function() {
                        self._el().edit();
                    });
                },

                edit: function()
                {
                    self._el().reset();

                    self.element.addClass('active');
                    self.element.addClass('editable-field');

                    var field;
                    if (self.element.attr("data-type") == 'select') {
                        field = AJS.$('<select style="width: 100%;" class="select2"></select>');

                        AJS.$.each(self.defaults.source, function(i, option) {
                            var $option = AJS.$("<option>"+ option.name +"</option>")
                                .attr("value", option.id)
                                .attr("data-icon", option.icon);

                            if (option.id == self.element.attr('data-pk')) {
                                $option.attr("selected", "selected");
                            }

                            field.append($option);
                        });
                    }
                    else if (self.element.attr("data-type") == 'textarea') {
                        field = AJS.$('<textarea class="textarea">'+ self.default_value +'</textarea>');

                        if (self.defaults.editor) {
                            field.addClass('redactor-text');
                        }
                    }
                    else if (self.element.attr("data-type") == 'datepicker') {
                        field = AJS.$('<input type="text" value="'+ self.default_value +'" class="text datepicker">');
                    }
                    else {
                        field = AJS.$('<input type="text" value="'+ self.default_value +'" class="text">');
                        if (self.element.attr('data-mask')) {
                            field.attr("masked", '1');
                            field.attr("data-mask", self.element.attr('data-mask'));
                        }
                    }

                    field.attr('name', self.element.attr('id'));

                    var $tpl_ed = AJS.template.load("ef-form-edit").fillHtml({
                        field: field.prop('outerHTML')
                    });

                    self.element.append(AJS.$($tpl_ed.toString()));
                    self._form().bind();
                    self.element.find('input').focus();
                },
                saving: function()
                {
                    self.element.addClass('saving');
                    self.element.find('input, textarea').attr('disabled', 'disabled');
                }
            };
        };

        this._tpl = {
            edit_icon: function() {
                return AJS.$('<span class="overlay-icon icon icon-edit-sml">');
            }
        };
    }

    return {
        'new': function(el, settings) {
            var $obj = new EditableField();
            $obj.init(el, settings);
            return $obj;
        }
    }
});