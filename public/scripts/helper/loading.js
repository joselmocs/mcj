(function($) {
    define(function() {
        var
            Loading = {
                show: function () {
                    var heightOfSprite = 440,
                        currentOffsetOfSprite = 0;
                    clearInterval(this.loadingTimer);
                    _get$loadingIndicator().show();
                    _get$loadingBackground().show();
                    this.loadingTimer = window.setInterval(function () {
                        if (currentOffsetOfSprite === heightOfSprite) {
                            currentOffsetOfSprite = 0
                        }
                        currentOffsetOfSprite = currentOffsetOfSprite + 40;
                        _get$loadingIndicator().css("backgroundPosition", "0 -" + currentOffsetOfSprite + "px")
                    }, 50);
                },
                hide: function () {
                    clearInterval(this.loadingTimer);
                    _get$loadingBackground().hide();
                    _get$loadingIndicator().hide()
                }
            };

        function _get$loadingBackground() {
            if (!Loading.$loadingBackground) {
                Loading.$loadingBackground = $("<div />").addClass("aui-blanket").css("display", "none").appendTo("body");
            }
            return Loading.$loadingBackground
        }

        function _get$loadingIndicator() {
            if (!Loading.$loadingIndicator) {
                Loading.$loadingIndicator = $("<div />").addClass("page-loading-indicator").css("zIndex", 9999).appendTo("body");
            }
            return Loading.$loadingIndicator
        }

        return Loading;
    });
})(AJS.$);