/*!
 * sound.js
 * Contém funções controlam o audio de notificações e mensagens
 *
 * Joselmo Cardozo
 */

define(["helper/sound"], function() {
    var
        initialize = function() {
            AJS.$.ionSound({
                sounds: [
                    "call",
                    "beep"
                ],
                path: "/sounds/"
            });
        },
        play = function(sound) {
            AJS.$.ionSound.play(sound);
        };

    initialize();
    return {
        play: play
    }
});