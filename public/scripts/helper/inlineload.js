/*!
 * inlineload.js
 * Faz o carregamento dinamico de uma url e coloca seu conteúdo
 * em um container pre-informado.
 *
 * Joselmo Cardozo
 */

define(function() {
    var
        defaults = {
            url: '',
            method: 'get',
            data: {},
            container: null,
            loadingType: 'loading-bar',
            loadingData: {},
            loadError: true,
            filldata: {},
            success: function(data, textStatus, obj) {},
            error: function(obj, textStatus, errorThrown) {},
            done: function() {}
        },
        options = {},
        resetOptions = function() {
            options = {};
        },
        fill = function(template) {
            AJS.$(options.container).html(template.fill(options.filldata));
        },
        onLoaded = {
            error: function(obj, textStatus, errorThrown) {
                if (options.loadError) {
                    var div = AJS.$("<div></div>").css('text-align', 'center')
                        .html('<span class="aui-icon aui-icon-error">Error</span> loading error');
                    fill(AJS.template(div[0].outerHTML));
                }

                if (typeof options.error === "function") {
                    options.error(obj, textStatus, errorThrown);
                }
            },
            success: function(data, textStatus, obj) {
                fill(AJS.template(data));
                if (typeof options.success === "function") {
                    options.success(data, textStatus, obj);
                }
            },
            done: function() {
                if (typeof options.done === "function") {
                    options.done();
                }
            }
        },
        inlineLoad = function(settings) {
            resetOptions();
            options = AJS.$.extend({}, defaults, settings);

            if (options.loadingType == 'loading-bar') {
                var div = AJS.$("<div />")
                    .css('text-align', 'center')
                    .css('margin', '20px 0');
                AJS.$('<img>').attr('src', '/images/loading-bar.gif').appendTo(div);
                fill(AJS.template(div[0].outerHTML));
            }
            else if (options.loadingType == 'message') {
                var div = AJS.$("<div />").addClass('aui-message '+ options.loadingData['type']);
                AJS.$('<span />').addClass('aui-icon aui-icon-wait').appendTo(div);
                AJS.$('<p />').html(options.loadingData['message']).appendTo(div);
                fill(AJS.template(div[0].outerHTML));
            }
            AJS.$.ajax({
                type: options.method,
                url: options.url,
                data: options.data,
                error: function(obj, textStatus, errorThrown) {
                    onLoaded.error(obj, textStatus, errorThrown);
                    onLoaded.done();
                },
                success: function(data, textStatus, obj) {
                    onLoaded.success(data, textStatus, obj);
                    onLoaded.done();
                }
            });
        };

    return {
        load: inlineLoad
    }
});