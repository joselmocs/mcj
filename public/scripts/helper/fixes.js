/*!
 * fixes.js
 * Contém funções que corrigem falhas ou implementam alguma funcionalidade
 *
 * Joselmo Cardozo
 */

define(["helper/core"], function(core) {
    var
        // Suporte a queryString na paginação do Laravel
        fixLaravelPag = function() {
            var pagEl = AJS.$('div.pagination li a');
            if (pagEl.length) {
                var qsPage = 'page';
                var defaultPage = core.uri.getQueryParamValue(qsPage) || null;
                AJS.$.each(pagEl, function(idx, element) {
                    var el = AJS.$(element);
                    var nextPag = el.attr("href").split("=");
                    core.uri.replaceQueryParam(qsPage, nextPag[nextPag.length -1]);
                    el.attr('href', core.uri.toString());
                });
                if (defaultPage == null) {
                    core.uri.deleteQueryParam(qsPage);
                }
                else {
                    core.uri.replaceQueryParam(qsPage, defaultPage);
                }
            }
        };

    return {
        laravelPagination: fixLaravelPag
    }
});