/*!
 * helper.messages.js
 * Exibe mensagens de alerta na página
 *
 * Joselmo Cardozo
 */

define(function() {
    var
        element = '' +
            '<div class="global-msg" style="margin-left: -197.5px; top: 20px;">' +
            '<div class="aui-message {type} closeable"><span class="aui-icon icon-{type}"></span>' +
            '<p>{title}</p><p>{message}</p><span class="aui-icon icon-close"></span></div></div>',

        init = function(type, title, message) {
            AJS.$('body').append(AJS.template(element).fill({
                'type': type,
                'title': title,
                'message': message
            }));
            AJS.$('.global-msg .icon-close').click(function() {
                AJS.$(this).parent().parent().remove();
            });
        };

    return {
        success: function(title, message) {
            init('success', title, message);
        },
        error: function(title, message) {
            init('error', title, message);
        },
        warning: function(title, message) {
            init('warning', title, message);
        }
    };
});