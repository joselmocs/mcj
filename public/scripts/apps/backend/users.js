/*!
 * users.js
 * Funções da página de administração de usuários no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/fixes",
        "helper/formdialog"
    ], function(core, fixes, formDialog) {
        var
            getDataElement = function(el) {
                return AJS.$(el).parent().parent();
            },
            getDataValue = function (el, dataAttr) {
                return getDataElement(el).attr(dataAttr) || null;
            };

        AJS.$("#create_user").click(function() {
            formDialog.show({
                title: "Novo Usuário",
                submitText: "Criar",
                targetUrl: "/admin/user/create",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("[name=edituser_link]").click(function() {
            formDialog.show({
                title: "Editar Perfil: "+ getDataValue(this, "data-fullname"),
                submitText: "Editar",
                targetUrl: "/admin/user/edit",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        user_id: getDataValue(this, "data-id"),
                        redirect: getDataValue(this, "data-redirect"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("[name=editgroups_link]").click(function() {
            formDialog.show({
                title: getDataValue(this, "data-fullname"),
                submitText: null,
                targetUrl: "/admin/user/group",
                redirectTo: core.uri.toString(),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        username: getDataValue(this, "data-user"),
                        redirect: getDataValue(this, "data-redirect"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("[name=editpass_link]").click(function() {
            formDialog.show({
                title: "Definir Senha: "+ getDataValue(this, "data-fullname"),
                submitText: "Atualizar",
                targetUrl: "/admin/user/password",
                redirectTo: core.uri.toString(),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        username: getDataValue(this, "data-user"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$(document).ready(function() {
            fixes.laravelPagination();
        });
    });
})();