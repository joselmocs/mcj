/*!
 * mail.js
 * Funções da página de servidor de emails no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/inlineload"
    ], function(inlineLoad) {
        var
            initialize = function() {
                changeEmailModeDesc();
                checkToField();
            },
            checkToField = function() {
                if (AJS.$('#to').val() == "users") {
                    AJS.$('.group-item').hide();
                    AJS.$('.users-item').show();
                }
                else if (AJS.$('#to').val() == "clients") {
                    AJS.$('.group-item').hide();
                    AJS.$('.clients-item').show();
                }
                else {
                    AJS.$('.group-item').hide();
                    AJS.$('.manual-item').show();
                }
            },
            changeEmailModeDesc = function() {
                var $modeSelected = AJS.$('#mode :selected');
                if ($modeSelected) {
                    $modeSelected.parent().parent().find('.description').html($modeSelected.attr('data-desc'));
                }
            };

        AJS.$("button[name=repair]").click(function() {
            window.location = AJS.$(this).attr('data-location');
        });

        AJS.$("#cpass").click(function() {
            AJS.$("div[name=field-password]").toggle();
        });

        AJS.$("#test-connection").click(function() {
            var button = AJS.$(this);
            inlineLoad.load({
                url: button.attr("data-url"),
                method: "get",
                data: AJS.$("form[name=mail-form]").serialize(),
                container: "#test-container",
                loadingType: 'message',
                loadingData: {
                    'type': 'info',
                    'message': 'Testando a conexão, por favor aguarde ...'
                }
            });
        });

        AJS.$("input[name=ok-sending-mail]").click(function() {
            window.location = AJS.$(this).attr('data-location');
        });

        AJS.$("a[name=select-all]").click(function() {
            AJS.$("#groups option").attr("selected", "selected")
        });

        AJS.$("a[name=remove-all]").click(function() {
            AJS.$("#groups option").removeAttr("selected")
        });

        AJS.$("#mode").change(function() {
            changeEmailModeDesc();
        });

        AJS.$('#to').change(function() {
            checkToField();
        });

        initialize();
    });
})();