/*!
 * backend.departments.js
 * Funções da página de administração de departamentos no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog",
        "helper/dialog"
    ], function(core, formDialog, dialog) {

        AJS.$("#create_department").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Criar Novo Departamento",
                submitText: "Criar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=edit]").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Editar Departamento",
                submitText: "Editar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=remove]").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");
            var name = AJS.$(this).attr('data-name');

            dialog.confirm("Remover Departamento", "Deseja remover o departamento <b>"+ name +"</b>?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

    });
})();