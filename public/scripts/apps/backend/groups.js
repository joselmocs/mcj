/*!
 * groups.js
 * Funções da página de grupos de usuários no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/dialog"
    ], function(dialog) {
        AJS.$("[name=bt_set_permission]").click(function() {
            AJS.$("form[name=set_permission]").submit();
        });
        AJS.$("[name=unset-group-permission]").click(function() {
            var location = AJS.$(this).attr("data-redirect");

            dialog.confirm("Remover Permissão", "Deseja remover a permissão <b>"+ AJS.$(this).attr("data-permission") +"</b> " +
                    "do grupo <b>"+ AJS.$(this).attr("data-group") +"</b>", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });
    });
})();