/*!
 * backend.mailgroups.js
 * Funções da página de administração de grupos de email no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog",
        "helper/dialog"
    ], function(core, formDialog, dialog) {

        AJS.$("#create_mailgroup").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Criar Novo Grupo de Email",
                submitText: "Criar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=edit]").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Editar Grupo de Email",
                submitText: "Editar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=remove]").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");
            var name = AJS.$(this).attr('data-name');

            dialog.confirm("Remover Grupo de Email", "Deseja remover o grupo de email <b>"+ name +"</b>?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

    });
})();