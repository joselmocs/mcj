/*!
 * backend.incoming.js
 * Funções da página de administração de clientes em negociação
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog",
        "helper/dialog"
    ], function(core, formDialog, dialog) {

        AJS.$("#create_incoming_status").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Criar Novo Status de Negociação",
                submitText: "Criar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=edit]").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Editar Status de Negociação",
                submitText: "Editar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=remove]").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");
            var name = AJS.$(this).attr('data-name');

            dialog.confirm("Remover Status de Negociação", "Deseja remover o status <b>"+ name +"</b>?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

    });
})();