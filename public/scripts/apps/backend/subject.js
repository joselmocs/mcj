/*!
 * backend.subject.js
 * Funções da página de administração de assuntos de chamados no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog",
        "helper/dialog"
    ], function(core, formDialog, dialog) {

        AJS.$("#create_subject").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Criar Novo Assunto",
                submitText: "Criar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=edit]").click(function() {
            var $self = AJS.$(this);
            formDialog.show({
                title: "Editar Assunto",
                submitText: "Editar",
                targetUrl: $self.attr('data-form'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=remove]").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");
            var subject = AJS.$(this).attr('data-subject');

            dialog.confirm("Remover Assunto", "Deseja remover o assunto <b>"+ subject +"</b>?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

    });
})();