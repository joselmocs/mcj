/*!
 * changelog.edit.js
 * Funções da página de edição de Conteúdo de Versão
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        'helper/core',
        'helper/editablefield',
        'helper/dialog',
        'helper/formdialog'
    ], function(core, editableField, dialog, formDialog) {
        editableField.new('#function-val');
        editableField.new('#description-val', {
            editor: true,
            upload_path: core.context.upload_path,
            success: function(data) {
                if (data.success) {
                    core.context.completed.description = true;
                }
            }
        });
        editableField.new('#result-val', {
            editor: true,
            upload_path: core.context.upload_path
        });
        editableField.new('#action-val', {
            editor: true,
            upload_path: core.context.upload_path
        });
        editableField.new('#observation-val', {
            editor: true,
            upload_path: core.context.upload_path
        });

        editableField.new('#module-val', {
            source: AJS.$.parseJSON(core.context.modules),
            success: function(data) {
                if (data.success) {
                    AJS.$('#module-val').css('color', '');
                }
            }
        });

        editableField.new('#classification-val', {
            source: AJS.$.parseJSON(core.context.classifications),
            success: function(data) {
                if (data.success) {
                    AJS.$('#classification-val').css('color', '');
                }
            }
        });

        AJS.$('[name=client]').select2({
            placeholder: 'Incluir um cliente',
            minimumInputLength: 2,
            ajax: {
                url: core.context.clientFind,
                dataType: 'jsonp',
                quietMillis: 300,
                data: function (term, page) {
                    return {
                        q: term,
                        page_limit: 10,
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    };
                },
                results: function (data, page) {
                    return {results: data.clients};
                }
            },
            formatResult: function(client) {
                return "<b>"+ client.text + "</b><br /><i style='font-size:12px'><b>#</b>"+ client.cod +", "+ client.cnpj +", "+ client.city +"</i>";
            }
        });

        AJS.$("#empty_access").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");

            dialog.confirm("Limpar Permissões", "<b>CUIDADO</b>: Esta ação irá remover a permissão de acesso de todos os clientes listadas acima. <br /><br />Deseja continuar?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

        AJS.$("[name=remove_access]").click(function() {
            var client_name = AJS.$(this).attr('data-client');
            var location = AJS.$(this).attr('data-location');
            dialog.confirm("Remover Permissão de Acesso", "Deseja remover a permissão de acesso do cliente <b>"+ client_name +"</b>.", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

        AJS.$("[name=change_access]").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");
            window.location = location;
        });

        AJS.$("button[name=edit_access_modules]").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Gerenciar acesso por Módulo",
                submitText: "Salvar",
                targetUrl: "/glossary/module_access/edit/"+ self.attr('data-glossary'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("[name=remove-item]").click(function() {
            var location = "/glossary/remove/"+ AJS.$(this).attr('data-item') +"?_token="+ AJS.$("meta[name=csrf-token]").attr("content");
            dialog.confirm("Remover Item", "Deseja realmente remover este item do Glossário?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });
    });
})();