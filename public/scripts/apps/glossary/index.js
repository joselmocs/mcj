/*!
 * glossary.index.js
 * Funções da página da listagem de glossário
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog"
    ], function(core, formDialog) {

        AJS.$("[name=new-item]").click(function() {
            formDialog.show({
                title: "Novo Item",
                submitText: "Criar",
                targetUrl: "/glossary/create",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

    });
})();