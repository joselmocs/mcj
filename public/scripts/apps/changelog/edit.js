/*!
 * changelog.edit.js
 * Funções da página de edição de Conteúdo de Versão
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        'helper/core',
        'helper/editablefield',
        'helper/dialog',
        'helper/formdialog'
    ], function(core, editableField, dialog, formDialog) {
        editableField.new('#summary-val');
        editableField.new('#version-val');

        editableField.new('#released-val');

        editableField.new('#year-val');
        editableField.new('#month-val', {
            source: core.context.changelog.meses
        });

        editableField.new('#description-val', {
            editor: true,
            success: function(data) {
                if (data.success) {
                    core.context.completed.description = true;
                }
            }
        });
        editableField.new('#result-val', {
            editor: true,
            success: function(data) {
                if (data.success) {
                    core.context.completed.result = true;
                }
            }
        });
        editableField.new('#action-val', {
            editor: true,
            success: function(data) {
                if (data.success) {
                    core.context.completed.action = true;
                }
            }
        });
        editableField.new('#observation-val', {
            editor: true
        });

        AJS.$("a[name=create_glossary]").click(function(e) {
            AJS.$('.icon-close').click();
            if (!core.context.completed.description || !core.context.completed.result || !core.context.completed.action) {
                AJS.messages.error("#container-message", {
                    title: 'É necessário preencher a Descrição, o Resultado Esperado e a Ação antes de enviar este item para o Glossário.'
                });
            }
            else {
                window.location = AJS.$(this).attr('data-location');
            }
        });

        AJS.$('[name=client]').select2({
            placeholder: 'Incluir um cliente',
            minimumInputLength: 2,
            ajax: {
                url: core.context.clientFind,
                dataType: 'jsonp',
                quietMillis: 300,
                data: function (term, page) {
                    return {
                        q: term,
                        page_limit: 10,
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    };
                },
                results: function (data, page) {
                    return {results: data.clients};
                }
            },
            formatResult: function(client) {
                return "<b>"+ client.text + "</b><br /><i style='font-size:12px'><b>#</b>"+ client.cod +", "+ client.cnpj +", "+ client.city +"</i>";
            }
        });

        AJS.$("#empty_access").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");

            dialog.confirm("Limpar Permissões", "<b>CUIDADO</b>: Esta ação irá remover a permissão de acesso de todos os clientes listadas acima. <br /><br />Deseja continuar?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

        AJS.$("[name=remove_access]").click(function() {
            var client_name = AJS.$(this).attr('data-client');
            var location = AJS.$(this).attr('data-location');
            dialog.confirm("Remover Permissão de Acesso", "Deseja remover a permissão de acesso do cliente <b>"+ client_name +"</b>.", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

        AJS.$("[name=change_access]").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");
            window.location = location;
        });

        AJS.$("button[name=edit_access_modules]").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Gerenciar acesso por Módulo",
                submitText: "Salvar",
                targetUrl: "/changelog/module_access/edit/"+ self.attr('data-changelog'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });
    });
})();