/*!
 * changelog.index.js
 * Funções da página de change log (conteúdo de versão)
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/fixes"
    ], function(core, fixes) {
        fixes.laravelPagination();
        AJS.$(".user-content-block").html(AJS.$.trim(AJS.$(".user-content-block").html()));
    });
})();