/*!
 * sac.support.chat.js
 * Funções do centro de atendimento ao cliente
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog",
        "helper/messages"
    ], function(core, formDialog, messages) {
        var
            update = true,
            up_interval = 3000,
            $form = AJS.$("div[data-hid="+ core.context.active_cid +"]").find('form'),
            lock_form = function(el) {
                AJS.$(el).find('input, textarea').attr('disabled', 'disabled');
            },
            unlock_form = function(el) {
                AJS.$(el).find('input, textarea').removeAttr('disabled');
                AJS.$(el).find('textarea').val('');
            },
            start_chat = function()
            {
                var messages = AJS.$("div[data-hid="+ core.context.active_cid +"]").find("ul.messages");
                messages.scroll(function() {
                    messages.parent().parent()
                        .find('[name=border_end],[name=border_start]')
                        .removeClass('active')
                        .removeClass('inactive');

                    if (check_scroll_end(messages)) {
                        messages.parent().parent().find('[name=border_end]').addClass("inactive");
                    }
                    else {
                        messages.parent().parent().find('[name=border_end]').addClass("active");
                    }

                    if (check_scroll_top(messages)) {
                        messages.parent().parent().find('[name=border_start]').addClass("inactive");
                    }
                    else {
                        messages.parent().parent().find('[name=border_start]').addClass("active");
                    }
                });

                $form.submit(function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    var self = this;
                    var $message = AJS.$(this).find('textarea[name=message]').val();

                    if ($message.trim().length) {
                        lock_form(self);
                        AJS.$.ajax({
                            url: core.context.urls.answer,
                            type: 'post',
                            data: {
                                'message': $message.trim()
                            },
                            success: function(response) {
                                unlock_form(self);
                                AJS.$(self).find('textarea').focus();
                            }
                        });
                    }
                });

                update_height();
                update_message_form();
            },
            end_call = function() {
                AJS.$.ajax({
                    url: core.context.urls.endcall,
                    type: 'post',
                    data: {
                        'active_cid': core.context.active_cid
                    },
                    success: function(response) {
                        disable_call();
                    }
                });
            },
            disable_call = function() {
                lock_form($form);
                AJS.$("#end_call").remove();
                update = false;
                feedback();
            },
            update_message_form = function() {
                // Atualiza o tamanho do formulário de resposta
                var chat_width = AJS.$(".aui-page-panel-content").width();
                AJS.$('form[name=reply-support]').css('width', chat_width);
            },
            update_height = function() {
                // Atualiza o tamanho do campo de conversação para gerar rolagem
                var form_height = 152;
                var top_message = 144;
                var window_height = AJS.$(window).height();

                AJS.$('ul.messages').slimscroll({
                    destroy: true,
                    height: window_height - top_message - form_height +'px'
                });
            },
            update_talks = function()
            {
                if (!update) return;

                var talks = [];
                AJS.$('div[name=chat-container]').each(function() {
                    var el = AJS.$(this);
                    talks.push([el.attr('data-hid'), el.attr('data-last')]);
                });

                AJS.$.ajax({
                    url: core.context.urls.talks,
                    type: 'post',
                    data: {
                        'talks': JSON.stringify(talks)
                    },
                    success: function(response) {
                        parse_talks(response);
                    }
                });
            },
            check_scroll_end = function(el) {
                return el.scrollTop() + el.innerHeight() >= el[0].scrollHeight;
            },
            check_scroll_top = function(el) {
                return el.scrollTop() == 0;
            },
            parse_talks = function(response) {
                AJS.$.each(response, function(i, v) {

                    var $el = AJS.$('div[name=chat-container][data-hid='+ i +']');

                    if ($el) {
                        var ul_messages = $el.find('ul.messages');
                        var is_in_end = check_scroll_end(ul_messages);

                        AJS.$.each(v, function(idx, talk) {

                            if (typeof talk == "boolean" && talk == false) {
                                disable_call();
                                return;
                            }

                            $el.attr('data-last', talk.id);

                            var last_message =  ul_messages.find('li:last');

                            if (last_message.attr('data-author') != talk.author) {
                                var $tpl = AJS.template.load("chat-line");
                                var obj = {
                                    'name': talk.name,
                                    'author': talk.author,
                                    'time': talk.time
                                };
                                ul_messages.append($tpl.fill(obj).toString());

                                last_message =  ul_messages.find('li:last');
                            }

                            last_message.find('.chat-messages').append(AJS.$("<div></div>").html(talk.message));
                        });

                        if (is_in_end) {
                            ul_messages.animate({
                                scrollTop: ul_messages[0].scrollHeight
                            }, 600);
                        }
                    }
                });
            },
            feedback = function() {
                formDialog.show({
                    title: "Avaliar Chamado",
                    submitText: "Avaliar",
                    targetUrl: "/support/feedback",
                    submitAjaxOptions: {
                        type: 'post',
                        data: {
                            cid: core.context.active_cid,
                            _token: AJS.$("meta[name=csrf-token]").attr("content")
                        }
                    },
                    onDialogFinished: function(dialog) {
                        dialog.remove();
                        messages.success("Feedback enviado", "Muito obrigado por nos ajudar a melhorar nosso atendimento bla bla bla.");
                    }
                });
            };

        AJS.$(window).resize(function() {
            update_height();
            update_message_form();
        });

        start_chat();
        setInterval(update_talks, up_interval);

        AJS.$("#end_call").click(function() {
            end_call();
        });
    });
})();