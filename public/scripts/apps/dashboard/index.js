/*!
 * dashboard.index.js
 * Funções da página de dashboard
 *
 * Joselmo Cardozo
 */

(function() {
    require([], function() {

        AJS.$("#bt-lozenge-previous").click(function() {
            AJS.$("#bt-lozenge-previous").hide();
            AJS.$("#bt-lozenge-current").show();
            AJS.$("#dash-gerencial-current").hide();
            AJS.$("#dash-gerencial-previous").show();
            AJS.$("#text-current-past").html("ANTERIOR");
        });

        AJS.$("#bt-lozenge-current").click(function() {
            AJS.$("#bt-lozenge-current").hide();
            AJS.$("#bt-lozenge-previous").show();
            AJS.$("#dash-gerencial-current").show();
            AJS.$("#dash-gerencial-previous").hide();
            AJS.$("#text-current-past").html("ATUAL");
        });

    });
})();