/*!
 * sac.client.view.js
 * Funções da página de de clientes
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog",
        "helper/dialog",
        "helper/editablefield"
    ], function(core, formDialog, dialog, editableField) {

        editableField.new('#incoming-status', {
            source: AJS.$.parseJSON(core.context.status),
            success: function(data) {
                if (data.update) {
                    window.location = '';
                }
                else {
                    AJS.$("#incoming-status").css("color", "#"+ data.color);
                }
            }
        });

        AJS.$("[name=bt-open-contact]").click(function() {
            var self = AJS.$(this);
            dialog.remote({
                idDialog: "full-contact",
                width: 640,
                title: "contato com "+ self.attr("data-contact") + " em "+ self.attr("data-date"),
                contentUrl: "/sac/client/incoming/contact?contact="+ self.attr("data-id") +"&_token="+ AJS.$("meta[name=csrf-token]").attr("content"),
                buttons: [{"label": "Fechar", "type": "link", "callback": null}]
            });
        });

        AJS.$("[name=start_incoming]").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Iniciar Negociação",
                submitText: "Iniciar",
                targetUrl: "/sac/client/incoming/start",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        client: self.attr("data-client"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("[name=bt_mail_contact]").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Enviar cópia de contato por email",
                submitText: "Enviar",
                targetUrl: "/sac/client/incoming/contact_mail",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        contact: self.attr("data-contact"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                },
                onDialogFinished: function(dialog) {
                    dialog.remove();
                    alert("Emails enviados com sucesso!");
                }
            });
        });

        AJS.$("[name=create_contact]").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Novo Contato",
                submitText: "Criar",
                targetUrl: "/sac/client/contact/create",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        client: self.attr("data-client"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("#create_incoming").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Nova Negociação",
                submitText: "Criar",
                width: 600,
                targetUrl: "/sac/client/incoming/create",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        incoming: self.attr("data-incoming"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=edit_contact]").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Editar Contato",
                submitText: "Editar",
                targetUrl: "/sac/client/contact/edit/"+ self.attr('data-contact'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$("a[name=remove_contact]").click(function() {
            var location = AJS.$(this).attr('data-location') +'?_token='+ AJS.$("meta[name=csrf-token]").attr("content");
            var name = AJS.$(this).attr('data-name');

            dialog.confirm("Remover Contato", "Deseja remover o contato <b>"+ name +"</b>?", function(result) {
                if (result) {
                    window.location = location;
                }
            });
        });

        AJS.$("button[name=edit_modules]").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Gerenciar Módulos",
                submitText: "Salvar",
                targetUrl: "/sac/client/module/edit/"+ self.attr('data-client'),
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

    });
})();