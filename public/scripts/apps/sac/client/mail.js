/*!
 * sac.client.mail.js
 * Funções de envio de email para os contatos do cliente
 *
 * Joselmo Cardozo
 */

(function() {
    require([], function() {
        AJS.$('.tr-module td').click(function() {
            var $el = AJS.$(this).parent();
            if ($el.hasClass('active')) {
                $el.removeClass('active').find('.checkbox').removeAttr('checked');
            }
            else {
                $el.addClass('active').find('.checkbox').attr('checked', 'checked');
            }
        });

        AJS.$('a[name=check_all]').click(function() {
            AJS.$('.tr-module').addClass('active');
            AJS.$('.tr-module [type=checkbox]').attr('checked', 'checked');
        });

        AJS.$('a[name=uncheck_all]').click(function() {
            AJS.$('.tr-module').removeClass('active');
            AJS.$('.tr-module [type=checkbox]').removeAttr('checked');
        });
    });
})();