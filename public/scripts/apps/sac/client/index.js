/*!
 * sac.client.index.js
 * Funções da página de de clientes
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/fixes",
        "helper/formdialog"
    ], function(core, fixes, formDialog) {

        AJS.$("a[name=view_all]").click(function() {
            AJS.$("input[name=all]").val("1");
            AJS.$("form[name=client-search]").submit();
        });

        AJS.$("#filter-status").select2();

        AJS.$("[name=new-item]").click(function() {
            formDialog.show({
                title: "Novo Cliente",
                submitText: "Criar",
                targetUrl: "/sac/client/create",
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

        AJS.$(document).ready(function() {
            fixes.laravelPagination();
        });
    });
})();