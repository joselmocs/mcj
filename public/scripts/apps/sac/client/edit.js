/*!
 * sac.client.edit.js
 * Funções da página de edição de clientes
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core",
        "helper/formdialog",
        "helper/dialog",
        "helper/editablefield"
    ], function(core, formDialog, dialog, editableField) {

        editableField.new('#client_since-val');
        editableField.new('#last_revision-val');

        editableField.new('#cgc-val');
        editableField.new('#nome-val');

        AJS.$("#edit-address").click(function() {
            var self = AJS.$(this);
            formDialog.show({
                title: "Editar Endereço",
                submitText: "Salvar",
                targetUrl: '/sac/client/edit/address',
                submitAjaxOptions: {
                    type: 'post',
                    data: {
                        client: self.attr("data-client"),
                        _token: AJS.$("meta[name=csrf-token]").attr("content")
                    }
                }
            });
        });

    });
})();