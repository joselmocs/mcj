/*!
 * sac.support.chat.js
 * Funções do centro de atendimento ao cliente
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core"
    ], function(core) {
        var
            update = true,
            active_hid = null,
            active_hids = [],
            up_interval = 3000,
            first_update = false,
            window_active = true,
            start_chat = function(hid, client, operator) {
                var
                    lock_form = function(el) {
                        AJS.$(el).find('input, textarea').attr('disabled', 'disabled');
                    },
                    unlock_form = function(el) {
                        AJS.$(el).find('input, textarea').removeAttr('disabled');
                        AJS.$(el).find('textarea').val('');
                    };

                var $tpl = AJS.template.load("chat-container");
                var v = {
                    'hash_id': hid,
                    'client': client
                };
                AJS.$("#section-content").append($tpl.fill(v).toString());

                var messages = AJS.$("div[data-hid="+ hid +"]").find("ul.messages");
                messages.scroll(function(){

                    messages.parent().parent()
                        .find('[name=border_end],[name=border_start]')
                        .removeClass('active')
                        .removeClass('inactive');

                    if (check_scroll_end(messages)) {
                        reset_badge(hid);
                        messages.parent().parent().find('[name=border_end]').addClass("inactive");
                    }
                    else {
                        messages.parent().parent().find('[name=border_end]').addClass("active");
                    }

                    if (check_scroll_top(messages)) {
                        messages.parent().parent().find('[name=border_start]').addClass("inactive");
                    }
                    else {
                        messages.parent().parent().find('[name=border_start]').addClass("active");
                    }
                });

                var $form = AJS.$("div[data-hid="+ hid +"]").find('form');

                if (operator == core.context.user_id)
                {
                    $form.submit(function(e) {
                        e.preventDefault();
                        e.stopPropagation();

                        var self = this;
                        var $message = AJS.$(this).find('textarea[name=message]').val();

                        if ($message.trim().length)
                        {
                            lock_form(self);
                            AJS.$.ajax({
                                url: core.context.urls.reply_support,
                                type: 'post',
                                data: {
                                    'hid': hid,
                                    'message': $message.trim()
                                },
                                success: function(response) {
                                    unlock_form(self);
                                }
                            });
                        }
                    });

                    update_height();
                    update_message_form();
                }
                else {
                    $form.remove();
                }
            },
            switch_chat = function(hid, client, operator) {
                AJS.$("#section-monitor").hide();
                AJS.$("#section-content").find("div[name=chat-container]").hide();

                var $chat = AJS.$("div[data-hid="+ hid +"]");
                if (!$chat.length) {
                    start_chat(hid, client, operator);
                }
                else {
                    $chat.show();
                }

                reset_badge(hid);

                if (core.context.user_id == operator) {
                    AJS.$(".aui-page-panel-sidebar").show();
                }
                else {
                    AJS.$(".aui-page-panel-sidebar").hide();
                }

                if (!first_update) {
                    update_message_form();
                    first_update = true;
                }
            },
            update_chats = function()
            {
                if (!update) return;

                AJS.$.ajax({
                    url: core.context.urls.active_chats,
                    type: 'post',
                    data: {
                        'active': active_hids
                    },
                    success: function(response) {
                        active_hids = response.active;
                        parse_chats(response.chats);
                        parse_deactive(response.deactive);
                        binds();
                    }
                });
            },
            update_talks = function()
            {
                if (!update) return;

                var talks = [];
                AJS.$('div[name=chat-container]').each(function() {
                    var el = AJS.$(this);
                    talks.push([el.attr('data-hid'), el.attr('data-last')]);
                });

                AJS.$.ajax({
                    url: core.context.urls.talks,
                    type: 'post',
                    data: {
                        'talks': JSON.stringify(talks)
                    },
                    success: function(response) {
                        parse_talks(response);
                    }
                });
            },
            parse_talks = function(response) {
                AJS.$.each(response, function(i, v) {

                    var $el = AJS.$('div[name=chat-container][data-hid='+ i +']');
                    if ($el) {
                        var ul_messages = $el.find('ul.messages');
                        var is_in_end = check_scroll_end(ul_messages);
                        var has_messages = ul_messages.find('li').length

                        AJS.$.each(v, function(idx, talk) {

                            if (typeof talk == "boolean" && talk == false) {
                                update = false;
                                return;
                            }

                            $el.attr('data-last', talk.id);

                            var last_message =  ul_messages.find('li:last');

                            if (last_message.attr('data-author') != talk.author) {
                                var $tpl = AJS.template.load("chat-line");
                                var obj = {
                                    'name': talk.name,
                                    'author': talk.author,
                                    'time': talk.time
                                };
                                ul_messages.append($tpl.fill(obj).toString());

                                last_message =  ul_messages.find('li:last');
                            }

                            last_message.find('.chat-messages').append(AJS.$("<div></div>").html(talk.message));

                            if (i != active_hid && has_messages) {
                                increment_badge(i);
                                core.sound.play('beep');
                            }
                            else {
                                if (!is_in_end) {
                                    increment_badge(i);
                                    core.sound.play('beep');
                                }
                                else if (!window_active) {
                                    core.sound.play('beep');
                                }
                            }
                        });

                        if (i == active_hid && is_in_end) {
                            ul_messages.animate({
                                scrollTop: ul_messages[0].scrollHeight
                            }, 600);
                        }
                    }
                });
            },
            reset_badge = function(hid) {
                var $badge = AJS.$("li[data-chat-id="+ hid +"]").find('span.aui-badge');
                $badge.attr('data-count', '0').html('0');
                $badge.hide();
            },
            increment_badge = function(id) {
                var $badge = AJS.$("li[data-chat-id="+ id +"]").find('span.aui-badge');
                var qtde_msgs = (parseInt($badge.attr('data-count')) || 0) + 1;

                $badge.attr('data-count', qtde_msgs).html(qtde_msgs);
                $badge.show();
            },
            parse_deactive = function(response) {
                if (response.length) {
                    AJS.$.each(response, function(i, v) {
                        var $el = AJS.$('div[name=chat-container][data-hid='+ v +']')
                        $el.find('textarea, input').attr('disabled', 'disabled');
                    });
                }
            },
            parse_chats = function(response) {
                if (response.length) {
                    AJS.$.each(response, function(i, v) {

                        var $container = AJS.$("#ul-audit-"+ v.operator_id);
                        if ($container.length) {
                            if (!$container.is(':visible')) {
                                $container.show();
                            }
                        }

                        if (!$container.length) {
                            var $tpl = AJS.template.load("ul-audit");
                            AJS.$("#tabs-container").append($tpl.fill(v).toString());
                        }

                        var $container = AJS.$("#ul-audit-"+ v.operator_id);
                        if ($container.length) {
                            var $tpl = AJS.template.load("list-chat-item");
                            v['operator'] = v.operator_id;
                            $container.find("ul").append($tpl.fill(v).toString());
                        }

                        start_chat(v.id, v.client, v.operator_id);
                    });
                    update_height();
                }
            },
            binds = function() {
                AJS.$("[name=ul-active-chats] li").unbind('click');

                AJS.$("[name=ul-active-chats] li").click(function() {
                    AJS.$("[name=ul-active-chats] li").removeClass('aui-nav-selected');
                    AJS.$("#ul-monitor li").removeClass('aui-nav-selected');
                    AJS.$(this).addClass('aui-nav-selected');
                    active_hid = AJS.$(this).attr('data-chat-id');
                    switch_chat(active_hid, AJS.$(this).attr('data-chat-client'), AJS.$(this).attr('data-operator'));
                });
            },
            show_monitor = function() {
                AJS.$(".aui-page-panel-sidebar").hide();
                AJS.$("div[name=chat-container]").hide();
                AJS.$("#section-monitor").show();
            },
            update_height = function() {
                // Atualiza o tamanho do campo de conversação para gerar rolagem
                var form_height = 152;
                var top_message = 144;
                var window_height = AJS.$(window).height();

                AJS.$('ul.messages').slimscroll({
                    destroy: true,
                    height: window_height - top_message - form_height +'px'
                });
            },
            update_message_form = function() {
                // Atualiza o tamanho do formulário de resposta
                var chat_width = AJS.$(".aui-page-panel-content").width();
                AJS.$('form[name=reply-support]').css('width', chat_width);
            },
            check_scroll_end = function(el) {
                return el.scrollTop() + el.innerHeight() >= el[0].scrollHeight;
            },
            check_scroll_top = function(el) {
                return el.scrollTop() == 0
            },
            end_call = function() {
                AJS.$.ajax({
                    url: core.context.urls.commands,
                    type: 'post',
                    data: {
                        'active_cid': active_hid,
                        'command': 'endcall'
                    },
                    success: function(response) {

                    }
                });
            },
            alive = function() {
                AJS.$.ajax({
                    url: core.context.urls.commands,
                    type: 'post',
                    data: {
                        'active_cids': active_hids,
                        'command': 'alive'
                    },
                    success: function(response) {
                        console.log(response);
                    }
                });
            };

        setInterval(update_chats, up_interval);
        setInterval(update_talks, up_interval);
        setInterval(alive, up_interval);

        AJS.$(window).resize(function() {
            update_height();
            update_message_form();
        });

        AJS.$(document).ready(function() {
            update_height();
            update_message_form();

            AJS.$("#end_call").click(function() {
                if (active_hid != null) {
                    end_call();
                }
            });

            AJS.$(window).focus(function () {
                window_active = true;
            });

            AJS.$(window).blur(function () {
                window_active = false;
            });
        });

        AJS.$("#ul-monitor li").click(function() {
            active_hid = null;
            AJS.$("[name=ul-active-chats] li").removeClass('aui-nav-selected');
            AJS.$(this).addClass('aui-nav-selected');

            show_monitor();
        });
    });
})();