/*!
 * sac.support.monitor.js
 * Funções do monitor do centro de atendimento ao cliente
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/core"
    ], function(core) {
        var
            up_interval = 3000,
            calling_time = 0,
            loaded = false,
            update_monitor = function()
            {
                if (!loaded) {
                    core.loading.hide();
                    loaded = true;
                }

                AJS.$.ajax({
                    url: core.context.urls.monitor,
                    type: 'post',
                    data: {},
                    success: function(response) {
                        parse_monitor(response.monitor);
                        parse_calls(response.calls);
                    }
                });
            },
            parse_monitor = function(response) {
                var $container = AJS.$("#sup-monitor-trs");
                $container.empty();
                if (response.length) {
                    AJS.$.each(response, function(i, v) {
                        var $tpl = AJS.template.load("sup-monitor-wreg");
                        $container.append($tpl.fill(v).toString());
                    });
                }
                else {
                    $container.append(AJS.template.load("sup-monitor-woreg"));
                }
            },
            parse_calls = function(response)
            {
                AJS.$('button[name=answer]').unbind('click');

                var $container = AJS.$("#sup-calls-trs");
                $container.empty();

                if (response.length) {
                    if (calling_time == 0 || calling_time == 3) {
                        core.sound.play('call');
                        calling_time = 1;
                    }
                    else {
                        calling_time += 1;
                    }

                    AJS.$.each(response, function(i, v) {
                        var $tpl = AJS.template.load("sup-calls-wreg");
                        $container.append($tpl.fill(v).toString());
                    });

                    AJS.$('button[name=answer]').click(function() {
                        var id = AJS.$(this).attr('data-id');
                        var interval = null;

                        core.loading.show();

                        AJS.$.ajax({
                            url: core.context.urls.answer,
                            type: 'get',
                            data: {
                                'id': id
                            },
                            success: function(response)
                            {
                                if (response.success) {
                                    interval = setInterval(function() {
                                        var chat = AJS.$("[name=ul-active-chats] li[data-chat-id="+ response.cid +"]");
                                        if (chat.length) {
                                            chat.click();
                                            clearInterval(interval);
                                            core.loading.hide();
                                        }
                                    }, 1000);
                                }
                                else {
                                    core.loading.hide();
                                }
                            }
                        });

                        calling_time = 0;
                    });
                }
                else {
                    $container.append(AJS.template.load("sup-calls-woreg"));
                }
            };

        AJS.$(document).ready(function() {
            core.loading.show();
        });

        setInterval(update_monitor, up_interval);
    });
})();