/*!
 * user.login.js
 * Funções da página de autenticação
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        "helper/inlineload"
    ], function(inlineLoad) {
        AJS.$(document).ready(function() {
            inlineLoad.load({
                url: "/user/login",
                method: "get",
                data: {
                    inline: true
                },
                container: "#gadget-form-login .item-content"
            });
        });
    });
})();