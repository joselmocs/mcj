/*!
 * os.undocumented.js
 * Funções da página de visualização de OS sem registro de documentação
 *
 * Joselmo Cardozo
 */

(function() {
    require([], function() {
        AJS.$("input[name=de]").datepicker({ dateFormat: "dd/mm/yy" });
        AJS.$("input[name=ate]").datepicker({ dateFormat: "dd/mm/yy" });
    });
})();