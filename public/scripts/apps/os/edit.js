/*!
 * os.edit.js
 * Funções da página de visualização de OS
 *
 * Joselmo Cardozo
 */

(function() {
    require([
        'helper/core',
        'helper/editablefield'
    ], function(core, editableField) {

        editableField.new('#summary-val', {
            success: function(data) {
                if (data.success) {
                    AJS.$('#summary-val').css('color', '');
                }
            }
        });

        editableField.new('#type-val', {
            source: AJS.$.parseJSON(core.context.types),
            success: function(data) {
                if (data.success) {
                    AJS.$('#type-val').css('color', '');
                }
            }
        });

        editableField.new('#module-val', {
            source: AJS.$.parseJSON(core.context.modules),
            success: function(data) {
                if (data.success) {
                    AJS.$('#module-val').css('color', '');
                }
            }
        });

    });
})();